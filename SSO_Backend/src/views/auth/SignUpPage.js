import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import Footer from '../../components/Footer';
import { TextField, Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { alertLoading, alertChange } from '../../actions/alert.actions';
import {
  userSignup, userSigninFacebook, userSigninGoogle, userSigninLIFF
} from '../../actions/user.actions';

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import liff from '@line/liff';
import { FACEBOOK_APP_ID, GOOGLE_CLIENT_ID, LIFF_ID } from '../../actions/types';


function SignUpPage(props) {
  const [process, setProcess] = useState(false);
  const [values, setValues] = useState({
    firstname: '',
    lastname: '',
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
    ip: '',
    url: window.location.href
  });
  const onChangeInput = (key, fromInput=true) => (event) => {
    if(fromInput) setValues({ ...values, [key]: event.target.value });
    else setValues({ ...values, [key]: event });
  };
  const onSubmit = async (e) => {
    e.preventDefault();
    if(values.password !== values.confirmPassword) {
      props.alertChange('Warning', 'ยืนยันรหัสผ่านไม่ตรง');
    } else {
      let res = await props.processSignup(values);
      setProcess(res);
    }
  };


  // START: Facebook
  const [clickedFacebook, setClickedFacebook] = useState(false);
  const onClickFacebook = () => {
    props.alertLoading(true);
    setClickedFacebook(true);
  };
  const onCallbackFacebook = async (val) => {
    if(clickedFacebook){
      if(!val.accessToken || !val.userID || !val.name){
        props.alertChange('Warning', 'สมัครสมาชิกด้วย Facebook Account ไม่สำเร็จ');
        setClickedFacebook(false);
      }else{
        let name = val.name.split(' ');
        let res = await props.userSigninFacebook({
          facebookId: val.userID,
          accessToken: val.accessToken,
          firstname: name[0]? name[0]: '',
          lastname: name[1]? name[1]: '',
          email: val.email? val.email: '',
          profile: val.picture && val.picture.data? val.picture.data.url: '',
          ip: values.ip,
          url: values.url
        });
        if(res){
          setTimeout(() => {
            window.location.href = '/admin';
          }, 300);
        }
      }
    }
  };
  // END: Facebook

  // START: Google
  const onClickGoogle = () => {
    props.alertLoading(true);
  };
  const onFailureGoogle = (e) => {
    if(e && e.error && e.error !== 'idpiframe_initialization_failed'){
      props.alertChange('Warning', 'สมัครสมาชิกด้วย Google Account ไม่สำเร็จ', e);
    }
  };
  const onSuccessGoogle = async (val) => {
    let res = await props.userSigninGoogle({
      googleId: val.getId,
      idToken: val.id_token,
      firstname: val.getGivenName,
      lastname: val.getFamilyName,
      email: val.getEmail,
      profile: '',
      ip: values.ip,
      url: values.url
    });
    if(res){
      setTimeout(() => {
        window.location.href = '/admin';
      }, 300);
    }
  };
  // END: Google

  // START: LIFF
  const onClickLIFF = (e) => {
    e.preventDefault();
    props.alertLoading(true);
    liff.init({ liffId: LIFF_ID }, () => {
      liff.login();
    }, err => console.error(err));
  };
  const onSigninLIFF = (e=null) => {
    if(e) e.preventDefault();
    props.alertLoading(true);
    liff.getProfile().then(async (profile) => {
      let name = profile.displayName.split(' ');
      let res = await props.userSigninLIFF({
        liffId: profile.userId,
        firstname: name[0]? name[0]: '',
        lastname: name[1]? name[1]: '',
        profile: profile.pictureUrl? profile.pictureUrl: '',
        ip: values.ip,
        url: values.url
      });
      if(res){
        setTimeout(() => {
          window.location.href = '/admin';
        }, 300);
      }
    }).catch(err => console.error(err));
  };
  // END: LIFF


  /* eslint-disable */
  useEffect(() => onMounted(onChangeInput('ip', false), true), []);
  useEffect(() => {
    liff.init({ liffId: LIFF_ID }, () => {
      if(liff.isLoggedIn()) onSigninLIFF();
    }, err => console.error(err));
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin forBackend={false} />
      <section className="auth-01">
        <div className="wrapper">
          <div className="bg-container">
            <div className="bg-img" style={{ backgroundImage: `url('/assets/img/bg/07.jpg')` }}></div>
          </div>
          {process? (
            <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
              <div className="auth-wrapper lg">
                <h5 className="fw-600 text-center">สมัครสมาชิกเสร็จแล้ว</h5>
                <p className="fw-400 color-black text-center mt-2">
                  คุณได้ทำการสมัครสมาชิกเรียบร้อยแล้ว กรุณาตรวจสอบอีเมลของคุณเพื่อเปิดใช้งานบัญชี
                </p>
                <div className="text-center mt-5">
                  <Button 
                    component={Link} to="/auth/signin" variant="contained" 
                    color="primary" size="large"
                  >
                    เข้าสู่ระบบ
                  </Button>
                </div>
              </div>
            </div>
          ): (
            <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
              <div className="auth-wrapper lg">
                <h5 className="fw-600 text-center">สมัครสมาชิก</h5>
                <form onSubmit={onSubmit} autoComplete="off">
                  <div className="grids">
                    <div className="grid lg-50 sm-100">
                      <TextField 
                        required={true} label="ชื่อจริง" variant="outlined" fullWidth={true} 
                        value={values.firstname} onChange={onChangeInput('firstname')} 
                      />
                    </div>
                    <div className="grid lg-50 sm-100">
                      <TextField 
                        required={true} label="นามสกุล" variant="outlined" fullWidth={true} 
                        value={values.lastname} onChange={onChangeInput('lastname')} 
                      />
                    </div>
                    <div className="grid lg-50 sm-100">
                      <TextField 
                        required={true} label="ชื่อผู้ใช้" variant="outlined" fullWidth={true} 
                        value={values.username} onChange={onChangeInput('username')} 
                      />
                    </div>
                    <div className="grid lg-50 sm-100">
                      <TextField 
                        required={true} type="email" label="อีเมล" 
                        variant="outlined" fullWidth={true} 
                        value={values.email} onChange={onChangeInput('email')} 
                      />
                    </div>
                    <div className="grid lg-50 sm-100">
                      <TextField 
                        required={true} type="password" label="รหัสผ่าน" 
                        variant="outlined" fullWidth={true} 
                        value={values.password} onChange={onChangeInput('password')} 
                      />
                    </div>
                    <div className="grid lg-50 sm-100">
                      <TextField 
                        required={true} type="password" label="ยืนยันรหัสผ่าน" 
                        variant="outlined" fullWidth={true} 
                        value={values.confirmPassword} onChange={onChangeInput('confirmPassword')} 
                      />
                    </div>
                    <div className="grid sm-100">
                      <Button 
                        type="submit" variant="contained" size="large" 
                        color="primary" fullWidth={true}
                      >
                        สมัครสมาชิก
                      </Button>
                    </div>
                  </div>
                </form>
                <div className="d-flex jc-space-between fw-wrap mt-4">
                  <Link to="/auth/signin" className="p fw-500 h-color-p ws-nowrap mt-2 mr-4">
                    มีบัญชีผู้ใช้แล้ว เข้าสู่ระบบ
                  </Link>
                </div>

                <div className="ss-or mt-5 mb-3"><div>หรือ</div></div>
                <div>
                  <FacebookLogin
                    appId={FACEBOOK_APP_ID} autoLoad={false} 
                    fields="name,email,picture" 
                    onClick={onClickFacebook} 
                    callback={onCallbackFacebook} 
                    render={renderProps => (
                      <Button 
                        type="button" className="btn-social btn-fw mt-1" variant="outlined" 
                        onClick={renderProps.onClick} fullWidth={true} size="large" 
                      >
                        สมัครสมาชิกด้วย 
                        <span className="icon"><i className="fa-brands fa-facebook-square"></i></span>
                      </Button>
                    )}
                  />
                  <GoogleLogin
                    clientId={GOOGLE_CLIENT_ID} 
                    cookiePolicy={'single_host_origin'} 
                    isSignedIn={true} 
                    onRequest={onClickGoogle} 
                    onSuccess={onSuccessGoogle} 
                    onFailure={onFailureGoogle} 
                    render={renderProps => (
                      <Button 
                        type="button" className="btn-social btn-gg mt-1" variant="outlined" 
                        onClick={renderProps.onClick} fullWidth={true} size="large" 
                      >
                        สมัครสมาชิกด้วย 
                        <span className="icon"><i className="fa-brands fa-google"></i></span>
                      </Button>
                    )} 
                  />
                  <Button 
                    type="button" className="btn-social btn-ln mt-1" variant="outlined" 
                    onClick={onClickLIFF} fullWidth={true} size="large" 
                  >
                    สมัครสมาชิกด้วย
                    <span className="icon"><i className="fa-brands fa-line"></i></span>
                  </Button>
                </div>
              </div>
            </div>
          )}

        </div>
      </section>
      <Footer />
    </>
  );
}

SignUpPage.defaultProps = {
	
};
SignUpPage.propTypes = {
  alertLoading: PropTypes.func.isRequired,
  alertChange: PropTypes.func.isRequired,
	processSignup: PropTypes.func.isRequired,
  userSigninFacebook: PropTypes.func.isRequired,
  userSigninGoogle: PropTypes.func.isRequired,
  userSigninLIFF: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  alertLoading: alertLoading,
  alertChange: alertChange, 
  processSignup: userSignup,
  userSigninFacebook: userSigninFacebook,
  userSigninGoogle: userSigninGoogle,
  userSigninLIFF: userSigninLIFF
})(SignUpPage);