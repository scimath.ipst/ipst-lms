import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { TextField, Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { alertLoading, alertChange } from '../../actions/alert.actions';
import {
  userSignin, userSigninFacebook, userSigninGoogle, userSigninLIFF
} from '../../actions/user.actions';

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import liff from '@line/liff';
import {
  TOKEN_KEY, FACEBOOK_APP_ID, GOOGLE_CLIENT_ID, LIFF_ID
} from '../../actions/types';
import jwt from 'jsonwebtoken';


function SignInRedirectPage(props) {
  const authKey = props.match.params.authKey? props.match.params.authKey: null;

  const [completed, setCompleted] = useState(false);
  const [redirectUrl, setRedirectUrl] = useState('');


  const [values, setValues] = useState({
    username: '',
    password: '',
    ip: '',
    url: window.location.href
  });
  const onChangeInput = (key, fromInput=true) => (event) => {
    if(fromInput) setValues({ ...values, [key]: event.target.value });
    else setValues({ ...values, [key]: event });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    let res = await props.userSignin({ ...values, redirect: true, redirectUrl: redirectUrl });
    if(res) setCompleted(true);
    else onChangeInput('password', false)('');
  };


  // START: Facebook
  const [clickedFacebook, setClickedFacebook] = useState(false);
  const onClickFacebook = () => {
    props.alertLoading(true);
    setClickedFacebook(true);
  };
  const onCallbackFacebook = async (val) => {
    if(clickedFacebook){
      if(!val.accessToken || !val.userID || !val.name){
        props.alertChange('Warning', 'เข้าสู่ระบบด้วย Facebook Account ไม่สำเร็จ');
        setClickedFacebook(false);
      }else{
        let name = val.name.split(' ');
        let res = await props.userSigninFacebook({
          facebookId: val.userID,
          accessToken: val.accessToken,
          firstname: name[0]? name[0]: '',
          lastname: name[1]? name[1]: '',
          email: val.email? val.email: '',
          profile: val.picture && val.picture.data? val.picture.data.url: '',
          ip: values.ip,
          url: values.url,
          redirect: true,
          redirectUrl: redirectUrl
        });
        if(res) setCompleted(true);
      }
    }
  };
  // END: Facebook

  // START: Google
  const onClickGoogle = () => {
    props.alertLoading(true);
  };
  const onFailureGoogle = (e) => {
    if(e && e.error && e.error !== 'idpiframe_initialization_failed'){
      props.alertChange('Warning', 'เข้าสู่ระบบด้วย Google Account ไม่สำเร็จ', e);
    }
  };
  const onSuccessGoogle = async (val) => {
    let res = await props.userSigninGoogle({
      googleId: val.getId,
      idToken: val.id_token,
      firstname: val.getGivenName,
      lastname: val.getFamilyName,
      email: val.getEmail,
      profile: '',
      ip: values.ip,
      url: values.url,
      redirect: true,
      redirectUrl: redirectUrl
    });
    if(res) setCompleted(true);
  };
  // END: Google

  // START: LIFF
  const onClickLIFF = (e) => {
    e.preventDefault();
    props.alertLoading(true);
    liff.init({ liffId: LIFF_ID }, () => {
      liff.login();
    }, err => console.log(err));
  };
  const onSigninLIFF = (e=null) => {
    if(e) e.preventDefault();
    props.alertLoading(true);
    liff.getProfile().then(async (profile) => {
      let name = profile.displayName.split(' ');
      let res = await props.userSigninLIFF({
        liffId: profile.userId,
        firstname: name[0]? name[0]: '',
        lastname: name[1]? name[1]: '',
        profile: profile.pictureUrl? profile.pictureUrl: '',
        ip: values.ip,
        url: values.url,
        redirect: true,
        redirectUrl: redirectUrl
      });
      if(res) setCompleted(true);
    }).catch(err => console.log(err));
  };
  // END: LIFF


  /* eslint-disable */
  useEffect(() => onMounted(onChangeInput('ip', false), true), []);
  useEffect(() => {
    liff.init({ liffId: LIFF_ID }, () => {
      if(liff.isLoggedIn()) onSigninLIFF();
    }, err => console.log(err));
  }, []);
  useEffect(() => {
    if(!authKey) window.location.href = '/page404';
    else{
      jwt.verify(authKey, TOKEN_KEY, (err, decoded) => {
        if(err) window.location.href = '/page404';
        else if(!decoded.externalAppId || !decoded.redirectUrl){
          window.location.href = '/page404';
        }else{
          setRedirectUrl(decoded.redirectUrl);
        }
      })
    }
  }, [authKey]);
  /* eslint-enable */

  return (
    <>
      <section className="auth-02">
        <div className="wrapper">
          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            {!completed? (
              <div className="auth-wrapper">
                <h5 className="fw-600 text-center">เข้าสู่ระบบ</h5>
                <form onSubmit={onSubmit} autoComplete="off">
                  <div className="grids">
                    <div className="grid sm-100">
                      <TextField 
                        required={true} label="อีเมล / ชื่อผู้ใช้" 
                        variant="outlined" fullWidth={true} 
                        value={values.username} onChange={onChangeInput('username')} 
                      />
                    </div>
                    <div className="grid sm-100">
                      <TextField 
                        required={true} type="password" label="รหัสผ่าน" 
                        variant="outlined" fullWidth={true} 
                        value={values.password} onChange={onChangeInput('password')} 
                      />
                    </div>
                    <div className="grid sm-100">
                      <Button 
                        type="submit" variant="contained" color="primary" 
                        fullWidth={true} size="large" 
                      >
                        เข้าสู่ระบบ
                      </Button>
                    </div>
                  </div>
                </form>

                <div className="ss-or mt-5 mb-3"><div>หรือ</div></div>
                <div>
                  <FacebookLogin
                    appId={FACEBOOK_APP_ID} autoLoad={false} 
                    fields="name,email,picture" 
                    onClick={onClickFacebook} 
                    callback={onCallbackFacebook} 
                    render={renderProps => (
                      <Button 
                        type="button" className="btn-social btn-fw mt-1" variant="outlined" 
                        onClick={renderProps.onClick} fullWidth={true} size="large" 
                      >
                        เข้าสู่ระบบด้วย 
                        <span className="icon"><i className="fa-brands fa-facebook-square"></i></span>
                      </Button>
                    )}
                  />
                  <GoogleLogin
                    clientId={GOOGLE_CLIENT_ID} 
                    cookiePolicy={'single_host_origin'} 
                    isSignedIn={true} 
                    onRequest={onClickGoogle} 
                    onSuccess={onSuccessGoogle} 
                    onFailure={onFailureGoogle} 
                    render={renderProps => (
                      <Button 
                        type="button" className="btn-social btn-gg mt-1" variant="outlined" 
                        onClick={renderProps.onClick} fullWidth={true} size="large" 
                      >
                        เข้าสู่ระบบด้วย 
                        <span className="icon"><i className="fa-brands fa-google"></i></span>
                      </Button>
                    )} 
                  />
                  <Button 
                    type="button" className="btn-social btn-ln mt-1" variant="outlined" 
                    onClick={onClickLIFF} fullWidth={true} size="large" 
                  >
                    เข้าสู่ระบบด้วย
                    <span className="icon"><i className="fa-brands fa-line"></i></span>
                  </Button>
                </div>
              </div>
            ): (
              <div className="auth-wrapper">
                <h5 className="fw-600 text-center">เข้าสู่ระบบสำเร็จแล้ว</h5>
              </div>
            )}
          </div>
        </div>
      </section>
    </>
  );
}

SignInRedirectPage.defaultProps = {
	
};
SignInRedirectPage.propTypes = {
  alertLoading: PropTypes.func.isRequired,
  alertChange: PropTypes.func.isRequired,
	userSignin: PropTypes.func.isRequired,
  userSigninFacebook: PropTypes.func.isRequired,
  userSigninGoogle: PropTypes.func.isRequired,
  userSigninLIFF: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  alertLoading: alertLoading,
  alertChange: alertChange,
  userSignin: userSignin,
  userSigninFacebook: userSigninFacebook,
  userSigninGoogle: userSigninGoogle,
  userSigninLIFF: userSigninLIFF
})(SignInRedirectPage);