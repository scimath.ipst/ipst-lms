export {default as SignInPage} from './SignInPage';
export {default as SignUpPage} from './SignUpPage';
export {default as ForgetPasswordPage} from './ForgetPasswordPage';
export {default as ResetPasswordPage} from './ResetPasswordPage';

export {default as SignInRedirectPage} from './SignInRedirectPage';

export {default as NoPermissionPage} from './NoPermissionPage';
export {default as Page404} from './404';
