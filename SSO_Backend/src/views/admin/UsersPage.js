import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
  IconButton, Avatar, TextField, Button
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processClear, processList, processDelete } from '../../actions/admin.actions';
import { UserModel, PaginateModel } from '../../models';


function UsersPage(props) {
  const user = new UserModel(props.user);
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'รูปโปรไฟล์', numeric: false, noPadding: false },
    { label: 'ชื่อ-นามสกุล', numeric: false, noPadding: false },
    { label: 'อีเมล', numeric: false, noPadding: false },
    { label: 'ระดับ', numeric: true, noPadding: false },
    { label: 'การเชื่อมต่อ', numeric: true, noPadding: false },
    { label: 'สถานะ', numeric: true, noPadding: false },
    { label: 'การกระทำ', numeric: true, noPadding: true },
  ];

  const [keyword, setKeyword] = useState('');
  const [paginate, setPaginate] = useState(new PaginateModel({}));
  const onChangePage = (e, newPage) => {
    setPaginate(new PaginateModel({ ...paginate, page: newPage+1 }));
    onLoadData({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate(new PaginateModel({ ...paginate, page: 1, pp: e.target.value }));
    onLoadData({ ...paginate, page: 1, pp: e.target.value });
  };
  const onKeywordSubmit = (e) => {
    e.preventDefault();
    setPaginate(new PaginateModel({ ...paginate, page: 1, keyword: keyword }));
    onLoadData({ ...paginate, page: 1, keyword: keyword });
  };

  const onLoadData = (listPaginate=null) => {
    props.processClear('users');
    props.processList('users', listPaginate? listPaginate: paginate, true).then(d => {
      setPaginate(new PaginateModel(d.paginate));
    });
  };

  const [selectedData, setSelectedData] = useState(null);
  const [dialog, setDialog] = useState(false);
  const onDialogOpen = (e, data) => {
    e.preventDefault();
    setSelectedData(data);
    setDialog(true);
  };
  const onDialogClose = (e) => {
    e.preventDefault();
    setSelectedData(null);
    setDialog(false);
  };
  const onDialogDelete = async (e) => {
    e.preventDefault();
    try {
      await props.processDelete('user', { id: selectedData.id }, true);
      onLoadData();
    } catch(err) {}
    setSelectedData(null);
    setDialog(false);
  };
  
  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => { onLoadData(); }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="การจัดการบัญชีผู้ใช้" />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids table-options">
            <div className="grid lg-25 md-40 sm-40">
              <form onSubmit={onKeywordSubmit}>
                <TextField 
                  label="ค้นหา" variant="outlined" fullWidth={true} size="small" 
                  value={keyword} onChange={e => setKeyword(e.target.value)} 
                />
              </form>
            </div>
            <div className="grid lg-25 md-40 sm-50">
              <Button 
                component={Link} to="/admin/user/create" startIcon={<AddIcon />} 
                variant="contained" color="primary" size="large" 
              >
                สร้างผู้ใช้
              </Button>
            </div>
          </div>
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  {columns.map((col, i) => (
                    <TableCell
                      key={i} className="ws-nowrap"
                      align={col.numeric ? 'center' : 'left'}
                      padding={col.noPadding ? 'none' : 'normal'}
                    >
                      {col.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {!props.list.length? (
                  <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): props.list.map((d, i) => (
                  <TableRow key={i} hover>
                    <TableCell align="center">{paginate.anchorId() + i}</TableCell>
                    <TableCell align="center"><Avatar src={d.profile} className="sm" /></TableCell>
                    <TableCell align="left" className="ws-nowrap">
                      <Link to={`/admin/user/view/${d.id}`}>{d.displayName()}</Link>
                    </TableCell>
                    <TableCell align="left" className="ws-nowrap">{d.email}</TableCell>
                    <TableCell align="center" className="ws-nowrap">{d.displayRole()}</TableCell>
                    <TableCell align="center" className="ws-nowrap">
                      <span className={`table-social fw ${d.facebook_id? 'active': ''}`}>
                        <em className="fa-brands fa-facebook-f"></em>
                      </span>
                      <span className={`table-social gg ${d.google_id? 'active': ''}`}>
                        <em className="fa-brands fa-google"></em>
                      </span>
                      <span className={`table-social ln ${d.liff_id? 'active': ''}`}>
                        <em className="fa-brands fa-line"></em>
                      </span>
                    </TableCell>
                    <TableCell align="center" className="ws-nowrap">{d.displayStatus()}</TableCell>
                    <TableCell align="center" className="ws-nowrap" padding="none">
                      <IconButton size="small" component={Link} to={`/admin/user/view/${d.id}`}>
                        <VisibilityIcon style={{ fontSize: '21px' }} />
                      </IconButton>
                      {(user.isSuperAdmin() && !d.isSuperAdmin()) || (user.isAdmin() && !d.isAdmin())? (
                        <>
                          <IconButton size="small" component={Link} to={`/admin/user/update/${d.id}`}>
                            <EditIcon style={{ fontSize: '21px' }} />
                          </IconButton>
                          <IconButton size="small" onClick={e => onDialogOpen(e, d)}>
                            <DeleteIcon style={{ fontSize: '21px' }} />
                          </IconButton>
                        </>
                      ): (<></>)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            labelRowsPerPage="แสดง" 
            rowsPerPageOptions={[10, 25, 50, 100]} 
            component="div" 
            count={paginate.total} 
            rowsPerPage={paginate.pp} 
            page={paginate.page - 1} 
            onPageChange={onChangePage} 
            onRowsPerPageChange={onChangePerPage} 
          />
        </div>

        <FooterAdmin />
      </div>

      <Dialog open={dialog} onClose={onDialogClose}>
        <DialogTitle>ยืนยันการลบบัญชีผู้ใช้</DialogTitle>
        <DialogContent>
          <DialogContentText>
            ยืนยันการลบข้อมูล โดยข้อมูลที่ถูกลบจะไม่สามารถกู้คืนได้
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDialogClose} color="primary">
            ยกเลิก
          </Button>
          <Button onClick={onDialogDelete} color="primary" autoFocus>
            ยืนยันการลบ
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

UsersPage.defaultProps = {
	activeIndex: 2
};
UsersPage.propTypes = {
	activeIndex: PropTypes.number,
  processClear: PropTypes.func.isRequired,
  processList: PropTypes.func.isRequired,
  processDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  list: state.admin.users
});

export default connect(mapStateToProps, {
  processClear: processClear,
  processList: processList, 
  processDelete: processDelete
})(UsersPage);