import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import {
  TextField, FormControl, InputLabel, Select, MenuItem, Button, IconButton,
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell,
  Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import {
  processRead, processCreate, processUpdate, processDelete
} from '../../actions/admin.actions';
import { UserModel, UserTypeModel, UserSubtypeModel } from '../../models';


function UserTypeViewPage(props) {
  const user = new UserModel(props.user);
  const history = useHistory();
  const process = props.match.params.process? props.match.params.process: 'view';
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new UserTypeModel({}));
  const onChangeInput = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setValues({ ...values, [key]: val });
  };


  // START: User Subtype
  const [selectedData, setSelectedData] = useState(new UserSubtypeModel({ status: 1 }));
  const onChangeSelectedData = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setSelectedData(new UserSubtypeModel({ ...selectedData, [key]: val }));
  };

  const [selectedProcess, setSelectedProcess] = useState('');
  const onProcess = (e=null, process='', data=null) => {
    if(e) e.preventDefault();
    if(process){
      setSelectedProcess(process);
      if(data) setSelectedData(data);
      else setSelectedData(new UserSubtypeModel({ status: 1 }));
    }else{
      setSelectedProcess('');
      setSelectedData(new UserSubtypeModel({ status: 1 }));
    }
  };

  const onSubmitProcess = async (e) => {
    e.preventDefault();
    if(user.isSuperAdmin()){
      if(selectedProcess === 'create'){
        let res = await props.processCreate('user-type', {
          parentId: dataId,
          name: selectedData.name,
          status: selectedData.status
        }, true);
        if(res){
          await props.processRead('user-type', { id: dataId }, true).then(d => {
            setValues(d);
          }).catch(() => history.push('/admin/user-types'));
          onProcess();
        }
      }else if(selectedProcess === 'update'){
        let res = await props.processUpdate('user-type', {
          id: selectedData.id,
          name: selectedData.name,
          status: selectedData.status
        }, true);
        if(res){
          await props.processRead('user-type', { id: dataId }, true).then(d => {
            setValues(d);
          }).catch(() => history.push('/admin/user-types'));
          onProcess();
        }
      }else{
        onProcess();
      }
    }else{
      onProcess();
    }
  };
  const onSubmitDelete = async (e) => {
    e.preventDefault();
    if(user.isSuperAdmin()){
      try {
        await props.processDelete('user-type', { id: selectedData.id }, true);
        await props.processRead('user-type', { id: dataId }, true).then(d => {
          setValues(d);
        }).catch(() => history.push('/admin/user-types'));
      } catch(err) {}
    }
    onProcess();
  };
  // END: User Subtype


  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(async () => {
    await props.processRead('user-type', { id: dataId }, true).then(d => {
      setValues(d);
    }).catch(() => history.push('/admin/user-types'));
  }, []);
  /* eslint-enable */
  
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'create'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}ประเภทผู้ใช้`} 
          back="/admin/user-types" 
        />
        
        <div className="app-card pt-0" data-aos="fade-up" data-aos-delay="150">
          <div className="grids ai-center">
            <div className="grid xl-1-3 lg-45 md-40 sm-50">
              <TextField 
                required={true} label="ชื่อประเภทผู้ใช้" variant="outlined" fullWidth={true} 
                value={values.name? values.name: ''} 
                onChange={e => onChangeInput('name', e.target.value)} 
                disabled={process === 'view'} 
              />
            </div>
            <div className="grid xl-1-3 lg-45 md-40 sm-50">
              <FormControl 
                variant="outlined" fullWidth={true} 
                disabled={process === 'view'} 
              >
                <InputLabel id="status">สถานะ *</InputLabel>
                <Select
                  labelId="status" label="สถานะ" required={true} 
                  value={values.status} 
                  onChange={e => onChangeInput('status', e.target.value, true)} 
                  disabled={process === 'view'} 
                >
                  <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                  <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="sep"></div>
            <div className="grid sm-100">
              <div className="mt-2">
                {user.isSuperAdmin()? (
                  <Button 
                    component={Link} to={`/admin/user-type/update/${dataId}`} variant="outlined" 
                    size="large" color="primary" startIcon={<EditIcon />} className={`mr-2`} 
                  >
                    แก้ไข
                  </Button>
                ): (<></>)}
                <Button 
                  component={Link} to="/admin/user-types" 
                  variant="outlined" color="primary" size="large"
                >
                  ย้อนกลับ
                </Button>
              </div>
            </div>
          </div>
        </div>
        
        <div className="app-card mt-5 pt-5" data-aos="fade-up" data-aos-delay="300">
          {user.isSuperAdmin()? (
            <div className="grids table-options jc-end">
              <div className="grid lg-25 md-40 sm-50">
                <Button 
                  onClick={e => onProcess(e, 'create')} startIcon={<AddIcon />} 
                  variant="contained" color="primary" size="large" 
                >
                  สร้างประเทภผู้ใช้ย่อย
                </Button>
              </div>
            </div>
          ): (<></>)}
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  <TableCell className="ws-nowrap" align="center" padding="none">ID</TableCell>
                  <TableCell className="ws-nowrap" align="left" padding="normal">ชื่อประเภทย่อย</TableCell>
                  <TableCell className="ws-nowrap" align="center" padding="normal">สถานะ</TableCell>
                  <TableCell className="ws-nowrap" align="center" padding="none">สถานะ</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {!values.subtypes.length? (
                  <TableRow>
                    <TableCell colSpan={4} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): values.subtypes.map((d, i) => {
                  let index = i + 1;
                  return (
                    <TableRow key={index} hover>
                      <TableCell align="center" style={{ minWidth: '3rem' }}>{index}</TableCell>
                      <TableCell align="left">
                        <div onClick={e => onProcess(e, 'view', d)} className="c-pointer">
                          {d.name}
                        </div>
                      </TableCell>
                      <TableCell align="center">{d.displayStatus()}</TableCell>
                      <TableCell align="center" padding="none" className="ws-nowrap">
                        <IconButton size="small" onClick={e => onProcess(e, 'view', d)}>
                          <VisibilityIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        {user.isSuperAdmin()? (
                          <>
                            <IconButton size="small" onClick={e => onProcess(e, 'update', d)}>
                              <EditIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                            <IconButton size="small" onClick={e => onProcess(e, 'delete', d)}>
                              <DeleteIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                          </>
                        ): (<></>)}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>

        <FooterAdmin />
      </div>
      
      {user.isSuperAdmin()? (
        <>
          <Dialog open={['create', 'view', 'update'].indexOf(selectedProcess) > -1}>
            <DialogTitle>
              {selectedProcess === 'create'? 'สร้าง': selectedProcess === 'view'? 'ดู': 'แก้ไข'}ประเภทผู้ใช้ย่อย
            </DialogTitle>
            <form onSubmit={onSubmitProcess}>
              <DialogContent className="pt-0">
                <div className="grids ai-center">
                  <div className="grid sm-100 mt-0">
                    <TextField 
                      required={true} label="ชื่อประเภทผู้ใช้ย่อย" variant="outlined" fullWidth={true} 
                      value={selectedData.name? selectedData.name: ''} 
                      onChange={e => onChangeSelectedData('name', e.target.value)} 
                      disabled={selectedProcess === 'view'} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <FormControl 
                      variant="outlined" fullWidth={true} 
                      disabled={selectedProcess === 'view'} 
                    >
                      <InputLabel id="status">สถานะ *</InputLabel>
                      <Select
                        labelId="status" label="สถานะ" required={true} 
                        value={selectedData.status} 
                        onChange={e => onChangeSelectedData('status', e.target.value, true)} 
                        disabled={selectedProcess === 'view'} 
                      >
                        <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                        <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                </div>
              </DialogContent>
              <DialogActions>
                <Button onClick={onProcess} color="primary">
                  ยกเลิก
                </Button>
                {['create', 'update'].indexOf(selectedProcess) > -1? (
                  <Button type="sumit" color="primary" autoFocus>
                    ยืนยันการ{selectedProcess === 'create'? 'สร้าง': selectedProcess === 'view'? 'ดู': 'แก้ไข'}
                  </Button>
                ): (<></>)}
              </DialogActions>
            </form>
          </Dialog>
          <Dialog open={selectedProcess === 'delete' && selectedData.isValid()}>
            <DialogTitle>ยืนยันการลบข้อมูล</DialogTitle>
            <DialogContent>
              <DialogContentText>
                ยืนยันการลบข้อมูล โดยข้อมูลที่ถูกลบจะไม่สามารถกู้คืนได้
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={onProcess} color="primary">
                ยกเลิก
              </Button>
              <Button onClick={onSubmitDelete} color="primary" autoFocus>
                ยืนยันการลบ
              </Button>
            </DialogActions>
          </Dialog>
        </>
      ): (<></>)}
    </>
  );
}

UserTypeViewPage.defaultProps = {
	activeIndex: 8
};
UserTypeViewPage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired,
  processCreate: PropTypes.func.isRequired,
  processUpdate: PropTypes.func.isRequired,
  processDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {
  processRead: processRead,
  processCreate: processCreate,
  processUpdate: processUpdate,
  processDelete: processDelete
})(UserTypeViewPage);