export {default as AdminDashboardPage} from './DashboardPage';
export {default as AdminProfilePage} from './ProfilePage';
export {default as AdminProfileUpdatePage} from './ProfileUpdatePage';

export {default as AdminRolesPage} from './RolesPage';
export {default as AdminRoleViewPage} from './RoleViewPage';
export {default as AdminRolePage} from './RolePage';

export {default as AdminUsersPage} from './UsersPage';
export {default as AdminUserViewPage} from './UserViewPage';
export {default as AdminUserPage} from './UserPage';

export {default as AdminExternalAppsPage} from './ExternalAppsPage';
export {default as AdminExternalAppViewPage} from './ExternalAppViewPage';
export {default as AdminExternalAppPage} from './ExternalAppPage';

export {default as AdminModulesPage} from './ModulesPage';
export {default as AdminModulePage} from './ModulePage';
export {default as AdminModuleViewPage} from './ModuleViewPage';

export {default as AdminReportTrafficsPage} from './ReportTrafficsPage';
export {default as AdminReportActionsPage} from './ReportActionsPage';
export {default as AdminReportRegistrationsPage} from './ReportRegistrationsPage';

export {default as AdminUserTypesPage} from './UserTypesPage';
export {default as AdminUserTypeViewPage} from './UserTypeViewPage';
export {default as AdminUserTypePage} from './UserTypePage';

export {default as AdminCustomColumnsPage} from './CustomColumnsPage';
export {default as AdminCustomColumnViewPage} from './CustomColumnViewPage';
export {default as AdminCustomColumnPage} from './CustomColumnPage';

