import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  IconButton, Button
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processClear, processList } from '../../actions/admin.actions';
import { UserModel } from '../../models';


function CustomColumnsPage(props) {
  const user = new UserModel(props.user);
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'ชื่อฟิลด์ข้อมูล', numeric: false, noPadding: false },
    { label: 'สถานะ', numeric: true, noPadding: false },
    { label: 'การกระทำ', numeric: true, noPadding: true },
  ];
  
  const [paginate, setPaginate] = useState({ page: 1, pp: 10 });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    props.processClear('custom-columns');
    props.processList('custom-columns', {}, true);
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="การจัดการฟิลด์ข้อมูล" />
        
        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          {user.isSuperAdmin()? (
            <div className="grids table-options jc-end">
              <div className="grid lg-25 md-40 sm-50">
                <Button 
                  component={Link} to="/admin/custom-column/create" startIcon={<AddIcon />} 
                  variant="contained" color="primary" size="large" 
                >
                  สร้างฟิลด์ข้อมูล
                </Button>
              </div>
            </div>
          ): (<></>)}
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  {columns.map((col, i) => (
                    <TableCell
                      key={i} className="ws-nowrap"
                      align={col.numeric ? 'center' : 'left'}
                      padding={col.noPadding ? 'none' : 'normal'}
                    >
                      {col.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {!props.list.length? (
                  <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): props.list
                .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                .map((d, i) => {
                  let index = (paginate.page-1)*paginate.pp + i + 1;
                  return (
                    <TableRow key={index} hover>
                      <TableCell align="center" style={{ minWidth: '3rem' }}>{index}</TableCell>
                      <TableCell align="left">
                        <Link to={`/admin/custom-column/view/${d.id}`}>{d.name}</Link>
                      </TableCell>
                      <TableCell align="center">{d.displayStatus()}</TableCell>
                      <TableCell align="center" padding="none" className="ws-nowrap">
                        <IconButton size="small" component={Link} to={`/admin/custom-column/view/${d.id}`}>
                          <VisibilityIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        {user.isSuperAdmin()? (
                          <IconButton size="small" component={Link} to={`/admin/custom-column/update/${d.id}`}>
                            <EditIcon style={{ fontSize: '21px' }} />
                          </IconButton>
                        ): (<></>)}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            labelRowsPerPage="แสดง" 
            rowsPerPageOptions={[10, 25, 50, 100]} 
            component="div" 
            count={props.list.length} 
            rowsPerPage={paginate.pp} 
            page={paginate.page - 1} 
            onPageChange={onChangePage} 
            onRowsPerPageChange={onChangePerPage} 
          />
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

CustomColumnsPage.defaultProps = {
	activeIndex: 9
};
CustomColumnsPage.propTypes = {
	activeIndex: PropTypes.number,
  processClear: PropTypes.func.isRequired,
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  list: state.admin.customColumns
});

export default connect(mapStateToProps, {
  processClear: processClear,
  processList: processList
})(CustomColumnsPage);