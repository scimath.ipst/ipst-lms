import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { TextField, FormControl, InputLabel, Select, MenuItem, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processRead, processCreate, processUpdate } from '../../actions/admin.actions';
import { UserModel, UserTypeModel } from '../../models';


function UserTypePage(props) {
  const user = new UserModel(props.user);
  const history = useHistory();
  const process = props.match.params.process? props.match.params.process: 'view';
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new UserTypeModel({ status: 1 }));
  const onChangeInput = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setValues({ ...values, [key]: val });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(process === 'create'){
      let res = await props.processCreate('user-type', values, true);
      if(res) history.push('/admin/user-types');
    }else if(process === 'update'){
      await props.processUpdate('user-type', values, true);
    }
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(async () => {
    if(!user.isSuperAdmin() || ['create', 'update'].indexOf(process) < 0){
      history.push('/admin/user-types');
    }else{
      if(process === 'update'){
        await props.processRead('user-type', { id: dataId }, true).then(d => {
          setValues(d);
        }).catch(() => history.push('/admin/user-types'));
      }
    }
  }, []);
  /* eslint-enable */
  
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'create'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}ประเภทผู้ใช้`} 
          back="/admin/user-types" 
        />

        <div className="app-card pt-0" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>
            <div className="grids ai-center">
              <div className="grid xl-1-3 lg-45 md-40 sm-50">
                <TextField 
                  required={true} label="ชื่อประเภทผู้ใช้" variant="outlined" fullWidth={true} 
                  value={values.name? values.name: ''} 
                  onChange={e => onChangeInput('name', e.target.value)} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40 sm-50">
                <FormControl 
                  variant="outlined" fullWidth={true} 
                  disabled={process === 'view'} 
                >
                  <InputLabel id="status">สถานะ *</InputLabel>
                  <Select
                    labelId="status" label="สถานะ" required={true} 
                    value={values.status} 
                    onChange={e => onChangeInput('status', e.target.value, true)} 
                    disabled={process === 'view'} 
                  >
                    <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                    <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="sep"></div>
              <div className="grid sm-100">
                <div className="mt-2">
                  {process === 'create'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<AddIcon />} className={`mr-2`} 
                    >
                      สร้าง
                    </Button>
                  ): process === 'update'? (
                    <>
                      <Button 
                        type="submit" variant="contained" color="primary" 
                        size="large" startIcon={<EditIcon />} className={`mr-2`} 
                      >
                        บันทึก
                      </Button>
                      <Button 
                        component={Link} to={`/admin/user-type/view/${dataId}`} variant="outlined" 
                        size="large" color="primary" className={`mr-2`} 
                      >
                        ดูข้อมูล
                      </Button>
                    </>
                  ): ('')}
                  <Button 
                    component={Link} to="/admin/user-types" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

UserTypePage.defaultProps = {
	activeIndex: 8
};
UserTypePage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired,
  processCreate: PropTypes.func.isRequired,
  processUpdate: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {
  processRead: processRead,
  processCreate: processCreate,
  processUpdate: processUpdate
})(UserTypePage);