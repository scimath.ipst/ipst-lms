import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { onMounted, formatNumber } from '../../helpers/frontend';
import ReactECharts from 'echarts-for-react';

import { connect } from 'react-redux';
import { processList } from '../../actions/admin.actions';


function DashboardPage(props) {
  
  const [totalReport1, setTotalReport1] = useState(0);
  const [totalReport2, setTotalReport2] = useState(0);
  
  const thisYear = new Date().getFullYear();
  const startDate =`${thisYear}-01-01`;
  const endDate = `${thisYear}-12-31`;
  const [graphReport, setGraphReport] = useState([]);
  
  /* eslint-disable */
  useEffect(() => {
    onMounted();
    props.processList('report-actions', { type: 'Total Report' }).then(d => {
      setTotalReport1(Number(d.result.total));
    });
    props.processList('report-registrations', { type: 'Total Report' }).then(d => {
      setTotalReport2(Number(d.result.total));
    });
    props.processList('report-registrations', {
      type: 'Daily Report',
      startDate: startDate,
      endDate: endDate
    }).then(d => {
      setGraphReport(d.result);
    });
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="หน้าแรก" />

        <div className="grids">
          <div className="grid md-50 sm-100 mt-0">
            <div className="app-card" data-aos="fade-up" data-aos-delay="150">
              <div className="text-center pt-2">
                <h6 className="fw-600">จำนวนการกระทำทั้งสิ้น</h6>
                <h2 className="fw-600 color-p">
                  {formatNumber(totalReport1, 0)}<span className="h5 fw-600 color-dark"> ครั้ง</span>
                </h2>
              </div>
            </div>
          </div>
          <div className="grid md-50 sm-100 xl-mt-0 lg-mt-0 md-mt-0">
            <div className="app-card" data-aos="fade-up" data-aos-delay="150">
              <div className="text-center pt-2">
                <h6 className="fw-600">มีผู้ลงทะเบียนทั้งสิ้น</h6>
                <h2 className="fw-600 color-p">
                  {formatNumber(totalReport2, 0)}<span className="h5 fw-600 color-dark"> คน</span>
                </h2>
              </div>
            </div>
          </div>
          <div className="grid sm-100">
            <div className="app-card" data-aos="fade-up" data-aos-delay="150">
              <h6 className="fw-600">จำนวนผู้ลงทะเบียนรายวันปี {thisYear}</h6>
              <div className="mt-4">
                <ReactECharts 
                  option={{
                    grid: { top: 20, right: 20, bottom: 30, left: 50 },
                    xAxis: {
                      type: 'time',
                      axisLine: {
                        lineStyle: { color: 'rgb(227, 226, 236, 0.8)' }
                      },
                      axisLabel: {
                        fontSize: 13, color: '#777777',
                        formatter: '{d} {MMM} {yyyy}'
                      }
                    },
                    yAxis: {
                      splitLine: {
                        lineStyle: { color: 'rgb(227, 226, 236, 0.8)' }
                      },
                      axisLine: {
                        lineStyle: { color: 'rgb(227, 226, 236, 0.8)' }
                      },
                      axisLabel: { fontSize: 13, color: '#777777' }
                    },
                    tooltip: {
                      show: true,
                      showContent: true,
                      alwaysShowContent: false,
                      triggerOn: 'mousemove',
                      trigger: 'axis',
                      axisPointer: {
                        label: { show: false }
                      }
                    },
                    series: [{
                      name: 'จำนวน',
                      type: 'bar', stack: 'Stack',
                      data: graphReport.map(d => ([ d.date, Number(d.total) ]))
                    }],
                    color: ['#5A8DEE']
                  }}
                />
              </div>
            </div>
          </div>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

DashboardPage.defaultProps = {
	activeIndex: 0
};
DashboardPage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  processList: processList
})(DashboardPage);