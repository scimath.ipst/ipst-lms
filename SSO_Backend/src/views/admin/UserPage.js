import PropTypes from 'prop-types';
import { useState, useEffect, Fragment } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import {
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Tabs, Tab
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processList, processRead, processCreate, processUpdate } from '../../actions/admin.actions';
import { UserModel, UserDetailModel } from '../../models';


function UserPage(props) {
  const user = new UserModel(props.user);
  const history = useHistory();
  const process = props.match.params.process? props.match.params.process: 'view';
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [tabValue, setTabValue] = useState(0);
  
  const [values, setValues] = useState(new UserModel({ status: 1 }));
  const onChangeInput = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setValues({ ...values, [key]: val });
  };
  
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  
  const [roleId, setRoleId] = useState('');
  const roleChoices = () => {
    return props.roles.filter(d => {
      if((user.isSuperAdmin() && !d.isSuperAdmin()) || (user.isAdmin() && !d.isAdmin())) return true;
      else return false;
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(process === 'create'){
      let res = await props.processCreate('user', {
        ...values, roleId: roleId,
        password: password, confirmPassword: confirmPassword
      }, true);
      if(res) history.push('/admin/users');
    }else if(process === 'update'){
      let updateInput = { ...values, roleId: roleId };
      if(password){
        updateInput['password'] = password;
        updateInput['confirmPassword'] = confirmPassword;
      }
      await props.processUpdate('user', updateInput, true).then(() => {
        setPassword('');
        setConfirmPassword('');
      });
    }
  };

  
  const [custom, setCustom] = useState({});
  const onChangeCustom = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setCustom({ ...custom, [key]: val });
  };

  const [detail, setDetail] = useState(new UserDetailModel({}));
  const onChangeDetail = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    if(key === 'user_type_id'){
      setDetail({ ...detail, [key]: val, user_subtype_id: null });
    }else{
      setDetail({ ...detail, [key]: val });
    }
  };
  const filteredUserSubtypes = (userTypeId) => {
    let temp = props.userTypes.filter(d => d.id === userTypeId);
    if(temp.length) return temp[0].subtypes;
    else return [];
  };
  const onSubmitDetail = async (e) => {
    e.preventDefault();
    if(process === 'update'){
      await props.processUpdate('user-detail', {
        ...detail, ...custom, user_id: dataId
      }, true);
    }
  };


  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(['create', 'update'].indexOf(process) < 0){
      history.push('/admin/users');
    }else{
      props.processList('roles');
      props.processList('user-types');
      props.processList('custom-columns');
      if(process === 'update'){
        props.processRead('user', { id: dataId }, true).then(d => {
          if(d.isAdmin() && !user.isSuperAdmin()){
            history.push('/admin/users');
          }else{
            setValues(d);
            setRoleId(d.role.id);
            setDetail(d.detail);
            setCustom(d.custom_data);
          }
        }).catch(() => history.push('/admin/users'));
      }
    }
  }, []);
  /* eslint-enable */
  
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'create'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}บัญชีผู้ใช้`} 
          back="/admin/users" 
        />

        {process === 'update'? (
          <div className="app-card app-card-header" data-aos="fade-up" data-aos-delay="150">
            <Tabs
              value={tabValue} onChange={(e, val) => setTabValue(val)} 
              indicatorColor="primary" textColor="primary" variant="scrollable" 
              scrollButtons="auto" aria-label="scrollable auto tabs example" 
            >
              <Tab label="บัญชีผู้ใช้" id="tab-0" aria-controls="tabpanel-0" />
              <Tab label="ข้อมูลทั่วไป" id="tab-1" aria-controls="tabpanel-1" />
            </Tabs>
          </div>
        ): (<></>)}

        {tabValue === 0? (
          <div className={`app-card ${process === 'update'? 'app-card-body': 'pt-0'}`} data-aos="fade-up" data-aos-delay="150">
            <form onSubmit={onSubmit}>
              <div className="grids ai-center">
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    required={true} label="ชื่อจริง" variant="outlined" fullWidth={true} 
                    value={values.firstname? values.firstname: ''} 
                    onChange={e => onChangeInput('firstname', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    required={true} label="นามสกุล" variant="outlined" fullWidth={true} 
                    value={values.lastname? values.lastname: ''} 
                    onChange={e => onChangeInput('lastname', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    required={true} label="อีเมล" variant="outlined" fullWidth={true} 
                    value={values.email? values.email: ''} type="email" 
                    onChange={e => onChangeInput('email', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    required={true} label="ชื่อผู้ใช้" variant="outlined" fullWidth={true} 
                    value={values.username? values.username: ''} 
                    onChange={e => onChangeInput('username', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="role">ระดับผู้ใช้ *</InputLabel>
                    <Select
                      labelId="role" label="ระดับผู้ใช้" required={true} 
                      value={roleId? roleId: ''} 
                      onChange={e => setRoleId(e.target.value)} 
                      disabled={process === 'view'} 
                    >
                      {roleChoices().map((d, i) => (
                        <MenuItem key={`role_${i}`} value={d.id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="status">สถานะ *</InputLabel>
                    <Select
                      labelId="status" label="สถานะ" required={true} 
                      value={values.status} 
                      onChange={e => onChangeInput('status', e.target.value, true)} 
                      disabled={process === 'view'} 
                    >
                      <MenuItem value={-1}>ขอลบบัญชี</MenuItem>
                      <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                      <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div className="sep"></div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    required={process === 'create'} variant="outlined" fullWidth={true} 
                    label={`รหัสผ่าน${process === 'update'? 'ใหม่': ''}`} 
                    value={password? password: ''} type="password" 
                    onChange={e => setPassword(e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    required={process === 'create'} variant="outlined" fullWidth={true} 
                    label={`ยืนยันรหัสผ่าน${process === 'update'? 'ใหม่': ''}`} 
                    value={confirmPassword? confirmPassword: ''} type="password" 
                    onChange={e => setConfirmPassword(e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid sm-100">
                  <div className="mt-2">
                    {process === 'create'? (
                      <Button 
                        type="submit" variant="contained" color="primary" 
                        size="large" startIcon={<AddIcon />} className={`mr-2`} 
                      >
                        สร้าง
                      </Button>
                    ): process === 'update'? (
                      <>
                        <Button 
                          type="submit" variant="contained" color="primary" 
                          size="large" startIcon={<EditIcon />} className={`mr-2`} 
                        >
                          บันทึก
                        </Button>
                        <Button 
                          component={Link} to={`/admin/user/view/${dataId}`} variant="outlined" 
                          size="large" color="primary" className={`mr-2`} 
                        >
                          ดูข้อมูล
                        </Button>
                      </>
                    ): ('')}
                    <Button 
                      component={Link} to="/admin/users" 
                      variant="outlined" color="primary" size="large"
                    >
                      ย้อนกลับ
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        ): (<></>)}
        
        {tabValue === 1? (
          <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
            <form onSubmit={onSubmitDetail}>
              <div className="grids ai-center">
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="user_type">ประเภทผู้ใช้</InputLabel>
                    <Select
                      labelId="user_type" label="ประเภทผู้ใช้" 
                      value={detail.user_type_id? detail.user_type_id: ''} 
                      onChange={e => onChangeDetail('user_type_id', e.target.value)} 
                      disabled={process === 'view'} 
                    >
                      {props.userTypes.map((d, i) => (
                        <MenuItem key={`ut_${i}`} value={d.id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                {detail.user_type_id && filteredUserSubtypes(detail.user_type_id).length? (
                  <div className="grid xl-1-3 lg-45 md-40 sm-50">
                    <FormControl 
                      variant="outlined" fullWidth={true} 
                      disabled={process === 'view'} 
                    >
                      <InputLabel id="user_subtype_id">ประเภทผู้ใช้ย่อย</InputLabel>
                      <Select
                        labelId="user_subtype_id" label="ประเภทผู้ใช้ย่อย" 
                        value={detail.user_subtype_id? detail.user_subtype_id: ''} 
                        onChange={e => onChangeDetail('user_subtype_id', e.target.value)} 
                        disabled={process === 'view'} 
                      >
                        {filteredUserSubtypes(detail.user_type_id).map((d, i) => (
                          <MenuItem key={`ust_${i}`} value={d.id}>{d.name}</MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                ): (<></>)}
                <div className="sep"></div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <TextField 
                    label="ที่อยู่" variant="outlined" fullWidth={true} multiline rows={2} 
                    value={detail.address? detail.address: ''} 
                    onChange={e => onChangeDetail('address', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <TextField 
                    label="เบอร์โทรศัพท์" variant="outlined" fullWidth={true} 
                    value={detail.phone? detail.phone: ''} 
                    onChange={e => onChangeDetail('phone', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    label="ตำแหน่ง" variant="outlined" fullWidth={true} 
                    value={detail.title? detail.title: ''} 
                    onChange={e => onChangeDetail('title', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40 sm-50">
                  <TextField 
                    label="บริษัท" variant="outlined" fullWidth={true} 
                    value={detail.company? detail.company: ''} 
                    onChange={e => onChangeDetail('company', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <TextField 
                    label="ที่อยู่บริษัท" variant="outlined" fullWidth={true} multiline rows={2} 
                    value={detail.company_address? detail.company_address: ''} 
                    onChange={e => onChangeDetail('company_address', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <TextField 
                    label="เบอร์โทรศัพท์บริษัท" variant="outlined" fullWidth={true} 
                    value={detail.company_phone? detail.company_phone: ''} 
                    onChange={e => onChangeDetail('company_phone', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>

                {props.customColumns && props.customColumns.length? (
                  props.customColumns.filter(d => d.status).map((d, i) => (
                    <Fragment key={`custom_${i}`}>
                      {i%2 === 0? (<div className="sep"></div>): (<></>)}
                      <div className="grid xl-1-3 lg-45 md-40 sm-50">
                        <TextField 
                          label={d.name.replace(/_/g, ' ')} variant="outlined" 
                          fullWidth={true} className="label-cap" 
                          value={custom[d.name]? custom[d.name]: ''} 
                          onChange={e => onChangeCustom(d.name, e.target.value)} 
                          disabled={process === 'view'} 
                        />
                      </div>
                    </Fragment>
                  ))
                ): (<></>)}

                <div className="sep"></div>
                <div className="grid sm-100">
                  <div className="mt-2">
                    {process === 'create'? (
                      <Button 
                        type="submit" variant="contained" color="primary" 
                        size="large" startIcon={<AddIcon />} className={`mr-2`} 
                      >
                        สร้าง
                      </Button>
                    ): process === 'update'? (
                      <>
                        <Button 
                          type="submit" variant="contained" color="primary" 
                          size="large" startIcon={<EditIcon />} className={`mr-2`} 
                        >
                          บันทึก
                        </Button>
                        <Button 
                          component={Link} to={`/admin/user/view/${dataId}`} variant="outlined" 
                          size="large" color="primary" className={`mr-2`} 
                        >
                          ดูข้อมูล
                        </Button>
                      </>
                    ): ('')}
                    <Button 
                      component={Link} to="/admin/users" 
                      variant="outlined" color="primary" size="large"
                    >
                      ย้อนกลับ
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        ): (<></>)}

        <FooterAdmin />
      </div>
    </>
  );
}

UserPage.defaultProps = {
	activeIndex: 2
};
UserPage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processCreate: PropTypes.func.isRequired,
  processUpdate: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  roles: state.admin.roles,
  userTypes: state.admin.userTypes,
  customColumns: state.admin.customColumns
});

export default connect(mapStateToProps, {
  processList: processList,
  processRead: processRead,
  processCreate: processCreate,
  processUpdate: processUpdate
})(UserPage);