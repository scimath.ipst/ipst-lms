import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  IconButton, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processClear, processList, processDelete } from '../../actions/admin.actions';
import { UserModel } from '../../models';


function RolesPage(props) {
  const user = new UserModel(props.user);
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'ชื่อระดับ', numeric: false, noPadding: false },
    { label: 'ลำดับ', numeric: true, noPadding: false },
    { label: 'เป็นค่าตั้งต้น', numeric: true, noPadding: false },
    { label: 'ระดับ Admin', numeric: true, noPadding: false },
    { label: 'ระดับ Super Admin', numeric: true, noPadding: false },
    { label: 'สถานะ', numeric: true, noPadding: false },
    { label: 'การกระทำ', numeric: true, noPadding: true },
  ];
  
  const [paginate, setPaginate] = useState({ page: 1, pp: 10 });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };

  const [selectedData, setSelectedData] = useState(null);
  const [dialog, setDialog] = useState(false);
  const onDialogOpen = (e, data) => {
    e.preventDefault();
    setSelectedData(data);
    setDialog(true);
  };
  const onDialogClose = (e) => {
    e.preventDefault();
    setSelectedData(null);
    setDialog(false);
  };
  const onDialogDelete = async (e) => {
    e.preventDefault();
    try {
      await props.processDelete('role', { id: selectedData.id }, true);
      props.processList('roles', {}, true);
    } catch(err) {}
    setSelectedData(null);
    setDialog(false);
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    props.processClear('roles');
    props.processList('roles', {}, true);
  }, []);
  /* eslint-enable */
  
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="การจัดการระดับผู้ใช้" />
        
        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          {user.isSuperAdmin() ? (
            <div className="grids table-options jc-end">
              <div className="grid lg-25 md-40 sm-50">
                <Button 
                  component={Link} to="/admin/role/create" startIcon={<AddIcon />} 
                  variant="contained" color="primary" size="large" 
                >
                  สร้างระดับผู้ใช้
                </Button>
              </div>
            </div>
          ): ('')}
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  {columns.map((col, i) => (
                    <TableCell
                      key={i} className="ws-nowrap"
                      align={col.numeric ? 'center' : 'left'}
                      padding={col.noPadding ? 'none' : 'normal'}
                    >
                      {col.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {!props.list.length? (
                  <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): props.list
                .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                .map((d, i) => {
                  let index = (paginate.page-1)*paginate.pp + i + 1;
                  return (
                    <TableRow key={index} hover>
                      <TableCell align="center" style={{ minWidth: '3rem' }}>
                        {index}
                      </TableCell>
                      <TableCell align="left" style={{ minWidth: '10rem', width: '100%' }}>
                        <Link to={`/admin/role/view/${d.id}`}>{d.name}</Link>
                      </TableCell>
                      <TableCell align="center" style={{ minWidth: '5rem' }}>{d.order}</TableCell>
                      <TableCell align="center" style={{ minWidth: '6rem' }}>{d.displayIsDefault()}</TableCell>
                      <TableCell align="center" style={{ minWidth: '6rem' }}>{d.displayIsAdmin()}</TableCell>
                      <TableCell align="center" style={{ minWidth: '6rem' }}>{d.displayIsSuperAdmin()}</TableCell>
                      <TableCell align="center" style={{ minWidth: '6rem' }}>{d.displayStatus()}</TableCell>
                      <TableCell align="center" padding="none" style={{ minWidth: '6rem' }} className="ws-nowrap">
                        <IconButton size="small" component={Link} to={`/admin/role/view/${d.id}`}>
                          <VisibilityIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        {user.isSuperAdmin() && !d.isSuperAdmin()? (
                          <>
                            <IconButton size="small" component={Link} to={`/admin/role/update/${d.id}`}>
                              <EditIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                            <IconButton size="small" onClick={e => onDialogOpen(e, d)}>
                              <DeleteIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                          </>
                        ): (<></>)}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            labelRowsPerPage="แสดง" 
            rowsPerPageOptions={[10, 25, 50, 100]} 
            component="div" 
            count={props.list.length} 
            rowsPerPage={paginate.pp} 
            page={paginate.page - 1} 
            onPageChange={onChangePage} 
            onRowsPerPageChange={onChangePerPage} 
          />
        </div>

        <FooterAdmin />
      </div>
      
      {user.isSuperAdmin()? (
        <Dialog open={dialog} onClose={onDialogClose}>
          <DialogTitle>ยืนยันการลบข้อมูล</DialogTitle>
          <DialogContent>
            <DialogContentText>
              ยืนยันการลบข้อมูล โดยข้อมูลที่ถูกลบจะไม่สามารถกู้คืนได้
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={onDialogClose} color="primary">
              ยกเลิก
            </Button>
            <Button onClick={onDialogDelete} color="primary" autoFocus>
              ยืนยันการลบ
            </Button>
          </DialogActions>
        </Dialog>
      ): (<></>)}
    </>
  );
}

RolesPage.defaultProps = {
	activeIndex: 1
};
RolesPage.propTypes = {
	activeIndex: PropTypes.number,
  user: PropTypes.object.isRequired,
  processClear: PropTypes.func.isRequired,
  processList: PropTypes.func.isRequired,
  processDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  list: state.admin.roles
});

export default connect(mapStateToProps, {
  processClear: processClear,
  processList: processList, 
  processDelete: processDelete
})(RolesPage);