import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import {
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Tabs, Tab,
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Checkbox
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processRead } from '../../actions/admin.actions';
import { UserModel, UserRoleModel, ModulePermissionModel } from '../../models';


function RoleViewPage(props) {
  const user = new UserModel(props.user);
  const history = useHistory();
  const process = props.match.params.process? props.match.params.process: 'view';
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [tabValue, setTabValue] = useState(0);
  
  const [values, setValues] = useState(new UserRoleModel({}));
  const onChangeInput = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setValues({ ...values, [key]: val });
  };

  const [permissions, setPermissions] = useState([]);

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(async () => {
    await props.processRead('role', { id: dataId }, true).then(d => {
      setValues(d);
    }).catch(() => history.push('/admin/roles'));
    await props.processRead('role-permissions', { role_id: dataId }, true).then(d => {
      setPermissions(d.map(k => new ModulePermissionModel(k)));
    }).catch(() => history.push('/admin/roles'));
  }, []);
  /* eslint-enable */
  
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'create'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}ระดับผู้ใช้`} 
          back="/admin/roles" 
        />
        
        <div className="app-card app-card-header" data-aos="fade-up" data-aos-delay="150">
          <Tabs
            value={tabValue} onChange={(e, val) => setTabValue(val)} 
            indicatorColor="primary" textColor="primary" variant="scrollable" 
            scrollButtons="auto" aria-label="scrollable auto tabs example" 
          >
            <Tab label="ข้อมูลระดับ" id="tab-0" aria-controls="tabpanel-0" />
            <Tab label="สิทธิ์การเข้าถึง" id="tab-1" aria-controls="tabpanel-1" />
          </Tabs>
        </div>
        
        {tabValue === 0? (
          <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
            <div className="grids ai-center">
              <div className="grid xl-2-3 lg-90 md-80 sm-100">
                <TextField 
                  required={true} label="ชื่อระดับ" variant="outlined" fullWidth={true} 
                  value={values.name? values.name: ''} 
                  onChange={e => onChangeInput('name', e.target.value)} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="sep"></div>
              <div className="grid xl-1-3 lg-45 md-40 sm-50">
                <FormControl 
                  variant="outlined" fullWidth={true} 
                  disabled={process === 'view'} 
                >
                  <InputLabel id="is_admin">ระดับ Admin *</InputLabel>
                  <Select
                    labelId="is_admin" label="ระดับ Admin" required={true} 
                    value={values.is_admin} 
                    onChange={e => onChangeInput('is_admin', e.target.value, true)} 
                    disabled={process === 'view'} 
                  >
                    <MenuItem value={1}>ใช่</MenuItem>
                    <MenuItem value={0}>ไม่ใช่</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="grid xl-1-3 lg-45 md-40 sm-50">
                <FormControl 
                  variant="outlined" fullWidth={true} 
                  disabled={process === 'view'} 
                >
                  <InputLabel id="is_default">เป็นค่าตั้งต้น *</InputLabel>
                  <Select
                    labelId="is_default" label="เป็นค่าตั้งต้น" required={true} 
                    value={values.is_default} 
                    onChange={e => onChangeInput('is_default', e.target.value, true)} 
                    disabled={process === 'view'} 
                  >
                    <MenuItem value={1}>ใช่</MenuItem>
                    <MenuItem value={0}>ไม่ใช่</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="sep"></div>
              <div className="grid xl-1-3 lg-45 md-40 sm-50">
                <TextField 
                  label="ลำดับ" variant="outlined" fullWidth={true} required={true} 
                  value={values.order? values.order: 1} type="number" 
                  onChange={e => onChangeInput('order', e.target.valueAsNumber)} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40 sm-50">
                <FormControl 
                  variant="outlined" fullWidth={true} 
                  disabled={process === 'view'} 
                >
                  <InputLabel id="status">สถานะ *</InputLabel>
                  <Select
                    labelId="status" label="สถานะ" required={true} 
                    value={values.status} 
                    onChange={e => onChangeInput('status', e.target.value, true)} 
                    disabled={process === 'view'} 
                  >
                    <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                    <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="sep"></div>
              <div className="grid sm-100">
                <div className="mt-2">
                  {user.isSuperAdmin() && !values.isSuperAdmin()? (
                    <Button 
                      component={Link} to={`/admin/role/update/${dataId}`} variant="outlined" 
                      size="large" color="primary" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      แก้ไข
                    </Button>
                  ): (<></>)}
                  <Button 
                    component={Link} to="/admin/roles" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          </div>
        ): (<></>)}

        {tabValue === 1? (
          <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
            <TableContainer>
              <Table size="medium">
                <TableHead>
                  <TableRow>
                    <TableCell align="left" className="ws-nowrap">โมดูล</TableCell>
                    <TableCell align="center" className="ws-nowrap">Create</TableCell>
                    <TableCell align="center" className="ws-nowrap">Read</TableCell>
                    <TableCell align="center" className="ws-nowrap">Update</TableCell>
                    <TableCell align="center" className="ws-nowrap">Delete</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {!permissions.length? (
                    <TableRow>
                      <TableCell colSpan={5} align="center">
                        ไม่พบข้อมูลในระบบ
                      </TableCell>
                    </TableRow>
                  ): permissions.map((d, i) => {
                    return (
                      <TableRow key={`p_${i}`} hover>
                        <TableCell align="left">{d.name}</TableCell>
                        <TableCell align="center">
                          <Checkbox defaultChecked={d.create? true: false} color="primary" disabled={true} />
                        </TableCell>
                        <TableCell align="center">
                          <Checkbox defaultChecked={d.read? true: false} color="primary" disabled={true} />
                        </TableCell>
                        <TableCell align="center">
                          <Checkbox defaultChecked={d.update? true: false} color="primary" disabled={true} />
                        </TableCell>
                        <TableCell align="center">
                          <Checkbox defaultChecked={d.delete? true: false} color="primary" disabled={true} />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <div className="grids">
              <div className="grid sm-100">
                <div className="mt-2">
                  {user.isSuperAdmin() && !values.isSuperAdmin()? (
                    <Button 
                      component={Link} to={`/admin/role/update/${dataId}`} variant="outlined" 
                      size="large" color="primary" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      แก้ไข
                    </Button>
                  ): (<></>)}
                  <Button 
                    component={Link} to="/admin/roles" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          </div>
        ): (<></>)}

        <FooterAdmin />
      </div>
    </>
  );
}

RoleViewPage.defaultProps = {
	activeIndex: 1
};
RoleViewPage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {
  processRead: processRead
})(RoleViewPage);