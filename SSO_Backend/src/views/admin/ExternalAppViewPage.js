import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { TextField, FormControl, InputLabel, Select, MenuItem, Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { processRead } from '../../actions/admin.actions';
import { ExternalAppModel } from '../../models';

import { FRONTEND_URL, TOKEN_KEY } from '../../actions/types';
import jwt from 'jsonwebtoken';


function ExternalAppViewPage(props) {
  const history = useHistory();
  const process = props.match.params.process? props.match.params.process: 'view';
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new ExternalAppModel({ status: 1 }));
  const onChangeInput = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setValues({ ...values, [key]: val });
  };

  const [redirectUrl, setRedirectUrl] = useState('');
  const [authKey, setAuthKey] = useState('');
  const onChangeRedirectUrl = (val) => {
    setRedirectUrl(val);
    if(val){
      let temp = jwt.sign({
        externalAppId: values.id,
        redirectUrl: val
      }, TOKEN_KEY);
      setAuthKey(FRONTEND_URL+'auth/signin-redirect/'+temp);
    }else{
      setAuthKey('');
    }
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    props.processRead('external-app', { id: dataId }, true).then(d => {
      setValues(d);
    }).catch(() => history.push('/admin/external-apps'));
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'create'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}แอปภายนอก`} 
          back="/admin/external-apps" 
        />

        <div className="app-card pt-0" data-aos="fade-up" data-aos-delay="150">
          <div className="grids ai-center">
            <div className="grid xl-2-3 lg-90 md-80 sm-100">
              <TextField 
                required={true} label="ชื่อแอปภายนอก" variant="outlined" fullWidth={true} 
                value={values.name? values.name: ''} 
                onChange={e => onChangeInput('name', e.target.value)} 
                disabled={process === 'view'} 
              />
            </div>
            <div className="sep"></div>
            <div className="grid xl-2-3 lg-90 md-80 sm-100">
              <TextField 
                label="คำบรรยาย" variant="outlined" fullWidth={true} multiline rows={2} 
                value={values.description? values.description: ''} 
                onChange={e => onChangeInput('description', e.target.value)} 
                disabled={process === 'view'} 
              />
            </div>
            <div className="sep"></div>
            <div className="grid xl-1-3 lg-45 md-40 sm-50">
              <TextField 
                required={true} label="ลิงค์" variant="outlined" fullWidth={true} 
                value={values.url? values.url: ''} 
                onChange={e => onChangeInput('url', e.target.value)} 
                disabled={process === 'view'} 
              />
            </div>
            <div className="grid xl-1-3 lg-45 md-40 sm-50">
              <FormControl 
                variant="outlined" fullWidth={true} 
                disabled={process === 'view'} 
              >
                <InputLabel id="status">สถานะ *</InputLabel>
                <Select
                  labelId="status" label="สถานะ" required={true} 
                  value={values.status} 
                  onChange={e => onChangeInput('status', e.target.value, true)} 
                  disabled={process === 'view'} 
                >
                  <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                  <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                </Select>
              </FormControl>
            </div>
            
            <div className="sep"></div>
            <div className="grid xl-2-3 lg-90 md-80 sm-100">
              <TextField 
                label="POST URL สำหรับ Redirect การเข้าสู่ระบบ" variant="outlined" fullWidth={true} 
                value={redirectUrl? redirectUrl: ''} 
                onChange={e => onChangeRedirectUrl(e.target.value)} 
              />
            </div>
            <div className="grid xl-2-3 lg-90 md-80 sm-100">
              <TextField 
                label="Redirect Auth Key" variant="outlined" fullWidth={true} 
                value={authKey} onChange={e => e.preventDefault()} 
              />
            </div>

            <div className="sep"></div>
            <div className="grid sm-100">
              <div className="mt-2">
                <Button 
                  component={Link} to={`/admin/external-app/update/${dataId}`} variant="outlined" 
                  size="large" color="primary" startIcon={<EditIcon />} className={`mr-2`} 
                >
                  แก้ไข
                </Button>
                <Button 
                  component={Link} to="/admin/external-apps" 
                  variant="outlined" color="primary" size="large"
                >
                  ย้อนกลับ
                </Button>
              </div>
            </div>
          </div>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ExternalAppViewPage.defaultProps = {
	activeIndex: 3
};
ExternalAppViewPage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {
  processRead: processRead
})(ExternalAppViewPage);