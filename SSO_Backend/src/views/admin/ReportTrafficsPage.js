import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { onMounted, formatNumber, formatDate, formatTime } from '../../helpers/frontend';
import ReactECharts from 'echarts-for-react';

import { 
  FormControl, InputLabel, Select, MenuItem, TextField,
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination
} from '@material-ui/core';

import { connect } from 'react-redux';
import { processList } from '../../actions/admin.actions';


function ReportTrafficsPage(props) {
  const [reportReady, setReportReady] = useState(true);
  const [reportType, setReportType] = useState('Total Report');

  const thisYear = new Date().getFullYear();
  const [startDate, setStartDate] = useState(`${thisYear}-01-01`);
  const [endDate, setEndDate] = useState(`${thisYear}-12-31`);


  const [totalReport, setTotalReport] = useState(0);
  const [graphReport, setGraphReport] = useState([]);

  const [tableReport, setTableReport] = useState([]);
  const [paginate, setPaginate] = useState({ page: 1, pp: 10 });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };


  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(async () => {
    if(reportReady){
      setReportReady(false);
      await props.processList('report-traffics', {
        type: reportType,
        startDate: startDate,
        endDate: endDate
      }, true).then(d => {
        if(reportType === 'Total Report'){
          setTotalReport(Number(d.result.total));
        }else if(['Daily Report', 'Monthly Report', 'Yearly Report'].indexOf(reportType) > -1){
          setGraphReport(d.result);
        }else if(reportType === 'Full Report'){
          setTableReport(d.result);
        }
      });
      setReportReady(true);
    }
  }, [reportType, startDate, endDate]);
  /* eslint-enable */
  
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="รายงานการใช้งาน" />

        <div className="app-card pt-1" data-aos="fade-up" data-aos-delay="150">
          <div className="grids">
            <div className="grid xl-25 lg-30 md-1-3 sm-100">
              <FormControl variant="outlined" fullWidth={true} className="text-left">
                <InputLabel id="report-type">รูปแบบรายงาน *</InputLabel>
                <Select
                  labelId="report-type" label="รูปแบบรายงาน" required={true} 
                  value={reportType? reportType: ''} 
                  onChange={e => setReportType(e.target.value)} 
                >
                  <MenuItem value={'Total Report'}>รายงานภาพรวม</MenuItem>
                  <MenuItem value={'Daily Report'}>รายงานรายวัน</MenuItem>
                  <MenuItem value={'Monthly Report'}>รายงานรายเดือน</MenuItem>
                  <MenuItem value={'Yearly Report'}>รายงานรายปี</MenuItem>
                  <MenuItem value={'Full Report'}>รายงานทั้งหมด</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="grid xl-25 lg-30 md-1-3 sm-50">
              <TextField
                label="วันที่เริ่ม" type="date" 
                variant="outlined" fullWidth={true} className="text-left" 
                value={startDate? startDate: ''} 
                onChange={e => setStartDate(e.target.value)} 
              />
            </div>
            <div className="grid xl-25 lg-30 md-1-3 sm-50">
              <TextField
                label="วันสิ้นสุด" type="date" 
                variant="outlined" fullWidth={true} className="text-left" 
                value={endDate? endDate: ''} 
                onChange={e => setEndDate(e.target.value)} 
              />
            </div>
          </div>
        </div>
        
        {reportType === 'Total Report'? (
          <div className="app-card mt-4" data-aos="fade-up" data-aos-delay="150">
            <div className="text-center pt-2">
              <h6 className="fw-600">จำนวนการเข้าใช้งานทั้งสิ้น</h6>
              <h2 className="fw-600 color-p">
                {formatNumber(totalReport, 0)}<span className="h5 fw-600 color-dark"> ครั้ง</span>
              </h2>
            </div>
          </div>
        ): (<></>)}
        
        {['Daily Report', 'Monthly Report', 'Yearly Report'].indexOf(reportType) > -1? (
          <div className="app-card mt-4" data-aos="fade-up" data-aos-delay="150">
            <ReactECharts 
              option={{
                grid: { top: 20, right: 20, bottom: 30, left: 50 },
                xAxis: {
                  type: 'time',
                  axisLine: {
                    lineStyle: { color: 'rgb(227, 226, 236, 0.8)' }
                  },
                  axisLabel: {
                    fontSize: 13, color: '#777777',
                    formatter: reportType.includes('Daily')? '{d} {MMM} {yyyy}'
                      : reportType.includes('Monthly')? '{MMM} {yyyy}'
                      : reportType.includes('Yearly')? '{yyyy}': ''
                  }
                },
                yAxis: {
                  splitLine: {
                    lineStyle: { color: 'rgb(227, 226, 236, 0.8)' }
                  },
                  axisLine: {
                    lineStyle: { color: 'rgb(227, 226, 236, 0.8)' }
                  },
                  axisLabel: { fontSize: 13, color: '#777777' }
                },
                tooltip: {
                  show: true,
                  showContent: true,
                  alwaysShowContent: false,
                  triggerOn: 'mousemove',
                  trigger: 'axis',
                  axisPointer: {
                    label: { show: false }
                  }
                },
                series: [{
                  name: 'จำนวน',
                  type: 'bar', stack: 'Stack',
                  data: graphReport.map(d => ([ d.date, Number(d.total) ]))
                }],
                color: ['#5A8DEE']
              }}
            />
          </div>
        ): (<></>)}

        {reportType === 'Full Report'? (
          <div className="app-card mt-4" data-aos="fade-up" data-aos-delay="150">
            <TableContainer>
              <Table size="medium">
                <TableHead>
                  <TableRow>
                    <TableCell className="ws-nowrap" align="center" padding="none">ID</TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">ชื่อ-นามสกุล</TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">ชื่อผู้ใช้</TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">การกระทำ</TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">แอปภายนอก</TableCell>
                    <TableCell className="ws-nowrap" align="center" padding="none">วันที่สมัครสมาชิก</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {!tableReport.length? (
                    <TableRow>
                      <TableCell colSpan={6} align="center">ไม่พบข้อมูลในระบบ</TableCell>
                    </TableRow>
                  ): tableReport
                  .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                  .map((d, i) => {
                    let index = (paginate.page-1)*paginate.pp + i + 1;
                    return (
                      <TableRow key={index} hover>
                        <TableCell align="center">{index}</TableCell>
                        <TableCell align="left">
                          <Link to={`/admin/role/view/${d.user_id}`}>
                            {d.firstname} {d.lastname}
                          </Link>
                        </TableCell>
                        <TableCell align="left">{d.username}</TableCell>
                        <TableCell align="left">{d.action}</TableCell>
                        <TableCell align="left">
                          {d.external_app_name? d.external_app_name: '-'}
                        </TableCell>
                        <TableCell align="center" padding="none">
                          {formatDate(d.visited_time)} {formatTime(d.visited_time)}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              labelRowsPerPage="แสดง" 
              rowsPerPageOptions={[10, 25, 50, 100]} 
              component="div" 
              count={tableReport.length} 
              rowsPerPage={paginate.pp} 
              page={paginate.page - 1} 
              onPageChange={onChangePage} 
              onRowsPerPageChange={onChangePerPage} 
            />
          </div>
        ): (<></>)}
        
        <FooterAdmin />
      </div>
    </>
  );
}

ReportTrafficsPage.defaultProps = {
	activeIndex: 5
};
ReportTrafficsPage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  processList: processList
})(ReportTrafficsPage);