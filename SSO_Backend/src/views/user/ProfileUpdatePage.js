import PropTypes from 'prop-types';
import { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import {
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Tabs, Tab,
  Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import {
  processList, userUpdate, userDetailUpdate, userPasswordUpdate, userRequestToDelete
} from '../../actions/user.actions';
import { UserModel, UserDetailModel } from '../../models';


function ProfileUpdatePage(props) {
  const user = new UserModel(props.user);
  const process = props.match.params.process? props.match.params.process: 'update';
  
  const [tabValue, setTabValue] = useState(0);
  
  const [values, setValues] = useState(new UserModel({ status: 1 }));
  const onChangeInput = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setValues({ ...values, [key]: val });
  };
  const onSubmit = async (e) => {
    e.preventDefault();
    await props.userUpdate(values);
  };
  

  const [custom, setCustom] = useState({});
  const onChangeCustom = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    setCustom({ ...custom, [key]: val });
  };

  const [detail, setDetail] = useState(new UserDetailModel({}));
  const onChangeDetail = (key, val, isNumber=false) => {
    if(isNumber) val = val || val===0? Number(val): '';
    if(key === 'user_type_id'){
      setDetail({ ...detail, [key]: val, user_subtype_id: null });
    }else{
      setDetail({ ...detail, [key]: val });
    }
  };
  const filteredUserSubtypes = (userTypeId) => {
    let temp = props.userTypes.filter(d => d.id === userTypeId);
    if(temp.length) return temp[0].subtypes;
    else return [];
  };
  const onSubmitDetail = async (e) => {
    e.preventDefault();
    await props.userDetailUpdate({ ...custom, ...detail });
  };
  

  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const onSubmitPassword = async (e) => {
    e.preventDefault();
    let res = await props.userPasswordUpdate({
      password: password,
      newPassword: newPassword,
      confirmNewPassword: confirmNewPassword
    });
    if(res){
      setPassword('');
      setNewPassword('');
      setConfirmNewPassword('');
    }
  };


  // START: Request To Delete
  const [dialog, setDialog] = useState(false);
  const onDialogOpen = (e=null) => {
    if(e) e.preventDefault();
    setDialog(true);
  };
  const onDialogClose = (e) => {
    e.preventDefault();
    setDialog(false);
  };
  const onDialogDelete = async (e) => {
    e.preventDefault();
    let res = await props.userRequestToDelete();
    if(res){
      setTimeout(() => {
        window.location.href = '/';
      }, 300);
    }
    setDialog(false);
  };
  // END: Request To Delete


  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    setValues(user);
    setDetail(user.detail);
    setCustom(user.custom_data);
    props.processList('user-types');
    props.processList('custom-columns');
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container app-full">
        <div className="container">
          <div style={{ maxWidth: 800, margin: '0 auto' }}>
            <AppTitle title={`แก้ไขข้อมูลโปรไฟล์`} />
            
            <div className="app-card app-card-header mt-4" data-aos="fade-up" data-aos-delay="150">
              <Tabs
                value={tabValue} onChange={(e, val) => setTabValue(val)} 
                indicatorColor="primary" textColor="primary" variant="scrollable" 
                scrollButtons="auto" aria-label="scrollable auto tabs example" 
              >
                <Tab label="บัญชีผู้ใช้" id="tab-0" aria-controls="tabpanel-0" />
                <Tab label="ข้อมูลทั่วไป" id="tab-1" aria-controls="tabpanel-1" />
                <Tab label="ตั้งรหัสผ่านใหม่" id="tab-2" aria-controls="tabpanel-2" />
                <Tab label="ขอลบบัญชี" id="tab-3" aria-controls="tabpanel-3" />
              </Tabs>
            </div>

            {tabValue === 0? (
              <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
                <form onSubmit={onSubmit}>
                  <div className="grids ai-center">
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="ชื่อจริง" variant="outlined" fullWidth={true} 
                        value={values.firstname? values.firstname: ''} 
                        onChange={e => onChangeInput('firstname', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="นามสกุล" variant="outlined" fullWidth={true} 
                        value={values.lastname? values.lastname: ''} 
                        onChange={e => onChangeInput('lastname', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="อีเมล" variant="outlined" fullWidth={true} 
                        value={values.email? values.email: ''} type="email" 
                        onChange={e => onChangeInput('email', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="ชื่อผู้ใช้" variant="outlined" fullWidth={true} 
                        value={values.username? values.username: ''} 
                        onChange={e => onChangeInput('username', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="ระดับผู้ใช้" variant="outlined" fullWidth={true} 
                        value={values.role.name? values.role.name: ''} 
                        onChange={() => {}} disabled={true} 
                      />
                    </div>
                    <div className="grid sm-50">
                      <FormControl variant="outlined" fullWidth={true} disabled={true}>
                        <InputLabel id="status">สถานะ *</InputLabel>
                        <Select
                          labelId="status" label="สถานะ" required={true} 
                          value={values.status} 
                          onChange={() => {}} disabled={true} 
                        >
                          <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                          <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                        </Select>
                      </FormControl>
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <div className="mt-2">
                        <Button 
                          type="submit" variant="contained" color="primary" 
                          size="large" startIcon={<EditIcon />} className={`mr-2`} 
                        >
                          บันทึก
                        </Button>
                        <Button 
                          component={Link} to="/user/profile" 
                          variant="outlined" color="primary" size="large"
                        >
                          ย้อนกลับ
                        </Button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            ): (<></>)}
            
            {tabValue === 1? (
              <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
                <form onSubmit={onSubmitDetail}>
                  <div className="grids ai-center">
                    <div className="grid sm-50">
                      <FormControl 
                        variant="outlined" fullWidth={true} 
                        disabled={process === 'view'} 
                      >
                        <InputLabel id="user_type">ประเภทผู้ใช้</InputLabel>
                        <Select
                          labelId="user_type" label="ประเภทผู้ใช้" 
                          value={detail.user_type_id? detail.user_type_id: ''} 
                          onChange={e => onChangeDetail('user_type_id', e.target.value)} 
                          disabled={process === 'view'} 
                        >
                          {props.userTypes.map((d, i) => (
                            <MenuItem key={`ut_${i}`} value={d.id}>{d.name}</MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </div>
                    {detail.user_type_id && filteredUserSubtypes(detail.user_type_id).length? (
                      <div className="grid sm-50">
                        <FormControl 
                          variant="outlined" fullWidth={true} 
                          disabled={process === 'view'} 
                        >
                          <InputLabel id="user_subtype_id">ประเภทผู้ใช้ย่อย</InputLabel>
                          <Select
                            labelId="user_subtype_id" label="ประเภทผู้ใช้ย่อย" 
                            value={detail.user_subtype_id? detail.user_subtype_id: ''} 
                            onChange={e => onChangeDetail('user_subtype_id', e.target.value)} 
                            disabled={process === 'view'} 
                          >
                            {filteredUserSubtypes(detail.user_type_id).map((d, i) => (
                              <MenuItem key={`ust_${i}`} value={d.id}>{d.name}</MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </div>
                    ): (<></>)}
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <TextField 
                        label="ที่อยู่" variant="outlined" fullWidth={true} multiline rows={2} 
                        value={detail.address? detail.address: ''} 
                        onChange={e => onChangeDetail('address', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <TextField 
                        label="เบอร์โทรศัพท์" variant="outlined" fullWidth={true} 
                        value={detail.phone? detail.phone: ''} 
                        onChange={e => onChangeDetail('phone', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-50">
                      <TextField 
                        label="ตำแหน่ง" variant="outlined" fullWidth={true} 
                        value={detail.title? detail.title: ''} 
                        onChange={e => onChangeDetail('title', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid sm-50">
                      <TextField 
                        label="บริษัท" variant="outlined" fullWidth={true} 
                        value={detail.company? detail.company: ''} 
                        onChange={e => onChangeDetail('company', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <TextField 
                        label="ที่อยู่บริษัท" variant="outlined" fullWidth={true} multiline rows={2} 
                        value={detail.company_address? detail.company_address: ''} 
                        onChange={e => onChangeDetail('company_address', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <TextField 
                        label="เบอร์โทรศัพท์บริษัท" variant="outlined" fullWidth={true} 
                        value={detail.company_phone? detail.company_phone: ''} 
                        onChange={e => onChangeDetail('company_phone', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>

                    {props.customColumns && props.customColumns.length? (
                      props.customColumns.filter(d => d.status).map((d, i) => (
                        <Fragment key={`custom_${i}`}>
                          {i%2 === 0? (<div className="sep"></div>): (<></>)}
                          <div className="grid xl-1-3 lg-45 md-40 sm-50">
                            <TextField 
                              label={d.name.replace(/_/g, ' ')} variant="outlined" 
                              fullWidth={true} className="label-cap" 
                              value={custom[d.name]? custom[d.name]: ''} 
                              onChange={e => onChangeCustom(d.name, e.target.value)} 
                              disabled={process === 'view'} 
                            />
                          </div>
                        </Fragment>
                      ))
                    ): (<></>)}

                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <div className="mt-2">
                        <Button 
                          type="submit" variant="contained" color="primary" 
                          size="large" startIcon={<EditIcon />} className={`mr-2`} 
                        >
                          บันทึก
                        </Button>
                        <Button 
                          component={Link} to="/user/profile" 
                          variant="outlined" color="primary" size="large"
                        >
                          ย้อนกลับ
                        </Button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            ): (<></>)}

            {tabValue === 2? (
              <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
                <form onSubmit={onSubmitPassword}>
                  <div className="grids ai-center">
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="รหัสผ่าน" variant="outlined" fullWidth={true} 
                        type="password" value={password? password: ''} 
                        onChange={e => setPassword(e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="รหัสผ่านใหม่" variant="outlined" fullWidth={true} 
                        type="password" value={newPassword? newPassword: ''} 
                        onChange={e => setNewPassword(e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid sm-50">
                      <TextField 
                        required={true} label="ยืนยันรหัสผ่านใหม่" variant="outlined" fullWidth={true} 
                        type="password" value={confirmNewPassword? confirmNewPassword: ''} 
                        onChange={e => setConfirmNewPassword(e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <div className="mt-2">
                        <Button 
                          type="submit" variant="contained" color="primary" 
                          size="large" startIcon={<EditIcon />} className={`mr-2`} 
                        >
                          บันทึก
                        </Button>
                        <Button 
                          component={Link} to="/user/profile" 
                          variant="outlined" color="primary" size="large"
                        >
                          ย้อนกลับ
                        </Button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            ): (<></>)}
            
            {tabValue === 3? (
              <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
                <div className="grids ai-center">
                  <div className="grid sm-100">
                    <div>
                      <Button 
                        onClick={onDialogOpen} variant="contained" color="secondary" 
                        size="large" startIcon={<DeleteIcon />} className={`mr-2`} 
                      >
                        ขอลบบัญชี
                      </Button>
                      <Button 
                        component={Link} to="/admin/profile" 
                        variant="outlined" color="primary" size="large"
                      >
                        ย้อนกลับ
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            ): (<></>)}

          </div>
        </div>
        <FooterAdmin />
      </div>
      
      <Dialog open={dialog} onClose={onDialogClose}>
        <DialogTitle>ยืนยันการขอลบบัญชี</DialogTitle>
        <DialogContent>
          <DialogContentText>
            โดยข้อมูลบัญชีผู้ใช้ของคุณจะถูกลบภายใน 24 ชั่วโมง
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDialogClose} color="primary">
            ยกเลิก
          </Button>
          <Button onClick={onDialogDelete} color="primary" autoFocus>
            ยืนยันการขอลบบัญชี
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

ProfileUpdatePage.defaultProps = {
	activeIndex: -1
};
ProfileUpdatePage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired,
  userUpdate: PropTypes.func.isRequired,
  userDetailUpdate: PropTypes.func.isRequired,
  userPasswordUpdate: PropTypes.func.isRequired,
  userRequestToDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  userTypes: state.frontend.userTypes,
  customColumns: state.frontend.customColumns
});

export default connect(mapStateToProps, {
  processList: processList,
  userUpdate: userUpdate,
  userDetailUpdate: userDetailUpdate,
  userPasswordUpdate: userPasswordUpdate,
  userRequestToDelete: userRequestToDelete
})(ProfileUpdatePage);