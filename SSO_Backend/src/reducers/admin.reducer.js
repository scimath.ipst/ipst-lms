import {
  ADMIN_USERS, ADMIN_ROLES, ADMIN_USER_TYPES,
  ADMIN_EXTERNAL_APPS, ADMIN_MODULES,
  ADMIN_CUSTOM_COLUMNS
} from '../actions/types';

const initialState = {
  roles: [],
  users: [],
  userTypes: [],
  externalApps: [],
  modules: [],
  customColumns: []
};

const adminReducer = (state = initialState, action) => {
  switch(action.type) {

    case ADMIN_ROLES:
      return { ...state, roles: action.payload };
    case ADMIN_USERS:
      return { ...state, users: action.payload };
    case ADMIN_USER_TYPES:
      return { ...state, userTypes: action.payload };
      
    case ADMIN_EXTERNAL_APPS:
      return { ...state, externalApps: action.payload };
    case ADMIN_MODULES:
      return { ...state, modules: action.payload };
      
    case ADMIN_CUSTOM_COLUMNS:
      return { ...state, customColumns: action.payload };

    default:
      return state;
  }
};

export default adminReducer;