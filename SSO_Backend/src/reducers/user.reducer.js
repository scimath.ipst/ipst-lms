import { USER_SIGNIN, USER_SIGNOUT, USER_UPDATE, USER_UPDATE_DETAIL } from '../actions/types';
import { UserModel, UserDetailModel } from '../models';

const tempUser = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
const initialState = new UserModel(tempUser? JSON.parse(tempUser): {});
var temp;
var temp2;

const userReducer = (state = initialState, action) => {
  switch(action.type) {

    case USER_SIGNIN:
      temp = new UserModel(action.payload);
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(temp));
      return { ...state, ...temp };

    case USER_SIGNOUT:
      temp = new UserModel({});
      localStorage.removeItem(`${process.env.REACT_APP_PREFIX}_USER`);
      return { ...state, ...temp };

    case USER_UPDATE:
      temp = JSON.parse(localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`));
      if(action.payload.firstname) temp.firstname = action.payload.firstname;
      if(action.payload.lastname) temp.lastname = action.payload.lastname;
      if(action.payload.username) temp.username = action.payload.username;
      if(action.payload.email) temp.email = action.payload.email;
      if(action.payload.accessToken) temp.accessToken = action.payload.accessToken;
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(temp));
      return { ...state, ...temp };

    case USER_UPDATE_DETAIL:
      temp = JSON.parse(localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`));
      if(action.payload) temp.detail = new UserDetailModel(action.payload);
      if(temp.custom_columns && temp.custom_columns.length){
        temp2 = {};
        temp.custom_columns.forEach(d => {
          if(action.payload[d.name]!==undefined) temp2[d.name] = action.payload[d.name];
        });
        temp.custom_data = temp2;
      }
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(temp));
      return { ...state, ...temp };

    default:
      return state;
  }
};

export default userReducer;