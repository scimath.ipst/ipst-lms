import { combineReducers } from 'redux';
import alertReducer from './alert.reducer';
import userReducer from './user.reducer';
import frontendReducer from './frontend.reducer';
import adminReducer from './admin.reducer';

export default combineReducers({
  alert: alertReducer,
  user: userReducer,
  frontend: frontendReducer,
  admin: adminReducer
});