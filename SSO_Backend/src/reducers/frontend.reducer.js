import {
  FRONTEND_USER_TYPES, FRONTEND_CUSTOM_COLUMNS
} from '../actions/types';

const initialState = {
  userTypes: [],
  customColumns: []
};

const frontendReducer = (state = initialState, action) => {
  switch(action.type) {

    case FRONTEND_USER_TYPES:
      return { ...state, userTypes: action.payload };

    case FRONTEND_CUSTOM_COLUMNS:
      return { ...state, customColumns: action.payload };

    default:
      return state;
  }
};

export default frontendReducer;