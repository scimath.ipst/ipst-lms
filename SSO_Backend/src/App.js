import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { UserModel } from './models';
import AlertPopup from './components/AlertPopup';

import CryptoJS from 'crypto-js';
import { TOKEN_KEY } from './actions/types';

import { 
  SignInPage, SignUpPage, ForgetPasswordPage, ResetPasswordPage,
  SignInRedirectPage, NoPermissionPage, Page404
} from './views/auth';
import { 
  AdminDashboardPage, AdminProfilePage, AdminProfileUpdatePage,
  AdminRolesPage, AdminRoleViewPage, AdminRolePage,
  AdminUsersPage, AdminUserViewPage, AdminUserPage,
  AdminExternalAppsPage, AdminExternalAppViewPage, AdminExternalAppPage,
  AdminModulesPage, AdminModulePage, AdminModuleViewPage,
  AdminReportTrafficsPage, AdminReportActionsPage, AdminReportRegistrationsPage,
  AdminUserTypesPage, AdminUserTypeViewPage, AdminUserTypePage,
  AdminCustomColumnsPage, AdminCustomColumnViewPage, AdminCustomColumnPage
} from './views/admin';
import { 
  UserProfilePage, UserProfileUpdatePage
} from './views/user';

function App() {
  VerifySignedIn();
  return (
    <BrowserRouter>
      <Switch>
        <NotSignedInRoute exact path="/" component={SignInPage} />
        <NotSignedInRoute exact path="/auth/signin" component={SignInPage} />
        <NotSignedInRoute exact path="/auth/signup" component={SignUpPage} />
        <NotSignedInRoute exact path="/auth/forget-password" component={ForgetPasswordPage} />
        <NotSignedInRoute exact path="/auth/reset-password" component={ResetPasswordPage} />
        <Route exact path="/auth/signin-redirect/:authKey" component={SignInRedirectPage} />


        {/* START: Admin */}
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin" component={AdminDashboardPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/profile" component={AdminProfilePage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/profile/update" component={AdminProfileUpdatePage} 
        />
        
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/roles" component={AdminRolesPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/role/view/:dataId" component={AdminRoleViewPage} 
        />
        <ProtectedRoute auth={GuardSuperAdmin()} redirect="/" exact 
          path="/admin/role/:process/:dataId?" component={AdminRolePage} 
        />
        
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/users" component={AdminUsersPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/user/view/:dataId" component={AdminUserViewPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/user/:process/:dataId?" component={AdminUserPage} 
        />

        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/user-types" component={AdminUserTypesPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/user-type/view/:dataId" component={AdminUserTypeViewPage} 
        />
        <ProtectedRoute auth={GuardSuperAdmin()} redirect="/" exact 
          path="/admin/user-type/:process/:dataId?" component={AdminUserTypePage} 
        />

        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/custom-columns" component={AdminCustomColumnsPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/custom-column/view/:dataId" component={AdminCustomColumnViewPage} 
        />
        <ProtectedRoute auth={GuardSuperAdmin()} redirect="/" exact 
          path="/admin/custom-column/:process/:dataId?" component={AdminCustomColumnPage} 
        />

        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/external-apps" component={AdminExternalAppsPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/external-app/view/:dataId" component={AdminExternalAppViewPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/external-app/:process/:dataId?" component={AdminExternalAppPage} 
        />

        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/modules" component={AdminModulesPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/module/view/:dataId" component={AdminModuleViewPage} 
        />
        <ProtectedRoute auth={GuardSuperAdmin()} redirect="/" exact 
          path="/admin/module/:process/:dataId?" component={AdminModulePage} 
        />

        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/report-traffics" component={AdminReportTrafficsPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/report-actions" component={AdminReportActionsPage} 
        />
        <ProtectedRoute auth={GuardAdmin()} redirect="/" exact 
          path="/admin/report-registrations" component={AdminReportRegistrationsPage} 
        />
        {/* END: Admin */}

        
        {/* START: User */}
        <ProtectedRoute auth={GuardUser()} redirect="/" exact 
          path="/user" component={UserProfilePage} 
        />
        <ProtectedRoute auth={GuardUser()} redirect="/" exact 
          path="/user/profile" component={UserProfilePage} 
        />
        <ProtectedRoute auth={GuardUser()} redirect="/" exact 
          path="/user/profile/update" component={UserProfileUpdatePage} 
        />
        {/* END: User */}


        <Route exact path="/no-permission" component={NoPermissionPage} />
        <Route path="*" component={Page404} />
      </Switch>
      <AlertPopup />
    </BrowserRouter>
  );
}


// Verify
const VerifySignedIn = async () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return true;

  user = JSON.parse(user);
  if(!user.accessToken) return true;
  
  let bytes = CryptoJS.AES.decrypt(user.accessToken, TOKEN_KEY);
  let accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/verify-signin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ accessToken: accessToken })
  });
  if(!fetch1.ok || fetch1.status !== 200){
    localStorage.removeItem(`${process.env.REACT_APP_PREFIX}_USER`);
    window.location.reload();
    return false;
  }else{
    return true;
  }
};


// Routes
const ProtectedRoute = ({ component: Component, auth, redirect="/", ...rest }) => {
  if(auth) {
    return (<Route {...rest} render={(props) => (<Component {...props} />)} />);
  } else {
    return <Redirect to={redirect} />;
  }
};
const NotSignedInRoute = ({ component: Component, ...rest }) => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return (<Route {...rest} render={(props) => (<Component {...props} />)} />);

  user = new UserModel(JSON.parse(user));
  if(!user.isSignedIn()) return (<Route {...rest} render={(props) => (<Component {...props} />)} />);

  if(user.isAdmin()) return <Redirect to="/admin" />;
  else if(user.isSignedIn()) return <Redirect to="/user" />;
  else return <Redirect to="/no-permission" />;
};


// Guards
const GuardSuperAdmin = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isSuperAdmin()) {
    return false;
  }
  return true;
};
const GuardAdmin = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isAdmin()) {
    return false;
  }
  return true;
};
const GuardUser = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isSignedIn() && !user.isAdmin()) {
    return false;
  }
  return true;
};


export default App;
