export class UserDetailModel {
  constructor(data) {
    this.id = data.id? data.id: null;
    this.user_id = data.user_id? data.user_id: null;

    this.user_type_id = data.user_type_id? data.user_type_id: null;
    this.user_subtype_id = data.user_subtype_id? data.user_subtype_id: null;

    this.address = data.address? data.address: null;
    this.phone = data.phone? data.phone: null;

    this.title = data.title? data.title: null;
    this.company = data.company? data.company: null;
    this.company_address = data.company_address? data.company_address: null;
    this.company_phone = data.company_phone? data.company_phone: null;
  }

  isValid() { return this.id? true: false; }
}
