import { Chip } from '@material-ui/core';
import { UserSubtypeModel } from '.';

export class UserTypeModel {
  constructor(data) {
    this.id = data.id? data.id: null;
    
    this.name = data.name? data.name: null;

    this.subtypes = data.subtypes && data.subtypes.length
      ? data.subtypes.map(d => new UserSubtypeModel(d)): [];
    
    this.status = data.status? Number(data.status): 0;
  }

  isValid() { return this.id? true: false; }
  
  displayStatus() {
    if(this.isValid() && this.status) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
    else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }
}
