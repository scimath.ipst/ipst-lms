import { Chip } from '@material-ui/core';

export class ExternalAppModel {
  constructor(data) {
    this.id = data.id? data.id: null;

    this.name = data.name? data.name: null;
    this.description = data.description? data.description: null;
    this.url = data.url? data.url: null;

    this.status = data.status? Number(data.status): 0;
  }

  isValid() { return this.id? true: false; }
  
  displayStatus() {
    if(this.isValid() && this.status) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
    else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }
}
