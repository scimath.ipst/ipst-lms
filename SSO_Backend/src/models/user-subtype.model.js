import { Chip } from '@material-ui/core';

export class UserSubtypeModel {
  constructor(data) {
    this.id = data.id? data.id: null;
    this.parent_id = data.parent_id? data.parent_id: null;
    
    this.name = data.name? data.name: null;
    
    this.status = data.status? Number(data.status): 0;
  }

  isValid() { return this.id? true: false; }
  
  displayStatus() {
    if(this.isValid() && this.status) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
    else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }
}
