import { Chip } from '@material-ui/core';

export class ModuleModel {
  constructor(data) {
    this.id = data.id? data.id: null;

    this.name = data.name? data.name: null;
    this.code = data.code? data.code: null;

    this.status = data.status? Number(data.status): 0;
  }

  isValid() { return this.id? true: false; }
  
  displayStatus() {
    if(this.isValid() && this.status) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
    else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }
}
