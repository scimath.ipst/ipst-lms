import { Chip } from '@material-ui/core';

export class ModulePermissionModel {
  constructor(data) {
    this.id = data.id? data.id: null;
    this.module_id = data.module_id? data.module_id: null;

    this.name = data.name? data.name: null;
    this.code = data.code? data.code: null;
    
    this.create = data.create? Number(data.create): 0;
    this.read = data.read? Number(data.read): 0;
    this.update = data.update? Number(data.update): 0;
    this.delete = data.delete? Number(data.delete): 0;

    this.status = data.status? Number(data.status): 0;
  }

  isValid() { return this.id? true: false; }
  
  displayStatus() {
    if(this.isValid() && this.status) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
    else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }
}
