import { Chip } from '@material-ui/core';

export class UserRoleModel {
  constructor(data) {
    this.id = data.id? data.id: null;
    
    this.name = data.name? data.name: null;
    
    this.is_admin = data.is_admin? Number(data.is_admin): 0;
    this.is_super_admin = data.is_super_admin? Number(data.is_super_admin): 0;
    
    this.order = data.order? Number(data.order): 1;
    this.is_default = data.is_default? Number(data.is_default): 0;
    this.status = data.status? Number(data.status): 0;
  }

  isValid() { return this.id? true: false; }

  isAdmin() {
    if(this.isValid() && (this.is_admin || this.is_super_admin)) return true;
    else return false;
  }
  isSuperAdmin() {
    if(this.isValid() && this.is_super_admin) return true;
    else return false;
  }

  displayIsDefault() {
    if(this.isValid() && this.is_default) return (<Chip label="ใช่" size="small" color="primary" />);
    else return (<Chip label="ไม่ใช่" size="small" color="secondary" />);
  }
  displayStatus() {
    if(this.isValid() && this.status) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
    else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }
  displayIsAdmin() {
    if(this.isAdmin()) return (<Chip label="ใช่" size="small" color="primary" />);
    else return (<Chip label="ไม่ใช่" size="small" color="secondary" />);
  }
  displayIsSuperAdmin() {
    if(this.isSuperAdmin()) return (<Chip label="ใช่" size="small" color="primary" />);
    else return (<Chip label="ไม่ใช่" size="small" color="secondary" />);
  }
}
