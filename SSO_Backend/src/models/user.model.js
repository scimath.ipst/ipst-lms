import { Chip } from '@material-ui/core';
import { UserRoleModel, UserDetailModel, CustomColumnModel } from '.';

export class UserModel {
  constructor(data) {
    this.id = data.id? data.id: null;
    
    this.firstname = data.firstname? data.firstname: null;
    this.lastname = data.lastname? data.lastname: null;
    this.username = data.username? data.username: null;
    this.email = data.email? data.email: null;

    this.role = new UserRoleModel(data.role? data.role: {});
    this.detail = new UserDetailModel(data.detail? data.detail: {});

    this.profile = data.profile? data.profile: '/assets/img/avatar/default.jpg';
    
    this.thai_id = data.thai_id? data.thai_id: null;
    this.thai_id_path = data.thai_id_path? data.thai_id_path: null;
    this.code = data.code? data.code: null;

    this.facebook_id = data.facebook_id? data.facebook_id: null;
    this.google_id = data.google_id? data.google_id: null;
    this.liff_id = data.liff_id? data.liff_id: null;

    this.status = data.status? Number(data.status): 0;
    
    this.accessToken = data.accessToken? data.accessToken: null;

    this.custom_data = data.custom_data? data.custom_data: {};
    this.custom_columns = data.custom_columns && data.custom_columns.length
      ? data.custom_columns.map(d => {
        if(data[d.name]) this.custom_data[d.name] = data[d.name];
        return new CustomColumnModel(d);
      }): [];
  }

  isValid() { return this.id? true: false; }

  displayName() {
    if(this.isValid()){
      if(this.firstname || this.lastname) {
        return this.firstname + ' ' + this.lastname;
      } else if (this.username) {
        return this.username;
      } else {
        return 'General User';
      }
    }else{
      return '';
    }
  }
  displayRole() {
    if(this.role && this.role.isValid()) return this.role.name;
    else return '';
  }
  displayStatus() {
    if(this.isValid()){
      if(this.status === 1) return (<Chip label="เปิดใช้งาน" size="small" color="primary" />);
      else if(this.status === -1) return (<Chip label="ขอลบบัญชี" size="small" color="secondary" />);
      else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
    }else return (<Chip label="ปิดใช้งาน" size="small" color="secondary" />);
  }

  isSignedIn() { return this.id && this.accessToken? true: false; }

  isAdmin() { return this.role.isAdmin(); }
  isSuperAdmin() { return this.role.isSuperAdmin(); }
}
