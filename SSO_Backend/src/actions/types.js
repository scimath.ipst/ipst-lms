
// Environtment Variables
export const FRONTEND_URL = `${process.env.REACT_APP_URL}`;
export const API_URL = `${process.env.REACT_APP_API_URL}`;

export const TOKEN_KEY = `${process.env.REACT_APP_TOKEN_KEY}`;
export const REFRESH_KEY = `${process.env.REACT_APP_REFRESH_KEY}`;

export const FACEBOOK_APP_ID = `${process.env.REACT_APP_FACEBOOK_APP_ID}`;
export const GOOGLE_CLIENT_ID = `${process.env.REACT_APP_GOOGLE_CLIENT_ID}`;
export const LIFF_ID = `${process.env.REACT_APP_LIFF_ID}`;


// Alert
export const ALERT_CHANGE = 'alert/change';
export const ALERT_DISAPPEAR = 'alert/disappear';
export const ALERT_LOADING = 'alert/loading';


// User
export const USER_SIGNIN = 'user/signin';
export const USER_SIGNOUT = 'user/signout';
export const USER_UPDATE = 'user/update';
export const USER_UPDATE_DETAIL = 'user/update-detail';


// Frontend
export const FRONTEND_USER_TYPES = 'frontend/user-types';
export const FRONTEND_CUSTOM_COLUMNS = 'frontend/custom-columns';


// Admin
export const ADMIN_ROLES = 'admin/roles';
export const ADMIN_USERS = 'admin/users';
export const ADMIN_USER_TYPES = 'admin/user-types';
export const ADMIN_EXTERNAL_APPS = 'admin/external-apps';
export const ADMIN_MODULES = 'admin/modules';
export const ADMIN_CUSTOM_COLUMNS = 'admin/custom-columns';
