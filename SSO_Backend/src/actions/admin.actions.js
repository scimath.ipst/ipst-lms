import { apiHeader } from '../helpers/header';
import { alertChange, alertLoading } from './alert.actions';
import {
  API_URL,
  ADMIN_USERS, ADMIN_ROLES, ADMIN_USER_TYPES,
  ADMIN_EXTERNAL_APPS, ADMIN_MODULES, ADMIN_CUSTOM_COLUMNS
} from './types';
import {
  PaginateModel, UserModel, UserRoleModel, UserTypeModel,
  ExternalAppModel, ModuleModel, CustomColumnModel
} from '../models';


export const processClear = (type) => (dispatch) => {
  if(type === 'roles') dispatch({ type: ADMIN_ROLES, payload: [] });
  else if(type === 'users') dispatch({ type: ADMIN_USERS, payload: [] });
  else if(type === 'user-types') dispatch({ type: ADMIN_USER_TYPES, payload: [] });
  else if(type === 'external-apps') dispatch({ type: ADMIN_EXTERNAL_APPS, payload: [] });
  else if(type === 'modules') dispatch({ type: ADMIN_MODULES, payload: [] });
  else if(type === 'custom-columns') dispatch({ type: ADMIN_CUSTOM_COLUMNS, payload: [] });
};


export const processList = (type, input={}, loading=false) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    if(loading) dispatch(alertLoading(true));
    try {
      const fetch1 = await fetch(`${API_URL}admin/${type}`, {
        method: 'POST',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){
        if(loading) dispatch(alertLoading(false));
        reject(data1); return false;
      }

      let res = {
        paginate: new PaginateModel(data1.data.paginate? data1.data.paginate: {}),
        dataFilter: data1.data.dataFilter? data1.data.dataFilter: {},
        result: []
      };
      if(type === 'roles'){
        res.result = data1.data.result.map(d => new UserRoleModel(d));
        dispatch({ type: ADMIN_ROLES, payload: res.result });
      }else if(type === 'users'){
        res.result = data1.data.result.map(d => new UserModel(d));
        dispatch({ type: ADMIN_USERS, payload: res.result });
      }else if(type === 'user-types'){
        res.result = data1.data.result.map(d => new UserTypeModel(d));
        dispatch({ type: ADMIN_USER_TYPES, payload: res.result });
      }else if(type === 'external-apps'){
        res.result = data1.data.result.map(d => new ExternalAppModel(d));
        dispatch({ type: ADMIN_EXTERNAL_APPS, payload: res.result });
      }else if(type === 'modules'){
        res.result = data1.data.result.map(d => new ModuleModel(d));
        dispatch({ type: ADMIN_MODULES, payload: res.result });
      }else if(type === 'custom-columns'){
        res.result = data1.data.result.map(d => new CustomColumnModel(d));
        dispatch({ type: ADMIN_CUSTOM_COLUMNS, payload: res.result });
      }else{
        res.result = data1.data.result;
      }

      if(loading) dispatch(alertLoading(false));
      resolve(res); return true;
    } catch (err) {
      console.log(err);
      if(loading) dispatch(alertLoading(false));
      reject(err); return false;
    }
  });
};
export const processRead = (type, input={}, loading=false) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    if(loading) dispatch(alertLoading(true));
    let url = `${API_URL}admin/${type}`;
    let sep = '?';
    Object.keys(input).forEach(k => {
      if (input[k] || input[k]===0){ url += `${sep}${k}=${input[k]}`; sep = '&'; }
    });
    try {
      const fetch1 = await fetch(url, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){
        if(loading) dispatch(alertLoading(false));
        reject(data1); return false;
      }
      
      let result = null;
      if(type === 'role'){
        result = new UserRoleModel(data1.data.result);
      }else if(type === 'user'){
        result = new UserModel(data1.data.result);
      }else if(type === 'user-type'){
        result = new UserTypeModel(data1.data.result);
      }else if(type === 'external-app'){
        result = new ExternalAppModel(data1.data.result);
      }else if(type === 'module'){
        result = new ModuleModel(data1.data.result);
      }else if(type === 'custom-column'){
        result = new CustomColumnModel(data1.data.result);
      }else{
        result = data1.data.result;
      }

      if(loading) dispatch(alertLoading(false));
      resolve(result); return true;
    } catch (err) {
      console.log(err);
      if(loading) dispatch(alertLoading(false));
      reject(err); return false;
    }
  });
};
export const processCreate = (type, input={}, loading=false) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    if(loading) dispatch(alertLoading(true));
    try {
      const fetch1 = await fetch(`${API_URL}admin/${type}`, {
        method: 'POST',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){
        if(loading) dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
        reject(data1); return false;
      }
      
      if(loading) dispatch(alertChange('Info', data1.message));
      resolve(data1); return true;
    } catch (err) {
      console.log(err);
      if(loading) dispatch(alertChange('Danger', 'Internal server error.'));
      reject(err); return false;
    }
  });
};
export const processUpdate = (type, input={}, loading=false) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    if(loading) dispatch(alertLoading(true));
    try {
      const fetch1 = await fetch(`${API_URL}admin/${type}`, {
        method: 'PATCH',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){
        if(loading) dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
        reject(data1); return false;
      }
      
      if(loading) dispatch(alertChange('Info', data1.message));
      resolve(data1); return true;
    } catch (err) {
      console.log(err);
      if(loading) dispatch(alertChange('Danger', 'Internal server error.'));
      reject(err); return false;
    }
  });
};
export const processDelete = (type, input={}, loading=false) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    if(loading) dispatch(alertLoading(true));
    try {
      const fetch1 = await fetch(`${API_URL}admin/${type}`, {
        method: 'DELETE',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){
        if(loading) dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
        reject(data1); return false;
      }
      
      if(loading) dispatch(alertChange('Info', data1.message));
      resolve(data1); return true;
    } catch (err) {
      console.log(err);
      if(loading) dispatch(alertChange('Danger', 'Internal server error.'));
      reject(err); return false;
    }
  });
};
