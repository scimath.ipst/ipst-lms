import {
  API_URL, TOKEN_KEY, LIFF_ID,
  USER_SIGNIN, USER_SIGNOUT, USER_UPDATE, USER_UPDATE_DETAIL,
  FRONTEND_USER_TYPES, FRONTEND_CUSTOM_COLUMNS
} from './types';
import {
  PaginateModel, UserTypeModel, CustomColumnModel
} from '../models';

import { alertChange, alertLoading } from './alert.actions';
import { apiHeader } from '../helpers/header';

import CryptoJS from 'crypto-js';
import liff from '@line/liff';


// Auth
export const userSignin = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}auth/signin`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });

    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    var user = data1.result;
    dispatch(alertChange('Info', 'เข้าสู่ระบบสำเร็จแล้ว'));
    if(input.redirect && input.redirectUrl){
      fetch(input.redirectUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ jwt: user.accessToken }),
      });
      return true;
    }else{
      user.accessToken = CryptoJS.AES.encrypt(user.accessToken, TOKEN_KEY).toString();
      dispatch({ type: USER_SIGNIN, payload: user });
      return true;
    }
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userSignout = () => (dispatch) => {
  if(window.FB && window.FB.logout) window.FB.logout();
  liff.init({ liffId: LIFF_ID }, () => liff.logout(), err => console.error(err));
  dispatch(alertChange('Info', 'ออกจากระบบสำเร็จแล้ว'));
  dispatch({ type: USER_SIGNOUT, payload: true });
};

export const userSignup = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}auth/signup`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    const data1 = await fetch1.json();
    console.log(data1)
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.error));
      return false;
    }
    
    dispatch(alertChange('Info', 'สมัครสมาชิกสำเร็จแล้ว'));
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};

export const userForgetPassword = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}auth/forget-password`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.error));
      return false;
    }
    
    dispatch(alertChange('Info', 'ขอการตั้งรหัสผ่านใหม่สำเร็จแล้ว'));
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userCheckResetPassword = (token) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      dispatch(alertLoading(true));
      const fetch1 = await fetch(`${API_URL}auth/check-reset-password?token=${token}`, {
        method: 'GET',
        headers: apiHeader()
      });
      dispatch(alertLoading(false));
      if(!fetch1.ok || fetch1.status !== 200) {
        resolve(false);
      } else {
        resolve(true);
      }
    } catch (error) {
      dispatch(alertLoading(false));
      console.log(error);
    }
  });
};
export const userResetPassword = (input) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      dispatch(alertLoading(true));
      const fetch1 = await fetch(`${API_URL}auth/reset-password`, {
        method: 'POST',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) {
        dispatch(alertChange('Warning', data1.message, data1.error));
        resolve(false);
      } else {
        dispatch(alertChange('Info', 'การตั้งรหัสผ่านใหม่สำเร็จแล้ว'));
        resolve(true);
      }
    } catch (error) {
      dispatch(alertLoading(false));
      console.log(error);
    }
  });
};


// Auth Social
export const userSigninFacebook = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}auth/signin-facebook`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });

    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    var user = data1.result;
    dispatch(alertChange('Info', 'เข้าสู่ระบบสำเร็จแล้ว'));
    if(input.redirect && input.redirectUrl){
      fetch(input.redirectUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ jwt: user.accessToken }),
      });
      return true;
    }else{
      user.accessToken = CryptoJS.AES.encrypt(user.accessToken, TOKEN_KEY).toString();
      dispatch({ type: USER_SIGNIN, payload: user });
      return true;
    }
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userSigninGoogle = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}auth/signin-google`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });

    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    var user = data1.result;
    dispatch(alertChange('Info', 'เข้าสู่ระบบสำเร็จแล้ว'));
    if(input.redirect && input.redirectUrl){
      fetch(input.redirectUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ jwt: user.accessToken }),
      });
      return true;
    }else{
      user.accessToken = CryptoJS.AES.encrypt(user.accessToken, TOKEN_KEY).toString();
      dispatch({ type: USER_SIGNIN, payload: user });
      return true;
    }
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userSigninLIFF = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}auth/signin-liff`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });

    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    var user = data1.result;
    dispatch(alertChange('Info', 'เข้าสู่ระบบสำเร็จแล้ว'));
    if(input.redirect && input.redirectUrl){
      fetch(input.redirectUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ jwt: user.accessToken }),
      });
      return true;
    }else{
      user.accessToken = CryptoJS.AES.encrypt(user.accessToken, TOKEN_KEY).toString();
      dispatch({ type: USER_SIGNIN, payload: user });
      return true;
    }
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};


// Profile
export const userUpdate = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}user/profile`, {
      method: 'PATCH',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200){
      dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    if(data1.data.accessToken){
      data1.data.accessToken = CryptoJS.AES.encrypt(data1.data.accessToken, TOKEN_KEY).toString();
    }
    dispatch({ type: USER_UPDATE, payload: data1.data });
    return true;
  } catch (err) {
    console.log(err);
    dispatch(alertChange('Danger', 'Internal server error.'));
    return false;
  }
};
export const userDetailUpdate = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}user/detail`, {
      method: 'PATCH',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200){
      dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({ type: USER_UPDATE_DETAIL, payload: data1.data });
    return true;
  } catch (err) {
    console.log(err);
    dispatch(alertChange('Danger', 'Internal server error.'));
    return false;
  }
};
export const userPasswordUpdate = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}user/password`, {
      method: 'PATCH',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200){
      dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({ type: USER_UPDATE_DETAIL, payload: data1.data });
    return true;
  } catch (err) {
    console.log(err);
    dispatch(alertChange('Danger', 'Internal server error.'));
    return false;
  }
};

export const userRequestToDelete = () => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${API_URL}user/request-to-delete`, {
      method: 'PATCH',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200){
      dispatch(alertChange('Warning', data1.message, data1.error? data1.error: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    if(window.FB && window.FB.logout) window.FB.logout();
    liff.init({ liffId: LIFF_ID }, () => liff.logout(), err => console.error(err));
    dispatch(alertChange('Info', 'ออกจากระบบสำเร็จแล้ว'));
    dispatch({ type: USER_SIGNOUT, payload: true });
    return true;
  } catch (err) {
    console.log(err);
    dispatch(alertChange('Danger', 'Internal server error.'));
    return false;
  }
};


// Frontend
export const processList = (type, input={}, loading=false) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    if(loading) dispatch(alertLoading(true));
    try {
      const fetch1 = await fetch(`${API_URL}user/${type}`, {
        method: 'POST',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){
        if(loading) dispatch(alertLoading(false));
        reject(data1); return false;
      }

      let res = {
        paginate: new PaginateModel(data1.data.paginate? data1.data.paginate: {}),
        dataFilter: data1.data.dataFilter? data1.data.dataFilter: {},
        result: []
      };
      if(type === 'user-types'){
        res.result = data1.data.result.map(d => new UserTypeModel(d));
        dispatch({ type: FRONTEND_USER_TYPES, payload: res.result });
      }else if(type === 'custom-columns'){
        res.result = data1.data.result.map(d => new CustomColumnModel(d));
        dispatch({ type: FRONTEND_CUSTOM_COLUMNS, payload: res.result });
      }else{
        res.result = data1.data.result;
      }

      if(loading) dispatch(alertLoading(false));
      resolve(res); return true;
    } catch (err) {
      console.log(err);
      if(loading) dispatch(alertLoading(false));
      reject(err); return false;
    }
  });
};
