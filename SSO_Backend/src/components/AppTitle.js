import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

function AppTitle(props) {
  return (
    <div className="app-title h6 fw-600" data-aos="fade-up" data-aos-delay="0">
      {props.back? (
        <IconButton component={Link} to={props.back} size="small" color="primary">
          <ChevronLeftIcon />
        </IconButton>
      ): ('')}
      <div className="text">{props.title}</div>
    </div>
  );
}

AppTitle.defaultProps = {
  back: ''
};
AppTitle.propTypes = {
	title: PropTypes.string.isRequired,
  back: PropTypes.string
};

export default AppTitle;