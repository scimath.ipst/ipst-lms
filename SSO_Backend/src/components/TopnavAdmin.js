import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Avatar, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import EqualizerIcon from '@material-ui/icons/Equalizer';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import DevicesIcon from '@material-ui/icons/Devices';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import SettingsIcon from '@material-ui/icons/Settings';

import { connect } from 'react-redux';
import { userSignout } from '../actions/user.actions';
import { UserModel } from '../models';

function TopnavAdmin(props) {
  const history = useHistory();
	const user = new UserModel(props.user);

	const [link, setLink] = useState('');
	const [offsetY, setOffsetY] = useState(0);
	const [sidenavActive, setSidenavStatus] = useState(false);
	const [menu, setMenu] = useState([]);
	
	const onSignout = (e) => {
		e.preventDefault();
		props.userSignout();
		setTimeout(() => {
			window.location.href = '/';
		}, 300);
	};
	
	const backToTop = () => {
		window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
	};

  /* eslint-disable */
	useEffect(() => {
    const unlisten = history.listen(() => {
      window.scrollTo(0, 0);
    });
		return async () => {
      unlisten();
    };
  }, []);
	useEffect(() => {
		if(user.isAdmin()){
			setLink('/admin');
			let tempMenu = [
				{
					group: 'ทั่วไป',
					children: [
						{ name: 'หน้าแรก', to: `/admin`, activeIndex: 0, icon: <EqualizerIcon />  },
						{ name: 'ระดับผู้ใช้', to: `/admin/roles`, activeIndex: 1, icon: <PersonAddIcon />  },
						{ name: 'บัญชีผู้ใช้', to: `/admin/users`, activeIndex: 2, icon: <PeopleAltIcon />  },
						{ name: 'ประเภทผู้ใช้', to: `/admin/user-types`, activeIndex: 8, icon: <ContactMailIcon />  },
						{ name: 'ฟิลด์ข้อมูล', to: `/admin/custom-columns`, activeIndex: 9, icon: <SettingsIcon />  },
					]
				}, {
					group: 'การเชื่อมต่อ',
					children: [
						{ name: 'แอปภายนอก', to: `/admin/external-apps`, activeIndex: 3, icon: <DevicesIcon />  },
						{ name: 'การจัดการโมดูล', to: `/admin/modules`, activeIndex: 4, icon: <ViewModuleIcon />  },
					]
				}, {
					group: 'รายงาน',
					children: [
						{ name: 'การใช้งาน', to: `/admin/report-traffics`, activeIndex: 5, icon: <EqualizerIcon />  },
						{ name: 'การกระทำ', to: `/admin/report-actions`, activeIndex: 6, icon: <EqualizerIcon />  },
						{ name: 'การลงทะเบียน', to: `/admin/report-registrations`, activeIndex: 7, icon: <EqualizerIcon />  },
					]
				}
			];
			setMenu(tempMenu);
		}else if(user.isSignedIn()){
			setLink('/user');
		}

		setOffsetY(window.pageYOffset);
		const handleScroll = () => {
			setOffsetY(window.pageYOffset);
		};
		window.addEventListener('scroll', handleScroll);
		return function cleanup() {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
	/* eslint-enable */

  return (
		<>
			<nav className="topnav-admin">
				<div className="wrapper">
					<div className="options">

						<div className="option">
							<div className="menu-option" onClick={() => setSidenavStatus(!sidenavActive)}>
								<div className={`hamburger ${sidenavActive? 'active': ''}`}>
									<div></div><div></div><div></div>
								</div>
							</div>
							<Link to={link} className="logo">
								<h6>IPST SSO</h6>
							</Link>
						</div>
						
						{user.isSignedIn()? (
							<div className="option">
								<div className="avatar-container">
									<Avatar src={user.avatar} className="sm" />
									<div className="dropdown">
										<List>
											<ListItem className="d-block">
												<p className="text-line fw-500">{user.displayName()}</p>
												<p className="text-line xs fw-400 op-60">{user.displayRole()}</p>
												<p className="text-line xs fw-400 op-60">{user.email}</p>
											</ListItem>
											{props.forBackend? (
												<ListItem button component={Link} to={`${link}/profile`}>
													<ListItemIcon>
														<AccountCircleIcon />
													</ListItemIcon>
													<ListItemText primary="ข้อมูลส่วนตัว" />
												</ListItem>
											): (<></>)}
											<ListItem button onClick={onSignout}>
												<ListItemIcon>
													<ExitToAppIcon />
												</ListItemIcon>
												<ListItemText primary="ออกจากระบบ" />
											</ListItem>
										</List>
									</div>
								</div>
							</div>
						): (<></>)}

					</div>
				</div>
			</nav>
			{props.forBackend? (
				<div className="topnav-spacer"></div>
			): (<></>)}

			{props.forBackend && user.isSignedIn() && user.isAdmin()? (
				<nav className={`sidenav-admin ${sidenavActive? 'active': ''}`}>
					<div className="wrapper">
						<div className="menu-container">
							{menu.map((g, i) => (
								<div className="menu-group" key={i}>
									<div className="title">{g.group}</div>
									{g.children.map((d, j) => (
										<Link className={`menu ${props.activeIndex === d.activeIndex? 'active': ''}`} 
											to={d.to} key={i+'_'+j}
										>
											<div className="icon">{d.icon}</div>
											{d.name}
										</Link>
									))}
								</div>
							))}
						</div>
					</div>
				</nav>
			): (<></>)}

			<div className={`back-to-top ${offsetY > 50? 'active': ''}`} onClick={backToTop}>
				<KeyboardArrowUpIcon style={{ fontSize: 32 }} />
			</div>
		</>
  );
}

TopnavAdmin.defaultProps = {
	activeIndex: 0,
	forBackend: true
};
TopnavAdmin.propTypes = {
	activeIndex: PropTypes.number,
	forBackend: PropTypes.bool,
	user: PropTypes.object.isRequired,
	userSignout: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user
});

export default connect(mapStateToProps, {userSignout})(TopnavAdmin);