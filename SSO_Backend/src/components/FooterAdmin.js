import { Divider } from '@material-ui/core';

function FooterAdmin(props) {
  return (
		<>			
			<nav className="footer-admin">
        <div className="wrapper">
          <Divider />
          <div className="inner-wrapper">
            <p className="xxs fw-400">
              Copyright © 2022 <span className="fw-600">IPST</span>. All rights reserved.
            </p>
          </div>

        </div>
			</nav>
		</>
  );
}

export default FooterAdmin;