import 'package:flutter/material.dart';

import '../../../constants/constants.dart';
import '../../../widgets/widgets.dart';

class listTile extends StatelessWidget {
  const listTile(
      {Key? key, required this.icon, required this.title, this.onPressed})
      : super(key: key);
  final IconData icon;
  final String title;
  final VoidCallback? onPressed;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.all(0),
      leading: Icon(
        icon,
        size: 20,
      ),
      title: CustomText(
        text: title,
        textSize: fontSizeM,
        fontWeight: fontWeightNormal,
      ),
      trailing: const Icon(Icons.navigate_next_rounded),
      onTap: onPressed,
    );
  }
}
