import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/widgets/control_widget/control_widget.dart';

class ThemeSetting extends StatelessWidget {
  const ThemeSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FrontendController>(builder: (controller) {
      return Positioned(
        right: 0,
        top: 40,
        child: PopupMenuButton<int>(
          icon: controller.darkMode
              ? Icon(
                  Icons.dark_mode,
                  color: kBlack.withOpacity(.6),
                )
              : const Icon(
                  Icons.light_mode,
                  color: kWhite,
                ),
          tooltip: "",
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(outlineRadius),
            ),
          ),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Row(
                children: [
                  Icon(
                    Icons.dark_mode,
                    color:
                        controller.darkMode ? kPrimary : kBlack.withOpacity(.6),
                  ),
                  const SizedBox(
                    width: 5.0,
                  ),
                  CustomText(
                    text: 'Dark',
                    textSize: fontSizeM,
                    fontWeight: fontWeightNormal,
                    textColor: controller.darkMode ? kPrimary : null,
                  ),
                ],
              ),
              value: 0,
            ),
            const PopupMenuDivider(
              height: 0,
            ),
            PopupMenuItem(
              child: Row(
                children: [
                  Icon(
                    Icons.light_mode,
                    color: !controller.darkMode ? kPrimary : null,
                  ),
                  const SizedBox(
                    width: 5.0,
                  ),
                  CustomText(
                    text: 'Light',
                    textSize: fontSizeM,
                    fontWeight: fontWeightNormal,
                    textColor: !controller.darkMode ? kPrimary : null,
                  ),
                ],
              ),
              value: 1,
            ),
          ],
          onSelected: (value) async {
            if (value == 0) {
              Get.changeThemeMode(ThemeMode.dark);
              await controller.switchDarkMode(true);
              await LocalStorage.saveTheme(ThemeMode.dark);
            } else {
              Get.changeThemeMode(ThemeMode.light);

              await controller.switchDarkMode(false);
              await LocalStorage.saveTheme(ThemeMode.light);
            }
          },
        ),
      );
    });
  }
}
