import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/screens/about/about_screen.dart';
import 'package:project14plus/screens/badge/badge_screen.dart';
import 'package:project14plus/screens/contact/contact_screen.dart';
import 'package:project14plus/screens/history/history_screen.dart';
import 'package:project14plus/screens/interest/interest_screen.dart';
import 'package:project14plus/screens/other/components/list_tile.dart';
import 'package:project14plus/screens/other/components/theme_setting.dart';
import 'package:project14plus/screens/personal_info/personal_info_screen.dart';
import 'package:project14plus/screens/update_password/update_password_screen.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';
import '../../constants/constants.dart';
import '../../services/services.dart';
import '../../utils/easy_loading/loading_dialog.dart';
import '../screens.dart';

class OtherScreen extends StatelessWidget {
  const OtherScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserController>(builder: (_userController) {
      return SafeArea(
        top: false,
        child: SingleChildScrollView(
          padding: EdgeInsets.zero,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: Get.width,
                color: kPrimary,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 60.0),
                            ImageProfileCircle(
                              imageUrl: _userController.userModel?.avatar ?? "",
                              radius: isMobile ? 60.0 : 100.0,
                              fit: BoxFit.cover,
                            ),
                            const SizedBox(height: 20.0),
                            CustomText(
                              text: _userController.userModel?.username ??
                                  "ชื่อผู้ใช้งาน",
                              textColor: kWhite,
                              fontWeight: fontWeightNormal,
                              textSize: fontSizeL,
                            ),
                            const SizedBox(height: 10.0),
                            CustomText(
                              text: _userController.userModel?.email ??
                                  'อีเมลผู้ใช้งาน',
                              textColor: kWhite,
                              fontWeight: fontWeightNormal,
                              textSize: fontSizeL,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const ThemeSetting(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  children: [
                    if (_userController.userModel?.id != null) ...[
                      listTile(
                        icon: Icons.manage_accounts_rounded,
                        title: 'ข้อมูลทั่วไป',
                        onPressed: () => Get.to(
                          () => PersonalInfoScreen(
                            id: _userController.userModel?.id ?? "",
                          ),
                        ),
                      ),
                      listTile(
                        icon: Icons.lock_rounded,
                        title: 'แก้ไขรหัสผ่าน',
                        onPressed: () => Get.to(
                          () => UpdatePasswordScreen(
                            id: _userController.userModel?.id ?? "",
                          ),
                        ),
                      ),
                      listTile(
                        icon: Icons.interests_rounded,
                        title: 'ความสนใจ',
                        onPressed: () => Get.to(() => const InterestScreen()),
                      ),
                      listTile(
                        icon: Icons.history_rounded,
                        title: 'ประวัติการชม',
                        onPressed: () => Get.to(
                          () => HistoryScreen(
                              userId: _userController.userModel?.id ?? ""),
                        ),
                      ),
                      listTile(
                        icon: Icons.military_tech_rounded,
                        title: 'เหรียญที่ได้รับ',
                        onPressed: () {
                          Get.to(
                            () => BadgeScreen(
                                userId: _userController.userModel?.id ?? ""),
                          );
                        },
                      ),
                      listTile(
                        icon: Icons.quiz_rounded,
                        title: 'การสอบออนไลน์',
                        onPressed: () {},
                        // Get.to(() => TestAuth()),
                      ),
                    ],
                    listTile(
                      icon: Icons.info_rounded,
                      title: 'เกี่ยวกับเรา',
                      onPressed: () => Get.to(() => const AboutScreen()),
                    ),
                    if (_userController.userModel == null) ...[
                      listTile(
                        icon: Icons.contact_support_rounded,
                        title: 'ติดต่อเรา',
                        onPressed: () => Get.to(() => const ContactScreen()),
                      ),
                    ],
                    if (_userController.userModel != null) ...[
                      listTile(
                          icon: Icons.help_rounded,
                          title: 'ศูนย์ช่วยเหลือ',
                          onPressed: () => Get.to(() => const HelpScreen())),
                      listTile(
                          icon: Icons.logout,
                          title: 'ออกจากระบบ',
                          onPressed: () {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => CustomAlert(
                                content: Center(
                                  child: CustomText(
                                    text: 'ยืนยันการออกจากระบบ',
                                    textSize: fontSizeM,
                                  ),
                                ),
                                title: 'ออกจากระบบ',
                                onpressed: () async {
                                  LoadingDialog.showLoading();
                                  LogOutService.logOut();
                                  LoadingDialog.showSuccess(
                                      title: 'ออกจากระบบสำเร็จ');
                                  LoadingDialog.closeDialog();
                                  Get.find<FrontendController>().refreshValue();
                                  Get.back();
                                },
                              ),
                            );
                          }),
                    ],
                    if (_userController.userModel?.id == null) ...[
                      listTile(
                          icon: Icons.login_rounded,
                          title: 'เข้าสู่ระบบ',
                          onPressed: () async {
                            final pdpa = await LocalStorage.getPdpa();
                            if (pdpa == null) {
                              Get.to(() => PdpaScreen(
                                    isFromLogin: true,
                                  ));
                            } else {
                              Get.to(
                                () => LoginScreen(
                                  isFromContent: false,
                                ),
                              );
                            }
                          }),
                      listTile(
                          icon: Icons.person_add_alt_1_rounded,
                          title: 'สมัครสมาชิก',
                          onPressed: () async {
                            Get.to(
                              () => RegisterScreen(isFromContent: false),
                            );
                          }),
                    ],
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
