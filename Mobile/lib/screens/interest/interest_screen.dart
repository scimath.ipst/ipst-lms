import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/personal/personal.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/user/user.dart';
import 'package:project14plus/screens/interest/components/add_interests.dart';
import 'package:project14plus/screens/interest/components/add_subject_interests.dart';

class InterestScreen extends StatefulWidget {
  const InterestScreen({Key? key}) : super(key: key);

  @override
  State<InterestScreen> createState() => _InterestScreenState();
}

class _InterestScreenState extends State<InterestScreen> {
  final FrontendController _frontendController = Get.find<FrontendController>();
  Level levelInterest = Level();
  List<Level> tagInterests = [];
  List<Level> subjectInterests = [];

  @override
  void initState() {
    getData();
    super.initState();
  }

  getData() async {
    LocalStorage.getUser().then((value) {
      setState(() {
        subjectInterests = value?.subjects ?? [];
        tagInterests = value?.tags ?? [];
        levelInterest = value?.level ?? Level();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: 'ความสนใจ',
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            // SelectClass(),
            CustomDropdown(
              title: 'ระดับชั้น',
              value: levelInterest,
              onChange: _onChangeLevel,
              dropdownType: DropdownType.LEVEL,
            ),
            const SizedBox(height: 20.0),
            InkWell(
              onTap: _onAddSubjectInterest,
              child: Container(
                padding: const EdgeInsets.fromLTRB(12.0, 12.0, 10.0, 10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(outlineRadius),
                  border: Border.all(
                    width: 1.5,
                    color: _frontendController.darkMode
                        ? const Color(0xFFFFFFFF)
                        : const Color(0xFFD3D3D3),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      text: subjectInterests.length == 0
                          ? "วิชาที่สนใจ"
                          : "${subjectInterests.length} วิชาที่สนใจ",
                      textSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                    Icon(
                      Icons.arrow_drop_down_sharp,
                      size: 24.0,
                      color: _frontendController.darkMode
                          ? const Color(0xFFFFFFFF)
                          : const Color(0xFF999999),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            InkWell(
              onTap: _onAddTagInterest,
              child: Container(
                padding: const EdgeInsets.fromLTRB(12.0, 12.0, 10.0, 12.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(outlineRadius),
                  border: Border.all(
                    width: 1.5,
                    color: _frontendController.darkMode
                        ? const Color(0xFFFFFFFF)
                        : const Color(0xFFD3D3D3),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      text: tagInterests.length == 0
                          ? "แท็กที่สนใจ"
                          : "${tagInterests.length} แท็กที่สนใจ",
                      textSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                    Icon(
                      Icons.arrow_drop_down_sharp,
                      size: 24.0,
                      color: _frontendController.darkMode
                          ? const Color(0xFFFFFFFF)
                          : const Color(0xFF999999),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: kButtonPadding,
        child: ButtonMain(title: 'บันทึก', onPressed: onSubmit),
      ),
    );
  }

  void onSubmit() async {
    await ApiPersonal.updateInterest(
        level: levelInterest, subject: subjectInterests, tag: tagInterests);
  }

  void _onAddTagInterest() async {
    Get.to(
      () => AddInterests(
        selectedData: tagInterests,
        onChange: _onChangeTagInterest,
      ),
      duration: const Duration(milliseconds: 1000),
      transition: Transition.size,
      fullscreenDialog: true,
    );
  }

  _onChangeTagInterest(List<Level> data) {
    setState(() => tagInterests = data);
  }

  void _onAddSubjectInterest() async {
    final result = await Get.to(
      () => AddSubjectInterests(
        selectedData: subjectInterests,
        onChange: _onChangeSubjectInterest,
      ),
      duration: const Duration(milliseconds: 1000),
      transition: Transition.size,
      fullscreenDialog: true,
    );
    if (result != null && result['isRefresh'] == true) {
      LocalStorage.getUser().then((value) {
        setState(() {
          subjectInterests = value?.subjects ?? [];
        });
      });
    }
  }

  _onChangeSubjectInterest(List<Level> data) {
    setState(() {
      subjectInterests = data;
    });
  }

  _onChangeLevel(Level data) {
    levelInterest = data;
  }
}
