import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/personal/personal.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/widgets.dart';

class AddInterests extends StatefulWidget {
  AddInterests({Key? key, required this.selectedData, required this.onChange})
      : super(key: key);
  final List<Level> selectedData;
  final Function(List<Level>) onChange;

  @override
  State<AddInterests> createState() => _AddInterestsState();
}

class _AddInterestsState extends State<AddInterests> {
  List<Level> list = [];
  List<Level> selected = [];
  // List<Level> submitSelected = [];

  @override
  void initState() {
    // TODO: implement initState
    getList();
    super.initState();
  }

  getList() async {
    selected = widget.selectedData;
    // submitSelected = widget.selectedData;

    list = await ApiPersonal.getTagList();

    setState(() {
      list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: CustomText(
              text: 'แท็กที่สนใจ',
              textSize: fontSizeL,
              fontWeight: FontWeight.w600,
            ),
          ),
          body: Container(
            padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Wrap(
                        children: list.map((Level item) {
                          return ListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                (selected.where((element) =>
                                        element.name == item.name)).isEmpty
                                    ? const Icon(
                                        Icons.check_box_outline_blank,
                                        color: Colors.grey,
                                        size: 22,
                                      )
                                    : const Icon(
                                        Icons.check_box,
                                        size: 22,
                                        color: kPrimary,
                                      ),
                                const SizedBox(
                                  width: 12.0,
                                ),
                                CustomText(
                                  text: item.name.toString(),
                                  textSize: fontSizeM,
                                  fontWeight: fontWeightNormal,
                                ),
                              ],
                            ),
                            onTap: () {
                              List<Level> temp = selected;

                              if ((selected.where(
                                      (element) => element.name == item.name))
                                  .isNotEmpty) {
                                temp.removeWhere((d) => d.name == item.name);
                              } else {
                                temp.add(item);
                              }
                              setState(() {
                                selected = temp;
                              });
                            },
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: _onSubmit,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 50.0,
                decoration: BoxDecoration(
                    color: kPrimary, borderRadius: BorderRadius.circular(8.0)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: CustomText(
                      text: 'บันทึก',
                      textColor: kWhite,
                      textSize: fontSizeM,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmit() async {
    List<Level> temp = [];
    setState(() {
      temp = selected;
    });
    widget.onChange(temp);
    Get.back();
  }
}
