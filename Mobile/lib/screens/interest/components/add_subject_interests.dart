import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/personal/personal.dart';
import 'package:project14plus/constants/constants.dart';
// import 'package:project14plus/controllers/profile/profile.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/theme.dart';
import 'package:project14plus/widgets/widgets.dart';

class AddSubjectInterests extends StatefulWidget {
  AddSubjectInterests({
    Key? key,
    required this.selectedData,
    required this.onChange,
  }) : super(key: key);

  final List<Level> selectedData;
  final Function(List<Level>) onChange;

  @override
  State<AddSubjectInterests> createState() => _AddSubjectInterestsState();
}

class _AddSubjectInterestsState extends State<AddSubjectInterests> {
  List<Level> list = [];
  List<Level> selected = [];

  @override
  void initState() {
    // TODO: implement initState
    getList();
    super.initState();
  }

  getList() async {
    selected = widget.selectedData;
    // submitSelected = widget.selectedData;

    list = await ApiPersonal.getSubjectList();

    setState(() {
      list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: CustomText(
              text: 'วิชาที่สนใจ',
              textSize: fontSizeL,
              fontWeight: FontWeight.w600,
            ),
          ),
          body: Container(
            padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Wrap(
                        children: list.map((Level item) {
                          return ListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                (selected.where((element) =>
                                        element.name == item.name)).isEmpty
                                    ? const Icon(
                                        Icons.check_box_outline_blank,
                                        color: Colors.grey,
                                        size: 22,
                                      )
                                    : const Icon(
                                        Icons.check_box,
                                        size: 22,
                                        color: kPrimary,
                                      ),
                                const SizedBox(
                                  width: 12.0,
                                ),
                                CustomText(
                                  text: item.name.toString(),
                                  textSize: fontSizeM,
                                  fontWeight: fontWeightNormal,
                                ),
                              ],
                            ),
                            onTap: () {
                              List<Level> temp = selected;

                              if ((selected.where(
                                      (element) => element.name == item.name))
                                  .isNotEmpty) {
                                temp.removeWhere((d) => d.name == item.name);
                              } else {
                                temp.add(item);
                              }
                              setState(() {
                                selected = temp;
                              });
                            },
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: _onSubmit,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 50.0,
                decoration: BoxDecoration(
                    color: kPrimary, borderRadius: BorderRadius.circular(8.0)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: CustomText(
                      text: 'บันทึก',
                      textSize: fontSizeM,
                      fontWeight: fontWeightNormal,
                      textColor: kWhite,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmit() async {
    List<Level> temp = [];
    setState(() {
      temp = selected;
    });
    widget.onChange(temp);
    Get.back();
  }
}
