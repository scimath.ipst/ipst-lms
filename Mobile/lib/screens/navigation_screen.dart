import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/screens/channels/channels_screen.dart';
import 'package:project14plus/screens/level/level_screen.dart';
import '../controllers/controllers.dart';
import 'screens.dart';

class NavigationScreen extends StatelessWidget {
  NavigationScreen({Key? key}) : super(key: key);

  final NotificationController _notificationController =
      Get.find<NotificationController>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FrontendController>(
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            top: false,
            child: IndexedStack(
              index: controller.navigationIndex,
              children: [
                const HomeScreen(),
                const LevelScreen(),
                NotificationsScreen(),
                const ProgramScreen(),
                const OtherScreen(),
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            onTap: (index) {
              controller.changeNavigationIndex(index);
              if (index == 0) {
                controller.refreshValue();
              }
            },
            currentIndex: controller.navigationIndex,
            type: BottomNavigationBarType.fixed,
            elevation: 0,
            items: [
              _bottomNavigationBarItem(
                icon: CupertinoIcons.house_fill,
                label: 'หน้าแรก',
              ),
              _bottomNavigationBarItem(
                icon: CupertinoIcons.square_stack_3d_down_right_fill,
                label: 'ระดับชั้น',
              ),
              _bottomNavNotiItem(
                icon: Icons.notifications,
                label: 'แจ้งเตือน',
              ),
              _bottomNavigationBarItem(
                icon: CupertinoIcons.play_rectangle_fill,
                label: 'เลือกรายการ',
              ),
              _bottomNavigationBarItem(
                icon: Icons.more_horiz,
                label: 'อื่นๆ',
              ),
            ],
            selectedLabelStyle: const TextStyle(
              decoration: TextDecoration.none,
              color: kPrimary,
              fontSize: fontSizeS,
              fontWeight: fontWeightNormal,
              fontFamily: "Sarabun",
            ),
            unselectedLabelStyle: const TextStyle(
              decoration: TextDecoration.none,
              color: kPrimary,
              fontSize: fontSizeS,
              fontWeight: fontWeightNormal,
              fontFamily: "Sarabun",
            ),
          ),
        );
      },
    );
  }

  _bottomNavigationBarItem({IconData? icon, String? label}) {
    return BottomNavigationBarItem(
      icon: Icon(
        icon,
      ),
      label: label,
    );
  }

  _bottomNavNotiItem({IconData? icon, String? label}) {
    return BottomNavigationBarItem(
      icon: Stack(
        children: <Widget>[
          Icon(icon),
          GetBuilder<UserController>(builder: (_userController) {
            return _userController.userModel?.id != null
                ? StreamBuilder<QuerySnapshot>(
                    stream: _notificationController.streamNotificationsSign,
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData) {
                        final items = snapshot.data;

                        if (items?.docs.length == null ||
                            items?.docs.length == 0) {
                          return const SizedBox.shrink();
                        }

                        return Positioned(
                          right: 0,
                          top: 0,
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Colors.red,
                              shape: BoxShape.circle,
                            ),
                            constraints: const BoxConstraints(
                              minWidth: 10,
                              minHeight: 10,
                            ),
                          ),
                        );
                      } else if (snapshot.hasError) {
                        return const SizedBox.shrink();
                      }

                      return const SizedBox.shrink();
                    })
                : const SizedBox.shrink();
          }),
        ],
      ),
      label: label,
    );
  }
}
