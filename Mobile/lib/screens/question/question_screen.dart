import 'dart:developer';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/video_questions/components/progress_bar.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';
import 'package:project14plus/widgets/widgets.dart';

class QuestionScreen extends StatefulWidget {
  QuestionScreen({
    Key? key,
    required this.model,
  }) : super(key: key);
  VideoContent model;

  @override
  State<QuestionScreen> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  final FrontendController _frontendController = Get.find<FrontendController>();
  List<QuestionModel> model = [];
  VideoContent? videoContent;
  List<List<Choices>> correctChoice2 = [];
  List<List<Choices>> selected2 = [];

  bool isDone = false;

  int current = 1;
  late int questionLength = widget.model.result?.questions?.length ?? 0;

  @override
  void initState() {
    model = widget.model.result?.questions ?? [];
    videoContent = widget.model;

    for (var i = 0; i < model.length; i++) {
      List<Choices> value = [];
      List<Choices> value2 = [];

      for (var j = 0; j < model[i].choices!.length; j++) {
        if (model[i].choices?[j].isCorrect == true) {
          value.add(model[i].choices![j]);
        }
      }
      correctChoice2.add(value);
      selected2.add(value2);
    }
    super.initState();
  }

  setChioces() {
    // model = videoContent!.result!.videoQuestions![index];
    // model.choices?.forEach((x) {
    //   if (x.isCorrect == true) {
    //     correctChoice.add(x);
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: 'คำถามท้ายบทเรียน',
          textSize: fontSizeM,
          fontWeight: fontWeightBold,
        ),
        leading: IconButton(
            onPressed: () => Get.back(result: {"isDone": false}),
            icon:
                Icon(Platform.isIOS ? CupertinoIcons.back : Icons.arrow_back)),
      ),
      body: CustomScrollView(
        shrinkWrap: false,
        slivers: [
          SliverPadding(
            padding: kPadding,
            sliver: SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    text: 'คำถามข้อที่ $current จาก $questionLength ข้อ',
                    textSize: fontSizeS,
                    fontWeight: fontWeightBold,
                  ),
                  const SizedBox(height: 10),
                  ProgressBar(
                    max: questionLength,
                    current: current,
                  ),
                  const SizedBox(height: 10),
                  Stack(
                    children: [
                      Column(
                        children: [
                          const SizedBox(height: 35),
                          AspectRatio(
                            aspectRatio: 16 / 9,
                            child: CustomCoverImage(
                              image: videoContent?.result?.image,
                            ),
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                            color: kBlueColor,
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 5,
                              color: kWhite,
                              style: BorderStyle.solid,
                            ),
                          ),
                          child: Center(
                            child: CustomText(
                              text: current.toString(),
                              textSize: fontSizeXL * 1.5,
                              fontWeight: fontWeightBold,
                              textColor: kWhite,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  CustomText(
                    text: 'คำถามข้อที่ $current จาก $questionLength ข้อ',
                    textSize: fontSizeS,
                    fontWeight: fontWeightNormal,
                    textColor:
                        _frontendController.darkMode ? kWhite : kBlackDefault2,
                  ),
                  const SizedBox(height: 10),
                  CustomText(
                    text:
                        "${model[current - 1].question ?? ''} ${correctChoice2[current - 1].length > 1 ? '(เลือกได้สูงสุด ${correctChoice2[current - 1].length} ข้อ)' : ''}",
                    textSize: fontSizeM,
                    fontWeight: fontWeightBold,
                  ),
                  const SizedBox(height: 10.0),
                  Column(
                    children:
                        List.generate(model[current - 1].choices!.length, (j) {
                      Choices choices = model[current - 1].choices![j];
                      return Padding(
                        padding: const EdgeInsets.only(bottom: kGapS),
                        child: GestureDetector(
                          onTap: () {
                            if (!isDone) {
                              if (correctChoice2[current - 1].length > 1) {
                                if (!selected2[current - 1].contains(choices)) {
                                  if (selected2[current - 1].length <
                                      correctChoice2[current - 1].length) {
                                    setState(() =>
                                        selected2[current - 1].add(choices));
                                  }
                                } else {
                                  setState(() =>
                                      selected2[current - 1].remove(choices));
                                }
                              } else {
                                if (selected2[current - 1].isEmpty) {
                                  setState(() =>
                                      selected2[current - 1].add(choices));
                                } else {
                                  if (!selected2[current - 1]
                                      .contains(choices)) {
                                    selected2[current - 1] = [];
                                    setState(() =>
                                        selected2[current - 1].add(choices));
                                  } else {
                                    setState(() =>
                                        selected2[current - 1].remove(choices));
                                  }
                                }
                              }
                            }
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: selected2[current - 1].contains(choices)
                                  ? kBlueColor
                                  : kBlackDefault2.withOpacity(.2),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(outlineRadius),
                              ),
                            ),
                            padding: const EdgeInsets.fromLTRB(
                                kQuarterGap, kHalfGap, kQuarterGap, kHalfGap),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: Get.width * 0.1,
                                  child: Center(
                                    child: Icon(
                                      selected2[current - 1].contains(choices)
                                          ? Icons.check_circle_sharp
                                          : Icons.circle,
                                      size: 30,
                                      color: kWhite,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: Get.width * 0.8,
                                  child: CustomText(
                                    text: choices.text ?? '',
                                    textColor:
                                        selected2[current - 1].contains(choices)
                                            ? kWhite
                                            : kBlack,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                  ),
                  if (isDone) ...[
                    const SizedBox(height: 10),
                    CustomText(
                      text: "***ไม่สามารถแก้ไขแบบทดสอบได้",
                      textSize: fontSizeS,
                      fontWeight: fontWeightBold,
                      textColor: kRedColor,
                    ),
                  ],
                ],
              ),
            ),
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 10.0)),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: kButtonPadding,
        child: Row(
          children: [
            if (current != 1) ...[
              Flexible(
                flex: 1,
                child: ButtonMain(
                  title: "ย้อนกลับ",
                  color: kBlueColor,
                  onPressed: () => isDone ? Get.back() : onBack(),
                ),
              ),
            ],
            const SizedBox(width: 10),
            Flexible(
              flex: 1,
              child: ButtonMain(
                title: isDone
                    ? 'ย้อนกลับ'
                    : current == questionLength
                        ? 'ส่งแบบทดสอบ'
                        : 'ถัดไป',
                color: kBlueColor,
                onPressed: () => isDone
                    ? Get.back()
                    : current == questionLength
                        ? onSubmit()
                        : onNext(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onNext() async => setState(() => current++);

  void onBack() async => setState(() => current--);

  void onSubmit() async {
    bool checkIsEmpty = true;
    for (int i = 0; i < correctChoice2.length; i++) {
      log("message ${correctChoice2[i]}");

      // for (int j = 0; j < correctChoice2[i].length; i++) {
      //   log("aaa");
      // }
    }
    // if (!checks.contains(-1)) {
    //   int minScore = 0;
    //   int score = checks.reduce((a, b) => a + b);
    //   for (var i = 0; i < videoContent!.userAction!.questions!.length; i++) {
    //     minScore += videoContent!.userAction!.questions![i].score!;
    //   }

    //   //UserAction ID
    //   final param = {
    //     'id': videoContent!.userAction!.id,
    //     'scoreType': videoContent!.userAction!.scoreType,
    //     'questions': videoContent!.userAction!.questions,
    //     'score': score,
    //     'maxScore': videoContent!.userAction!.maxScore,
    //     'minScore': minScore,
    //   };
    //   final res = await ApiPersonal.updateQuestions(param);
    //   if (res == 1) {
    //     showDialog<String>(
    //       context: context,
    //       barrierDismissible: false,
    //       builder: (BuildContext context) => CustomAlert(
    //         title:
    //             "คุณได้คะแนน $score จาก ${videoContent!.userAction!.questions!.length} คะแนน",
    //         buttonKey: "ตกลง",
    //         isShowCancel: false,
    //         onpressed: () {
    //           int count = 0;
    //           Get.until((_) => count++ >= 2);
    //         },
    //         content: Center(
    //           child: CustomText(
    //             text:
    //                 'นักเรียนอาจจะยังไม่ค่อยเข้าใจนะ ลองทบทวนเนื้อหาดูอีกครั้ง แล้วทำให้ได้อย่างน้อย $minScore คะแนน',
    //             textSize: fontSizeM,
    //             fontWeight: FontWeight.w400,
    //           ),
    //         ),
    //       ),
    //     );
    //   } else if (res == 2) {
    //     showDialog<String>(
    //       context: context,
    //       barrierDismissible: false,
    //       builder: (BuildContext context) => CustomAlert(
    //         title:
    //             "คุณได้คะแนน $score จาก ${videoContent!.userAction!.questions!.length} คะแนน",
    //         buttonKey: "ตกลง",
    //         isShowCancel: false,
    //         onpressed: () {
    //           int count = 0;
    //           Get.until((_) => count++ >= 2);
    //         },
    //         content: Column(
    //           children: [
    //             Center(
    //               child: CustomText(
    //                 text: 'ยินดีด้วย คุณผ่านแบบทดสอบ',
    //                 textSize: fontSizeM,
    //                 fontWeight: FontWeight.w400,
    //               ),
    //             ),
    //             if (videoContent?.userAction?.content?.badgeName != null &&
    //                 videoContent?.userAction?.content?.badge != null)
    //               Center(
    //                 child: CustomText(
    //                   text:
    //                       'ได้รับ ${videoContent?.userAction?.content?.badgeName ?? ''}',
    //                   textSize: fontSizeM,
    //                   fontWeight: FontWeight.w400,
    //                 ),
    //               ),
    //             const SizedBox(
    //               height: 5.0,
    //             ),
    //             if (videoContent?.userAction?.content?.badgeName != null &&
    //                 videoContent?.userAction?.content?.badge != null)
    //               SizedBox(
    //                 child: CustomCoverImage(
    //                   image: videoContent?.userAction?.content?.badge
    //                           ?.toString() ??
    //                       "",
    //                   height: 50.0,
    //                   width: 50.0,
    //                   fit: BoxFit.contain,
    //                 ),
    //               )
    //           ],
    //         ),
    //       ),
    //     );
    //   }
    // } else {
    //   LoadingDialog.showToast(title: 'กรุณาเลืกคำตอบ');
    // }
  }
}


  // CustomScrollView(
  //             shrinkWrap: true,
  //             slivers: [
  //               SliverPadding(
  //                 padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
  //                 sliver: SliverToBoxAdapter(
  //                   child: CustomText(
  //                     text: 'บทเรียน ${widget.title}',
  //                     textSize: fontSizeM,
  //                     fontWeight: fontWeightBold,
  //                   ),
  //                 ),
  //               ),
  //               SliverToBoxAdapter(
  //                 child: Column(
  //                   children: List.generate(item.result?.questions?.length ?? 0,
  //                       (index) {
  //                     QuestionModel question = item.result!.questions![index];
  //                     init(item.result!.questions!.length,
  //                         item.result!.questions![index].choices!.length, item);
  //                     return Column(
  //                       crossAxisAlignment: CrossAxisAlignment.start,
  //                       children: [
  //                         CustomText(
  //                           text:
  //                               '${index + 1}. ${question.question.toString()}',
  //                           textSize: fontSizeM,
  //                           fontWeight: fontWeightBold,
  //                         ),
  //                         const SizedBox(
  //                           height: 10.0,
  //                         ),
  //                         Column(
  //                           children:
  //                               List.generate(question.choices!.length, (j) {
  //                             Choices choices = question.choices![j];
  //                             return Padding(
  //                               padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
  //                               child: Row(
  //                                 mainAxisAlignment: MainAxisAlignment.start,
  //                                 crossAxisAlignment: CrossAxisAlignment.start,
  //                                 children: [
  //                                   SizedBox(
  //                                     height: 30.0,
  //                                     width: 40.0,
  //                                     child: Checkbox(
  //                                       activeColor: kPrimary,
  //                                       shape: const RoundedRectangleBorder(
  //                                           borderRadius: BorderRadius.all(
  //                                               Radius.circular(4.0))),
  //                                       value: choices.selected,
  //                                       onChanged: (v) {
  //                                         for (var i = 0;
  //                                             i < question.choices!.length;
  //                                             i++) {
  //                                           question.choices![i].selected =
  //                                               false;
  //                                         }
  //                                         choices.selected = v;

  //                                         if (v == true) {
  //                                           if (choices.selected ==
  //                                               question
  //                                                   .choices![j].isCorrect) {
  //                                             checks[index] = question.score!;
  //                                           } else {
  //                                             checks[index] = 0;
  //                                           }

  //                                           videoContent!
  //                                               .userAction!
  //                                               .questions![index]
  //                                               .selectedChoice = j.toString();
  //                                         } else {
  //                                           checks[index] = -1;
  //                                           videoContent!
  //                                                   .userAction!
  //                                                   .questions![index]
  //                                                   .selectedChoice =
  //                                               (-1).toString();
  //                                         }
  //                                       },
  //                                     ),
  //                                   ),
  //                                   Expanded(
  //                                       child: Padding(
  //                                     padding:
  //                                         const EdgeInsets.fromLTRB(2, 2, 0, 0),
  //                                     child: Column(
  //                                       crossAxisAlignment:
  //                                           CrossAxisAlignment.start,
  //                                       mainAxisAlignment:
  //                                           MainAxisAlignment.start,
  //                                       children: [
  //                                         CustomText(
  //                                           text: choices.text.toString(),
  //                                           // textSize: SizeConstant.textM,
  //                                           fontWeight: FontWeight.w400,
  //                                         ),
  //                                       ],
  //                                     ),
  //                                   ))
  //                                 ],
  //                               ),
  //                             );
  //                           }),
  //                         ),
  //                         if (index !=
  //                             videoContent!.userAction!.questions!.length -
  //                                 1) ...[
  //                           const SizedBox(height: 10.0),
  //                           const Divider(color: kBlackDefault, thickness: .2),
  //                           const SizedBox(height: 10.0),
  //                         ],
  //                       ],
  //                     );
  //                   }),
  //                 ),
  //               ),
  //               const SliverToBoxAdapter(
  //                   child: SizedBox(
  //                 height: 10.0,
  //               )),
  //             ],
  //           ),
        