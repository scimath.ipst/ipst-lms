import 'dart:developer';
import 'package:project14plus/model/content/content_model.dart';
import 'package:project14plus/widgets/no_more_data.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';

/*
  Sorting
    1 = ใหม่ที่สุด
    2 = เก่าที่สุด
    3 = เป็นที่นิยม
 */

class LevelDetailScreen extends StatefulWidget {
  LevelDetailScreen({
    Key? key,
    required this.title,
    required this.url,
    this.isFromHome = false,
  }) : super(key: key);
  final String title;
  final String url;

  bool isFromHome;

  @override
  State<LevelDetailScreen> createState() => _LevelDetailScreenState();
}

class _LevelDetailScreenState extends State<LevelDetailScreen>
    with TickerProviderStateMixin {
  TextEditingController keyword = TextEditingController();

  String? sKeyword;

  Level subject = Level();
  Level sorting = Level();

  List<ContentModel> _data = [];

  late ScrollController scrollController;

  bool isGoToTop = false;
  int page = 0;
  bool isLoading = false;
  bool isEnded = false;

  @override
  void initState() {
    loadData();
    scrollController = ScrollController()..addListener(loadMore);

    super.initState();
  }

  @override
  void dispose() {
    scrollController.removeListener(loadMore);
    super.dispose();
  }

  Future<void> onRefresh() async {
    setState(() {
      page = 0;
      isLoading = false;
      isEnded = false;
      _data = [];
      isGoToTop = false;
    });
    loadData();
  }

  Future<void> loadData() async {
    if (!isEnded && !isLoading) {
      try {
        page += 1;
        setState(() {
          isLoading = true;
        });
        ApiContent.getLevelList(
          widget.url,
          page,
          subjectUrl: subject.url,
          sorting: sorting.id == "" || sorting.id == null
              ? 0
              : int.parse(sorting.id.toString()),
          keyword: sKeyword,
        ).then((value) {
          for (var i = 0; i < value.result!.length; i++) {
            _data.add(value.result![i]);
          }
          setState(() {
            _data;
            if (_data.length >= (value.paginate?.total ?? 0)) {
              isEnded = true;
              isLoading = false;
            } else if (value != null) {
              isLoading = false;
            }
          });
        });
      } catch (e) {
        log("$e");
      }
    }
  }

  void onLoadMore(info) async {
    if (info.visibleFraction > 0 && !isEnded && !isLoading) {
      await loadData();
    }
  }

  void _search() async {
    final result = await Get.to(
      () => LevelDetailSearch(
        keyword: keyword,
        sorting: sorting,
        subject: subject,
      ),
      duration: const Duration(milliseconds: 400),
      transition: Transition.size,
      fullscreenDialog: true,
    );
    if (result != null) {
      setState(() {
        keyword = result["keyword"];
        sKeyword = keyword.text;
        sorting = result["sorting"];
        subject = result["subject"];
        isLoading = true;
      });
      onRefresh();
    } else {
      if (sKeyword != null) {
        keyword.text = sKeyword!;
      } else {
        keyword.clear();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: widget.title,
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () => _search(),
          ),
        ],
      ),
      body: Stack(
        children: [
          RefreshIndicator(
            onRefresh: () async => onRefresh(),
            color: kPrimary,
            child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                controller: scrollController,
                child: isEnded && _data.isEmpty
                    ? Padding(
                        padding: kPadding,
                        child: NoData(onPressed: (() {
                          LoadingDialog.showLoading();
                          onRefresh();
                          LoadingDialog.closeDialog(withDelay: true);
                        })),
                      )
                    : Column(
                        children: [
                          _data.isEmpty
                              ? const SizedBox.shrink()
                              : GridView.builder(
                                  padding: const EdgeInsets.fromLTRB(
                                      kGapS, kGap, kGapS, 0),
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: _data.length,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: isMobile
                                        ? (orientation == Orientation.portrait)
                                            ? 2
                                            : 3
                                        : (orientation == Orientation.portrait)
                                            ? 4
                                            : 5,
                                    mainAxisSpacing: 0.0,
                                    crossAxisSpacing: 10,
                                    mainAxisExtent: 260,
                                  ),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    ContentModel item = _data[index];

                                    return GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () => slideToVideoContent(
                                          item.url!, item.id!, null),
                                      child: Stack(
                                        children: [
                                          SizedBox(
                                            width: double.infinity,
                                            height: 150.0,
                                            child: CustomCoverImage(
                                              image: item.image.toString(),
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 90.0,
                                            left: 10.0,
                                            child: ImageProfileCircle(
                                              imageUrl: item.user?.avatar
                                                      .toString() ??
                                                  "",
                                              radius: 20,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 20.0,
                                            left: 0.0,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  width: 200.0,
                                                  child: CustomText(
                                                    text: item.name.toString(),
                                                    textSize: fontSizeS,
                                                    fontWeight: FontWeight.w600,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    maxLine: 2,
                                                  ),
                                                ),
                                                const SizedBox(height: 5.0),
                                                SizedBox(
                                                  width: 200.0,
                                                  child: CustomText(
                                                    text:
                                                        '${item.user!.firstname.toString()} ${item.user!.lastname.toString()} • ผู้ชม ${item.visitCount.toString()} คน',
                                                    textSize: fontSizeS,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    maxLine: 1,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                          if (isEnded && _data.isNotEmpty) ...[
                            const Padding(
                              padding: kPadding,
                              child: NoMoreData(),
                            ),
                          ],
                          if (!isEnded) ...[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  kGapS, 0, kGapS, kGap),
                              child: VisibilityDetector(
                                key: const Key('loader-widget'),
                                onVisibilityChanged: onLoadMore,
                                child: const ProgramShimmer(
                                  length: 2,
                                ),
                              ),
                            ),
                          ],
                        ],
                      )),
          ),
          CustomCircleButton(
            ignoring: !isGoToTop && _data.isNotEmpty,
            isShow: isGoToTop && _data.isNotEmpty,
            onPressed: () {
              scrollController.animateTo(
                0.0,
                curve: Curves.easeOut,
                duration: const Duration(milliseconds: 400),
              );
            },
          ),
        ],
      ),
    );
  }

  void loadMore() async {
    if (scrollController.offset != scrollController.position.minScrollExtent) {
      setState(() => isGoToTop = true);
    } else {
      setState(() => isGoToTop = false);
    }
  }
}
