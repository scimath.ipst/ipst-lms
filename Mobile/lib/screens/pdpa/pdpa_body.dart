import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api_content/api_content.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/widgets/widgets.dart';

class PdpaBody extends StatefulWidget {
  PdpaBody({
    Key? key,
    this.isFromLogin = false,
    this.isFromContent = false,
    this.parameterModel,
    this.showFloatingButton = true,
  }) : super(key: key);
  bool isFromLogin;
  bool isFromContent;
  ParameterModel? parameterModel;
  bool showFloatingButton;

  @override
  State<PdpaBody> createState() => _PdpaBodyState();
}

class _PdpaBodyState extends State<PdpaBody> {
  bool isRead = false;

  ScrollController scrollController = ScrollController();

  String textButton = "ถัดไป";
  bool maxScroll = false;

//connect API PDPA
  late final Future<List<LevelModel>> _future = ApiContent.getLevels();

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  bool _onScroll(ScrollNotification scrollState) {
    final _position = scrollController.position;
    bool isEnd = (_position.pixels == _position.maxScrollExtent);
    if (isEnd & maxScroll == true) {
      setState(() => textButton = "ยอมรับ");
    } else {
      setState(() {
        if (_position.pixels == _position.maxScrollExtent &&
            maxScroll == false) {
          textButton = "ถัดไป";
          maxScroll = true;
        }
      });
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: NotificationListener<ScrollNotification>(
          onNotification: _onScroll,
          child: Scrollbar(
            controller: scrollController,
            isAlwaysShown: true,
            radius: const Radius.circular(10),
            child: CustomScrollView(
              controller: scrollController,
              physics: const ClampingScrollPhysics(),
              scrollDirection: Axis.vertical,
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: kPadding,
                    child: FutureBuilder(
                      future: _future,
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          return SizedBox(
                            child: CustomText(
                              text:
                                  '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget arcu augue. Praesent semper ex id neque rutrum, eu venenatis enim fringilla. Suspendisse aliquet sagittis iaculis. Duis nec mollis lectus. Sed iaculis nibh ut mi luctus, at feugiat nisi consectetur. Donec sit amet varius orci. Praesent gravida massa felis, at aliquam quam malesuada ac. Sed non nulla ipsum. Aenean sit amet nibh a nisi luctus scelerisque facilisis eu metus. Aenean et purus eros. Mauris facilisis ex leo, nec gravida odio ultricies non. Proin pharetra facilisis dui, ac congue dolor.

Fusce nec libero sed risus molestie congue a nec purus. In velit sapien, iaculis quis tempor vitae, lacinia ut dolor. Proin non nunc at risus auctor volutpat et quis nulla. Nam sagittis consequat urna in pharetra. Fusce ac mi ipsum. Duis tempus nibh ante, id malesuada enim sollicitudin at. Duis sit amet velit eros. Fusce suscipit nisi in dignissim egestas. Fusce sagittis nisl augue, quis imperdiet enim suscipit venenatis. Morbi quis viverra sem. Morbi non quam eros. Praesent vel luctus est, eget lobortis sem. Nunc odio ipsum, vulputate eget libero quis, viverra aliquam eros. Integer ullamcorper lectus sed fermentum varius. Nam efficitur sapien mauris, nec ornare leo tincidunt at.

Quisque fermentum nibh ac odio lacinia semper. Praesent id tellus ut eros scelerisque consequat. In a maximus metus. Fusce vitae nulla odio. Suspendisse tincidunt dolor id ligula tristique aliquet sed nec est. Donec non tincidunt ipsum, a tincidunt est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec a ligula eu metus porttitor commodo sit amet at ex. Nam vel nunc nec mauris posuere dapibus. Mauris dignissim nulla id augue luctus porta. Sed in justo ultrices, efficitur velit sit amet, blandit eros. In quis erat accumsan, aliquam urna et, pulvinar neque. Maecenas eleifend finibus lectus interdum imperdiet. In vulputate nulla vel lectus eleifend venenatis. Suspendisse egestas dignissim turpis, in pellentesque libero dapibus sed. Etiam quis risus quis sapien lobortis porttitor nec eu nisl.

Nunc viverra nulla eget malesuada rutrum. Nullam a tempor nunc. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris elementum ut sem ac ullamcorper. Mauris in sapien semper, molestie ipsum nec, mattis nibh. Donec accumsan eget urna elementum consequat. Aenean non ante pretium, porttitor massa et, semper turpis. Etiam placerat, ex a mollis ullamcorper, tortor metus imperdiet libero, et dapibus tellus magna et nunc. Praesent ut porttitor ante. Etiam luctus leo nec hendrerit imperdiet. Nulla vestibulum libero nec cursus eleifend. Donec in nibh eu dui convallis ullamcorper. Cras a sapien semper, facilisis enim non, feugiat turpis. Nunc imperdiet ex vel lorem convallis laoreet. Donec congue commodo mauris non egestas. Vivamus non enim tempus erat efficitur tempor.

Vivamus dignissim, odio id pellentesque luctus, dolor urna mollis sapien, ut rhoncus dui turpis quis justo. Duis eget nulla elit. Aenean id pretium arcu. Praesent at pharetra sem. Praesent ultricies faucibus malesuada. Vestibulum mollis ornare purus vel rhoncus. Nulla eget imperdiet ipsum, suscipit lacinia ex. Donec sagittis erat a mauris sollicitudin aliquam. Vivamus dignissim, odio id pellentesque luctus, dolor urna mollis sapien, ut rhoncus dui turpis quis justo. Duis eget nulla elit. Aenean id pretium arcu. Praesent at pharetra sem. Praesent ultricies faucibus malesuada. Vestibulum mollis ornare purus vel rhoncus. Nulla eget imperdiet ipsum, suscipit lacinia ex. Donec sagittis erat a mauris sollicitudin aliquam. Vivamus dignissim, odio id pellentesque luctus, dolor urna mollis sapien, ut rhoncus dui turpis quis justo. Duis eget nulla elit. Aenean id pretium arcu. Praesent at pharetra sem. Praesent ultricies faucibus malesuada. Vestibulum mollis ornare purus vel rhoncus. Nulla eget imperdiet ipsum, suscipit lacinia ex. Donec sagittis erat a mauris sollicitudin aliquam. ''',
                              textSize: fontSizeM,
                              fontWeight: fontWeightNormal,
                            ),
                          );
                        } else if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                            child: CircularProgressIndicator(color: kPrimary),
                          );
                        }
                        return const Center(
                          child: CircularProgressIndicator(color: kPrimary),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: widget.showFloatingButton
            ? Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: CustomButton(
                        onPressed: () => onDenied(),
                        text: CustomText(
                          text: 'ไม่ยอมรับ',
                          textColor: kPrimary,
                          textSize: fontSizeM,
                          fontWeight: FontWeight.w500,
                        ),
                        buttonColor: kWhite,
                        isTransparent: true,
                      ),
                    ),
                    const SizedBox(width: 10.0),
                    Expanded(
                      child: CustomButton(
                        onPressed: () => _next(),
                        text: CustomText(
                          text: textButton,
                          textColor: kWhite,
                          textSize: fontSizeM,
                          fontWeight: FontWeight.w500,
                        ),
                        buttonColor: kPrimary,
                      ),
                    ),
                  ],
                ),
              )
            : const SizedBox.shrink(),
      ),
    );
  }

  void onDenied() {
    LocalStorage.savePdpa(
      PdpaModel(
        result: false,
        date: DateTime.now(),
      ),
    );
    //Replace
    if (widget.isFromLogin) {
      if (widget.isFromContent) {
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                LoginScreen(
              isFromContent: true,
              parameterModel: widget.parameterModel!,
            ),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              const begin = Offset(0.0, 1.0);
              const end = Offset.zero;
              const curve = Curves.ease;

              var tween =
                  Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

              return SlideTransition(
                position: animation.drive(tween),
                child: child,
              );
            },
          ),
        );
      } else {
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                LoginScreen(
              isFromContent: false,
            ),
          ),
        );
      }
    } else {
      Get.back();
    }
  }

  void _next() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (scrollController.hasClients) {
        final _position = scrollController.position;
        if (maxScroll) {
          LocalStorage.savePdpa(
            PdpaModel(
              result: true,
              date: DateTime.now(),
            ),
          );
          //Replace
          if (widget.isFromLogin) {
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    LoginScreen(
                  isFromContent: false,
                ),
              ),
            );
          } else {
            Get.back();
          }
        } else {
          if (maxScroll == false) {
            scrollController.animateTo(
              _position.pixels + 200,
              curve: Curves.easeOut,
              duration: const Duration(milliseconds: 300),
            );
          }
        }
      }
    });
  }
}
