import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/widgets/widgets.dart';

class PdpaScreen extends StatelessWidget {
  PdpaScreen({
    Key? key,
    this.isFromLogin = false,
    this.isFromContent = false,
    this.showBackButton = false,
    this.showFloatingButton = true,
    this.parameterModel,
  }) : super(key: key);
  bool isFromLogin;
  bool isFromContent;
  bool showBackButton;
  bool showFloatingButton;
  ParameterModel? parameterModel;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        appBar: AppBar(
          title: CustomText(
            text: 'เงื่อนไขและข้อกำหนด',
            textSize: fontSizeXL,
            fontWeight: FontWeight.w600,
          ),
          automaticallyImplyLeading: showBackButton,
        ),
        body: PdpaBody(
          isFromLogin: isFromLogin,
          isFromContent: isFromContent,
          parameterModel: parameterModel,
          showFloatingButton: showFloatingButton,
        ),
      ),
    );
  }
}
