import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';

class NotificationsNews extends StatelessWidget {
  const NotificationsNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomCoverImage(),
        const SizedBox(
          height: 10,
        ),
        CustomText(
          text:
              "ส่วนต่าง ๆ ของร่างกายเรามีอะไรบ้าง ตอนที่ 1 (วิทย์ ป.1 เล่ม 1 หน่วย 2 บท 1)",
          textSize: fontSizeM,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            const ImageProfileCircle(
              imageUrl: '',
              radius: 20.0,
            ),
            const SizedBox(
              width: 10.0,
            ),
            CustomText(
              text: 'Firstname Lastname',
              textSize: fontSizeM,
              fontWeight: FontWeight.w500,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        CustomText(
          text:
              "  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
          textSize: fontSizeM,
          fontWeight: fontWeightNormal,
        ),
      ],
    );
  }
}
