import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/notification_controller.dart';
import 'package:project14plus/utils/easy_loading/loading_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:project14plus/constants/words.dart';
import 'package:project14plus/controllers/controllers.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/widgets.dart';

class NotificationsScreen extends StatelessWidget {
  NotificationsScreen({Key? key}) : super(key: key);

  final NotificationController _notificationController =
      Get.find<NotificationController>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserController>(builder: (_userController) {
      return _userController.userModel?.id != null
          ? StreamBuilder<QuerySnapshot>(
              stream: _notificationController.streamNotifications,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  final items = snapshot.data;

                  if (items?.docs.length == null || items?.docs.length == 0) {
                    return Scaffold(
                      appBar: AppBar(
                        title: CustomText(
                          text: 'การแจ้งเตือน',
                          textSize: fontSizeXL,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      body: const NoData(),
                    );
                  }

                  return Scaffold(
                    appBar: AppBar(
                      title: CustomText(
                        text: 'การแจ้งเตือน',
                        textSize: fontSizeXL,
                        fontWeight: FontWeight.w600,
                      ),
                      actions: [
                        if (items?.docs.length != null ||
                            items?.docs.length != 0) ...[
                          IconButton(
                            onPressed: () async {
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => CustomAlert(
                                  content: Center(
                                    child: CustomText(
                                      text: 'คุณต้องการลบการแจ้งเตือนทั้งหมด ?',
                                      textSize: fontSizeM,
                                    ),
                                  ),
                                  title: 'ลบการแจ้งเตือน',
                                  onpressed: () async {
                                    Get.back();

                                    LoadingDialog.showLoading();
                                    _notificationController
                                        .deleteAllNotifications();
                                    LoadingDialog.closeDialog();
                                  },
                                ),
                              );
                            },
                            icon: const Icon(
                              CupertinoIcons.trash_fill,
                            ),
                          ),
                        ],
                      ],
                    ),
                    body: ListView.builder(
                      padding: kPadding,
                      // physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: false,
                      itemCount: items?.docs.length,
                      itemBuilder: (BuildContext ctx, index) {
                        NotificationModel item = NotificationModel(
                          id: items?.docs[index]['id'],
                          title: items?.docs[index]["title"],
                          body: items?.docs[index]["body"],
                          isRead: items?.docs[index]["isRead"],
                          sentTime: items?.docs[index]["sentTime"],
                          url: items?.docs[index]["url"],
                        );

                        return Dismissible(
                          key: UniqueKey(),
                          direction: DismissDirection.endToStart,
                          onDismissed: (_) =>
                              _notificationController.deleteNotification(
                                  items?.docs[index].reference.id ?? ''),
                          child: Card(
                            elevation: 0,
                            color: item.isRead == null || item.isRead == false
                                ? kBlackDefault.withOpacity(0.1)
                                : null,
                            margin: const EdgeInsets.only(bottom: kHalfGap),
                            child: ListTile(
                              contentPadding: const EdgeInsets.all(kHalfGap),
                              isThreeLine: true,
                              onTap: () {
                                _notificationController.readNotification(
                                    items?.docs[index].reference.id);
                                slideToVideoContent(
                                    item.url ?? '', item.id ?? '', 0);
                              },
                              leading: const CircleAvatar(
                                backgroundColor: kPrimary,
                                child: Icon(
                                  Icons.video_collection_rounded,
                                  color: kWhite,
                                ),
                              ),
                              title: Text(
                                item.title ?? '',
                                style: const TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                ),
                                maxLines: 1,
                              ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${item.body}",
                                    style: const TextStyle(
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    maxLines: 2,
                                  ),
                                  Text(item.displayDateTime()),
                                ],
                              ),
                              trailing:
                                  const Icon(Icons.arrow_back_ios_new_rounded),
                            ),
                          ),
                          background: Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            decoration: const BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.all(
                                Radius.circular(outlineRadius),
                              ),
                            ),
                            alignment: Alignment.centerRight,
                            child: const Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                } else if (snapshot.hasError) {
                  Scaffold(
                    appBar: AppBar(
                      title: CustomText(
                        text: 'การแจ้งเตือน',
                        textSize: fontSizeXL,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    body: const Center(
                      child: Text("เกิดข้อผิดพลาด"),
                    ),
                  );
                }

                return Scaffold(
                  appBar: AppBar(
                    title: CustomText(
                      text: 'การแจ้งเตือน',
                      textSize: fontSizeXL,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  body: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              })
          : SizedBox(
              height: Get.height,
              width: Get.width,
              child: Scaffold(
                appBar: AppBar(
                  title: CustomText(
                    text: 'การแจ้งเตือน',
                    textSize: fontSizeXL,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                body: const NoData(title: PLS_LOGIN),
              ),
            );
    });
  }
}
