import 'package:flutter/material.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/utils/app_utils.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';

import '../../widgets/widgets.dart';

class UpdatePasswordScreen extends StatefulWidget {
  const UpdatePasswordScreen({Key? key, required this.id}) : super(key: key);
  final String id;

  @override
  State<UpdatePasswordScreen> createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController cPassword = TextEditingController();
  TextEditingController cNewPassword = TextEditingController();
  TextEditingController cConfirmNewPassword = TextEditingController();

  FocusNode fPassword = FocusNode();
  FocusNode fNewPassword = FocusNode();
  FocusNode fConfirmNewPassword = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: "แก้ไขรหัสผ่าน",
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(height: 10.0),
                CustomTextFromField(
                  label: 'รหัสผ่าน',
                  controller: cPassword,
                  focusNode: fPassword,
                  onChanged: (String? value) {},
                  isPassword: true,
                  isShowPassEye: true,
                  isRequired: true,
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_) => fNewPassword.requestFocus(),
                  validate: (value) => AppUtils.validateString(value),
                ),
                const SizedBox(height: 20.0),
                CustomTextFromField(
                  label: 'รหัสผ่านใหม่',
                  controller: cNewPassword,
                  focusNode: fNewPassword,
                  isPassword: true,
                  isShowPassEye: true,
                  isRequired: true,
                  onChanged: (String? value) {},
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_) => fConfirmNewPassword.requestFocus(),
                  validate: (value) => AppUtils.validatePassword(
                      cNewPassword.text.toString(),
                      cConfirmNewPassword.text.toString(),
                      1),
                ),
                const SizedBox(height: 20.0),
                CustomTextFromField(
                  label: 'ยืนยันรหัสผ่านใหม่',
                  focusNode: fConfirmNewPassword,
                  controller: cConfirmNewPassword,
                  isPassword: true,
                  isShowPassEye: true,
                  isRequired: true,
                  onChanged: (String? value) {},
                  textInputAction: TextInputAction.done,
                  validate: (value) => AppUtils.validatePassword(
                      cNewPassword.text.toString(),
                      cConfirmNewPassword.text.toString(),
                      2),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: kButtonPadding,
        child: ButtonMain(title: 'บันทึก', onPressed: onSubmit),
      ),
    );
  }

  void onSubmit() async {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState!.validate()) {
      await ApiPersonal.updatePassword(widget.id, cPassword.text.toString(),
          cNewPassword.text.toString(), cConfirmNewPassword.text.toString());
    }
  }
}
