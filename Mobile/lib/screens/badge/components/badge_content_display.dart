import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import '../../../constants/constants.dart';
import '../../../model/model.dart';
import '../../../widgets/widgets.dart';

class BadgeContentDisplay extends StatelessWidget {
  const BadgeContentDisplay({Key? key, required this.item}) : super(key: key);
  final BadgeModel item;

  @override
  Widget build(BuildContext context) {
    String scoreType = item.displayScoreType();
    final date = item.displayDate();
    return Container(
      width: Get.width,
      height: Get.height,
      padding: const EdgeInsets.fromLTRB(10, 16, 10, 16),
      child: Center(
        child: CustomScrollView(
          shrinkWrap: true,
          slivers: [
            SliverToBoxAdapter(
              child: Stack(
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                      child: Column(
                        children: [
                          ListTile(
                            contentPadding: EdgeInsets.zero,
                            leading: const ImageProfileCircle(),
                            title: CustomText(
                              text: item.content?.badgeName ?? "ไม่มีข้อมูล",
                              textSize: fontSizeM,
                              fontWeight: fontWeightNormal,
                              maxLine: 1,
                              textAlign: TextAlign.start,
                            ),
                          ),
                          const SizedBox(height: 10.0),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(outlineRadius),
                            child: CustomCoverImage(
                              fit: BoxFit.contain,
                              image: item.content?.badge,
                              // height: 130.0,
                            ),
                          ),
                          const SizedBox(height: 10.0),
                          textRow("คะแนนที่ได้ : ", item.score.toString(),
                              " คะแนน"),
                          const SizedBox(
                            height: 10.0,
                          ),
                          textRow("จำนวนข้อสอบ : ",
                              item.questions!.length.toString(), " ข้อ"),
                          const SizedBox(
                            height: 10.0,
                          ),
                          textRow("ประเภทข้อสอบ : ", scoreType, null),
                          const SizedBox(
                            height: 10.0,
                          ),
                          textRow("วันที่ได้รับ : ", date, null),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: const Alignment(1, -1),
                    child: IconButton(
                      onPressed: () => Get.back(),
                      icon: const Icon(Icons.close_rounded),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget textRow(String prefix, String value, String? suffix) {
    return Row(
      children: [
        CustomText(
          text: prefix,
          textSize: fontSizeM,
          fontWeight: fontWeightBold,
          maxLine: 1,
          textAlign: TextAlign.start,
        ),
        CustomText(
          text: value,
          textSize: fontSizeM,
          fontWeight: fontWeightNormal,
          maxLine: 1,
          textAlign: TextAlign.start,
        ),
        if (suffix != null)
          CustomText(
            text: suffix,
            textSize: fontSizeM,
            fontWeight: fontWeightNormal,
            maxLine: 1,
            textAlign: TextAlign.start,
          ),
      ],
    );
  }
}
