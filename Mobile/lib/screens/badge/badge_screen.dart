import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/screens/badge/components/badge_content_display.dart';
import 'package:project14plus/utils/utils.dart';

/* 
  scoreType :
    0 = ไม่เก็บคะแนน
    1 = เก็บคะแนนสูงสุด
    2 = เก็บคะแนนครั้งล่าสุด
*/

class BadgeScreen extends StatefulWidget {
  const BadgeScreen({
    Key? key,
    required this.userId,
  }) : super(key: key);
  final String userId;

  @override
  State<BadgeScreen> createState() => _BadgeScreenState();
}

class _BadgeScreenState extends State<BadgeScreen> {
  late Future<List<BadgeModel>>? future;

  @override
  void initState() {
    super.initState();
    getValueBadge();
  }

  Future<void> getValueBadge() async =>
      future = ApiPersonal.getBadgeList(widget.userId);

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: "เหรียญที่ได้รับ",
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: RefreshIndicator(
        color: kPrimary,
        onRefresh: getValueBadge,
        child: FutureBuilder<List<BadgeModel>>(
          future: future,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              List<BadgeModel>? items = snapshot.data;
              if (items == null) {
                return NoData(
                  title: "ไม่พบเหรียญที่ได้รับ",
                  onPressed: () {
                    LoadingDialog.showLoading();
                    getValueBadge();
                    LoadingDialog.closeDialog(withDelay: true);
                  },
                );
              }
              return Padding(
                padding: kPadding,
                child: GridView.builder(
                  padding: EdgeInsets.zero,
                  itemCount: items.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: isMobile
                          ? (orientation == Orientation.portrait)
                              ? 3
                              : 5
                          : (orientation == Orientation.portrait)
                              ? 5
                              : 7,
                      mainAxisSpacing: 0.0,
                      crossAxisSpacing: 10,
                      mainAxisExtent: 190.0
                      // childAspectRatio: 2,
                      ),
                  itemBuilder: (BuildContext context, int index) {
                    BadgeModel item = items[index];
                    final date = item.createdAt != null
                        ? DateFormatter.formatterddMMYYYY(item.updatedAt!,
                            withHour: false)
                        : "-";

                    return InkWell(
                      onTap: () => showCupertinoModalPopup(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return BadgeContentDisplay(item: item);
                          }),
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      focusColor: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomCoverImage(
                            image: item.content?.badge.toString() ?? "",
                            height: 100.0,
                            fit: BoxFit.contain,
                          ),
                          const SizedBox(height: 5.0),
                          CustomText(
                            text: item.content?.badgeName ?? "ไม่มีข้อมูล",
                            textSize: fontSizeM,
                            fontWeight: fontWeightNormal,
                            maxLine: 1,
                            textAlign: TextAlign.center,
                          ),
                          CustomText(
                            text: date,
                            textSize: fontSizeM,
                            fontWeight: FontWeight.w600,
                            maxLine: 1,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    );
                  },
                ),
              );
            }
            return const Padding(
              padding: kPadding,
              child: ProgramShimmer(
                length: 20,
                crossAxisCount: 3,
                imageHeight: 100.0,
                gridHeight: 175.0,
              ),
            );
          },
        ),
      ),
    );
  }
}
