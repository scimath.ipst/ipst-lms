// import 'dart:developer';

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:project14plus/api/auth/api_auth.dart';
// import 'package:project14plus/constants/constants.dart';
// import 'package:project14plus/controllers/frontend_controller.dart';
// import 'package:project14plus/model/model.dart';
// import 'package:project14plus/screens/screens.dart';
// import 'package:project14plus/utils/utils.dart';
// import 'package:project14plus/widgets/widgets.dart';

// /*
//   pageIndex
//     0 = Display login
//     1 = Display register
//     2 = Display reset password
//     3 = Display successful register
//     4 = Display ขอตั้งรหัสสำเร็จ
//     5 = Display OTP
// */

// class LoginBody extends StatefulWidget {
//   const LoginBody({
//     Key? key,
//     this.parameterModel,
//   }) : super(key: key);
//   final ParameterModel? parameterModel;

//   @override
//   State<LoginBody> createState() => _LoginBodyState();
// }

// class _LoginBodyState extends State<LoginBody> {
//   final _formKey = GlobalKey<FormState>();

//   TextEditingController cEmail = TextEditingController();
//   TextEditingController cPassword = TextEditingController();

//   FocusNode fEmail = FocusNode();
//   FocusNode fPassword = FocusNode();

//   @override
//   Widget build(BuildContext context) {
//     return GetBuilder<FrontendController>(builder: (_frontendController) {
//       return _frontendController.pageIndex == 0
//           ? SafeArea(
//               child: Center(
//                   child: Wrap(
//                 children: [
//                   Card(
//                     margin: EdgeInsets.symmetric(
//                         horizontal: isMobile ? 20.0 : 230.0),
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(radius),
//                     ),
//                     child: Padding(
//                       padding: const EdgeInsets.symmetric(
//                           horizontal: 20.0, vertical: 30.0),
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           CustomText(
//                             text: 'เข้าสู่ระบบ',
//                             textSize: fontSizeXL,
//                             fontWeight: FontWeight.w600,
//                           ),
//                           const SizedBox(height: 30.0),
//                           Form(
//                             key: _formKey,
//                             child: Column(
//                               children: [
//                                 CustomTextFromField(
//                                   controller: cEmail,
//                                   focusNode: fEmail,
//                                   label: 'อีเมล / ชื่อผู้ใช้ / เบอร์มือถือ',
//                                   isRequired: true,
//                                   onChanged: (String? value) {},
//                                   textInputAction: TextInputAction.next,
//                                   onFieldSubmitted: (_) =>
//                                       fPassword.requestFocus(),
//                                   validate: (value) =>
//                                       AppUtils.validateString(value),
//                                 ),
//                                 const SizedBox(height: 20.0),
//                                 CustomTextFromField(
//                                   controller: cPassword,
//                                   focusNode: fPassword,
//                                   label: 'รหัสผ่าน',
//                                   isPassword: true,
//                                   isRequired: true,
//                                   isShowPassEye: true,
//                                   textInputAction: TextInputAction.done,
//                                   onChanged: (String? value) {},
//                                   validate: (value) =>
//                                       AppUtils.validateString(value),
//                                 ),
//                               ],
//                             ),
//                           ),
//                           const SizedBox(height: 20.0),
//                           CustomButton(
//                             text: CustomText(
//                               text: 'เข้าสู่ระบบ',
//                               textSize: fontSizeM,
//                               fontWeight: FontWeight.w600,
//                             ),
//                             buttonColor: cPrimary,
//                             onPressed: () async {
//                               FocusScope.of(context).unfocus();
//                               if (_formKey.currentState!.validate()) {
//                                 await ApiAuth.signIn(cEmail.text.toString(),
//                                         cPassword.text.toString(),
//                                         parameterModel: widget.parameterModel)
//                                     .then((value) {
//                                   // Get.back();

//                                   // if (value == true) {
//                                   //   if (widget.parameterModel?.isFromPlaylist !=
//                                   //           null &&
//                                   //       widget.parameterModel!.isFromPlaylist) {
//                                   //     Get.to(
//                                   //       () => VideoContentPlaylistScreen(
//                                   //         playlistUrl: widget.parameterModel
//                                   //                 ?.playlistUrl ??
//                                   //             '',
//                                   //         playlistName: widget.parameterModel
//                                   //                 ?.playlistName ??
//                                   //             '',
//                                   //       ),
//                                   //       duration:
//                                   //           const Duration(milliseconds: 400),
//                                   //       transition: Transition.downToUp,
//                                   //       fullscreenDialog: true,
//                                   //     );
//                                   //   } else if (widget.parameterModel
//                                   //               ?.isFromPlaylist !=
//                                   //           null &&
//                                   //       !widget
//                                   //           .parameterModel!.isFromPlaylist) {
//                                   //     Get.to(
//                                   //       () => VideoContentScreen(
//                                   //         contentUrl: widget
//                                   //             .parameterModel!.contentUrl
//                                   //             .toString(),
//                                   //         contentId: widget
//                                   //                 .parameterModel?.contentId ??
//                                   //             '',
//                                   //         watchingSeconds: widget.parameterModel
//                                   //                 ?.watchingSeconds ??
//                                   //             0,
//                                   //       ),
//                                   //       duration:
//                                   //           const Duration(milliseconds: 400),
//                                   //       transition: Transition.downToUp,
//                                   //       fullscreenDialog: true,
//                                   //     );
//                                   //   }
//                                   // }
//                                 });
//                               }
//                             },
//                           ),
//                           const SizedBox(height: 20.0),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               CustomTextButton(
//                                 onPressed: () =>
//                                     _frontendController.initAuthScreen(1),
//                                 text: CustomText(
//                                   text: 'สมัครสมาชิก เพื่อเริ่มใช้งาน',
//                                   textSize: fontSizeM,
//                                   fontWeight: FontWeight.w600,
//                                 ),
//                               ),
//                               CustomTextButton(
//                                 onPressed: () =>
//                                     _frontendController.initAuthScreen(2),
//                                 text: CustomText(
//                                   text: 'ลืมรหัสผ่าน',
//                                   textSize: fontSizeM,
//                                   fontWeight: FontWeight.w600,
//                                 ),
//                               ),
//                             ],
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                 ],
//               )),
//             )
//           : _frontendController.pageIndex == 1
//               ? RegisterScreen(
//                   parameterModel: widget.parameterModel,
//                 )
//               : _frontendController.pageIndex == 2
//                   ? ResetPasswordScreen()
//                   : _frontendController.pageIndex == 3
//                       ? SafeArea(
//                           child: Center(
//                             child: Wrap(
//                               children: [
//                                 Card(
//                                   margin: const EdgeInsets.symmetric(
//                                       horizontal: 20.0, vertical: 30.0),
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(radius),
//                                   ),
//                                   child: Padding(
//                                     padding: const EdgeInsets.all(20.0),
//                                     child: Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         CustomText(
//                                           text: 'สมัครสมาชิกเสร็จแล้ว',
//                                           textSize: fontSizeXL,
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         const SizedBox(
//                                           height: 30.0,
//                                         ),
//                                         CustomText(
//                                           text:
//                                               'คุณได้ทำการสมัครสมาชิกเรียบร้อยแล้ว กรุณาตรวจสอบอีเมลของคุณเพื่อเปิดใช้งานบัญชี',
//                                           textSize: fontSizeL,
//                                           // maxLine: 4,
//                                           textAlign: TextAlign.center,
//                                         ),
//                                         const SizedBox(
//                                           height: 20.0,
//                                         ),
//                                         CustomButton(
//                                             onPressed: () => _frontendController
//                                                 .initAuthScreen(0),
//                                             text: CustomText(
//                                               text: 'เข้าสู่ระบบ',
//                                               textSize: fontSizeM,
//                                               fontWeight: FontWeight.w600,
//                                             ),
//                                             buttonColor: cPrimary)
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         )
//                       : _frontendController.pageIndex == 4
                          // ? SafeArea(
                          //     child: Center(
                          //         child: Wrap(
                          //       children: [
                          //         Card(
                          //           margin: const EdgeInsets.symmetric(
                          //               horizontal: 20.0, vertical: 30.0),
                          //           shape: RoundedRectangleBorder(
                          //             borderRadius:
                          //                 BorderRadius.circular(radius),
                          //           ),
                          //           child: Padding(
                          //             padding: const EdgeInsets.all(20.0),
                                      // child: Column(
                                      //   mainAxisAlignment:
                                      //       MainAxisAlignment.spaceBetween,
                                      //   children: [
                                      //     CustomText(
                                      //       text: 'ขอตั้งรหัสผ่านใหม่สำเร็จ',
                                      //       textSize: fontSizeXL,
                                      //       fontWeight: FontWeight.w600,
                                      //     ),
                                      //     const SizedBox(
                                      //       height: 30.0,
                                      //     ),
                                      //     CustomText(
                                      //       text:
                                      //           'คุณได้ทำการขอตั้งรหัสผ่านใหม่เรียบร้อยแล้ว กรุณาตรวจสอบอีเมลของคุณเพื่อทำตามขั้นตอนต่อไป',
                                      //       textSize: fontSizeL,
                                      //       textAlign: TextAlign.center,
                                      //     ),
                                      //     const SizedBox(
                                      //       height: 20.0,
                                      //     ),
                                      //     CustomButton(
                                      //       onPressed: () => _frontendController
                                      //           .initAuthScreen(0),
                                      //       text: CustomText(
                                      //         text: 'เข้าสู่ระบบ',
                                      //         fontWeight: FontWeight.w600,
                                      //         textSize: fontSizeM,
                                      //       ),
                                      //       buttonColor: cPrimary,
                                      //     ),
                                      //   ],
                                      // ),
                          //           ),
                          //         ),
                          //       ],
                          //     )),
                          //   )
//                           : OTPConfirmation();
//     });
//   }
// }
