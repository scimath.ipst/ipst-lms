import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/auth/api_auth.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/utils/app_utils.dart';
import 'package:project14plus/widgets/widgets.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({
    Key? key,
    this.isFromContent = true,
    this.parameterModel,
  }) : super(key: key);
  final bool isFromContent;
  final ParameterModel? parameterModel;

  final _formKey = GlobalKey<FormState>();

  TextEditingController cEmail = TextEditingController();
  TextEditingController cPassword = TextEditingController();

  FocusNode fEmail = FocusNode();
  FocusNode fPassword = FocusNode();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: kPrimary,
          image: DecorationImage(
            image: AssetImage(PATH_AUTH_BG01),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        child: Stack(
          children: [
            Padding(
              padding: kPadding,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    child: AspectRatio(
                      aspectRatio: 2.8,
                      child: Image.asset(
                        "assets/images/logo-01.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  Form(
                    key: _formKey,
                    child: SizedBox(
                      width: 370,
                      child: Column(
                        children: [
                          CustomTextFromField(
                            controller: cEmail,
                            focusNode: fEmail,
                            bgColor: kWhite,
                            label: 'อีเมล / ชื่อผู้ใช้ / เบอร์มือถือ',
                            isRequired: true,
                            displaySuffixIcon: true,
                            suffixIcon: Icons.email,
                            onChanged: (String? value) {},
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (_) => fPassword.requestFocus(),
                            validate: (value) => AppUtils.validateString(value),
                          ),
                          const SizedBox(height: 20.0),
                          CustomTextFromField(
                            controller: cPassword,
                            focusNode: fPassword,
                            label: 'รหัสผ่าน',
                            isPassword: true,
                            isRequired: true,
                            isShowPassEye: true,
                            displaySuffixIcon: true,
                            textInputAction: TextInputAction.done,
                            validate: (value) => AppUtils.validateString(value),
                          ),
                          const SizedBox(height: 20.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              // CustomText(
                              //   text: 'เข้าสู่ระบบ',
                              //   textSize: fontSizeM,
                              //   fontWeight: FontWeight.w500,
                              // ),
                              GestureDetector(
                                onTap: () => Get.to(() => ResetPasswordScreen(
                                      isFromContent: false,
                                    )),
                                child: CustomText(
                                  text: 'ลืมรหัสผ่าน?',
                                  textSize: fontSizeM,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  SizedBox(
                    width: 370,
                    child: CustomButton(
                      text: CustomText(
                        text: 'เข้าสู่ระบบ',
                        textSize: fontSizeM,
                        fontWeight: FontWeight.w600,
                      ),
                      buttonColor: kBlueColor,
                      onPressed: () async {
                        FocusScope.of(context).unfocus();
                        if (_formKey.currentState!.validate()) {
                          await ApiAuth.signIn(cEmail.text.toString(),
                                  cPassword.text.toString(),
                                  parameterModel: parameterModel)
                              .then((value) {
                            if (isFromContent) {
                              Get.back();
                            } else {
                              Get.until((route) =>
                                  Get.currentRoute == "/NavigationScreen");
                            }

                            if (value == true) {
                              if (parameterModel?.isFromPlaylist != null &&
                                  parameterModel!.isFromPlaylist) {
                                Get.to(
                                  () => VideoContentPlaylistScreen(
                                    playlistUrl:
                                        parameterModel?.playlistUrl ?? '',
                                    playlistName:
                                        parameterModel?.playlistName ?? '',
                                  ),
                                  duration: const Duration(milliseconds: 400),
                                  transition: Transition.downToUp,
                                  fullscreenDialog: true,
                                );
                              } else if (parameterModel?.isFromPlaylist !=
                                      null &&
                                  !parameterModel!.isFromPlaylist) {
                                Get.to(
                                  () => VideoContentScreen(
                                    contentUrl:
                                        parameterModel!.contentUrl.toString(),
                                    contentId: parameterModel?.contentId ?? '',
                                    watchingSeconds:
                                        parameterModel?.watchingSeconds ?? 0,
                                  ),
                                  duration: const Duration(milliseconds: 400),
                                  transition: Transition.downToUp,
                                  fullscreenDialog: true,
                                );
                              }
                            }
                          });
                        }
                      },
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomTextButton(
                        onPressed: () {},
                        text: CustomText(
                          text: 'ยังไม่มีบัญชีผู้ใช้? ',
                          textSize: fontSizeM,
                          fontWeight: FontWeight.w600,
                          textColor: kBlackDefault2,
                        ),
                      ),
                      CustomTextButton(
                        onPressed: () =>
                            Get.off(() => RegisterScreen(isFromContent: false)),
                        text: CustomText(
                          text: 'สมัครสมาชิก',
                          textSize: fontSizeM,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: AppBar(
                title: CustomText(
                  text: "เข้าสู่ระบบ",
                  fontWeight: FontWeight.w900,
                  textSize: fontSizeXL,
                  textColor: kBlack2,
                ),
                leading: isFromContent
                    ? IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.expand_more_rounded,
                                size: 40, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"isLogin": false});
                        },
                      )
                    : IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.arrow_back_ios_new_rounded,
                                size: 25, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"isLogin": false});
                        },
                      ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
