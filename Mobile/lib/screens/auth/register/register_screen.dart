import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/auth/auth.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/auth/login/login.dart';
import 'package:project14plus/screens/auth/register/teletephone_formatter.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/widgets.dart';

/*
  pageIndex
    0 = Display login
    1 = Display register
    2 = Display reset password
    3 = Display successful register
*/

class RegisterScreen extends StatefulWidget {
  RegisterScreen({
    Key? key,
    this.parameterModel,
    this.isFromContent = true,
  }) : super(key: key);
  ParameterModel? parameterModel;
  bool isFromContent;

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController cFirstname = TextEditingController();
  TextEditingController cLastname = TextEditingController();
  TextEditingController cUsername = TextEditingController();
  TextEditingController cEmail = TextEditingController();
  TextEditingController cPassword = TextEditingController();
  TextEditingController cConfirmPassword = TextEditingController();

  FocusNode fFirstname = FocusNode();
  FocusNode fLastname = FocusNode();
  FocusNode fUsername = FocusNode();
  FocusNode fEmail = FocusNode();
  FocusNode fPassword = FocusNode();
  FocusNode fConfirmPassword = FocusNode();

  bool useEmail = true;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: kPrimary,
          image: DecorationImage(
            image: AssetImage(PATH_AUTH_BG01),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: ListView(
                shrinkWrap: true,
                children: [
                  Padding(
                    padding: kPadding,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            child: AspectRatio(
                              aspectRatio: 2.8,
                              child: Image.asset(
                                "assets/images/logo-01.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          const SizedBox(height: 30.0),
                          SizedBox(
                            width: 370,
                            child: Column(
                              children: [
                                CustomTextFromField(
                                  label: 'ชื่อจริง',
                                  focusNode: fFirstname,
                                  controller: cFirstname,
                                  onChanged: (String? value) {},
                                  isRequired: true,
                                  displaySuffixIcon: true,
                                  suffixIcon: Icons.person,
                                  onFieldSubmitted: (_) =>
                                      fLastname.requestFocus(),
                                  textInputAction: TextInputAction.next,
                                  validate: (value) => AppUtils.validateString(
                                      value,
                                      minLength: 3),
                                ),
                                const SizedBox(height: 20.0),
                                CustomTextFromField(
                                  label: 'นามสกุล',
                                  focusNode: fLastname,
                                  controller: cLastname,
                                  isRequired: true,
                                  displaySuffixIcon: true,
                                  suffixIcon: Icons.person,
                                  onChanged: (String? value) {},
                                  onFieldSubmitted: (_) =>
                                      fUsername.requestFocus(),
                                  textInputAction: TextInputAction.next,
                                  validate: (value) => AppUtils.validateString(
                                      value,
                                      minLength: 3),
                                ),
                                const SizedBox(height: 20.0),
                                CustomTextFromField(
                                  label: 'ชื่อผู้ใช้',
                                  focusNode: fUsername,
                                  controller: cUsername,
                                  isRequired: true,
                                  displaySuffixIcon: true,
                                  suffixIcon: Icons.person,
                                  onFieldSubmitted: (_) =>
                                      fEmail.requestFocus(),
                                  textInputAction: TextInputAction.next,
                                  validate: (value) => AppUtils.validateString(
                                      value,
                                      minLength: 3),
                                ),
                                const SizedBox(height: 20.0),
                                CustomTextFromField(
                                  label: useEmail ? 'อีเมล' : 'เบอร์มือถือ',
                                  focusNode: fEmail,
                                  inputFormatter: useEmail
                                      ? null
                                      : <TextInputFormatter>[
                                          LengthLimitingTextInputFormatter(12),
                                          CardFormatter(
                                            mask: 'xxx-xxx-xxxx',
                                            separator: '-',
                                          ),
                                        ],
                                  inputType: useEmail
                                      ? TextInputType.emailAddress
                                      : TextInputType.phone,
                                  controller: cEmail,
                                  isRequired: true,
                                  displaySuffixIcon: true,
                                  suffixIcon:
                                      useEmail ? Icons.email : Icons.phone,
                                  onChanged: (String? value) {},
                                  onFieldSubmitted: (_) =>
                                      fPassword.requestFocus(),
                                  textInputAction: TextInputAction.next,
                                  validate: (value) => useEmail
                                      ? AppUtils.validateEmail(value)
                                      : AppUtils.validatePhone(value),
                                ),
                                const SizedBox(height: 20.0),
                                CustomTextFromField(
                                  label: 'รหัสผ่าน',
                                  focusNode: fPassword,
                                  controller: cPassword,
                                  isPassword: true,
                                  isRequired: true,
                                  isShowPassEye: true,
                                  displaySuffixIcon: true,
                                  onChanged: (String? value) {},
                                  onFieldSubmitted: (_) =>
                                      fConfirmPassword.requestFocus(),
                                  textInputAction: TextInputAction.next,
                                  validate: (value) =>
                                      AppUtils.validatePassword(
                                          cPassword.text.toString(),
                                          cConfirmPassword.text.toString(),
                                          1),
                                ),
                                const SizedBox(height: 20.0),
                                CustomTextFromField(
                                  focusNode: fConfirmPassword,
                                  controller: cConfirmPassword,
                                  label: 'ยืนยันรหัสผ่าน',
                                  isPassword: true,
                                  isRequired: true,
                                  isShowPassEye: true,
                                  displaySuffixIcon: true,
                                  textInputAction: TextInputAction.done,
                                  validate: (value) =>
                                      AppUtils.validatePassword(
                                          cPassword.text.toString(),
                                          cConfirmPassword.text.toString(),
                                          2),
                                ),
                                const SizedBox(height: 30.0),
                                CustomButton(
                                    text: CustomText(
                                      text: 'สมัครสมาชิก',
                                      textSize: fontSizeM,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    buttonColor: kBlueColor,
                                    onPressed: () async {
                                      if (_formKey.currentState!.validate()) {
                                        if (useEmail) {
                                          await ApiAuth.signUp(
                                              firstname: cFirstname.text.trim(),
                                              lastname: cLastname.text.trim(),
                                              username: cUsername.text.trim(),
                                              email: cEmail.text.trim(),
                                              password: cPassword.text.trim(),
                                              cPassword:
                                                  cConfirmPassword.text.trim(),
                                              parameterModel:
                                                  widget.parameterModel);
                                        } else {
                                          //OTP
                                          final telephone = cEmail.text
                                              .replaceFirst('0', '+66');
                                          telephone.replaceAll('-', '');
                                          Get.to(() => OtpScreen(
                                                isFromContent: false,
                                                telephone: telephone,
                                              ));
                                        }
                                      }
                                    }),
                                const SizedBox(height: 20.0),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    CustomTextButton(
                                      onPressed: () => Get.off(
                                        () => LoginScreen(
                                          isFromContent: false,
                                        ),
                                      ),
                                      text: CustomText(
                                        text: 'มีบัญชีผู้ใช้แล้ว เข้าสู่ระบบ',
                                        textSize: fontSizeM,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    CustomTextButton(
                                      onPressed: () {
                                        setState(() {
                                          useEmail = !useEmail;
                                        });
                                        cEmail.clear();
                                      },
                                      text: CustomText(
                                        text: useEmail
                                            ? 'ใช้เบอร์มือถือ'
                                            : 'ใช้อีเมล',
                                        textSize: fontSizeM,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: AppBar(
                title: CustomText(
                  text: "สมัครสมาชิก",
                  fontWeight: FontWeight.w900,
                  textSize: fontSizeXL,
                  textColor: kBlack2,
                ),
                leading: widget.isFromContent
                    ? IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.expand_more_rounded,
                                size: 40, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"isLogin": false});
                        },
                      )
                    : IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.arrow_back_ios_new_rounded,
                                size: 25, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"isLogin": false});
                        },
                      ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
