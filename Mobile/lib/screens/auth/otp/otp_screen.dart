import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/screens/auth/login/login.dart';
import 'package:project14plus/utils/easy_loading/loading_dialog.dart';
import 'package:project14plus/widgets/widgets.dart';

const int OTP_LENGTH = 4;

class OtpScreen extends StatefulWidget {
  OtpScreen({
    Key? key,
    this.isFromContent = true,
    required this.telephone,
  }) : super(key: key);
  final bool isFromContent;
  final String telephone;

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  FrontendController _frontendController = Get.find<FrontendController>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController controller = TextEditingController();

  var errorController = StreamController<ErrorAnimationType>();

  late Timer? _timer;
  int _start = 30;
  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();
    if (_timer != null) _timer!.cancel();
    super.dispose();
  }

  void startTimer() {
    _start = 30;
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() => timer.cancel());
        } else {
          setState(() => _start--);
        }
      },
    );
  }

  onTap() {
    if (controller.text.length != OTP_LENGTH) {
      return errorController.add(ErrorAnimationType.shake);
    }
    return Get.to(() => LoginScreen(
          isFromContent: false,
        ));
  }

  void _onResendOTP() async {
    controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: kPrimary,
          image: DecorationImage(
            image: AssetImage(PATH_AUTH_BG01),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: kPadding,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      child: AspectRatio(
                        aspectRatio: 2.8,
                        child: Image.asset(
                          "assets/images/logo-01.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    const SizedBox(height: 30.0),
                    CustomText(
                      text: 'กรุณาระบุหมายเลข OTP',
                      textSize: fontSizeXL,
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 20.0),
                    CustomText(
                      text:
                          'กรุณาระบุหมายเลข OTP ที่ส่งไปยังเบอร์ ${widget.telephone}',
                      textSize: fontSizeL,
                      textColor: kBlackDefault2,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 20.0),
                    SizedBox(
                      width: 370,
                      child: Form(
                        key: _formKey,
                        child: PinCodeTextField(
                          length: OTP_LENGTH,
                          obscureText: false,
                          animationType: AnimationType.fade,
                          controller: controller,
                          keyboardType: TextInputType.number,
                          cursorColor: kPrimary,
                          // textStyle: title,
                          hintCharacter: '-',
                          // hintStyle: title,
                          enableActiveFill: true,

                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            borderRadius: BorderRadius.circular(5),
                            fieldHeight: 60,
                            fieldWidth: 50,
                            borderWidth: 1,
                            inactiveColor: _frontendController.darkMode
                                ? kWhite
                                : kBlackDefault2,
                            activeColor: _frontendController.darkMode
                                ? kWhite
                                : kBlackDefault2,
                            selectedColor: _frontendController.darkMode
                                ? kWhite
                                : kBlackDefault2,
                            selectedFillColor:
                                _frontendController.darkMode ? kBlack2 : kWhite,
                            inactiveFillColor:
                                _frontendController.darkMode ? kBlack2 : kWhite,
                            activeFillColor:
                                _frontendController.darkMode ? kBlack2 : kWhite,
                          ),
                          animationDuration: const Duration(milliseconds: 300),
                          errorAnimationController: errorController,
                          onCompleted: (v) {
                            onTap();
                          },

                          beforeTextPaste: (text) {
                            return true;
                          },
                          appContext: context,
                          onChanged: (String value) {},
                        ),
                      ),
                    ),
                    const SizedBox(height: 30.0),
                    SizedBox(
                      width: 370,
                      child: CustomButton(
                          text: CustomText(
                            text: 'ยืนยัน',
                            textSize: fontSizeM,
                            fontWeight: FontWeight.w600,
                          ),
                          buttonColor: kBlueColor,
                          onPressed: onTap),
                    ),
                    const SizedBox(height: 20.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: _start == 0
                              ? () {
                                  LoadingDialog.showToast(
                                      title: 'ส่ง OTP ใหม่สำเร็จ');
                                  startTimer();
                                }
                              : null,
                          child: RichText(
                            text: TextSpan(
                              text: 'ขอรับ OTP ใหม่',
                              style: TextStyle(
                                color: _start == 0 ? kBlack : kBlackDefault2,
                                fontFamily: 'Sarabun',
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.underline,
                              ),
                              children: <TextSpan>[
                                if (_start != 0) ...[
                                  TextSpan(
                                    text: " $_start วินาที",
                                    style: const TextStyle(
                                      color: kBlack,
                                      fontFamily: 'Sarabun',
                                      fontWeight: FontWeight.w500,
                                      decoration: TextDecoration.none,
                                    ),
                                  ),
                                ]
                              ],
                            ),
                          ),
                        ),
                        CustomTextButton(
                          text: CustomText(
                            text: '',
                            textDecoration: TextDecoration.underline,
                            textSize: fontSizeM,
                            fontWeight: fontWeightBold,
                          ),
                          onPressed: () => {},
                        ),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: AppBar(
                title: CustomText(
                  text: "ยืนยัน OTP",
                  fontWeight: FontWeight.w900,
                  textSize: fontSizeXL,
                  textColor: kBlack2,
                ),
                leading: widget.isFromContent
                    ? IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.expand_more_rounded,
                                size: 40, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"isLogin": false});
                        },
                      )
                    : IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.arrow_back_ios_new_rounded,
                                size: 25, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"isLogin": false});
                        },
                      ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
