// 0
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/control_widget/control_widget.dart';

class SuccessfullyResetPassword extends StatelessWidget {
  const SuccessfullyResetPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: kPrimary,
        image: DecorationImage(
          image: AssetImage(PATH_AUTH_BG01),
          fit: BoxFit.cover,
          alignment: Alignment.center,
        ),
      ),
      child: Stack(
        children: [
          Padding(
            padding: kPadding,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  child: AspectRatio(
                    aspectRatio: 2.8,
                    child: Image.asset(
                      "assets/images/logo-01.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                const SizedBox(height: 30.0),
                SizedBox(
                  width: 370,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomText(
                        text: 'ขอตั้งรหัสผ่านใหม่สำเร็จ',
                        textSize: fontSizeXL,
                        fontWeight: FontWeight.w600,
                      ),
                      const SizedBox(height: 20.0),
                      CustomText(
                        text:
                            'คุณได้ทำการขอตั้งรหัสผ่านใหม่เรียบร้อยแล้ว กรุณาตรวจสอบอีเมลของคุณเพื่อทำตามขั้นตอนต่อไป',
                        textSize: fontSizeL,
                        textColor: kBlackDefault2,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 30.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 370,
                      child: CustomButton(
                        onPressed: () => Get.until(
                            (route) => Get.currentRoute == '/LoginScreen'),
                        text: CustomText(
                          text: 'เข้าสู่ระบบ',
                          fontWeight: FontWeight.w600,
                          textSize: fontSizeM,
                        ),
                        buttonColor: kBlueColor,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
