import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/auth/api_auth.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/parameter_model.dart';
import 'package:project14plus/screens/auth/login/login.dart';
import 'package:project14plus/screens/auth/register/register_screen.dart';
import 'package:project14plus/screens/auth/reset_password/components/successfully_reset_password.dart';
import 'package:project14plus/screens/video_content/video_content_screen.dart';
import 'package:project14plus/screens/video_content_playlist/video_content_playlist_screen.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/widgets.dart';

class ResetPasswordScreen extends StatelessWidget {
  ResetPasswordScreen({
    Key? key,
    this.isFromContent = true,
    this.parameterModel,
  }) : super(key: key);
  final bool isFromContent;
  final ParameterModel? parameterModel;

  final _formKey = GlobalKey<FormState>();

  TextEditingController cEmail = TextEditingController();

  FocusNode fEmail = FocusNode();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: kPrimary,
          image: DecorationImage(
            image: AssetImage(PATH_AUTH_BG01),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        child: Stack(
          children: [
            Padding(
              padding: kPadding,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    child: AspectRatio(
                      aspectRatio: 2.8,
                      child: Image.asset(
                        "assets/images/logo-01.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  Form(
                    key: _formKey,
                    child: SizedBox(
                      width: 370,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomText(
                            text: 'ลืมรหัสผ่าน',
                            textSize: fontSizeXL,
                            fontWeight: FontWeight.w600,
                          ),
                          const SizedBox(height: 20.0),
                          CustomText(
                            text: 'ใส่อีเมลของคุณเพื่อขอการตั้งรหัสผ่านใหม่',
                            textSize: fontSizeL,
                            textColor: kBlackDefault2,
                          ),
                          const SizedBox(height: 20.0),
                          CustomTextFromField(
                            controller: cEmail,
                            focusNode: fEmail,
                            bgColor: kWhite,
                            label: 'อีเมล',
                            isRequired: true,
                            displaySuffixIcon: true,
                            suffixIcon: Icons.email,
                            onChanged: (String? value) {},
                            textInputAction: TextInputAction.next,
                            validate: (value) => AppUtils.validateEmail(value),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 370,
                        child: CustomButton(
                          text: CustomText(
                            text: 'ลืมรหัสผ่าน',
                            textSize: fontSizeM,
                            fontWeight: FontWeight.w600,
                          ),
                          buttonColor: kBlueColor,
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            if (_formKey.currentState!.validate()) {
                              Get.off(() => const SuccessfullyResetPassword());
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: AppBar(
                title: CustomText(
                  text: "ลืมรหัสผ่าน",
                  fontWeight: FontWeight.w900,
                  textSize: fontSizeXL,
                  textColor: kBlack2,
                ),
                leading: isFromContent
                    ? IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.expand_more_rounded,
                                size: 40, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"resetPassword": false});
                        },
                      )
                    : IconButton(
                        icon: Container(
                          decoration: const BoxDecoration(
                            color: kWhite,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(Icons.arrow_back_ios_new_rounded,
                                size: 25, color: kBlack),
                          ),
                        ),
                        onPressed: () {
                          Get.back(result: {"resetPassword": false});
                        },
                      ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
