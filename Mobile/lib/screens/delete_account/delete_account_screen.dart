import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/utils/utils.dart';

class DeleteAccountScreen extends StatefulWidget {
  const DeleteAccountScreen({Key? key}) : super(key: key);

  @override
  State<DeleteAccountScreen> createState() => _DeleteAccountScreenState();
}

class _DeleteAccountScreenState extends State<DeleteAccountScreen> {
  final formkey = GlobalKey<FormState>();

  TextEditingController description = TextEditingController();

  FocusNode focusDescription = FocusNode();

  FocusNode focusReason = FocusNode();

  Level reason = Level();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: "คำร้องขอลบบัญชี",
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 16, 10, 16),
            child: Form(
              key: formkey,
              child: Column(
                children: [
                  CustomDropdown(
                    focusNode: focusReason,
                    title: 'เหตุผลในการลบบัญชี',
                    value: reason,
                    onChange: _onChangeLevel,
                    dropdownType: DropdownType.REASON,
                    validator: (value) =>
                        AppUtils.validateString(value?.name ?? ''),
                    isRequired: true,
                  ),
                  const SizedBox(height: 10.0),
                  CustomTextFromField(
                    label: 'คำอธิบาย',
                    focusNode: focusDescription,
                    inputType: TextInputType.multiline,
                    textInputAction: TextInputAction.newline,
                    maxLength: 1500,
                    controller: description,
                    maxLines: null,
                    minLines: 4,
                    isRequired: true,
                    validate: (value) => AppUtils.validateString(value),
                    onChanged: (String? value) {},
                    onFieldSubmitted: (_) => {},
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ButtonMain(title: 'ส่งคำร้อง', onPressed: onSubmit)),
    );
  }

  _onChangeLevel(Level data) => reason = data;

  void onSubmit() {
    if (formkey.currentState!.validate()) {
      Get.back();
    }
  }
}
