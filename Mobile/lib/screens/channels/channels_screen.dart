import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/widgets.dart';

class ProgramScreen extends StatefulWidget {
  const ProgramScreen({Key? key}) : super(key: key);

  @override
  State<ProgramScreen> createState() => _ProgramScreenState();
}

class _ProgramScreenState extends State<ProgramScreen> {
  Future<List<ChannelsModel>> _future = ApiContent.getChannels();

  String? sKeyword;

  Level sorting = Level();

  TextEditingController keyword = TextEditingController();

  void searchData() async {
    final result = await Get.to(
      () => PlaylistSearch(
        keyword: keyword,
        sorting: sorting,
      ),
      duration: const Duration(milliseconds: 400),
      transition: Transition.size,
      fullscreenDialog: true,
    );
    if (result != null) {
      setState(() {
        keyword = result["keyword"];
        sKeyword = keyword.text;
        sorting = result["sorting"];
        _future = ApiContent.getChannels(
            sorting: sorting.id != null ? int.parse(sorting.id ?? "0") : 0,
            keyword: sKeyword.toString());
      });
    } else {
      if (sKeyword != null) {
        setState(() => keyword.text = sKeyword ?? '');
      } else {
        setState(() => keyword.clear());
      }
    }
  }

  Future<void> refreshData() async {
    setState(() {
      _future = ApiContent.getChannels(
          sorting: sorting != null ? int.parse(sorting.id ?? "0") : 0,
          keyword: sKeyword?.trim());
    });
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;

    return CustomAppBar(
      title: "เลือกจากรายการ",
      bannerPath: PATH_LEVELS_BANNER,
      onPressedTrailing: () => searchData(),
      body: RefreshIndicator(
        color: kPrimary,
        onRefresh: refreshData,
        child: FutureBuilder<List<ChannelsModel>>(
          future: _future,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              List<ChannelsModel>? items = snapshot.data;
              if (items!.isEmpty) {
                return NoData(
                  title: 'ไม่พบรายการในระบบ',
                  onPressed: () {
                    LoadingDialog.showLoading();
                    refreshData();
                    LoadingDialog.closeDialog(withDelay: true);
                  },
                );
              }
              return GridView.builder(
                padding: const EdgeInsets.fromLTRB(kGapS, kGap, kGapS, 0),
                itemCount: items.length,
                physics: const AlwaysScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: isMobile
                      ? (orientation == Orientation.portrait)
                          ? 2
                          : 3
                      : (orientation == Orientation.portrait)
                          ? 4
                          : 5,
                  mainAxisSpacing: 0.0,
                  crossAxisSpacing: 10,
                  mainAxisExtent: 230.0,
                ),
                itemBuilder: (BuildContext context, int index) {
                  ChannelsModel item = items[index];

                  return InkWell(
                    onTap: () => Get.to(() => PlayListScreen(
                          channelsModel: item,
                        )),
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    focusColor: Colors.transparent,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomCoverImage(image: item.image.toString()),
                        const SizedBox(height: 5.0),
                        CustomText(
                          text: item.name.toString(),
                          textSize: fontSizeM,
                          fontWeight: FontWeight.w600,
                          maxLine: 2,
                        ),
                        const SizedBox(height: 5.0),
                        CustomText(
                          text: "โดย ${item.department!.name.toString()}",
                          textSize: fontSizeS,
                          fontWeight: FontWeight.w600,
                          maxLine: 1,
                        )
                      ],
                    ),
                  );
                },
              );
            } else if (snapshot.hasError) {
              return const Padding(
                padding: kPadding,
                child: NoData(title: 'เกิดข้อผิดพลาด'),
              );
            }
            return const Padding(
              padding: kPadding,
              child: ProgramShimmer(
                length: 6,
                gridHeight: 220,
              ),
            );
          },
        ),
      ),
    );
  }
}
