import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';

import '../../model/user/user_model.dart';

class PersonalInfoScreen extends StatefulWidget {
  const PersonalInfoScreen({Key? key, required this.id}) : super(key: key);
  final String id;

  @override
  State<PersonalInfoScreen> createState() => _PersonalInfoScreenState();
}

class _PersonalInfoScreenState extends State<PersonalInfoScreen> {
  final UserController _userController = Get.find<UserController>();
  final _formKey = GlobalKey<FormState>();

  var cFirstname = TextEditingController();
  var cLastname = TextEditingController();
  var cUsername = TextEditingController();
  var cEmail = TextEditingController();

  FocusNode fFirstname = FocusNode();
  FocusNode fLastname = FocusNode();
  FocusNode fUsername = FocusNode();
  FocusNode fEmail = FocusNode();

  var picker = ImagePicker();
  var imageFile = XFile("");
  var isPicked = false;
  var imageUrl = "";

  @override
  void initState() {
    if (_userController.userModel != null) {
      imageUrl = _userController.userModel?.avatar ?? '';
      cFirstname.text = _userController.userModel?.firstname ?? '';
      cLastname.text = _userController.userModel?.lastname ?? '';
      cUsername.text = _userController.userModel?.username ?? '';
      cEmail.text = _userController.userModel?.email ?? '';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: "แก้ไขข้อมูลส่วนตัว",
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  const SizedBox(height: 20.0),
                  Stack(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: kPrimary, width: 3.0),
                        ),
                        child: imageFile.path != '' && isPicked == true
                            ? CircleAvatar(
                                backgroundColor: kPrimary,
                                backgroundImage:
                                    Image.file(File(imageFile.path)).image,
                                radius: 100.0,
                              )
                            : ImageProfileCircle(
                                imageUrl: imageUrl,
                                radius: 100.0,
                                fit: BoxFit.cover,
                              ),
                      ),
                      Positioned(
                        bottom: 10,
                        right: 20,
                        child: GestureDetector(
                          onTap: () async {
                            final _file = await picker.pickImage(
                                source: ImageSource.gallery);
                            if (_file != null) {
                              setState(() {
                                imageFile = _file;
                                isPicked = true;
                              });
                            }
                          },
                          child: Container(
                              padding: const EdgeInsets.all(1.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                  border:
                                      Border.all(color: kPrimary, width: 3.0)),
                              child: const Padding(
                                padding: EdgeInsets.all(5.0),
                                child: Icon(
                                  CupertinoIcons.photo_camera,
                                  color: kPrimary,
                                ),
                              )),
                        ),
                      ),
                      (imageFile.path.isNotEmpty && isPicked == true) ||
                              imageUrl.isNotEmpty
                          ? Positioned(
                              top: 10,
                              right: 20,
                              child: GestureDetector(
                                onTap: () async {
                                  setState(() {
                                    imageFile = XFile("");
                                    isPicked = false;
                                    imageUrl = '';
                                  });
                                },
                                child: Container(
                                    padding: const EdgeInsets.all(1.0),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        color: kPrimary,
                                        width: 3.0,
                                      ),
                                    ),
                                    child: const Padding(
                                      padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.remove_outlined,
                                        color: kPrimary,
                                      ),
                                    )),
                              ),
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFromField(
                    label: 'ชื่อจริง',
                    isRequired: true,
                    controller: cFirstname,
                    focusNode: fFirstname,
                    onChanged: (String? value) {},
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (_) => fLastname.requestFocus(),
                    validate: (value) => AppUtils.validateString(value),
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFromField(
                    label: 'นามสกุล',
                    controller: cLastname,
                    focusNode: fLastname,
                    onChanged: (String? value) {},
                    isRequired: true,
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (_) => fUsername.requestFocus(),
                    validate: (value) => AppUtils.validateString(value),
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFromField(
                    label: 'ชื่อผู้ใช้',
                    controller: cUsername,
                    isRequired: true,
                    onChanged: (String? value) {},
                    textInputAction: TextInputAction.done,
                    validate: (value) => AppUtils.validateString(value),
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFromField(
                    label: 'อีเมล',
                    controller: cEmail,
                    focusNode: fEmail,
                    isRequired: true,
                    readOnly: true,
                    onChanged: (String? value) {},
                    validate: (value) => AppUtils.validateEmail(value),
                  ),
                  const SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: kButtonPadding,
        child: ButtonMain(title: 'บันทึก', onPressed: onSubmit),
      ),
    );
  }

  void onSubmit() async {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState!.validate()) {
      await ApiUpload.uploadAvatar(imageFile).then((path) async {
        if (path == 'E') {
          LoadingDialog.showError(title: 'เกิดข้อผิดพลาดในการแก้ไรูปภาพ');
        } else if (path == "Large") {
          LoadingDialog.showError(title: 'รูปภาพมีขนาดใหญ่เกินไป');
        } else {
          UserModel temp = UserModel(
            id: widget.id,
            email: cEmail.text,
            username: cUsername.text,
            firstname: cFirstname.text,
            lastname: cLastname.text,
            avatar: path != null ? path : imageUrl,
          );

          await ApiPersonal.editProfile(temp);
        }
      });
    }
  }
}
