import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api_content/api_content.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/content/level_model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/utils/easy_loading/easy_loading.dart';
import 'package:project14plus/widgets/widgets.dart';

class LevelScreen extends StatefulWidget {
  const LevelScreen({Key? key}) : super(key: key);

  @override
  State<LevelScreen> createState() => _LevelScreenState();
}

class _LevelScreenState extends State<LevelScreen> {
  late Future<List<LevelModel>> future = ApiContent.getLevels();

  Future<void> refreshData() async => future = ApiContent.getLevels();

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;

    return CustomAppBar(
      title: "ระดับชั้น",
      bannerPath: PATH_LEVELS_BANNER,
      body: RefreshIndicator(
        onRefresh: refreshData,
        color: kPrimary,
        child: FutureBuilder<List<LevelModel>>(
            future: future,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData) {
                List<LevelModel>? items = snapshot.data;

                //Has data but data is empty
                if (snapshot.data == null) {
                  return NoData(
                    title: 'ไม่พบระดับชั้นในระบบ',
                    onPressed: () {
                      LoadingDialog.showLoading();
                      refreshData();
                      LoadingDialog.closeDialog(withDelay: true);
                    },
                  );
                }

                //Has data
                return SizedBox(
                  height: Get.height,
                  child: GridView.builder(
                    padding: kPadding,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: items!.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: isMobile
                            ? (orientation == Orientation.portrait)
                                ? 2
                                : 3
                            : (orientation == Orientation.portrait)
                                ? 4
                                : 5,
                        mainAxisSpacing: 0.0,
                        crossAxisSpacing: 10,
                        mainAxisExtent: 190.0),
                    itemBuilder: (BuildContext context, int index) {
                      LevelModel item = items[index];
                      return InkWell(
                        onTap: () {
                          log("message");
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LevelDetailScreen(
                                title: item.name.toString(),
                                url: item.url.toString(),
                              ),
                            ),
                          );
                        },
                        child: Column(
                          children: [
                            CustomCoverImage(image: item.image.toString()),
                            const SizedBox(height: 10.0),
                            CustomText(
                              text: item.name.toString(),
                              textSize: fontSizeM,
                              fontWeight: FontWeight.w600,
                              maxLine: 1,
                            )
                          ],
                        ),
                      );
                    },
                  ),
                );
              } else if (snapshot.hasError) {
                return const ProgramShimmer();
              }
              return const ProgramShimmer();
            }),
      ),
    );
  }
}
