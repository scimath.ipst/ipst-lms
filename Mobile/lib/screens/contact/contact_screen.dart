import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:project14plus/widgets/control_widget/control_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactScreen extends StatelessWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: "ติดต่อเรา Project 14+",
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          padding: const EdgeInsets.fromLTRB(10, 16.0, 10.0, 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(outlineRadius),
                child: Image.asset(
                  PATH_LOGO_CONTACT,
                  height: 100.0,
                  fit: BoxFit.contain,
                ),
              ),
              const SizedBox(height: 20.0),
              CustomText(
                text: "Line Openchat Project 14",
                textSize: fontSizeL,
                fontWeight: FontWeight.w600,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 10.0),
              InkWell(
                onTap: () async {
                  String url =
                      'https://line.me/ti/g2/Chl46ZROFBbb3CyuQfSDEw'.trim();
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(outlineRadius),
                  child: Image.asset(
                    PATH_CONTACT_LINE,
                    fit: BoxFit.none,
                  ),
                ),
              ),
              const SizedBox(height: 10.0),
              CustomText(
                text: " ",
                textSize: fontSizeL,
                fontWeight: FontWeight.w600,
                textAlign: TextAlign.center,
              ),
              SelectableLinkify(
                onOpen: (link) async {
                  String? encodeQueryParameters(Map<String, String> params) {
                    return params.entries
                        .map((e) =>
                            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
                        .join('&');
                  }

                  final Uri emailLaunchUri = Uri(
                    scheme: 'mailto',
                    path: 'project14plus@ipst.ac.th',
                    query:
                        encodeQueryParameters(<String, String>{'subject': ''}),
                  );

                  if (await canLaunchUrl(emailLaunchUri)) {
                    await launchUrl(emailLaunchUri);
                  } else {}
                },
                text: "อีเมล project14plus@ipst.ac.th",
                textAlign: TextAlign.center,
                style: const TextStyle(
                  // color: Colors.black,
                  fontSize: fontSizeL,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Sarabun",
                ),
                linkStyle: const TextStyle(
                  color: kPrimary,
                  fontSize: fontSizeL,
                  decoration: TextDecoration.underline,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Sarabun",
                ),
              ),
              const SizedBox(height: 10.0),
              CustomText(
                text: "เพื่อติดตามข่าวสาร สอบถาม และแจ้งปัญหาการใช้งาน",
                textSize: fontSizeL,
                fontWeight: FontWeight.w600,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
