import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/widgets/widgets.dart';

class HelpScreen extends StatelessWidget {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: CustomText(
            text: "ศูนย์ช่วยเหลือ",
            textSize: fontSizeXL,
            fontWeight: FontWeight.w600,
          ),
        ),
        body: const HelpBody());
  }
}
