import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/screens/contact/contact_screen.dart';
import 'package:project14plus/screens/delete_account/delete_account_screen.dart';
import 'package:project14plus/screens/other/components/list_tile.dart';

class HelpBody extends StatelessWidget {
  const HelpBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                children: [
                  listTile(
                    icon: Icons.contact_support_rounded,
                    title: 'ติดต่อเรา',
                    onPressed: () => Get.to(() => const ContactScreen()),
                  ),
                  listTile(
                    icon: Icons.description_rounded,
                    title: 'คำร้องขอลบบัญชี',
                    onPressed: () => Get.to(() => const DeleteAccountScreen()),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
