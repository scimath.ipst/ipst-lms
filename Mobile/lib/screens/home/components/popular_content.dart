import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/widgets/widgets.dart';

class PopularContent extends StatelessWidget {
  PopularContent({Key? key}) : super(key: key);
  final FrontendController _frontendController = Get.find<FrontendController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: CustomListViewContent(
        future: _frontendController.popularCon,
        title: 'วีดีโอยอดนิยม',
      ),
    );
  }
}
