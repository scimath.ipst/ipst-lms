import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';

class CarouselSliderScreen extends StatefulWidget {
  const CarouselSliderScreen({Key? key}) : super(key: key);

  @override
  State<CarouselSliderScreen> createState() => _CarouselSliderScreenState();
}

class _CarouselSliderScreenState extends State<CarouselSliderScreen> {
  final FrontendController _frontendController = Get.find<FrontendController>();
  int bannerIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<ContentModel>>(
          future: _frontendController.bannerCon,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              List<ContentModel>? items = snapshot.data;

              //Has data but data is empty
              if (items!.isEmpty) {
                return const SizedBox.shrink();
              }
              var orientation = MediaQuery.of(context).orientation;
              //Has data
              return Column(
                children: [
                  Expanded(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: isMobile
                            ? orientation == Orientation.portrait
                                ? 310
                                : 615
                            : 410,
                        viewportFraction: 1.0,
                        enlargeCenterPage: false,
                        autoPlay: true,
                        onPageChanged: (index, value) {
                          setState(() {
                            bannerIndex = index;
                          });
                        },
                      ),
                      items: items.map((item) {
                        final List<LevelModel> list = [];
                        list.addAll(item.levels ?? []);
                        // list.addAll(item.tags ?? []);
                        return GestureDetector(
                          onTap: () =>
                              slideToVideoContent(item.url!, item.id!, null),
                          child: Column(
                            children: [
                              item.image != null
                                  ? Center(
                                      child: Image.network(
                                        item.image.toString(),
                                        fit: BoxFit.fitWidth,
                                        height: isMobile ? 200 : 300,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                                    )
                                  : Center(
                                      child: Container(
                                        height: 200,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        color: Colors.grey,
                                      ),
                                    ),
                              const SizedBox(height: 5.0),
                              ListTile(
                                leading: ImageProfileCircle(
                                  imageUrl: item.user!.avatar.toString(),
                                  radius: 25,
                                ),
                                title: CustomText(
                                  text: item.name.toString(),
                                  textSize: fontSizeM,
                                  fontWeight: FontWeight.w500,
                                  maxLine: 2,
                                ),
                                subtitle: CustomText(
                                  text: item.user!.firstname.toString(),
                                  maxLine: 1,
                                  textSize: fontSizeS,
                                  fontWeight: FontWeight.w500,
                                  textOverflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                      carouselController: _frontendController.carouseCon,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: items.asMap().entries.map((entry) {
                      return GestureDetector(
                          onTap: () => _frontendController.carouseCon
                              .animateToPage(entry.key),
                          child: Container(
                            width: 8.0,
                            height: 8.0,
                            margin: const EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: (Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? Colors.white
                                        : Colors.black)
                                    .withOpacity(
                                        bannerIndex == entry.key ? 0.9 : 0.4)),
                          ));
                    }).toList(),
                  ),
                ],
              );
            }

            //Loading
            return const CarouselShimmer();
          }),
    );
  }
}
