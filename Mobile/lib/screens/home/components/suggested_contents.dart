import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/content/content.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/shimmer/text_shimmer.dart';
import 'package:project14plus/widgets/widgets.dart';

class SuggestedContents extends StatelessWidget {
  const SuggestedContents({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: GetBuilder<FrontendController>(builder: (_frontendController) {
          return Column(
            children: [
              FutureBuilder<List<ContentModel>>(
                  future: _frontendController.suggestedCon,
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      List<ContentModel>? items = snapshot.data;

                      //Has data and data is empty
                      if (snapshot.data == null) {
                        return const SizedBox.shrink();
                      }

                      //Has data and data is not empty
                      return Column(
                        children: [
                          const SizedBox(height: 10.0),
                          const Divider(color: kBlackDefault, thickness: .2),
                          const SizedBox(height: 10.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(
                                text: 'วีดีโอแนะนำ',
                                textSize: fontSizeM,
                                fontWeight: FontWeight.w600,
                              ),
                            ],
                          ),
                          const SizedBox(height: 10.0),
                          SizedBox(
                            height: 230.0,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: items!.length,
                                itemBuilder: (BuildContext context, int index) {
                                  ContentModel item = items[index];

                                  return GestureDetector(
                                    onTap: () => slideToVideoContent(
                                        item.url!.toString(),
                                        item.id.toString(),
                                        0),
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: Stack(
                                        children: [
                                          SizedBox(
                                            height: 130.0,
                                            width: 200.0,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      outlineRadius),
                                              child: CustomCoverImage(
                                                image: item.image.toString(),
                                                height: 130.0,
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 80.0,
                                            left: 10.0,
                                            child: ImageProfileCircle(
                                              imageUrl: item.user?.avatar
                                                      .toString() ??
                                                  "",
                                              radius: 20.0,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 15.0,
                                            left: 0.0,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  width: 200.0,
                                                  child: CustomText(
                                                    text: item.name.toString(),
                                                    textSize: fontSizeM,
                                                    fontWeight: FontWeight.w500,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    maxLine: 2,
                                                  ),
                                                ),
                                                const SizedBox(height: 5.0),
                                                SizedBox(
                                                  width: 200.0,
                                                  child: CustomText(
                                                    text:
                                                        "${item.user?.firstname.toString()} • ผู้ชม ${item.visitCount} คน",
                                                    textSize: fontSizeS,
                                                    fontWeight: FontWeight.w500,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    maxLine: 1,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ],
                      );
                    }
                    return Column(
                      children: const [
                        SizedBox(height: 10.0),
                        Divider(color: kBlackDefault, thickness: .2),
                        SizedBox(height: 10.0),
                        TextShimmerLoad(),
                        SizedBox(height: 10.0),
                        HomeShimmer(),
                      ],
                    );
                  }),
            ],
          );
        }));
  }
}
