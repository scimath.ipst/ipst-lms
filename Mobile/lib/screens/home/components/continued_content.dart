import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/shimmer/text_shimmer.dart';
import 'package:project14plus/widgets/widgets.dart';

class ContinuedContent extends StatelessWidget {
  const ContinuedContent({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserController>(builder: (_userController) {
      return _userController.userModel != null
          ? GetBuilder<FrontendController>(builder: (_frontendController) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: FutureBuilder<List<ContinuedModel>>(
                  future: _frontendController.continuedCon,
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      List<ContinuedModel>? items = snapshot.data;

                      //Has data and data is empty
                      if (items == null || items.isEmpty) {
                        return const SizedBox.shrink();
                      }

                      //Has data and data is not empty
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 10.0),
                          const Divider(color: kBlackDefault, thickness: .2),
                          const SizedBox(height: 10.0),
                          CustomText(
                            text: 'ดูต่อสำหรับคุณ',
                            textSize: fontSizeM,
                            fontWeight: FontWeight.w600,
                          ),
                          const SizedBox(height: 10.0),
                          SizedBox(
                            height: 230.0,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: items.length,
                                itemBuilder: (BuildContext context, int index) {
                                  ContinuedModel item = items[index];

                                  return GestureDetector(
                                    onTap: () => slideToVideoContent(
                                        item.content!.url!,
                                        item.content!.id!,
                                        item.watchingSeconds),
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: Stack(
                                        children: [
                                          SizedBox(
                                            height: 130.0,
                                            width: 200.0,
                                            child: Stack(
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          outlineRadius),
                                                  child: CustomCoverImage(
                                                    image:
                                                        item.content?.image ??
                                                            '',
                                                    height: 130.0,
                                                  ),
                                                ),
                                                if (item.watchingSeconds !=
                                                        null &&
                                                    item.watchingSeconds != 0)
                                                  Positioned(
                                                    bottom: 0,
                                                    right: 0,
                                                    child: Container(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      decoration:
                                                          const BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          bottomRight:
                                                              Radius.circular(
                                                                  outlineRadius),
                                                          topLeft:
                                                              Radius.circular(
                                                                  outlineRadius),
                                                        ),
                                                        color: kPrimary,
                                                      ),
                                                      child: CustomText(
                                                        text: item
                                                            .displayWatchingSeconds(),
                                                        textSize: fontSizeS - 2,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        textColor: kWhite,
                                                      ),
                                                    ),
                                                  ),
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 80.0,
                                            left: 10.0,
                                            child: ImageProfileCircle(
                                              imageUrl: item
                                                      .content!.user?.avatar
                                                      .toString() ??
                                                  "",
                                              radius: 20.0,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 15.0,
                                            left: 0.0,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  width: 200.0,
                                                  child: CustomText(
                                                    text: item.content!.name
                                                        .toString(),
                                                    textSize: fontSizeM,
                                                    fontWeight: FontWeight.w500,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    maxLine: 2,
                                                  ),
                                                ),
                                                const SizedBox(height: 5.0),
                                                SizedBox(
                                                  width: 200.0,
                                                  child: CustomText(
                                                    text:
                                                        "${item.content!.user?.firstname.toString()} • ผู้ชม ${item.content!.visitCountFormat()} คน",
                                                    textSize: fontSizeS,
                                                    fontWeight: FontWeight.w500,
                                                    textOverflow:
                                                        TextOverflow.ellipsis,
                                                    maxLine: 1,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ],
                      );
                    } else if (snapshot.hasError) {
                      //Do Somthing with error show error
                      return const SizedBox.shrink();
                    }
                    return Column(
                      children: const [
                        SizedBox(height: 10.0),
                        Divider(color: kBlackDefault, thickness: .2),
                        SizedBox(height: 10.0),
                        TextShimmerLoad(),
                        SizedBox(height: 10.0),
                        HomeShimmer(),
                      ],
                    );
                  },
                ),
              );
            })
          : const SizedBox.shrink();
    });
  }
}

String formatTime(int seconds) {
  return '${(Duration(seconds: seconds))}'.split('.')[0].padLeft(8, '0');
}
