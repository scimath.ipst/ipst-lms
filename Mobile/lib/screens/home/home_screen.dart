import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/notification_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/notification_model.dart';
import 'package:project14plus/model/user/user_model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/services/logout_service.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  final UserController _userController = Get.find<UserController>();
  final FrontendController _frontendController = Get.find<FrontendController>();

  UserModel? userCheck;

  void _search() async {
    Get.to(
      () => LevelDetailScreen(
        title: 'ค้นหา',
        url: '',
        isFromHome: true,
      ),
      duration: const Duration(milliseconds: 0),
      transition: Transition.size,
      fullscreenDialog: false,
    );
  }

  void getPdpaInfo() async {
    await LocalStorage.getUser().then((user) async {
      setState(() => userCheck = user);
      if (user != null) {
        await LocalStorage.getPdpa().then((value) {
          if (value == null) {
            Get.to(() => PdpaScreen(
                  isFromContent: true,
                ));
          } else {
            if (value.date!.compareTo(DateTime.now()) > 0) {
              Get.to(() => PdpaScreen(
                    isFromContent: true,
                  ));
            }
          }
        });
      }
    });
  }

  Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(PREF_USER);
    prefs.remove(PREF_PDPA);
    _userController.clearUser();
    // Delete all secure storage
    await storage.deleteAll();
    _frontendController.refreshValue();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        await LocalStorage.getUser().then((user) async {
          setState(() => userCheck = user);
          if (user != null) {
            await LocalStorage.getPdpa().then((value) {
              if (value == null) {
                Get.to(() => PdpaScreen(isFromContent: true));
              } else if (value.result == false) {
                if (value.date!.compareTo(DateTime.now()) > 0) logout();
              }
            });
          }
        });
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: CustomText(
          text: appName,
          textSize: 24,
          fontWeight: FontWeight.w700,
          textAlign: TextAlign.start,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              onPressed: () {
                // _search();

                final model = NotificationModel(
                  id: '85555',
                  title: 'ทดสอบแจ้งเตือน',
                  subtitle: '',
                  body: 'ทดสอบการแจ้งเตือนเมื่อไม่ได้เข้าใช้งาน',
                  isRead: false,
                  sentTime: '2022-09-14T14:38:39.969Z',
                  url: '',
                  docId: '',
                );
                Get.find<NotificationController>()
                    .saveNotifications(model, isForeground: true);
              },
              icon: const Icon(Icons.search),
            ),
          )
        ],
      ),
      body: RefreshIndicator(
        color: kPrimary,
        onRefresh: () async => _frontendController.refreshValue(),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                    height: isMobile
                        ? orientation == Orientation.portrait
                            ? 310
                            : 615
                        : 410.0,
                    child: const CarouselSliderScreen()),
                const ContinuedContent(),
                const SuggestedContents(),
                NewestContents(),
                PopularContent(),
                const SizedBox(height: 20.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    getPdpaInfo();
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }
}
