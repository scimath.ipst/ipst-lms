import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:visibility_detector/visibility_detector.dart';

/*
  status :
    0 = Visited
    1 = Watching
    2 = Watched
    3 = Completed
  scoreType :
    0 = ไม่เก็บคะแนน
    1 = เก็บคะแนนสูงสุด
    2 = เก็บคะแนนครั้งล่าสุด
*/

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({Key? key, required this.userId}) : super(key: key);
  final String userId;

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  late ScrollController scrollController;
  bool isGoToTop = false;

  List<HistoryModel> _data = [];

  int page = 0;
  bool isLoading = false;
  bool isEnded = false;

  @override
  void initState() {
    loadData();
    scrollController = ScrollController()..addListener(loadMore);

    super.initState();
  }

  @override
  void dispose() {
    scrollController.removeListener(loadMore);
    super.dispose();
  }

  Future<void> onRefresh() async {
    setState(() {
      page = 0;
      isLoading = false;
      isEnded = false;
      _data = [];
      isGoToTop = false;
    });
    loadData();
  }

  Future<void> loadData() async {
    if (!isEnded && !isLoading) {
      try {
        page += 1;
        setState(() {
          isLoading = true;
        });
        ApiPersonal.getHistory(
          widget.userId,
          page,
        ).then((value) {
          for (var i = 0; i < value.result!.length; i++) {
            _data.add(value.result![i]);
          }
          setState(() {
            _data;
            if (_data.length >= (value.paginate?.total ?? 0)) {
              isEnded = true;
              isLoading = false;
            } else if (value != null) {
              isLoading = false;
            }
          });
        });
      } catch (e) {
        log("$e");
      }
    }
  }

  void onLoadMore(info) async {
    if (info.visibleFraction > 0 && !isEnded && !isLoading) {
      await loadData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: 'ประวัติการรับชม',
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: Stack(
        children: [
          RefreshIndicator(
            onRefresh: () async => onRefresh(),
            color: kPrimary,
            child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                controller: scrollController,
                padding: kPadding,
                child: Column(
                  children: [
                    _data.isEmpty
                        ? const SizedBox.shrink()
                        : ListView.separated(
                            shrinkWrap: true,
                            itemCount: _data.length,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              HistoryModel item = _data[index];
                              final date = item.updatedAt != null
                                  ? DateFormatter.formatterddMMYYYY(
                                      item.updatedAt!,
                                      withHour: false)
                                  : "";
                              return ListTile(
                                onTap: () => slideToVideoContent(
                                    item.content!.url!,
                                    item.content!.id!,
                                    item.watchingSeconds.toDouble()),
                                title: Padding(
                                  padding: const EdgeInsets.only(bottom: 10.0),
                                  child: CustomText(
                                    text: item.content?.name ?? "",
                                    textSize: fontSizeM,
                                    fontWeight: fontWeightNormal,
                                    maxLine: 1,
                                  ),
                                ),
                                leading: ConstrainedBox(
                                    constraints: const BoxConstraints(
                                      minWidth: 50,
                                      minHeight: 50,
                                      maxWidth: 80,
                                      maxHeight: 60,
                                    ),
                                    child: CustomCoverImage(
                                      image: item.content?.image ?? "",
                                      fit: BoxFit.cover,
                                    )),
                                subtitle: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    CustomStatusText(
                                      status: item.status,
                                    ),
                                    CustomText(
                                      text: date,
                                      textSize: fontSizeM,
                                      fontWeight: fontWeightNormal,
                                      maxLine: 1,
                                    ),
                                  ],
                                ),
                                trailing:
                                    item.status == 3 && item.scoreType == 0
                                        ? const Icon(
                                            Icons.star_rounded,
                                            color: kPrimary,
                                            size: 26,
                                          )
                                        : CustomText(
                                            text: item.score != 0
                                                ? item.score.toString()
                                                : '-',
                                            textSize: fontSizeM,
                                            fontWeight: fontWeightNormal,
                                            maxLine: 1,
                                          ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return const Divider(
                                thickness: 1,
                                color: Color(0x66999999),
                              );
                            },
                          ),
                    if (isEnded && _data.isEmpty) ...[
                      NoData(onPressed: (() {
                        LoadingDialog.showLoading();
                        onRefresh();
                        LoadingDialog.closeDialog(withDelay: true);
                      }))
                    ],
                    if (!isEnded) ...[
                      if (_data.isNotEmpty) ...[
                        const Divider(
                          thickness: 1,
                          color: Color(0x66999999),
                        ),
                      ],
                      VisibilityDetector(
                        key: const Key('loader-widget'),
                        onVisibilityChanged: onLoadMore,
                        child: const HistoryShimmer(
                          length: 6,
                        ),
                      ),
                    ],
                  ],
                )),
          ),
          CustomCircleButton(
            ignoring: !isGoToTop && _data.isNotEmpty,
            isShow: isGoToTop && _data.isNotEmpty,
            onPressed: () {
              scrollController.animateTo(
                0.0,
                curve: Curves.easeOut,
                duration: const Duration(milliseconds: 400),
              );
            },
          ),
        ],
      ),
    );
  }

  void loadMore() async {
    if (scrollController.offset != scrollController.position.minScrollExtent) {
      setState(() => isGoToTop = true);
    } else {
      setState(() => isGoToTop = false);
    }
  }
}
