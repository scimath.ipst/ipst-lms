import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: "เกี่ยวกับเรา",
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          padding: const EdgeInsets.fromLTRB(10, 16.0, 10.0, 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(outlineRadius),
                child: Image.asset(
                  PATH_LOGO_CONTACT,
                  height: 100.0,
                  fit: BoxFit.contain,
                ),
              ),
              const SizedBox(height: 10.0),
              CustomText(
                text: "โครงการสอนออนไลน์ Project 14+",
                textSize: fontSizeXL,
                fontWeight: FontWeight.w600,
              ),
              const SizedBox(height: 10.0),
              CustomText(
                text: ABOUT_CONTENT,
                textSize: fontSizeM,
                fontWeight: FontWeight.w400,
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
