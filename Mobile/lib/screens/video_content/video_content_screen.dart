import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/screens/screens.dart';

class VideoContentScreen extends StatelessWidget {
  const VideoContentScreen({
    Key? key,
    required this.contentUrl,
    required this.contentId,
    this.watchingSeconds,
  }) : super(key: key);

  final String contentUrl;
  final String contentId;
  final double? watchingSeconds;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ContentController>(builder: (controller) {
      return SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: controller.isFullScreen ? kBlack : null,
          body: VideoContentBody(
            contentUrl: contentUrl,
            watchingSeconds: watchingSeconds,
            contentId: contentId,
          ),
        ),
      );
    });
  }
}
