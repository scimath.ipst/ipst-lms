import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/controllers/controllers.dart';
import 'package:project14plus/model/content/content.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class ContentDescription extends GetView<ContentController> {
  ContentDescription({
    Key? key,
    required this.videoContent,
  }) : super(key: key);
  VideoContent videoContent;
  @override
  Widget build(BuildContext context) {
    VideoContent? item = videoContent;
    final createdAt = DateFormatter.formatterddMMYYYY(item.result!.createdAt!,
        withHour: false);
    final List<LevelModel> list = [];
    list.addAll(item.result?.levels ?? []);
    list.addAll(item.result?.tags ?? []);

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(kGapS, kGap, kGapS, 0),
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => controller.setDesCheck(false),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, kGapS, 0, kGapS),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomText(
                    text: "คำอธิบาย",
                    textSize: fontSizeM,
                    fontWeight: fontWeightNormal,
                  ),
                  const Icon(Icons.close),
                ],
              ),
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(kGapS, 0, kGapS, 0),
          child: Divider(
            color: kBlackDefault,
            thickness: 1,
            height: 1,
          ),
        ),
        Expanded(
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(kGapS, kGap, kGapS, kGapS),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {},
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                text: item.result!.name.toString(),
                                textSize: fontSizeM,
                                fontWeight: FontWeight.w500,
                              ),
                              const SizedBox(height: 10.0),
                              CustomText(
                                text: item.result?.displayVisitCount() ?? '-',
                                textSize: fontSizeS,
                                fontWeight: FontWeight.w500,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        children: [
                          ImageProfileCircle(
                            imageUrl: item.result!.user!.avatar.toString(),
                            radius: 20,
                          ),
                          const SizedBox(width: 10.0),
                          CustomText(
                            text:
                                '${item.result!.user!.firstname.toString()} ${item.result!.user!.lastname.toString()}',
                            textSize: fontSizeM,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      if (item.result?.description != null &&
                          item.result?.description != '') ...[
                        SelectableLinkify(
                          onOpen: (link) async {
                            if (await canLaunch(link.url)) {
                              await launch(link.url);
                            } else {
                              throw 'Could not launch $link';
                            }
                          },
                          text: item.result!.description.toString(),
                          textScaleFactor: 0.96,
                          style: const TextStyle(
                              fontSize: fontSizeM,
                              decoration: TextDecoration.none,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Sarabun"),
                          linkStyle: const TextStyle(
                              color: kPrimary,
                              fontSize: fontSizeM,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Sarabun"),
                        ),
                        const SizedBox(height: 20.0),
                      ],
                      if (list.isNotEmpty) ...[
                        Row(
                          children: [
                            Expanded(
                              child: Wrap(
                                children: List.generate(list.length, (index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        right: 10.0, bottom: 10.0),
                                    child: Container(
                                      padding: const EdgeInsets.all(8.0),
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(20),
                                        ),
                                        color: kPrimary,
                                      ),
                                      child: CustomText(
                                        text: list[index].name.toString(),
                                        textSize: fontSizeM,
                                        fontWeight: FontWeight.w500,
                                        textColor: kWhite,
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20.0),
                      ],
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
