import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/model/content/content.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/image/image_profile_circle.dart';
import 'package:project14plus/widgets/widgets.dart';

class ContentTitle extends GetView<ContentController> {
  ContentTitle({
    Key? key,
    required this.onQuestionPressed,
    required this.videoContent,
  }) : super(key: key);
  void Function()? onQuestionPressed;
  VideoContent videoContent;

  @override
  Widget build(BuildContext context) {
    String visitCount = videoContent.result?.displayVisitCount() ?? '-';

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            controller.setDesCheck(true);
          },
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 3,
                      child: CustomText(
                        text: videoContent.result?.name.toString() ?? "",
                        textSize: fontSizeM,
                        fontWeight: FontWeight.w500,
                        textOverflow: TextOverflow.ellipsis,
                        maxLine: 2,
                      ),
                    ),
                    const Flexible(
                        flex: 1, child: Icon(Icons.keyboard_arrow_down)),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        CustomText(
          text: visitCount,
          textSize: fontSizeS,
          textOverflow: TextOverflow.ellipsis,
          maxLine: 1,
        ),
        const SizedBox(
          height: 10.0,
        ),
        Row(
          children: [
            ImageProfileCircle(
              imageUrl: videoContent.result!.user!.avatar.toString(),
              radius: 20.0,
            ),
            const SizedBox(
              width: 10.0,
            ),
            CustomText(
              text:
                  '${videoContent.result!.user!.firstname.toString()} ${videoContent.result!.user!.lastname.toString()}',
              textSize: fontSizeM,
              fontWeight: FontWeight.w500,
            ),
          ],
        ),
        videoContent.userAction?.status == 3
            ? const SizedBox(
                height: 10.0,
              )
            : const SizedBox.shrink(),
        videoContent.userAction?.status == 3
            ? QuestionButton(
                item: videoContent.userAction!,
                onPressed: onQuestionPressed,
              )
            : const SizedBox.shrink(),
      ],
    );
  }
}
