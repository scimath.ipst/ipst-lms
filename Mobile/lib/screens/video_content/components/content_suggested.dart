import 'package:flutter/material.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/video_content/video_content_screen.dart';
import 'package:project14plus/services/update_user_action.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class ContentSuggested extends StatefulWidget {
  ContentSuggested({
    Key? key,
    required this.contentId,
    required this.youtubeController,
    required this.videoType,
    required this.onChange,
    required this.videoContent,
  }) : super(key: key);
  final String contentId;
  late YoutubePlayerController youtubeController;
  final int videoType;
  final Function(ContentModel?) onChange;
  final VideoContent videoContent;

  @override
  State<ContentSuggested> createState() => _ContentSuggestedState();
}

class _ContentSuggestedState extends State<ContentSuggested> {
  late Future<List<ContentModel>> _future = ApiContent.getVideoContent(
      ContentType.SUGGESTED,
      contentId: widget.contentId);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ContentModel>>(
        future: _future,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            List<ContentModel>? items = snapshot.data;

            //Has data and data is empty
            if (snapshot.data == null) {
              return NoData(
                  title: 'ไม่มีข้อมูล',
                  onPressed: () {
                    setState(() {
                      _future = ApiContent.getVideoContent(
                          ContentType.SUGGESTED,
                          contentId: widget.contentId);
                    });
                  });
            }
            widget.onChange(items?.first);

            //Has data and data is not empty
            return Wrap(
              children: List.generate(items!.length, (index) {
                ContentModel? item = items[index];
                final createdAt = DateFormatter.formatterddMMYYYY(
                    item.createdAt!,
                    withHour: false);
                return GestureDetector(
                  onTap: () {
                    if (widget.videoType == 1 || widget.videoType == 2) {
                      UpdateUserAction.updateUserAction(
                          id: widget.videoContent.userAction!.id!,
                          watchingSeconds: widget
                              .youtubeController.value.position.inSeconds
                              .toDouble(),
                          status: widget.videoContent.userAction!.status!,
                          position: widget
                              .youtubeController.value.position.inSeconds);
                    }
                    Navigator.pushReplacement(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (context, animation, secondaryAnimation) =>
                            VideoContentScreen(
                          contentUrl: item.url!,
                          contentId: item.id!,
                        ),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          const begin = Offset(0.0, 1.0);
                          const end = Offset.zero;
                          const curve = Curves.ease;

                          var tween = Tween(begin: begin, end: end)
                              .chain(CurveTween(curve: curve));

                          return SlideTransition(
                            position: animation.drive(tween),
                            child: child,
                          );
                        },
                      ),
                    );
                  },
                  child: Container(
                    height: 100.0,
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 150.0,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(outlineRadius),
                            child: CustomCoverImage(
                              image: item.image.toString(),
                              height: 130.0,
                            ),
                          ),
                        ),
                        const SizedBox(width: 10.0),
                        Flexible(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CustomText(
                                text: item.name.toString(),
                                textSize: fontSizeM,
                                fontWeight: FontWeight.w500,
                                textOverflow: TextOverflow.ellipsis,
                                maxLine: 2,
                              ),
                              const SizedBox(height: 10.0),
                              CustomText(
                                text:
                                    '${item.user!.firstname.toString()} ${item.user!.lastname.toString()}',
                                textSize: fontSizeS,
                                fontWeight: FontWeight.w500,
                                textOverflow: TextOverflow.ellipsis,
                                maxLine: 1,
                              ),
                              CustomText(
                                text:
                                    'ผู้ชม ${item.visitCount.toString()} คน \u2022 $createdAt',
                                textSize: fontSizeS,
                                fontWeight: FontWeight.w500,
                                textOverflow: TextOverflow.ellipsis,
                                maxLine: 1,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
            );
          } else if (snapshot.hasError) {
            //Do Somthing with error show error
            return const NoData(
              title: 'เกิดข้อผิดพลาด',
            );
          }
          return const ContentListShimmer();
        });
  }
}
