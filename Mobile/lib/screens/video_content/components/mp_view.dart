import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:video_player/video_player.dart';

class MpView extends StatelessWidget {
  MpView({
    Key? key,
    required this.filePath,
  }) : super(key: key);
  String filePath;
  @override
  Widget build(BuildContext context) {
    return _BumbleBeeRemoteVideo(filePath: filePath);
  }
}

class _BumbleBeeRemoteVideo extends StatefulWidget {
  const _BumbleBeeRemoteVideo({Key? key, required this.filePath})
      : super(key: key);
  final String filePath;
  @override
  _BumbleBeeRemoteVideoState createState() => _BumbleBeeRemoteVideoState();
}

class _BumbleBeeRemoteVideoState extends State<_BumbleBeeRemoteVideo> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.asset(
      'assets/test.mp4',
      videoPlayerOptions: VideoPlayerOptions(
        mixWithOthers: true,
        allowBackgroundPlayback: true,
      ),
    );
    //     network(
    //   widget.filePath,
    //   // closedCaptionFile: _loadCaptions(),
    //   videoPlayerOptions: VideoPlayerOptions(
    //     mixWithOthers: true,
    //     allowBackgroundPlayback: true,
    //   ),
    // );

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: _controller.value.aspectRatio,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          VideoPlayer(_controller),
          _ControlsOverlay(controller: _controller),
          // VideoProgressIndicator(
          //   _controller,
          //   allowScrubbing: true,
          //   colors: VideoProgressColors(playedColor: cPrimary),
          // ),
          Row(
            children: [
              // IconButton(onPressed: (){}, icon: Icon()),
              Expanded(
                child: VideoProgressIndicator(
                  _controller,
                  allowScrubbing: true,
                  colors: VideoProgressColors(playedColor: kPrimary),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class _ControlsOverlay extends StatefulWidget {
  const _ControlsOverlay({Key? key, required this.controller})
      : super(key: key);
  final VideoPlayerController controller;

  static const List<Duration> _exampleCaptionOffsets = <Duration>[
    Duration(seconds: -10),
    Duration(seconds: -3),
    Duration(seconds: -1, milliseconds: -500),
    Duration(milliseconds: -250),
    Duration(milliseconds: 0),
    Duration(milliseconds: 250),
    Duration(seconds: 1, milliseconds: 500),
    Duration(seconds: 3),
    Duration(seconds: 10),
  ];
  static const List<double> _examplePlaybackRates = <double>[
    0.25,
    0.5,
    1.0,
    1.5,
    2.0,
    3.0,
    5.0,
    10.0,
  ];

  @override
  State<_ControlsOverlay> createState() => _ControlsOverlayState();
}

class _ControlsOverlayState extends State<_ControlsOverlay>
    with TickerProviderStateMixin {
  late AnimationController _animController;
  void _playPauseListener() => widget.controller.value.isPlaying
      ? _animController.forward()
      : _animController.reverse();
  @override
  void initState() {
    _animController = AnimationController(
      vsync: this,
      value: 0,
      duration: const Duration(milliseconds: 300),
    );
    widget.controller.addListener(_playPauseListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: Material(
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: () {
                    Duration currentPosition = widget.controller.value.position;
                    Duration targetPosition =
                        currentPosition - const Duration(seconds: 5);
                    widget.controller.seekTo(targetPosition);
                    setState(() {});
                  },
                  child: const Icon(
                    Icons.replay_10_outlined,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: () => widget.controller.value.isPlaying
                      ? widget.controller.pause()
                      : widget.controller.play(),
                  child: AnimatedIcon(
                    icon: AnimatedIcons.play_pause,
                    progress: _animController.view,
                    color: Colors.white,
                    size: 60.0,
                  ),
                ),
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: () {
                    Duration currentPosition = widget.controller.value.position;
                    Duration targetPosition =
                        currentPosition + const Duration(seconds: 5);
                    widget.controller.seekTo(targetPosition);
                    setState(() {});
                  },
                  child: const Icon(
                    Icons.forward_10_outlined,
                    color: Colors.white,
                    size: 30.0,
                  ),
                ),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: PopupMenuButton<double>(
            initialValue: widget.controller.value.playbackSpeed,
            tooltip: 'Playback speed',
            onSelected: (double speed) {
              widget.controller.setPlaybackSpeed(speed);
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuItem<double>>[
                for (final double speed
                    in _ControlsOverlay._examplePlaybackRates)
                  PopupMenuItem<double>(
                    value: speed,
                    child: Text('${speed}x'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 16,
              ),
              child: Text('${widget.controller.value.playbackSpeed}x'),
            ),
          ),
        ),
      ],
    );
  }
}
