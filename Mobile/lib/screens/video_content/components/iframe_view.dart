import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:webview_flutter/webview_flutter.dart';

class IFrameCover extends StatelessWidget {
  IFrameCover({
    Key? key,
    this.image,
    required this.url,
  }) : super(key: key);
  final String? image;
  String url;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kWhite,
      height: Get.height * 0.25,
      width: Get.width,
      child: Stack(
        children: [
          if (url != null && url != '') ...[
            ClipRRect(
              borderRadius: BorderRadius.circular(outlineRadius),
              child: CustomCoverImage(
                image: image,
                height: Get.height * 0.25,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 5, 5),
              child: Align(
                alignment: const Alignment(0, 0),
                child: InkWell(
                  onTap: () => Get.to(() => IFrameView(url: url)),
                  child: Container(
                    padding: const EdgeInsets.all(4.0),
                    decoration: BoxDecoration(
                        color: kBlack.withOpacity(.6),
                        borderRadius: const BorderRadius.all(
                            Radius.circular(outlineRadius))),
                    child: CustomText(
                      textAlign: TextAlign.center,
                      text: 'เปิด URL',
                      textSize: fontSizeM,
                      textColor: kWhite,
                    ),
                  ),
                ),
              ),
            ),
          ] else ...[
            Center(
              child: CustomText(
                text: 'ไม่สามารถแสดงเอกสารนี้ได้',
                textSize: fontSizeM,
                fontWeight: fontWeightNormal,
                textColor: kBlack,
              ),
            ),
          ],
        ],
      ),
    );
  }
}

class IFrameView extends StatefulWidget {
  IFrameView({
    Key? key,
    this.cookieManager,
    required this.url,
  }) : super(key: key);
  final CookieManager? cookieManager;
  final String url;

  @override
  State<IFrameView> createState() => _IFrameViewState();
}

class _IFrameViewState extends State<IFrameView> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Scaffold(
        body: Container(
          color: kWhite,
          height: Get.height,
          width: Get.width,
          child: Stack(
            children: [
              WebView(
                initialUrl: widget.url.isNotEmpty
                    ? widget.url.trim()
                    : 'https://project14plus.ipst.ac.th/'.trim(),
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
                onProgress: (int progress) {
                  print('WebView is loading (progress : $progress%)');
                },
                javascriptChannels: <JavascriptChannel>{
                  _toasterJavascriptChannel(context),
                },
                navigationDelegate: (NavigationRequest request) {
                  if (kDebugMode) {
                    print('allowing navigation to $request');
                  }
                  return NavigationDecision.navigate;
                },
                onPageStarted: (String url) {
                  if (kDebugMode) {
                    print('Page started loading: $url');
                  }
                },
                onPageFinished: (String url) {
                  if (kDebugMode) {
                    print('Page finished loading: $url');
                  }
                },
                gestureNavigationEnabled: true,
                backgroundColor: const Color(0x00000000),
              ),
              SafeArea(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      decoration: BoxDecoration(
                          color: kBlackDefault.withOpacity(.7),
                          borderRadius: const BorderRadius.all(
                              Radius.circular(outlineRadius))),
                      child: NavigationControls(_controller.future)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
                content: CustomText(
              text: message.message,
              fontWeight: fontWeightNormal,
              textSize: fontSizeM,
            )),
          );
        });
  }
}

class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture, {Key? key})
      : assert(_webViewControllerFuture != null),
        super(key: key);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        final WebViewController? controller = snapshot.data;
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: kWhite,
              ),
              onPressed: !webViewReady
                  ? null
                  : () async {
                      if (await controller!.canGoBack()) {
                        await controller.goBack();
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                              content: CustomText(
                            text: 'No back history item',
                            fontWeight: fontWeightNormal,
                            textSize: fontSizeM,
                          )),
                        );
                        return;
                      }
                    },
            ),
            IconButton(
              icon: const Icon(
                Icons.arrow_forward_ios,
                color: kWhite,
              ),
              onPressed: !webViewReady
                  ? null
                  : () async {
                      if (await controller!.canGoForward()) {
                        await controller.goForward();
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                              content: CustomText(
                            text: 'No forward history item',
                            fontWeight: fontWeightNormal,
                            textSize: fontSizeM,
                          )),
                        );
                        return;
                      }
                    },
            ),
            IconButton(
              icon: const Icon(
                Icons.replay,
                color: kWhite,
              ),
              onPressed: !webViewReady ? null : () => controller!.reload(),
            ),
            IconButton(
                icon: const Icon(
                  Icons.close_rounded,
                  color: kWhite,
                ),
                onPressed: () => Get.back()),
          ],
        );
      },
    );
  }
}
