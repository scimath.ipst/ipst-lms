import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class PdfView extends StatefulWidget {
  PdfView({
    Key? key,
    required this.filePath,
  }) : super(key: key);
  String filePath;

  @override
  State<PdfView> createState() => _PdfViewState();
}

class _PdfViewState extends State<PdfView> {
  int i = 1;
  int pageLength = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kWhite,
      height: Get.height * 0.25,
      width: Get.width,
      child: Stack(
        children: [
          if (widget.filePath != '') ...[
            PDF(
                swipeHorizontal: true,
                onRender: (index) => setState(() => pageLength = index ?? 0),
                onPageChanged: (m, n) {
                  setState(() {
                    pageLength = n!;
                    i = m! + 1;
                  });
                }).cachedFromUrl(
              widget.filePath,
              placeholder: (progress) => Center(
                child: CustomText(
                  text: '$progress %',
                  textSize: fontSizeM,
                  fontWeight: fontWeightNormal,
                ),
              ),
              errorWidget: (error) => Center(
                child: CustomText(
                  text: 'เกิดข้อผิดพลาด',
                  textSize: fontSizeM,
                  fontWeight: fontWeightNormal,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 5, 5, 0),
              child: Align(
                alignment: const Alignment(1, -1),
                child: Container(
                  padding: const EdgeInsets.all(.5),
                  decoration: BoxDecoration(
                      color: kBlackDefault.withOpacity(.5),
                      borderRadius: const BorderRadius.all(
                          Radius.circular(outlineRadius))),
                  child: CustomText(
                    textAlign: TextAlign.center,
                    text: '$i / $pageLength',
                    textSize: fontSizeM,
                    textColor: kWhite,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 5, 5),
              child: Align(
                alignment: const Alignment(1, 1),
                child: InkWell(
                  onTap: () async {
                    if (await canLaunch(widget.filePath)) {
                      await launch(widget.filePath);
                    } else {
                      throw 'Could not launch ${widget.filePath}';
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.all(4.0),
                    decoration: BoxDecoration(
                        color: kBlackDefault.withOpacity(.5),
                        borderRadius: const BorderRadius.all(
                            Radius.circular(outlineRadius))),
                    child: CustomText(
                      textAlign: TextAlign.center,
                      text: 'เปิดเอกสาร',
                      textSize: fontSizeM,
                      textColor: kWhite,
                    ),
                  ),
                ),
              ),
            ),
          ] else ...[
            Center(
              child: CustomText(
                text: 'ไม่สามารถแสดงเอกสารนี้ได้',
                textSize: fontSizeM,
                fontWeight: fontWeightNormal,
                textColor: kBlack,
              ),
            ),
          ],
        ],
      ),
    );
  }
}
