import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api_content/api_content.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/screens/video_questions/video_questions_screen.dart';
import 'package:project14plus/services/update_user_action.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

/*
  status :
    0 = Visited
    1 = Watching
    2 = Watched
    3 = Completed
  type :
    1 = Youtube video : need to provide youtubeVideoId
    2 = Video MP4 : need to provide OneDrive/GoogleDrive Url
    3 = PDF : need to provide OneDrive/GoogleDrive Url
    4 = iFrame : need to provide filePath
  scoreType :
    0 = ไม่เก็บคะแนน
    1 = เก็บคะแนนสูงสุด
    2 = เก็บคะแนนครั้งล่าสุด
  approver : Map with Quality Control Office
  approvedStatus : Every content need to be approved first!
    -1 = Pending
    0  = Not approved
    1  = Approved
*/
class VideoContentBody extends StatefulWidget {
  const VideoContentBody({
    Key? key,
    required this.contentUrl,
    required this.contentId,
    this.watchingSeconds,
  }) : super(key: key);

  final String contentUrl;
  final String contentId;
  final double? watchingSeconds;

  @override
  State<VideoContentBody> createState() => _VideoContentBodyState();
}

class _VideoContentBodyState extends State<VideoContentBody>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  //-------------------- Alert Dialog --------------------
  int _counter = 0;
  late StreamController<int> _events;
  Timer _timer = Timer(const Duration(seconds: 0), () {});

  late ContentController controller = Get.find<ContentController>();
  late AnimationController animationController;
  late YoutubePlayerController youtubeController;

  bool _isPlayerReady = false;
  bool isFullScreen = false;
  double playbackRate = 1;
  int startAt = 0;
  bool isIframe = false;

  late Future<VideoContent> futureContent =
      ApiContent.getContent(widget.contentUrl);

  List<double> playback = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2.0];

  int videoType = 1;

  ContentModel? playNext;
  VideoContent? videoContent;

  bool goToVideoQuestion = false;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        if (videoType == 1 || videoType == 2) {
          UpdateUserAction.updateUserAction(
              id: videoContent!.userAction!.id!,
              watchingSeconds:
                  youtubeController.value.position.inSeconds.toDouble(),
              status: videoContent!.userAction!.status!,
              position: youtubeController.value.position.inSeconds);
        }
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance?.addObserver(this);

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {});
    animationController.repeat(reverse: false);

    if (widget.watchingSeconds != null && widget.watchingSeconds != 0) {
      setState(() => startAt = widget.watchingSeconds!.round());
    }

    youtubeController = YoutubePlayerController(
      initialVideoId: widget.contentUrl,
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
        startAt: startAt,
      ),
    )..addListener(listener);
    youtubeController.setVolume(100);
    youtubeController.setPlaybackRate(playbackRate);

    super.initState();
  }

  Future<void> listener() async {
    if (_isPlayerReady && mounted && !youtubeController.value.isFullScreen) {}

    if (MediaQuery.of(context).orientation == DeviceOrientation.landscapeLeft ||
        MediaQuery.of(context).orientation ==
            DeviceOrientation.landscapeRight) {
      isFullScreen = true;
      controller.updateIsFullScreen(true);
    }

    if (_isPlayerReady && videoContent!.result!.videoQuestions!.isNotEmpty) {
      final temp = videoContent!.result!.videoQuestions!.firstWhere(
          (it) => it.at == youtubeController.value.position.inSeconds,
          orElse: () {
        return QuestionModel(question: null);
      });
      if (temp.question != null &&
          temp.question != '' &&
          goToVideoQuestion == false) {
        youtubeController.pause();
        goToVideoQuestion = true;
        await Get.to(() => VideoQuestionsScreen(
              model: videoContent!,
              at: temp.at,
              maxIndex: videoContent!.result!.videoQuestions!.length,
            ))?.then((value) {
          goToVideoQuestion = false;
          youtubeController.seekTo(Duration(
              seconds: youtubeController.value.position.inSeconds + 1));
          youtubeController.play();
        });
      }
    }
  }

  @override
  void deactivate() {
    youtubeController.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);

    animationController.removeListener(() {});
    youtubeController.removeListener(listener);
    _isPlayerReady = false;

    youtubeController.dispose();
    animationController.dispose();
    super.dispose();
  }

  void _startTimer() {
    _counter = 10;
    if (_timer != null) _timer.cancel();
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_counter > 0) {
        _counter--;
      } else {
        _timer.cancel();
        Get.back();

        //Go to New Content
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                VideoContentScreen(
              contentUrl: playNext?.url ?? "",
              contentId: playNext?.id ?? "",
            ),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              const begin = Offset(0.0, 1.0);
              const end = Offset.zero;
              const curve = Curves.ease;

              var tween =
                  Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

              return SlideTransition(
                position: animation.drive(tween),
                child: child,
              );
            },
          ),
        );
      }
      _events.add(_counter);
    });
  }

  setController(int type) {
    switch (type) {
      case 1:
        break;
      default:
        animationController.removeListener(() {});
        youtubeController.removeListener(listener);
        _isPlayerReady = false;
        youtubeController.dispose();
        animationController.dispose();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FutureBuilder<VideoContent>(
            future: futureContent,
            builder: (BuildContext content, AsyncSnapshot<dynamic> snapshot) {
              VideoContent? item = snapshot.data;

              if (snapshot.hasData) {
                if (item == null) {
                  return const NoData();
                }

                // item.result!.type = 4;
                videoContent = item;
                setController(item.result!.type!);
                videoType = item.result!.type!;
                return Column(
                  children: [
                    item.result?.type == 1
                        ? YoutubePlayerBuilder(
                            onExitFullScreen: () {
                              SystemChrome.setPreferredOrientations([
                                DeviceOrientation.landscapeLeft,
                                DeviceOrientation.landscapeRight,
                                DeviceOrientation.portraitUp,
                              ]);
                              setState(() {
                                isFullScreen = false;
                                controller.updateIsFullScreen(false);
                              });
                            },
                            onEnterFullScreen: () {
                              setState(() {
                                isFullScreen = true;
                                controller.updateIsFullScreen(true);
                              });
                            },
                            player: YoutubePlayer(
                              controlsTimeOut: const Duration(seconds: 60),
                              controller: youtubeController,
                              showVideoProgressIndicator: false,
                              progressIndicatorColor: kPrimary,
                              liveUIColor: kPrimary,
                              bottomActions: [
                                CurrentPosition(controller: youtubeController),
                                IconButton(
                                  onPressed: () => playbackSetting(),
                                  icon: const Icon(
                                    Icons.speed,
                                    color: kWhite,
                                  ),
                                ),
                                ProgressBar(
                                  controller: youtubeController,
                                  colors: const ProgressBarColors(
                                    backgroundColor: Color(0xFFBDBDBD),
                                    playedColor: kPrimary,
                                    bufferedColor: Color(0xFFF5F5F5),
                                    handleColor: kPrimary,
                                  ),
                                  isExpanded: true,
                                ),
                                RemainingDuration(
                                    controller: youtubeController),
                                FullScreenButton(controller: youtubeController),
                              ],
                              topActions: <Widget>[
                                const SizedBox(width: 8.0),
                                Expanded(
                                  child: Text(
                                    youtubeController.metadata.title,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                              ],
                              onReady: () {
                                _isPlayerReady = true;
                              },
                              onEnded: (data) {
                                if (item.result?.type == 1 ||
                                    item.result?.type == 2) {
                                  UpdateUserAction.updateUserAction(
                                      isOnEnd: true,
                                      id: item.userAction!.id!,
                                      watchingSeconds: youtubeController
                                          .value.position.inSeconds
                                          .toDouble(),
                                      status: item.userAction!.status!,
                                      position: youtubeController
                                          .value.position.inSeconds);
                                }
                                if (item.result!.questions!.isNotEmpty) {
                                  youtubeController.pause();
                                  _events = StreamController<int>();
                                  _events.add(10);
                                  _startTimer();
                                  showDialog<String>(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context) =>
                                        CustomAlert(
                                      buttonKey: "เริ่มทำแบบทดสอบ",
                                      onpressed: () async {
                                        _timer.cancel();
                                        Get.back();
                                        Get.to(() => QuestionScreen(
                                              model: item,
                                            ));
                                        //                                   model: videoContent!,
                                        // at: temp.at,
                                        // maxIndex: videoContent!.result!.videoQuestions!.length,
                                      },
                                      onCancelPressed: () {
                                        _timer.cancel();
                                        Get.back();
                                      },
                                      content: StreamBuilder<int>(
                                          stream: _events.stream,
                                          builder: (BuildContext context,
                                              AsyncSnapshot<int> snapshot) {
                                            return Wrap(
                                              children: [
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    CustomText(
                                                      text:
                                                          'กดปุ่มเพิ่มเริ่มทำแบบทดสอบ หรือรอเพื่อเล่นเนื้อหาถัดไปในอีก ${snapshot.data.toString()}',
                                                      textSize: fontSizeM * 1.2,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            );
                                          }),
                                    ),
                                  );
                                } else {
                                  //Play next
                                  youtubeController.pause();
                                  _events = StreamController<int>();
                                  _events.add(10);
                                  _startTimer();
                                  showDialog<String>(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context) =>
                                        CustomAlert(
                                      onCancelPressed: () {
                                        _timer.cancel();
                                        Get.back();
                                      },
                                      content: StreamBuilder<int>(
                                          stream: _events.stream,
                                          builder: (BuildContext context,
                                              AsyncSnapshot<int> snapshot) {
                                            return Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                CustomText(
                                                  text:
                                                      'กำลังที่จะเล่นเนื้อหาต่อไป ${snapshot.data.toString()}',
                                                  textSize: fontSizeM * 1.2,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ],
                                            );
                                          }),
                                    ),
                                  );
                                }
                              },
                            ),
                            builder: (context, player) {
                              return isFullScreen
                                  ? SafeArea(
                                      child: Column(
                                        children: [
                                          Container(
                                              decoration: const BoxDecoration(
                                                  color: kBlack),
                                              width: context.width,
                                              height: context.height,
                                              child: Center(
                                                child: FittedBox(
                                                  fit: BoxFit.fitWidth,
                                                  child: SizedBox(
                                                    width: context.width,
                                                    height: context.height,
                                                    child: player,
                                                  ),
                                                ),
                                              )),
                                        ],
                                      ),
                                    )
                                  : Column(
                                      children: [
                                        player,
                                      ],
                                    );
                            })
                        : item.result?.type == 2
                            ? MpView(filePath: item.result?.filePath ?? '')
                            : item.result?.type == 3
                                ? PdfView(filePath: item.result?.filePath ?? '')
                                : item.result?.type == 4
                                    ? IFrameCover(
                                        image: item.result?.image ?? '',
                                        url: item.result?.filePath ?? '',
                                      )
                                    : Container(),
                    if (!isFullScreen) ...[
                      Expanded(
                        child: GetBuilder<ContentController>(
                            builder: (_contentController) {
                          return !_contentController.desCheck
                              ? CustomScrollView(
                                  shrinkWrap: true,
                                  slivers: [
                                    SliverPadding(
                                        padding: const EdgeInsets.fromLTRB(
                                            kGapS, kGap, kGapS, 0),
                                        sliver: SliverToBoxAdapter(
                                          child: ContentTitle(
                                            videoContent: item,
                                            onQuestionPressed: () {
                                              youtubeController.pause();
                                              Get.to(
                                                () =>
                                                    QuestionScreen(model: item),
                                              );
                                            },
                                          ),
                                        )),
                                    SliverPadding(
                                        padding: const EdgeInsets.fromLTRB(
                                            kGapS, 0, kGapS, 0),
                                        sliver: SliverToBoxAdapter(
                                          child: Column(
                                            children: const [
                                              SizedBox(height: 10.0),
                                              Divider(
                                                  color: kBlackDefault,
                                                  thickness: .2),
                                              SizedBox(height: 10.0),
                                            ],
                                          ),
                                        )),
                                    SliverPadding(
                                      padding: const EdgeInsets.fromLTRB(
                                          kGapS, 0, kGapS, 0),
                                      sliver: SliverToBoxAdapter(
                                        child: CustomText(
                                          text: 'วีดีโอแนะนำ',
                                          textSize: fontSizeXL,
                                          fontWeight: fontWeightBold,
                                        ),
                                      ),
                                    ),
                                    const SliverPadding(
                                      padding: EdgeInsets.fromLTRB(
                                          kGapS, 0, kGapS, 0),
                                      sliver: SliverToBoxAdapter(
                                        child: SizedBox(
                                          height: 10.0,
                                        ),
                                      ),
                                    ),
                                    SliverPadding(
                                      padding: const EdgeInsets.fromLTRB(
                                          kGapS, 0, kGapS, kGap),
                                      sliver: SliverToBoxAdapter(
                                        child: ContentSuggested(
                                          contentId: widget.contentId,
                                          youtubeController: youtubeController,
                                          videoType: videoType,
                                          onChange: (contentModel) {
                                            playNext = contentModel;
                                          },
                                          videoContent: item,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : ContentDescription(
                                  videoContent: item,
                                );
                        }),
                      ),
                    ],
                  ],
                );
              }

              return Container(
                color: kBlack,
                height: Get.height * 0.25,
                width: Get.width,
                child: const Center(
                  child: CircularProgressIndicator(
                    color: kWhite,
                  ),
                ),
              );
            }),
        if (!isFullScreen)
          ElevatedButton(
            style: ButtonStyle(
              overlayColor: MaterialStateProperty.resolveWith(
                  (states) => Colors.transparent),
              shadowColor: MaterialStateProperty.resolveWith(
                  (states) => Colors.transparent),
              backgroundColor: MaterialStateProperty.resolveWith(
                  (states) => Colors.black.withOpacity(0.7)),
              shape: MaterialStateProperty.resolveWith(
                  (states) => const CircleBorder()),
            ),
            child: const Icon(
              Icons.keyboard_arrow_down,
              color: Colors.white,
            ),
            onPressed: () {
              if ((videoType == 1 || videoType == 2) &&
                  videoContent!.userAction!.id != null) {
                UpdateUserAction.updateUserAction(
                    id: videoContent!.userAction!.id!,
                    watchingSeconds:
                        youtubeController.value.position.inSeconds.toDouble(),
                    status: videoContent!.userAction!.status!,
                    position: youtubeController.value.position.inSeconds);
              }

              controller.setDesCheck(false);

              Get.back();
            },
          ),
      ],
    );
  }

  Future playbackSetting() {
    return showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(outlineRadius)),
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return BottomSheet(
          onClosing: () {},
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (BuildContext context, setState) {
              return SafeArea(
                top: true,
                child: CustomScrollView(
                  shrinkWrap: true,
                  slivers: [
                    SliverAppBar(
                      pinned: true,
                      automaticallyImplyLeading: false,
                      title: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Get.back();
                        },
                        child: Stack(
                          children: [
                            Align(
                              alignment: const Alignment(-0.8, 0),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: CustomText(
                                  text: 'Cancel',
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            const Align(
                              alignment: Alignment(-1, 0),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Icon(Icons.close_rounded),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: SingleChildScrollView(
                          child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 16, 10, 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: List.generate(
                              playback.length,
                              (index) => GestureDetector(
                                    behavior: HitTestBehavior.opaque,
                                    onTap: () {
                                      setState(
                                          () => playbackRate = playback[index]);
                                      youtubeController
                                          .setPlaybackRate(playbackRate);
                                      Get.back();
                                    },
                                    child: Stack(
                                      children: [
                                        Align(
                                          alignment: const Alignment(-0.8, 0),
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 10, 0, 10),
                                            child: CustomText(
                                              text: playback[index] == 1
                                                  ? "Normal"
                                                  : "${playback[index].toString()}x",
                                              textSize: playbackRate ==
                                                      playback[index]
                                                  ? 18
                                                  : 16,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: const Alignment(-1, 0),
                                          child: playbackRate == playback[index]
                                              ? const Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      0, 10, 0, 10),
                                                  child:
                                                      Icon(Icons.check_rounded),
                                                )
                                              : null,
                                        )
                                      ],
                                    ),
                                  )),
                        ),
                      )),
                    ),
                  ],
                ),
              );
            });
          },
        );
      },
    );
  }
}
