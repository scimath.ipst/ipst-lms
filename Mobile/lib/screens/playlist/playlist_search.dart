import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/widgets.dart';

class PlaylistSearch extends StatelessWidget {
  PlaylistSearch({Key? key, required this.sorting, required this.keyword})
      : super(key: key);
  Level sorting = Level();
  TextEditingController keyword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Scaffold(
          appBar: AppBar(
            title: CustomText(
              text: 'ค้นหา',
              textSize: fontSizeL,
              fontWeight: FontWeight.w600,
            ),
          ),
          body: Container(
            padding: const EdgeInsets.fromLTRB(10.0, 16.0, 10.0, 16.0),
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      CustomTextFromField(
                        label: 'ค้นหา ',
                        controller: keyword,
                      ),
                      const SizedBox(
                        height: 15.0,
                      ),
                      CustomDropdown(
                        title: 'เรียงลำดับ',
                        value: sorting,
                        onChange: _onChangeSorting,
                        dropdownType: DropdownType.SORTING_PLAYLIST,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              onTap: () {
                Get.back(result: {
                  "keyword": keyword,
                  "sorting": sorting,
                });
              },
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 50.0,
                decoration: BoxDecoration(
                    color: kPrimary, borderRadius: BorderRadius.circular(8.0)),
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'ค้นหา',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _onChangeSorting(Level data) {
    sorting = data;
    if (sorting.id == "") sorting = Level();
  }
}
