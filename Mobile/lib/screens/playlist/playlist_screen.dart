import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/widgets/no_more_data.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:visibility_detector/visibility_detector.dart';

/*
  Sorting
    1 = ใหม่ที่สุด
    2 = เก่าที่สุด
    3 = ชื่อ ก-ฮ
    4 = ชื่อ ฮ-ก
 */

class PlayListScreen extends StatefulWidget {
  PlayListScreen({Key? key, required this.channelsModel}) : super(key: key);
  ChannelsModel channelsModel;
  @override
  State<PlayListScreen> createState() => _PlayListScreenState();
}

class _PlayListScreenState extends State<PlayListScreen> {
  String? sKeyword;

  List<ChannelsModel> _data = [];

  Level sorting = Level();

  TextEditingController keyword = TextEditingController();

  late ScrollController scrollController;

  int page = 0;

  bool isGoToTop = false;
  bool isLoading = false;
  bool isEnded = false;

  @override
  void initState() {
    loadData();
    scrollController = ScrollController()..addListener(loadMore);

    super.initState();
  }

  @override
  void dispose() {
    scrollController.removeListener(loadMore);
    super.dispose();
  }

  Future<void> onRefresh() async {
    setState(() {
      page = 0;
      isLoading = false;
      isEnded = false;
      _data = [];
      isGoToTop = false;
    });
    loadData();
  }

  Future<void> loadData() async {
    if (!isEnded && !isLoading) {
      try {
        page += 1;
        setState(() {
          isLoading = true;
        });

        ApiContent.getPlaylist(widget.channelsModel.url!, page,
                keyword: sKeyword,
                sorting:
                    sorting.id != null ? int.parse(sorting.id.toString()) : 0)
            .then((value) {
          for (var i = 0; i < value.result!.length; i++) {
            _data.add(value.result![i]);
          }
          setState(() {
            _data;
            if (_data.length >= (value.paginate?.total ?? 0)) {
              isEnded = true;
              isLoading = false;
            } else if (value != null) {
              isLoading = false;
            }
          });
        });
      } catch (e) {
        log("$e");
      }
    }
  }

  void onLoadMore(info) async {
    if (info.visibleFraction > 0 && !isEnded && !isLoading) {
      await loadData();
    }
  }

  void _search() async {
    final result = await Get.to(
      () => PlaylistSearch(
        keyword: keyword,
        sorting: sorting,
      ),
      duration: const Duration(milliseconds: 400),
      transition: Transition.size,
      fullscreenDialog: true,
    );
    if (result != null) {
      setState(() {
        keyword = result["keyword"];
        sKeyword = keyword.text;
        sorting = result["sorting"];
      });
      onRefresh();
    } else {
      if (sKeyword != null) {
        keyword.text = sKeyword!;
      } else {
        keyword.clear();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: widget.channelsModel.name.toString(),
          textSize: fontSizeXL,
          fontWeight: FontWeight.w600,
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              _search();
            },
          ),
        ],
      ),
      body: Stack(
        children: [
          RefreshIndicator(
            onRefresh: () async => onRefresh(),
            color: kPrimary,
            child: CustomScrollView(
              scrollDirection: Axis.vertical,
              controller: scrollController,
              slivers: [
                isEnded && _data.isEmpty
                    ? SliverFillRemaining(
                        child: Padding(
                          padding: kPadding,
                          child: NoData(onPressed: (() {
                            onRefresh();
                          })),
                        ),
                      )
                    : SliverToBoxAdapter(
                        child: Padding(
                          padding: kPadding,
                          child: Column(
                            children: [
                              _data.isEmpty
                                  ? const SizedBox.shrink()
                                  : GridView.builder(
                                      itemCount: _data.length,
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: isMobile
                                            ? (orientation ==
                                                    Orientation.portrait)
                                                ? 2
                                                : 3
                                            : (orientation ==
                                                    Orientation.portrait)
                                                ? 4
                                                : 5,
                                        mainAxisSpacing: 0.0,
                                        crossAxisSpacing: 10,
                                        mainAxisExtent: 200,
                                        // childAspectRatio: 2,
                                      ),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        ChannelsModel item = _data[index];

                                        return InkWell(
                                          focusColor: Colors.transparent,
                                          hoverColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          splashColor: Colors.transparent,
                                          onTap: () {
                                            slideToVideoPlaylistContent(
                                                item.url!, item.name!);
                                          },
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              CustomCoverImage(
                                                  image: item.image.toString()),
                                              const SizedBox(height: 5.0),
                                              CustomText(
                                                text: item.name.toString(),
                                                textSize: fontSizeM,
                                                fontWeight: FontWeight.w600,
                                                maxLine: 2,
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                              if (isEnded && _data.isNotEmpty) ...[
                                const NoMoreData(),
                              ],
                              if (!isEnded) ...[
                                VisibilityDetector(
                                  key: const Key('loader-widget'),
                                  onVisibilityChanged: onLoadMore,
                                  child: const ProgramShimmer(
                                    length: 6,
                                    gridHeight: 220,
                                  ),
                                ),
                              ],
                            ],
                          ),
                        ),
                      )
              ],
            ),
          ),
          CustomCircleButton(
            ignoring: !isGoToTop && _data.isNotEmpty,
            isShow: isGoToTop && _data.isNotEmpty,
            onPressed: () {
              scrollController.animateTo(
                0.0,
                curve: Curves.easeOut,
                duration: const Duration(milliseconds: 400),
              );
            },
          ),
        ],
      ),
    );
  }

  void loadMore() async {
    if (scrollController.offset != scrollController.position.minScrollExtent) {
      setState(() => isGoToTop = true);
    } else {
      setState(() => isGoToTop = false);
    }
  }
}
