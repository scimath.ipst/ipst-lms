import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/dashboards/dashboard_bliding.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/screens/onboarding/onboarding_screen.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/widgets/widgets.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = "/splashScreen";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController animation;
  late AnimationController splashAnim;
  late Animation<double> _fadeInFadeOut;
  int count = 0;
  @override
  void initState() {
    super.initState();
    initStart();
  }

  initStart() async {
    animation = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 800),
    );
    splashAnim = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1500),
    );
    _fadeInFadeOut = Tween<double>(begin: 0.5, end: 1.0).animate(animation);
    animation.addStatusListener((status) async {
      if (status == AnimationStatus.completed) {
        setState(() {
          count++;
        });
        if (count < 3) {
          animation.reverse();
        } else {
          animation.dispose();

          // CHECK: if user has logged in
          final appIntro = await LocalStorage.getAppIntro();
          if (appIntro == true) {
            final UserController _userController = Get.find<UserController>();
            await _userController.initUser();

            Get.offAll(() => NavigationScreen());
          } else {
            Get.offAll(() => const OnboardingScreen());
          }
        }
      } else if (status == AnimationStatus.dismissed) {
        animation.forward();
      }
    });
    splashAnim.forward();
    animation.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: kAppColor,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(PATH_AUTH_BG01),
          fit: BoxFit.cover,
        ),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FadeTransition(
              opacity: _fadeInFadeOut,
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Image.asset(
                  "assets/images/splash-03.png",
                  fit: BoxFit.contain,
                ),
              ),
            ),
            CustomText(
              text: "Project 14+",
              fontWeight: fontWeightBold,
              textSize: fontSizeXXL,
              textColor: kPrimary,
            ),
          ],
        ),
      ),
    );
  }
}
