import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/onboarding_model.dart';
import 'package:project14plus/model/pdpa_model.dart';
import 'package:project14plus/screens/navigation_screen.dart';
import 'package:project14plus/screens/onboarding/onboarding_pages.dart';
import 'package:project14plus/screens/pdpa/pdpa_screen.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';
import 'package:project14plus/widgets/control_widget/control_widget.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  Future<Map<String, dynamic>?>? _future;
  final introKey = GlobalKey<IntroductionScreenState>();
  final pdpaTest =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  bool acceptPDPA = true;

  void _onIntroEnd() async {
    // await LocalStorage.save(PREF_SKIP_INTRO, true);
    // Get.offAll(() => SignInMenuScreen(isFirstState: true));
  }

  @override
  void initState() {
    // _future = ApiService.processRead(
    //   "cms-content",
    //   input: {"url": "privacy-policy"},
    // );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    PageDecoration pageDecoration = const PageDecoration(
      bodyPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );
    String _body =
        "Duis id commodo leo, vitae tristique massa. Donec quis nisi a velit gravida ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Pellentesque ornare scelerisque diam eu viverra";
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: Get.height,
            width: Get.width,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(PATH_AUTH_BG01),
                fit: BoxFit.cover,
              ),
            ),
            child: IntroductionScreen(
              key: introKey,
              globalBackgroundColor: Colors.transparent,
              pages: [
                onboardingPages(
                  OnboardingModel(
                    title: 'Verification',
                    body: _body,
                    image: 'assets/images/splash-01.png',
                  ),
                ),
                onboardingPages(
                  OnboardingModel(
                    title: 'Verification',
                    body: _body,
                    image: 'assets/images/splash-02.png',
                  ),
                ),
                onboardingPages(
                  OnboardingModel(
                    title: 'Verification',
                    body: _body,
                    image: 'assets/images/splash-03.png',
                  ),
                  SizedBox(
                    height: 35,
                    width: 310,
                    child: ButtonMain(
                      title: "เริ่มต้นใช้งาน",
                      onPressed: () async {
                        final PdpaModel model =
                            PdpaModel(result: true, date: DateTime.now());
                        await LocalStorage.saveAppIntro(true);
                        await LocalStorage.savePdpa(model);
                        Get.offAll(() => NavigationScreen());
                      },
                    ),
                  ),
                ),
              ],
              onDone: _onIntroEnd,
              showSkipButton: false,
              skipOrBackFlex: 0,
              nextFlex: 0,
              showBackButton: false,
              showNextButton: false,
              showDoneButton: false,
              back: const Icon(Icons.arrow_back),
              skip: const Text('Skip',
                  style: TextStyle(fontWeight: FontWeight.w600)),
              next: const Icon(Icons.arrow_forward),
              done: const Text('Done',
                  style: TextStyle(fontWeight: FontWeight.w600)),
              curve: Curves.fastLinearToSlowEaseIn,
              controlsMargin: const EdgeInsets.symmetric(vertical: 65),
              controlsPadding: const EdgeInsets.all(0),
              dotsDecorator: DotsDecorator(
                shape: const CircleBorder(),
                color: kBlueColor.withOpacity(.65),
                activeColor: kBlueColor,
                activeSize: const Size(30.0, 9.0),
                activeShape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(outlineRadius)),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: kButtonPadding,
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () => Get.to(() => PdpaScreen(
                      showBackButton: true,
                      showFloatingButton: false,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.check_box,
                      color: kBlueColor,
                    ),
                    SizedBox(width: 10),
                    Text(
                      'เมื่อกดปุ่มเริ่มต้นแล้ว, คุณยอมรับ ',
                      style: TextStyle(fontSize: fontSizeXS),
                    ),
                    Text(
                      "นโยบายความเป็นส่วนตัว",
                      style: TextStyle(fontSize: fontSizeXS),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding:
                  const EdgeInsets.only(top: kToolbarHeight * 0.7, right: kGap),
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () async {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => CustomAlert(
                      content: Center(
                        child: CustomText(
                          text: 'คุณต้องการข้ามการแนะนำแอป ?',
                          textSize: fontSizeM,
                        ),
                      ),
                      title: 'ข้ามการแนะนำ',
                      onpressed: () async {
                        Get.back();

                        final PdpaModel model =
                            PdpaModel(result: true, date: DateTime.now());
                        await LocalStorage.saveAppIntro(true);
                        await LocalStorage.savePdpa(model);
                        Get.offAll(() => NavigationScreen());
                      },
                    ),
                  );
                },
                child: const Text(
                  'ข้าม',
                  style: TextStyle(
                    fontSize: fontSizeXS,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w700,
                    color: kBlueColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Future<String?> pdpaAlert(BuildContext context) {
  //   return showDialog<String>(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return FutureBuilder<Map<String, dynamic>?>(
  //         future: _future,
  //         builder: (context, snapshot) {
  //           CmsContentModel? item;

  //           if (snapshot.hasData) {
  //             item = CmsContentModel.fromJson(snapshot.data!['result']);

  //             if (item == null) {
  //               return const SizedBox.shrink();
  //             }

  //             final _title =
  //                 item.title == '' ? 'นโยบายความเป็นส่วนตัว' : item.title;
  //             final _content = item.content;
  //             return AlertDialog(
  //               title: Text(
  //                 _title,
  //                 style: subtitle1.copyWith(fontWeight: FontWeight.w500),
  //               ),
  //               content: Scrollbar(
  //                 isAlwaysShown: true,
  //                 trackVisibility: false,
  //                 child: Container(
  //                   height: Get.height * 0.4,
  //                   width: Get.width,
  //                   padding: const EdgeInsets.fromLTRB(0, 0, kGap, 0),
  //                   decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.circular(kButtonRadius),
  //                   ),
  //                   child: ListView(
  //                     children: [
  //                       HtmlContent(content: _content),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //               actions: <Widget>[
  //                 TextButton(
  //                   onPressed: () => Get.back(),
  //                   child: Text(
  //                     Utils.translate('close', context: context),
  //                   ),
  //                 ),
  //               ],
  //             );
  //           }
  //           return const Center(
  //             child: CircularProgressIndicator(),
  //           );
  //         },
  //       );
  //     },
  //   );
  // }
}
