import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:project14plus/model/onboarding_model.dart';

PageViewModel onboardingPages(OnboardingModel model, [Widget? footer]) {
  return PageViewModel(
    title: model.title,
    bodyWidget: Text(
      model.body,
      textAlign: TextAlign.center,
      style: const TextStyle(
        color: Color(0xFF77838F),
      ),
    ),
    image: Image.asset(model.image, height: 200, width: 200),
    decoration: const PageDecoration(
      bodyPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.transparent,
      imagePadding: EdgeInsets.zero,
    ),
    footer: footer,
  );
}
