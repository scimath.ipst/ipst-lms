import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';

class NoInternetScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kWhite,
      child: Center(
        child: ListView(
          shrinkWrap: true,
          children: [
            // SvgPicture.asset(
            //   "assets/icons/error_network.svg",
            //   height: DeviceUtils.size(context).height * 0.4,
            //   width: DeviceUtils.size(context).width * 0.4,
            // ),
            const SizedBox(
              height: 30.0,
            ),
            CustomText(
              text: "Bad Gateway",
              textSize: 20.0,
              maxLine: 2,
              textColor: kBlack,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
