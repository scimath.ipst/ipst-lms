import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/content/question_model.dart';
import 'package:project14plus/widgets/buttons/button_main.dart';
import 'package:project14plus/widgets/control_widget/custom_text.dart';

class VideoQuestionDialog extends StatelessWidget {
  const VideoQuestionDialog({
    Key? key,
    required this.selected,
    required this.title,
    required this.correctChoice,
  }) : super(key: key);
  final String title;
  final List<Choices> selected;
  final List<Choices> correctChoice;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBlueColor.withOpacity(0.7),
      body: Center(
          child: Container(
        decoration: const BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.all(
            Radius.circular(outlineRadius),
          ),
        ),
        constraints: BoxConstraints(
          maxWidth: Get.width * 0.95 > 370 ? 370 : Get.width * 0.95,
          maxHeight: Get.width * 0.7 > 550 ? 550 : Get.height * 0.7,
          minWidth: Get.width * 0.9 > 350 ? 350 : Get.width * 0.9,
          minHeight: Get.width * 0.5 > 500 ? 500 : Get.height * 0.5,
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: CupertinoScrollbar(
              isAlwaysShown: true,
              child: CustomScrollView(
                shrinkWrap: false,
                slivers: [
                  SliverPadding(
                    padding: kPadding,
                    sliver: SliverToBoxAdapter(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            text: title,
                            textSize: fontSizeM,
                            fontWeight: fontWeightBold,
                          ),
                          const SizedBox(height: 10),
                          const Divider(color: kBlackDefault, thickness: .2),
                          const SizedBox(height: 10),
                          CustomText(
                            text: "คำตอบที่เลือก",
                            textSize: fontSizeM,
                            fontWeight: fontWeightNormal,
                          ),
                          const SizedBox(height: 10),
                          Column(
                            children: List.generate(selected.length, (index) {
                              Choices choices = selected[index];
                              return Padding(
                                padding: const EdgeInsets.only(bottom: kGapS),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: correctChoice.contains(choices)
                                            ? kGreenColor
                                            : kRedColor,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(outlineRadius),
                                        ),
                                      ),
                                      padding: const EdgeInsets.fromLTRB(
                                          kQuarterGap,
                                          kHalfGap,
                                          kQuarterGap,
                                          kHalfGap),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: Get.width * 0.1,
                                            child: Center(
                                              child: Icon(
                                                correctChoice.contains(choices)
                                                    ? Icons.check_circle_sharp
                                                    : Icons.cancel_sharp,
                                                size: 30,
                                                color: kWhite,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: Get.width * 0.6,
                                            child: CustomText(
                                              text: choices.text.toString(),
                                              textColor:
                                                  selected.contains(choices)
                                                      ? kWhite
                                                      : kBlack,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    if (Set.of(selected)
                                            .containsAll(correctChoice) &&
                                        choices.explaination != '') ...[
                                      const SizedBox(height: 10),
                                      SizedBox(
                                        width: Get.width * 0.7,
                                        child: CustomText(
                                          text:
                                              'คำอธิบาย : ${choices.explaination}',
                                          textColor: kBlackDefault,
                                          textSize: fontSizeS,
                                        ),
                                      ),
                                    ],
                                  ],
                                ),
                              );
                            }),
                          ),
                          if (!Set.of(selected).containsAll(correctChoice)) ...[
                            const SizedBox(height: 10),
                            const Divider(color: kBlackDefault, thickness: .2),
                            const SizedBox(height: 10),
                            CustomText(
                              text: "คำตอบที่ถูก",
                              textSize: fontSizeM,
                              fontWeight: fontWeightNormal,
                            ),
                            const SizedBox(height: 10),
                            Column(
                              children:
                                  List.generate(correctChoice.length, (index) {
                                Choices choices = correctChoice[index];
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: kGapS),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: kBlackDefault2.withOpacity(.2),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(outlineRadius),
                                          ),
                                        ),
                                        padding: const EdgeInsets.fromLTRB(
                                            kQuarterGap,
                                            kHalfGap,
                                            kQuarterGap,
                                            kHalfGap),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: Get.width * 0.1,
                                              child: Center(
                                                child: Icon(
                                                  correctChoice
                                                          .contains(choices)
                                                      ? Icons.check_circle_sharp
                                                      : Icons.cancel_sharp,
                                                  size: 30,
                                                  color: kWhite,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: Get.width * 0.6,
                                              child: CustomText(
                                                text: choices.text.toString(),
                                                textColor: kBlack,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      if (choices.explaination != '') ...[
                                        const SizedBox(height: 10),
                                        SizedBox(
                                          width: Get.width * 0.7,
                                          child: CustomText(
                                            text:
                                                'คำอธิบาย : ${choices.explaination}',
                                            textColor: kBlackDefault,
                                            textSize: fontSizeS,
                                          ),
                                        ),
                                      ],
                                    ],
                                  ),
                                );
                              }),
                            ),
                          ],
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: Padding(
            padding: kButtonPadding,
            child: ButtonMain(title: 'ปิด', onPressed: () => Get.back()),
          ),
        ),
      )),
    );
  }
}
