import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';

class ProgressBar extends StatelessWidget {
  final int max;
  final int current;
  final Color color;

  const ProgressBar(
      {Key? key,
      required this.max,
      required this.current,
      this.color = kBlueColor})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, boxConstraints) {
        var x = boxConstraints.maxWidth;
        var percent = (current / max) * x;
        return Stack(
          children: [
            Container(
              width: x,
              height: 10,
              decoration: BoxDecoration(
                color: kBlueColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(2),
              ),
            ),
            AnimatedContainer(
              duration: const Duration(milliseconds: 250),
              width: percent,
              height: 10,
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(2),
              ),
            ),
          ],
        );
      },
    );
  }
}
