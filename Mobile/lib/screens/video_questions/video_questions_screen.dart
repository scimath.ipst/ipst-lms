import 'dart:developer';
import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/video_questions/components/progress_bar.dart';
import 'package:project14plus/screens/video_questions/components/video_question_dialog.dart';
import 'package:project14plus/utils/easy_loading/easy_loading.dart';
import 'package:project14plus/widgets/widgets.dart';

class VideoQuestionsScreen extends StatefulWidget {
  const VideoQuestionsScreen({
    Key? key,
    required this.model,
    required this.at,
    required this.maxIndex,
  }) : super(key: key);
  final VideoContent model;
  final int at;
  final int maxIndex;
  @override
  State<VideoQuestionsScreen> createState() => _VideoQuestionsScreenState();
}

class _VideoQuestionsScreenState extends State<VideoQuestionsScreen> {
  final FrontendController _frontendController = Get.find<FrontendController>();
  late QuestionModel model;
  VideoContent? videoContent;
  List<Choices> selected = [];
  List<Choices> correctChoice = [];
  bool isDone = false;

  int current = 1;
  late String questionLength = widget.maxIndex.toString();

  @override
  void initState() {
    videoContent = widget.model;
    final index = widget.model.result?.videoQuestions
        ?.indexWhere((e) => e.at == widget.at);
    if (index != null && index > -1) {
      current = index + 1;
      model = videoContent!.result!.videoQuestions![index];
      model.choices?.forEach((x) {
        if (x.isCorrect == true) {
          correctChoice.add(x);
        }
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          text: 'คำถามระหว่างบทเรียน',
          textSize: fontSizeM,
          fontWeight: fontWeightBold,
        ),
        leading: IconButton(
            onPressed: () => Get.back(result: {"isDone": false}),
            icon:
                Icon(Platform.isIOS ? CupertinoIcons.back : Icons.arrow_back)),
      ),
      body: CustomScrollView(
        shrinkWrap: false,
        slivers: [
          SliverPadding(
            padding: kPadding,
            sliver: SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    text: 'คำถามข้อที่ $current จาก $questionLength ข้อ',
                    textSize: fontSizeS,
                    fontWeight: fontWeightBold,
                  ),
                  const SizedBox(height: 10),
                  ProgressBar(
                    max: widget.maxIndex,
                    current: current,
                  ),
                  const SizedBox(height: 10),
                  Stack(
                    children: [
                      Column(
                        children: [
                          const SizedBox(height: 35),
                          AspectRatio(
                            aspectRatio: 16 / 9,
                            child: CustomCoverImage(
                              image: videoContent?.result?.image,
                            ),
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                            color: kBlueColor,
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 5,
                              color: kWhite,
                              style: BorderStyle.solid,
                            ),
                          ),
                          child: Center(
                            child: CustomText(
                              text: current.toString(),
                              textSize: fontSizeXL * 1.5,
                              fontWeight: fontWeightBold,
                              textColor: kWhite,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  CustomText(
                    text: 'คำถามข้อที่ $current จาก $questionLength ข้อ',
                    textSize: fontSizeS,
                    fontWeight: fontWeightNormal,
                    textColor:
                        _frontendController.darkMode ? kWhite : kBlackDefault2,
                  ),
                  const SizedBox(height: 10),
                  CustomText(
                    text:
                        "${model.question ?? ''} ${correctChoice.length > 1 ? '(เลือกได้สูงสุด ${correctChoice.length} ข้อ)' : ''}",
                    textSize: fontSizeM,
                    fontWeight: fontWeightBold,
                  ),
                  const SizedBox(height: 10.0),
                  Column(
                    children: List.generate(model.choices!.length, (j) {
                      Choices choices = model.choices![j];
                      return Padding(
                        padding: const EdgeInsets.only(bottom: kGapS),
                        child: GestureDetector(
                          onTap: () {
                            if (!isDone) {
                              if (correctChoice.length > 1) {
                                if (!selected.contains(choices)) {
                                  if (selected.length < correctChoice.length) {
                                    setState(() => selected.add(choices));
                                  }
                                } else {
                                  setState(() => selected.remove(choices));
                                }
                              } else {
                                if (selected.isEmpty) {
                                  setState(() => selected.add(choices));
                                } else {
                                  if (!selected.contains(choices)) {
                                    selected = [];
                                    setState(() => selected.add(choices));
                                  } else {
                                    setState(() => selected.remove(choices));
                                  }
                                }
                              }
                            }
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: selected.contains(choices)
                                  ? kBlueColor
                                  : kBlackDefault2.withOpacity(.2),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(outlineRadius),
                              ),
                            ),
                            padding: const EdgeInsets.fromLTRB(
                                kQuarterGap, kHalfGap, kQuarterGap, kHalfGap),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: Get.width * 0.1,
                                  child: Center(
                                    child: Icon(
                                      selected.contains(choices)
                                          ? Icons.check_circle_sharp
                                          : Icons.circle,
                                      size: 30,
                                      color: kWhite,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: Get.width * 0.8,
                                  child: CustomText(
                                    text: choices.text ?? '',
                                    textColor: selected.contains(choices)
                                        ? kWhite
                                        : kBlack,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                  ),
                  if (isDone) ...[
                    const SizedBox(height: 10),
                    CustomText(
                      text: "***ไม่สามารถแก้ไขแบบทดสอบได้",
                      textSize: fontSizeS,
                      fontWeight: fontWeightBold,
                      textColor: kRedColor,
                    ),
                  ],
                ],
              ),
            ),
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 10.0)),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: kButtonPadding,
        child: GestureDetector(
          onTap: () => isDone ? Get.back() : onSubmit(),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 50.0,
            decoration: BoxDecoration(
                color: kBlueColor,
                borderRadius: BorderRadius.circular(outlineRadius)),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: CustomText(
                  text: isDone ? 'ย้อนกลับ' : 'ส่งแบบทดสอบ',
                  textColor: kWhite,
                  textSize: fontSizeL,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onSubmit() async {
    if (selected.isNotEmpty) {
      setState(() => isDone = true);
      Get.to(
        () => VideoQuestionDialog(
          selected: selected,
          correctChoice: correctChoice,
          title: model.question ?? '',
        ),
        transition: Transition.downToUp,
        opaque: false,
        fullscreenDialog: true,
      );
    } else {
      LoadingDialog.showToast(title: 'กรุณาเลืกคำตอบ');
    }
  }
}
