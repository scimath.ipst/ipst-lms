import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api_content/api_content.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/model/content/content.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/services/update_user_action.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoContentPlaylistBody extends StatefulWidget {
  const VideoContentPlaylistBody({
    Key? key,
    required this.playlistUrl,
    required this.url,
    required this.playlistList,
    required this.playlistName,
    this.watchingSeconds,
  }) : super(key: key);
  final String playlistUrl;
  final String url;
  final String playlistName;
  final List<ContentModel> playlistList;
  final double? watchingSeconds;

  @override
  State<VideoContentPlaylistBody> createState() =>
      _VideoContentPlaylistBodyState();
}

class _VideoContentPlaylistBodyState extends State<VideoContentPlaylistBody>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  //-------------------- Alert Dialog --------------------
  int _counter = 0;
  late StreamController<int> _events;
  Timer _timer = Timer(const Duration(seconds: 0), () => null);

  /*--------------------------------- Youtube --------------------------------*/
  late AnimationController animationController;
  late YoutubePlayerController youtubeController;
  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  bool _isPlayerReady = false;
  bool isFullScreen = false;
  int startAt = 0;

  double playbackRate = 1;
  List<double> playback = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2.0];
  /*----------------------------------- App ----------------------------------*/
  late ContentController controller = Get.find<ContentController>();
  ScrollController scrollController = ScrollController();

  int videoType = 0;
  VideoContent? videoContent;
  int playlistNext = 0;

  late final Future<VideoContent> _future =
      ApiContent.getContentPlaylistVideo(widget.playlistUrl, widget.url);

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        if (videoType == 1 || videoType == 2) {
          UpdateUserAction.updateUserAction(
              id: videoContent!.userAction!.id!,
              watchingSeconds:
                  youtubeController.value.position.inSeconds.toDouble(),
              status: videoContent!.userAction!.status!,
              position: youtubeController.value.position.inSeconds);
        }

        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance?.addObserver(this);

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {});
    animationController.repeat(reverse: false);

    if (widget.watchingSeconds != null && widget.watchingSeconds != 0) {
      setState(() {
        startAt = widget.watchingSeconds!.round();
      });
    }

    youtubeController = YoutubePlayerController(
      initialVideoId: widget.url,
      flags: YoutubePlayerFlags(
          mute: false,
          autoPlay: true,
          disableDragSeek: false,
          loop: false,
          isLive: false,
          forceHD: false,
          enableCaption: true,
          startAt: startAt),
    )..addListener(listener);
    youtubeController.setVolume(100);
    youtubeController.setPlaybackRate(playbackRate);
    _videoMetaData = const YoutubeMetaData();
    _playerState = PlayerState.unknown;

    super.initState();
  }

  void listener() {
    if (_isPlayerReady && mounted && !youtubeController.value.isFullScreen) {
      _playerState = youtubeController.value.playerState;
      _videoMetaData = youtubeController.metadata;
    }
  }

  @override
  void deactivate() {
    youtubeController.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);

    animationController.removeListener(() {});
    youtubeController.removeListener(listener);
    _playerState = PlayerState.unknown;
    _videoMetaData = const YoutubeMetaData();
    _isPlayerReady = false;
    youtubeController.dispose();
    animationController.dispose();
    super.dispose();
  }

  void _startTimer() {
    _counter = 10;
    if (_timer != null) _timer.cancel();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (_counter > 0) {
        _counter--;
      } else {
        _timer.cancel();
        Get.back();

        //Go to New Content
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                VideoContentPlaylistScreen(
              playlistUrl: widget.playlistUrl,
              playlistName: widget.playlistName,
              playFirstIndex: playlistNext,
            ),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              const begin = Offset(0.0, 1.0);
              const end = Offset.zero;
              const curve = Curves.ease;

              var tween =
                  Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

              return SlideTransition(
                position: animation.drive(tween),
                child: child,
              );
            },
          ),
        );
      }
      _events.add(_counter);
    });
  }

  setController(int type) {
    switch (type) {
      case 1:
        break;
      case 2:
        animationController.removeListener(() {});
        youtubeController.removeListener(listener);
        _playerState = PlayerState.unknown;
        _videoMetaData = const YoutubeMetaData();
        _isPlayerReady = false;
        youtubeController.dispose();
        animationController.dispose();
        break;
      case 3:
        //Remove youtube ...
        break;
      case 4:
        Future.delayed(const Duration(seconds: 1)).then((value) {
          setState(() {
            // isIframe = true;
          });
        }); //Remove youtube ...
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                FutureBuilder<VideoContent>(
                    future: _future,
                    builder: (BuildContext content,
                        AsyncSnapshot<dynamic> snapshot) {
                      VideoContent? item = snapshot.data;
                      if (snapshot.hasData) {
                        if (item == null) {
                          return const NoData();
                        }
                        // item.result!.type = 4;
                        videoContent = item;
                        setController(item.result!.type!);
                        videoType = item.result!.type!;
                        return Column(
                          children: [
                            item.result?.type == 1
                                ? YoutubePlayerBuilder(
                                    onExitFullScreen: () {
                                      SystemChrome.setPreferredOrientations([
                                        DeviceOrientation.landscapeLeft,
                                        DeviceOrientation.landscapeRight,
                                        DeviceOrientation.portraitUp,
                                      ]);
                                      setState(() {
                                        isFullScreen = false;
                                      });
                                    },
                                    onEnterFullScreen: () {
                                      setState(() {
                                        isFullScreen = true;
                                      });
                                    },
                                    player: YoutubePlayer(
                                      controlsTimeOut:
                                          const Duration(seconds: 60),
                                      controller: youtubeController,
                                      showVideoProgressIndicator: false,
                                      progressIndicatorColor: kPrimary,
                                      liveUIColor: kPrimary,
                                      bottomActions: [
                                        CurrentPosition(
                                            controller: youtubeController),
                                        IconButton(
                                          onPressed: () => playbackSetting(),
                                          icon: const Icon(
                                            Icons.speed,
                                            color: kWhite,
                                          ),
                                        ),
                                        ProgressBar(
                                            controller: youtubeController,
                                            colors: const ProgressBarColors(
                                              backgroundColor:
                                                  Color(0xFFBDBDBD),
                                              playedColor: kPrimary,
                                              bufferedColor: Color(0xFFF5F5F5),
                                              handleColor: kPrimary,
                                            ),
                                            isExpanded: true),
                                        RemainingDuration(
                                            controller: youtubeController),
                                        FullScreenButton(
                                            controller: youtubeController),
                                      ],
                                      topActions: <Widget>[
                                        const SizedBox(width: 8.0),
                                        Expanded(
                                          child: Text(
                                            youtubeController.metadata.title,
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 18.0,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                          ),
                                        ),
                                      ],
                                      onReady: () => _isPlayerReady = true,
                                      onEnded: (data) {
                                        if (item.result?.type == 1 ||
                                            item.result?.type == 2) {
                                          UpdateUserAction.updateUserAction(
                                              isOnEnd: true,
                                              id: item.userAction!.id!,
                                              watchingSeconds: youtubeController
                                                  .value.position.inSeconds
                                                  .toDouble(),
                                              status: item.userAction!.status!,
                                              position: youtubeController
                                                  .value.position.inSeconds);
                                        }

                                        if (item
                                            .result!.questions!.isNotEmpty) {
                                          youtubeController.pause();
                                          _events = StreamController<int>();
                                          _events.add(10);
                                          _startTimer();
                                          showDialog<String>(
                                            context: context,
                                            barrierDismissible: false,
                                            builder: (BuildContext context) =>
                                                CustomAlert(
                                              buttonKey: "เริ่มทำแบบทดสอบ",
                                              onpressed: () async {
                                                _timer.cancel();
                                                Get.back();
                                                Get.to(() => QuestionScreen(
                                                    model: item));
                                              },
                                              onCancelPressed: () {
                                                _timer.cancel();
                                                Get.back();
                                              },
                                              content: StreamBuilder<int>(
                                                  stream: _events.stream,
                                                  builder:
                                                      (BuildContext context,
                                                          AsyncSnapshot<int>
                                                              snapshot) {
                                                    return Wrap(
                                                      children: [
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: <Widget>[
                                                            CustomText(
                                                              text:
                                                                  'กดปุ่มเพิ่มเริ่มทำแบบทดสอบ หรือรอเพื่อเล่นเนื้อหาถัดไปในอีก ${snapshot.data.toString()}',
                                                              textSize:
                                                                  fontSizeM *
                                                                      1.2,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    );
                                                  }),
                                            ),
                                          );
                                        } else {
                                          //Play next
                                          youtubeController.pause();
                                          _events = StreamController<int>();
                                          _events.add(10);
                                          _startTimer();
                                          showDialog<String>(
                                            context: context,
                                            barrierDismissible: false,
                                            builder: (BuildContext context) =>
                                                CustomAlert(
                                              onCancelPressed: () {
                                                _timer.cancel();
                                                Get.back();
                                              },
                                              content: StreamBuilder<int>(
                                                  stream: _events.stream,
                                                  builder:
                                                      (BuildContext context,
                                                          AsyncSnapshot<int>
                                                              snapshot) {
                                                    return Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        CustomText(
                                                          text:
                                                              'กำลังที่จะเล่นเนื้อหาต่อไป ${snapshot.data.toString()}',
                                                          textSize:
                                                              fontSizeM * 1.2,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ],
                                                    );
                                                  }),
                                            ),
                                          );
                                        }
                                      },
                                    ),
                                    builder: (context, player) {
                                      return isFullScreen
                                          ? Center(
                                              child: SizedBox(
                                                height: Get.height,
                                                width: Get.width,
                                                child: player,
                                              ),
                                            )
                                          : Column(children: [
                                              player,
                                            ]);
                                    })
                                : item.result?.type == 2
                                    ? MpView(
                                        filePath: item.result?.filePath ?? '')
                                    : item.result?.type == 3
                                        ? PdfView(
                                            filePath:
                                                item.result?.filePath ?? '')
                                        : item.result?.type == 4
                                            ? IFrameCover(
                                                image:
                                                    item.result?.image ?? null,
                                                url:
                                                    item.result?.filePath ?? '',
                                              )
                                            : Container(),
                            if (!isFullScreen)
                              Expanded(
                                child: GetBuilder<ContentController>(
                                    builder: (_contentController) {
                                  return !_contentController.desCheck
                                      ? CustomScrollView(
                                          shrinkWrap: true,
                                          slivers: [
                                            SliverPadding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      kGapS, kGap, kGapS, 0),
                                              sliver: SliverToBoxAdapter(
                                                child: ContentTitle(
                                                  videoContent: item,
                                                  onQuestionPressed: () {
                                                    youtubeController.pause();
                                                    Get.to(() => QuestionScreen(
                                                        model: item));
                                                  },
                                                ),
                                              ),
                                            ),
                                            SliverPadding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        kGapS, 0, kGapS, 0),
                                                sliver: SliverToBoxAdapter(
                                                  child: Column(
                                                    children: const [
                                                      SizedBox(height: 10.0),
                                                      Divider(
                                                          color: kBlackDefault,
                                                          thickness: .2),
                                                      SizedBox(height: 10.0),
                                                    ],
                                                  ),
                                                )),
                                            SliverPadding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      kGapS, 0, kGapS, 0),
                                              sliver: SliverToBoxAdapter(
                                                child: CustomText(
                                                  text:
                                                      'Playlist ${widget.playlistName}',
                                                  textSize: fontSizeXL,
                                                  fontWeight: fontWeightBold,
                                                ),
                                              ),
                                            ),
                                            const SliverPadding(
                                              padding: EdgeInsets.fromLTRB(
                                                  kGapS, 0, kGapS, 0),
                                              sliver: SliverToBoxAdapter(
                                                child: SizedBox(
                                                  height: 10.0,
                                                ),
                                              ),
                                            ),
                                            SliverPadding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      kGapS, 0, kGapS, kGap),
                                              sliver: SliverToBoxAdapter(
                                                child: VideoContentPlaylistList(
                                                  list: widget.playlistList,
                                                  url: widget.url,
                                                  playlistName:
                                                      widget.playlistName,
                                                  playlistUrl:
                                                      widget.playlistUrl,
                                                  youtubeController:
                                                      youtubeController,
                                                  videoType: videoType,
                                                  videoContent: item,
                                                  onChange: (value) {
                                                    playlistNext = value;
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      : ContentDescription(
                                          videoContent: item,
                                        );
                                }),
                              ),
                          ],
                        );
                      }

                      return Container(
                        color: kBlack,
                        height: Get.height * 0.25,
                        width: Get.width,
                        child: const Center(
                          child: CircularProgressIndicator(
                            color: kWhite,
                          ),
                        ),
                      );
                    }),
                if (!isFullScreen)
                  ElevatedButton(
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.resolveWith(
                          (states) => Colors.transparent),
                      shadowColor: MaterialStateProperty.resolveWith(
                          (states) => Colors.transparent),
                      backgroundColor: MaterialStateProperty.resolveWith(
                          (states) => Colors.black.withOpacity(0.7)),
                      shape: MaterialStateProperty.resolveWith(
                          (states) => CircleBorder()),
                    ),
                    child: const Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      if (videoType == 1 || videoType == 2) {
                        UpdateUserAction.updateUserAction(
                            id: videoContent!.userAction!.id!,
                            watchingSeconds: youtubeController
                                .value.position.inSeconds
                                .toDouble(),
                            status: videoContent!.userAction!.status!,
                            position:
                                youtubeController.value.position.inSeconds);
                      }

                      controller.setDesCheck(false);
                      Get.back();
                    },
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future playbackSetting() {
    return showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(outlineRadius)),
      backgroundColor: Colors.white,
      builder: (BuildContext context) {
        return BottomSheet(
          onClosing: () {},
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (BuildContext context, setState) {
              return SafeArea(
                top: true,
                child: CustomScrollView(
                  shrinkWrap: true,
                  slivers: [
                    SliverAppBar(
                      pinned: true,
                      automaticallyImplyLeading: false,
                      title: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Get.back();
                        },
                        child: Stack(
                          children: [
                            Align(
                              alignment: const Alignment(-0.8, 0),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: CustomText(
                                  text: 'Cancel',
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            const Align(
                              alignment: Alignment(-1, 0),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Icon(Icons.close_rounded),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: SingleChildScrollView(
                          child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 16, 10, 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: List.generate(
                              playback.length,
                              (index) => GestureDetector(
                                    behavior: HitTestBehavior.opaque,
                                    onTap: () {
                                      setState(() {
                                        playbackRate = playback[index];
                                      });
                                      youtubeController
                                          .setPlaybackRate(playbackRate);
                                      Get.back();
                                    },
                                    child: Stack(
                                      children: [
                                        Align(
                                          alignment: const Alignment(-0.8, 0),
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 10, 0, 10),
                                            child: CustomText(
                                              text: playback[index] == 1
                                                  ? "Normal"
                                                  : "${playback[index].toString()}x",
                                              textSize: playbackRate ==
                                                      playback[index]
                                                  ? 18
                                                  : 16,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: const Alignment(-1, 0),
                                          child: playbackRate == playback[index]
                                              ? const Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      0, 10, 0, 10),
                                                  child:
                                                      Icon(Icons.check_rounded),
                                                )
                                              : null,
                                        )
                                      ],
                                    ),
                                  )),
                        ),
                      )),
                    ),
                  ],
                ),
              );
            });
          },
        );
      },
    );
  }
}
