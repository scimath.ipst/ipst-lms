import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/services/update_user_action.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:project14plus/widgets/widgets.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoContentPlaylistList extends StatefulWidget {
  VideoContentPlaylistList({
    Key? key,
    required this.list,
    required this.url,
    required this.playlistName,
    required this.playlistUrl,
    required this.youtubeController,
    required this.videoType,
    required this.videoContent,
    required this.onChange,
  }) : super(key: key);
  List<ContentModel> list;
  final String url;
  final String playlistUrl;
  final String playlistName;
  late YoutubePlayerController youtubeController;
  final int videoType;
  final VideoContent videoContent;
  final Function(int) onChange;

  @override
  State<VideoContentPlaylistList> createState() =>
      _VideoContentPlaylistListState();
}

class _VideoContentPlaylistListState extends State<VideoContentPlaylistList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Wrap(
        children: List.generate(widget.list.length, (index) {
          ContentModel? item = widget.list[index];
          final createdAt =
              DateFormatter.formatterddMMYYYY(item.createdAt!, withHour: false);
          if (widget.url == item.url) {
            widget.onChange(index + 1);
          }
          return Container(
            height: 100.0,
            color:
                widget.url == item.url ? kBlackDefault.withOpacity(0.3) : null,
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: GestureDetector(
              onTap: () {
                if (widget.videoType == 1 || widget.videoType == 2) {
                  UpdateUserAction.updateUserAction(
                      id: widget.videoContent.userAction!.id!,
                      watchingSeconds: widget
                          .youtubeController.value.position.inSeconds
                          .toDouble(),
                      status: widget.videoContent.userAction!.status!,
                      position:
                          widget.youtubeController.value.position.inSeconds);
                }
                Navigator.pushReplacement(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation, secondaryAnimation) =>
                        VideoContentPlaylistScreen(
                      playlistUrl: widget.playlistUrl,
                      playlistName: widget.playlistName,
                      playFirstIndex: index,
                    ),
                    transitionsBuilder:
                        (context, animation, secondaryAnimation, child) {
                      const begin = Offset(0.0, 1.0);
                      const end = Offset.zero;
                      const curve = Curves.ease;

                      var tween = Tween(begin: begin, end: end)
                          .chain(CurveTween(curve: curve));

                      return SlideTransition(
                        position: animation.drive(tween),
                        child: child,
                      );
                    },
                  ),
                );
              },
              child: Row(
                children: [
                  SizedBox(
                    width: 150.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(outlineRadius),
                      child: CustomCoverImage(
                        image: item.image.toString(),
                        height: 130.0,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10.0),
                  Flexible(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomText(
                          text: item.name.toString(),
                          textSize: fontSizeS,
                          fontWeight: FontWeight.w600,
                          textOverflow: TextOverflow.ellipsis,
                          maxLine: 2,
                        ),
                        const SizedBox(
                          height: 10.0,
                        ),
                        CustomText(
                          text:
                              '${item.user!.firstname.toString()} ${item.user!.lastname.toString()}',
                          textSize: fontSizeS,
                          fontWeight: FontWeight.w400,
                          textOverflow: TextOverflow.ellipsis,
                          maxLine: 1,
                        ),
                        CustomText(
                          text:
                              'ผู้ชม ${item.visitCount.toString()} คน \u2022 $createdAt',
                          textSize: fontSizeS,
                          fontWeight: FontWeight.w400,
                          textOverflow: TextOverflow.ellipsis,
                          maxLine: 1,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
