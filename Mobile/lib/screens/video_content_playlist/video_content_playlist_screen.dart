import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api_content/api_content.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';
import 'package:project14plus/widgets/widgets.dart';

class VideoContentPlaylistScreen extends StatefulWidget {
  const VideoContentPlaylistScreen({
    Key? key,
    required this.playlistUrl,
    this.playFirstIndex = 0,
    required this.playlistName,
  }) : super(key: key);
  final String playlistUrl;
  final int playFirstIndex;
  final String playlistName;

  @override
  State<VideoContentPlaylistScreen> createState() =>
      _VideoContentPlaylistScreenState();
}

class _VideoContentPlaylistScreenState
    extends State<VideoContentPlaylistScreen> {
  late Future<List<ContentModel>> _future =
      ApiContent.getContentPlaylist(widget.playlistUrl);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Scaffold(
        body: GetBuilder<ContentController>(
          builder: (controller) {
            return FutureBuilder<List<ContentModel>>(
                future: _future,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.hasData) {
                    List<ContentModel>? items = snapshot.data;

                    //Has data and data is empty
                    if (snapshot.data == null) {
                      return NoData(
                        onPressed: () {
                          setState(() {
                            _future = ApiContent.getContentPlaylist(
                                widget.playlistUrl);
                          });
                        },
                      );
                    }

                    //Has data and data is not empty
                    return VideoContentPlaylistBody(
                      playlistUrl: widget.playlistUrl,
                      url: items![widget.playFirstIndex].url!,
                      playlistList: items,
                      playlistName: widget.playlistName,
                    );
                  } else if (snapshot.hasError) {
                    //Do Somthing with error show error
                    return const NoData(
                      title: 'เกิดข้อผิดพลาด',
                    );
                  }
                  return const Center(
                      child: CircularProgressIndicator(
                    color: kPrimary,
                  ));
                });
          },
        ),
      ),
    );
  }
}
