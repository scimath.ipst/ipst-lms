import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'constants/constants.dart';

abstract class _LightColors {
  static const background = Color(0xFFFAFAFA);
  static const appbarBackground = Colors.white;
  static const card = kWhite;
}

abstract class _DarkColors {
  static const background = Color(0xFF151515);
  static const card = Color(0xFF151515);
  // static const appbarBackground = Color.fromARGB(255, 13, 12, 12);
}

/// Reference to the application theme.
abstract class AppTheme {
  static const accentColor = Color(0xFFD6755B);
  static final visualDensity = VisualDensity.adaptivePlatformDensity;

  static final darkBase = ThemeData.dark();
  static final lightBase = ThemeData.light();

  /// Light theme and its settings.
  static ThemeData light() => ThemeData(
        fontFamily: 'Sarabun',
        brightness: Brightness.light,
        colorScheme: lightBase.colorScheme.copyWith(secondary: accentColor),
        visualDensity: visualDensity,
        backgroundColor: _LightColors.background,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        appBarTheme: lightBase.appBarTheme.copyWith(
          iconTheme: const IconThemeData(color: kPrimary),
          backgroundColor: _LightColors.appbarBackground,
          elevation: 0,
          centerTitle: true,
          titleTextStyle: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 17,
            color: kPrimary,
          ),
          systemOverlayStyle: SystemUiOverlayStyle.dark,
        ),
        // progressIndicatorTheme: ProgressIndicatorThemeData(color: Colors.red),
        scaffoldBackgroundColor: _LightColors.background,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(primary: kPrimary),
        ),
        cardColor: _LightColors.card,
        primaryTextTheme: const TextTheme(
          headline6: TextStyle(color: kPrimary),
        ),
        inputDecorationTheme: InputDecorationTheme(
          iconColor:
              MaterialStateColor.resolveWith((Set<MaterialState> states) {
            final Color color = states.contains(MaterialState.focused)
                ? kPrimary
                : const Color(0xFF999999);
            return color;
          }),
        ),
        iconTheme: const IconThemeData(color: Color(0xFF000000)),
        bottomNavigationBarTheme:
            const BottomNavigationBarThemeData(selectedItemColor: kPrimary),
      );

  /// Dark theme and its settings.
  static ThemeData dark() => ThemeData(
        fontFamily: 'Sarabun',
        brightness: Brightness.dark,
        colorScheme: darkBase.colorScheme.copyWith(secondary: accentColor),
        visualDensity: visualDensity,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        backgroundColor: _DarkColors.background,

        appBarTheme: darkBase.appBarTheme.copyWith(
          backgroundColor: _DarkColors.background,
          elevation: 0,
          centerTitle: true,
          titleTextStyle: const TextStyle(
            color: kWhite,
            fontWeight: FontWeight.bold,
            fontSize: 17,
          ),
          iconTheme: const IconThemeData(color: kWhite),
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        inputDecorationTheme: InputDecorationTheme(
          iconColor:
              MaterialStateColor.resolveWith((Set<MaterialState> states) {
            final Color color = states.contains(MaterialState.focused)
                ? kPrimary
                : const Color(0xFFFFFFFF);

            return color;
          }),
        ),
        scaffoldBackgroundColor: _DarkColors.background,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(primary: kPrimary),
        ),
        cardColor: _DarkColors.card,
        primaryTextTheme: const TextTheme(
          headline6: TextStyle(color: kPrimary),
        ),
        bottomNavigationBarTheme:
            const BottomNavigationBarThemeData(selectedItemColor: kPrimary),
        // navigationBarTheme: NavigationBarThemeData(
        //   labelTextStyle: MaterialStateProperty.all(
        //     TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
        //   ),
        //   iconTheme: MaterialStateProperty.all(
        //     IconThemeData(color: Colors.red),
        //   ),
        // ),
      );
}
