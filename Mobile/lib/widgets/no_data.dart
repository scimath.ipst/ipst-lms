import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/constants/words.dart';
import 'package:project14plus/widgets/widgets.dart';

class NoData extends StatelessWidget {
  const NoData({
    Key? key,
    this.onPressed,
    this.title,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final String? title;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 20.0),
          CustomText(
            text: title ?? NO_DATA_FOUND,
            textSize: 16,
            fontWeight: FontWeight.w500,
          ),
          const SizedBox(height: 12.0),
          if (onPressed != null)
            ElevatedButton.icon(
              onPressed: onPressed,
              icon: const Icon(Icons.refresh_rounded),
              label: CustomText(
                text: 'รีเฟรช',
                textColor: Colors.white,
                textSize: 16,
                fontWeight: FontWeight.w500,
              ),
              style: ElevatedButton.styleFrom(
                primary: kPrimary,
              ),
            ),
          const SizedBox(height: 20.0),
        ],
      ),
    );
  }
}
