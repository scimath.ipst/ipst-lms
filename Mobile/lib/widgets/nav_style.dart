import 'package:get/get.dart';
import 'package:project14plus/controllers/content_controller.dart';
import 'package:project14plus/model/model.dart';

Future<void> slideToVideoContent(
    String contentUrl, String contentId, double? watchingSeconds) async {
  ParameterModel temp = ParameterModel(
    contentId: contentId,
    contentUrl: contentUrl,
    watchingSeconds: watchingSeconds,
  );

  await Get.find<ContentController>().checkUser(temp);
}

Future<void> slideToVideoPlaylistContent(
    String playlistUrl, String playlistName) async {
  ParameterModel temp = ParameterModel(
      contentId: null,
      contentUrl: null,
      playlistName: playlistName,
      playlistUrl: playlistUrl,
      watchingSeconds: null,
      isFromPlaylist: true);

  await Get.find<ContentController>().checkUser(temp);
}
