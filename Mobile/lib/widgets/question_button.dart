import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/widgets/widgets.dart';

class QuestionButton extends StatelessWidget {
  QuestionButton({
    Key? key,
    required this.item,
    required this.onPressed,
  }) : super(key: key);
  UserActionModel item;
  void Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
            style: ButtonStyle(
              elevation: MaterialStateProperty.all<double>(0.0),
              backgroundColor: MaterialStateProperty.all<Color>(
                kPrimary,
              ),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(outlineRadius),
                ),
              ),
            ),
            onPressed: onPressed,
            child: CustomText(
              text: 'ทำแบบทดสอบ',
              textColor: kWhite,
              textSize: fontSizeM,
              fontWeight: FontWeight.w500,
            ),
          ),
          if (item.content?.badge != null && item.content?.badgeName != null)
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    CustomText(
                      text: 'เมื่อคุณผ่านแบบทดสอบจะได้รับ',
                      textSize: fontSizeM,
                    ),
                    CustomText(
                      text: item.content?.badgeName ?? 'เหรียญ',
                      textColor: kPrimary,
                      textSize: fontSizeM,
                      fontWeight: fontWeightBold,
                    ),
                  ],
                ),
                const SizedBox(
                  width: 5.0,
                ),
                SizedBox(
                  child: CustomCoverImage(
                    image: item.content?.badge.toString() ?? "",
                    height: 50.0,
                    width: 50.0,
                    fit: BoxFit.contain,
                  ),
                )
              ],
            ),
        ],
      ),
    );
  }
}
