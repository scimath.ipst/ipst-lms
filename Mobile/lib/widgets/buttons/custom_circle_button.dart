import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';

class CustomCircleButton extends StatelessWidget {
  CustomCircleButton({
    Key? key,
    required this.ignoring,
    required this.isShow,
    required this.onPressed,
  }) : super(key: key);
  bool ignoring;
  bool isShow;
  void Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: ignoring,
      child: AnimatedOpacity(
        opacity: isShow ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 450),
        child: Container(
          margin: const EdgeInsets.only(bottom: 20.0),
          child: Padding(
            padding: const EdgeInsets.only(right: 10.0, bottom: 10.0),
            child: Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                backgroundColor: kPrimary,
                onPressed: onPressed,
                child: const Icon(
                  Icons.keyboard_arrow_up_rounded,
                  size: 35.0,
                  color: kWhite,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
