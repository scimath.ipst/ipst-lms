import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';

class ButtonMain extends StatelessWidget {
  const ButtonMain({
    Key? key,
    required this.title,
    this.color,
    this.icon,
    required this.onPressed,
    this.height = 56.0,
  }) : super(key: key);

  final String title;
  final Color? color;
  final Widget? icon;
  final VoidCallback onPressed;
  final double height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: height,
      child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          primary: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(outlineRadius),
          ),
        ),
        onPressed: onPressed,
        label: Text(
          title,
          style: const TextStyle(color: Colors.white),
        ),
        icon: Container(
          child: icon,
        ),
      ),
    );
  }
}
