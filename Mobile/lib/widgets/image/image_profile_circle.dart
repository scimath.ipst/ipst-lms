import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/utils/utils.dart';

class ImageProfileCircle extends StatelessWidget {
  const ImageProfileCircle({
    Key? key,
    this.imageUrl = '',
    this.radius = 30,
    this.fit = BoxFit.contain,
  }) : super(key: key);

  final String imageUrl;
  final double radius;
  final BoxFit fit;

  @override
  Widget build(BuildContext context) {
    if (!AppUtils.hasValue(imageUrl)) {
      return CircleAvatar(
        backgroundColor: kPrimary,
        foregroundColor: kWhite,
        radius: radius,
        child: errorWidget(),
      );
    }

    return CircleAvatar(
        backgroundColor: kPrimary,
        foregroundColor: kWhite,
        radius: radius,
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: imageProvider,
                fit: fit,
              ),
            ),
          ),
          placeholder: (context, url) => placeholder(),
          errorWidget: (context, url, error) => errorWidget(),
        ));
  }

  Widget placeholder() {
    return const Center(
      child: SizedBox(
        width: 16,
        height: 16,
        child: CircularProgressIndicator(strokeWidth: 2),
      ),
    );
  }

  Widget errorWidget() {
    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: AssetImage(PATH_AVATAR_DEFAULT),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
