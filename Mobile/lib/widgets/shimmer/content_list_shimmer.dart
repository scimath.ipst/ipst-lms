import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:shimmer/shimmer.dart';

class ContentListShimmer extends StatelessWidget {
  const ContentListShimmer({Key? key, this.length = 12}) : super(key: key);

  final int length;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.zero,
      child: ListView.separated(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        itemBuilder: (c, index) {
          return _listShimmer();
        },
        itemCount: length,
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            thickness: 1,
            color: Color(0x66999999),
          );
        },
      ),
    );
  }

  Shimmer _listShimmer() {
    return Shimmer.fromColors(
      child: Wrap(
        children: [
          Container(
            height: 100.0,
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
              children: [
                Container(
                  width: 150.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                Flexible(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 20.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(outlineRadius),
                          // color: kWhiteColor,
                          color: kBlackDefault,
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      Column(
                        children: [
                          Container(
                            height: 20.0,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(outlineRadius),
                              // color: kWhiteColor,
                              color: kBlackDefault,
                            ),
                          ),
                          const SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            height: 20.0,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(outlineRadius),
                              // color: kWhiteColor,
                              color: kBlackDefault,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      baseColor: kBlackDefault,
      highlightColor: Colors.grey.withOpacity(0.4),
    );
  }
}
