import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:shimmer/shimmer.dart';

class TextShimmerLoad extends StatelessWidget {
  const TextShimmerLoad({Key? key, this.height = 20}) : super(key: key);

  final double height;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: Container(
        height: 20,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.grey,
        ),
      ),
      baseColor: kBlackDefault,
      highlightColor: Colors.grey.withOpacity(0.4),
    );
  }
}
