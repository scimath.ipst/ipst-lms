import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:shimmer/shimmer.dart';

class CarouselShimmer extends StatelessWidget {
  const CarouselShimmer({Key? key, this.length = 2}) : super(key: key);
  final int length;

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;

    return SizedBox(
      height: isMobile
          ? orientation == Orientation.portrait
              ? 310
              : 615
          : 410.0,
      child: Shimmer.fromColors(
        child: Column(
          children: [
            Container(
              height: 200,
              color: kBlackDefault,
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: kBlackDefault,
                ),
                child: const SizedBox(width: 24, height: 24),
              ),
              title: Container(
                height: 16,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: kBlackDefault,
                ),
              ),
              subtitle: Container(
                height: 16,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: kBlackDefault,
                ),
              ),
            ),
            // Container(
            //   height: 20,
            //   color: cBlackDefault,
            // ),
          ],
        ),
        baseColor: kBlackDefault,
        highlightColor: Colors.grey.withOpacity(0.4),
      ),
    );
  }
}
