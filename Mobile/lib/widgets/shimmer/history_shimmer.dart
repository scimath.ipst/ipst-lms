import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:shimmer/shimmer.dart';

class HistoryShimmer extends StatelessWidget {
  const HistoryShimmer({Key? key, this.length = 12}) : super(key: key);

  final int length;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      itemBuilder: (c, index) {
        return _listShimmer();
      },
      itemCount: length,
      separatorBuilder: (BuildContext context, int index) {
        return const Divider(
          thickness: 1,
          color: Color(0x66999999),
        );
      },
    );
  }

  Shimmer _listShimmer() {
    return Shimmer.fromColors(
      child: ListTile(
        title: Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Container(
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              color: Colors.grey,
            ),
          ),
        ),
        leading: ConstrainedBox(
          constraints: const BoxConstraints(
            minWidth: 50,
            minHeight: 50,
            maxWidth: 80,
            maxHeight: 60,
          ),
          child: Container(
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              // color: kWhiteColor,
              color: Colors.grey,
            ),
          ),
        ),
        subtitle: Container(
          height: 20,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            color: Colors.grey,
          ),
        ),
      ),
      baseColor: kBlackDefault,
      highlightColor: Colors.grey.withOpacity(0.4),
    );
  }
}
