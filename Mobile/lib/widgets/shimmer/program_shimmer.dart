import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:shimmer/shimmer.dart';

class ProgramShimmer extends StatelessWidget {
  const ProgramShimmer({
    Key? key,
    this.length = 10,
    this.crossAxisCount = 2,
    this.imageHeight = 150.0,
    this.gridHeight = 230.0,
  }) : super(key: key);
  final int length;
  final int crossAxisCount;
  final double imageHeight;
  final double gridHeight;

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: isMobile
            ? (orientation == Orientation.portrait)
                ? crossAxisCount
                : 3
            : (orientation == Orientation.portrait)
                ? 4
                : 5,
        mainAxisSpacing: 0.0,
        crossAxisSpacing: 10,
        mainAxisExtent: gridHeight,
      ),
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      itemBuilder: (c, index) {
        return _listShimmer();
      },
      itemCount: length,
    );
  }

  Shimmer _listShimmer() {
    return Shimmer.fromColors(
      child: Column(
        children: [
          Container(
            height: imageHeight,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(outlineRadius),
              color: kBlackDefault,
            ),
          ),
          const SizedBox(height: 10.0),
          Container(
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(outlineRadius),
              color: kBlackDefault,
            ),
          ),
          const SizedBox(height: 10.0),
          Container(
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(outlineRadius),
              color: kBlackDefault,
            ),
          ),
        ],
      ),
      baseColor: kBlackDefault,
      highlightColor: Colors.grey.withOpacity(0.4),
    );
  }
}
