// import 'package:flutter/material.dart';
// import 'package:project14plus/constants/constants.dart';
// import 'package:shimmer/shimmer.dart';

// class ContentShimmer extends StatelessWidget {
//   const ContentShimmer({Key? key, this.length = 10}) : super(key: key);
//   final int length;

//   @override
//   Widget build(BuildContext context) {
//     var orientation = MediaQuery.of(context).orientation;
//     return GridView.builder(
//       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//         crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
//         mainAxisSpacing: 0.0,
//         crossAxisSpacing: 10,
//         mainAxisExtent: 250.0,
//         // childAspectRatio: 2,
//       ),
//       padding: EdgeInsets.zero,
//       shrinkWrap: true,
//       itemBuilder: (c, index) {
//         return _listShimmer();
//       },
//       itemCount: length,
//     );
//   }

//   Shimmer _listShimmer() {
//     return Shimmer.fromColors(
//       child: Column(
//         children: [
//           Container(
//             height: 150,
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(outlineRadius),
//               // color: kWhiteColor,
//               color: Colors.grey,
//             ),
//           ),
//           const SizedBox(
//             height: 20.0,
//           ),
//           Container(
//             height: 30,
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(outlineRadius),
//               // color: kWhiteColor,
//               color: Colors.grey,
//             ),
//           ),
//           const SizedBox(
//             height: 10.0,
//           ),
//           Container(
//             height: 20,
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(outlineRadius),
//               // color: kWhiteColor,
//               color: Colors.grey,
//             ),
//           ),
//         ],
//       ),
//       baseColor: Colors.grey,
//       highlightColor: Colors.grey.withOpacity(0.4),
//     );
//   }
// }
