import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:shimmer/shimmer.dart';

class HomeShimmer extends StatelessWidget {
  const HomeShimmer({Key? key, this.length = 2}) : super(key: key);
  final int length;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _listShimmer(),
    );
  }

  Shimmer _listShimmer() {
    return Shimmer.fromColors(
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: Column(
              children: [
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 10.0,
          ),
          Flexible(
            flex: 1,
            child: Column(
              children: [
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(outlineRadius),
                    // color: kWhiteColor,
                    color: kBlackDefault,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      baseColor: kBlackDefault,
      highlightColor: Colors.grey.withOpacity(0.4),
    );
  }
}
