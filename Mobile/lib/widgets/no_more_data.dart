import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/constants/words.dart';
import 'package:project14plus/widgets/widgets.dart';

class NoMoreData extends StatelessWidget {
  const NoMoreData({
    Key? key,
    this.title = NO_MORE_DATA,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, kGap, 0, kGap),
        child: CustomText(
          text: title,
          textSize: 16,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
