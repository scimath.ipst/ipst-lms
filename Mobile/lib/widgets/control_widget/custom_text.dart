import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';

class CustomText extends StatelessWidget {
  CustomText({
    Key? key,
    required this.text,
    this.maxLine,
    this.textSize = fontSizeM,
    this.textColor,
    this.textOverflow,
    this.textDecoration = TextDecoration.none,
    this.fontWeight = FontWeight.normal,
    this.textAlign = TextAlign.start,
  }) : super(key: key);
  String text;
  Color? textColor;
  double? textSize;
  FontWeight fontWeight;
  TextDecoration textDecoration;
  TextAlign textAlign;
  TextOverflow? textOverflow;
  int? maxLine;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLine,
      textScaleFactor: 0.95,
      overflow: textOverflow,
      style: TextStyle(
        decoration: textDecoration,
        color: textColor,
        fontSize: textSize,
        fontWeight: fontWeight,
        fontFamily: "Sarabun",
      ),
      textAlign: textAlign,
    );
  }
}
