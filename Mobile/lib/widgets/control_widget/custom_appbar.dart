import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/widgets/widgets.dart';

class CustomAppBar extends StatelessWidget {
  CustomAppBar({
    Key? key,
    required this.title,
    required this.bannerPath,
    required this.body,
    this.onPressedTrailing,
    this.onActionPressed,
  }) : super(key: key);
  final String title;
  final String bannerPath;
  final Widget body;
  final void Function()? onPressedTrailing;
  final VoidCallback? onActionPressed;
  FrontendController _frontendController = Get.find<FrontendController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 100.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: onPressedTrailing != null
                    ? Stack(
                        children: [
                          Container(
                            alignment: Alignment.bottomCenter,
                            child: CustomText(
                              text: title,
                              textSize: fontSizeXL,
                              fontWeight: FontWeight.w600,
                              textColor: innerBoxIsScrolled
                                  ? _frontendController.darkMode
                                      ? Colors.white
                                      : kPrimary
                                  : Colors.white,
                            ),
                          ),
                          Container(
                            alignment: Alignment.bottomRight,
                            child: IconButton(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                alignment: Alignment.bottomRight,
                                padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                icon: Icon(
                                  Icons.search_rounded,
                                  color: innerBoxIsScrolled
                                      ? _frontendController.darkMode
                                          ? Colors.white
                                          : kPrimary
                                      : Colors.white,
                                ),
                                onPressed: onPressedTrailing),
                          )
                        ],
                      )
                    : CustomText(
                        text: title,
                        textSize: fontSizeXL,
                        fontWeight: FontWeight.w600,
                        textColor: innerBoxIsScrolled
                            ? _frontendController.darkMode
                                ? Colors.white
                                : kPrimary
                            : Colors.white,
                      ),
                background: Image.asset(
                  bannerPath,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ];
        },
        body: body,
      ),
    );
  }
}
