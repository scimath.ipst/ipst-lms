import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';

/*
  status :
    0 = Visited
    1 = Watching
    2 = Watched
    3 = Completed
*/

class CustomStatusText extends StatelessWidget {
  const CustomStatusText({Key? key, required this.status}) : super(key: key);
  final int status;

  @override
  Widget build(BuildContext context) {
    String temp = '';
    Color backgroundColor;
    Color textColor;
    switch (status) {
      case 0:
        temp = "Visited";
        backgroundColor = const Color(0xFFE0E0E0);
        textColor = kBlack;
        break;
      case 1:
        temp = "Watching";
        backgroundColor = const Color(0xFFF50057);
        textColor = kWhite;
        break;
      case 2:
        temp = "Watched";
        backgroundColor = kPrimary;
        textColor = kWhite;
        break;
      case 3:
        temp = "Completed";
        backgroundColor = kPrimary;
        textColor = kWhite;
        break;
      default:
        backgroundColor = kPrimary;
        textColor = kWhite;
        break;
    }
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: const BorderRadius.all(
          Radius.circular(outlineRadius),
        ),
      ),
      padding: const EdgeInsets.all(4.0),
      child: CustomText(
        text: temp,
        fontWeight: fontWeightNormal,
        textSize: fontSizeM,
        textColor: textColor,
      ),
    );
  }
}
