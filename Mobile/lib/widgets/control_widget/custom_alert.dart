import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/widgets/widgets.dart';

class CustomAlert extends StatelessWidget {
  const CustomAlert({
    Key? key,
    this.title,
    this.content,
    this.isShowCancel = true,
    this.onpressed,
    this.buttonKey = "ตกลง",
    this.onCancelPressed,
  }) : super(key: key);
  final String? title;
  final String buttonKey;
  final bool isShowCancel;
  final Widget? content;
  final VoidCallback? onpressed;
  final VoidCallback? onCancelPressed;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      titlePadding: title != null
          ? const EdgeInsets.fromLTRB(0, 10, 0, 10)
          : EdgeInsets.zero,
      title: title != null
          ? Center(
              child: CustomText(
                text: title!,
                textSize: fontSizeL,
                fontWeight: FontWeight.w500,
              ),
            )
          : null,
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            if (isShowCancel == true)
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                  elevation: MaterialStateProperty.all<double>(0.0),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      side: const BorderSide(color: kPrimary),
                      borderRadius: BorderRadius.circular(outlineRadius),
                    ),
                  ),
                ),
                onPressed: onCancelPressed ?? () => Get.back(),
                child: CustomText(
                  text: 'ยกเลิก',
                  textColor: kPrimary,
                  textSize: fontSizeM,
                  fontWeight: FontWeight.w500,
                ),
              ),
            if (onpressed != null)
              ElevatedButton(
                style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double>(0.0),
                  backgroundColor: MaterialStateProperty.all<Color>(
                    kPrimary,
                  ),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(outlineRadius),
                    ),
                  ),
                ),
                onPressed: onpressed,
                child: CustomText(
                  text: buttonKey,
                  textColor: kWhite,
                  textSize: fontSizeM,
                  fontWeight: FontWeight.w500,
                ),
              ),
          ],
        )
      ],
      content: Wrap(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: content != null ? content : null,
          ),
        ],
      ),
    );
  }
}
