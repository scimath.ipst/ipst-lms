import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/personal/personal.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/model/user/user.dart';
import 'package:project14plus/widgets/widgets.dart';

class CustomDropdown extends StatefulWidget {
  const CustomDropdown({
    Key? key,
    required this.value,
    required this.title,
    required this.onChange,
    // required this.valueId,
    required this.dropdownType,
    this.isRequired = false,
    this.focusNode,
    this.validator,
  }) : super(key: key);
  final String title;
  final Level value;
  final Function(Level) onChange;
  final DropdownType dropdownType;
  final bool isRequired;
  final FocusNode? focusNode;
  final String? Function(Level?)? validator;
  @override
  State<CustomDropdown> createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  FrontendController _frontendController = Get.find<FrontendController>();

  List<Level> list = [];
  @override
  void initState() {
    getList();
    super.initState();
  }

  getList() async {
    switch (widget.dropdownType) {
      case DropdownType.SUBJECT:
        list = await ApiPersonal.getSubjectList();
        list.insert(0, Level(name: "", url: "", id: ""));
        break;
      case DropdownType.LEVEL:
        list = await ApiPersonal.getLevelList();
        list.insert(0, Level(name: "", url: "", id: ""));
        break;
      case DropdownType.SORTING:
        list = [
          Level(name: "", url: "", id: ""),
          Level(name: "ใหม่ที่สุด", url: "", id: "1"),
          Level(name: "เก่าที่สุด", url: "", id: "2"),
          Level(name: "เป็นที่นิยม", url: "", id: "3"),
        ];
        break;
      case DropdownType.SORTING_PLAYLIST:
        list = [
          Level(name: "", url: "", id: ""),
          Level(name: "ใหม่ที่สุด", url: "", id: "1"),
          Level(name: "เก่าที่สุด", url: "", id: "2"),
          Level(name: "ชื่อ ก-ฮ", url: "", id: "3"),
          Level(name: "ชื่อ ฮ-ก", url: "", id: "4"),
        ];
        break;
      case DropdownType.REASON:
        list = [
          Level(name: "Reason 1", url: "", id: "1"),
          Level(name: "Reason 2", url: "", id: "2"),
          Level(name: "Reason 3", url: "", id: "3"),
          Level(name: "Reason 4", url: "", id: "4"),
        ];
        break;
    }

    setState(() => list);
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<Level>(
      focusNode: widget.focusNode,
      validator: widget.validator,
      isExpanded: true,
      hint: widget.value.name == null
          ? null
          : CustomText(
              text: widget.value.name.toString(),
            ),
      // iconDisabledColor: const Color(0xFFD3D3D3),
      // iconEnabledColor: const Color(0xFFD3D3D3),
      decoration: InputDecoration(
        label: widget.isRequired
            ? RichText(
                textAlign: TextAlign.start,
                textScaleFactor: 0.95,
                text: TextSpan(
                  style: TextStyle(
                    fontSize: fontSizeM,
                    fontWeight: fontWeightNormal,
                    fontFamily: "Sarabun",
                    color: _frontendController.darkMode ? kWhite : kBlack,
                    decoration: TextDecoration.none,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: widget.title,
                      style: TextStyle(
                        fontSize: fontSizeM,
                        fontWeight: fontWeightNormal,
                        fontFamily: "Sarabun",
                        color: _frontendController.darkMode ? kWhite : kBlack,
                      ),
                    ),
                    const TextSpan(
                      text: '*',
                      style: TextStyle(
                        fontSize: fontSizeM,
                        fontWeight: fontWeightNormal,
                        fontFamily: "Sarabun",
                        color: Color(0xFFD50000),
                      ),
                    ),
                  ],
                ),
              )
            : CustomText(
                text: widget.title,
                textSize: fontSizeM,
                fontWeight: fontWeightNormal,
              ),
        // labelText: widget.title,
        labelStyle: TextStyle(
          fontSize: fontSizeM,
          fontFamily: 'Sarabun',
          fontWeight: fontWeightNormal,
          color: _frontendController.darkMode
              ? const Color(0xFFFFFFFF)
              : const Color(0xFF000000),
        ),
        floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
            Color color = states.contains(MaterialState.focused)
                ? kPrimary
                : _frontendController.darkMode
                    ? const Color(0xFFFFFFFF)
                    : const Color(0xFF999999);
            return TextStyle(color: color, letterSpacing: 1.3);
          },
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(outlineRadius),
          borderSide: const BorderSide(color: kPrimary, width: 1.5),
        ),
        errorBorder: OutlineInputBorder(
          gapPadding: 0.0,
          borderRadius: BorderRadius.circular(outlineRadius),
          borderSide: const BorderSide(
            color: Color(0xFFD50000),
            width: 1.5,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          gapPadding: 0.0,
          borderRadius: BorderRadius.circular(outlineRadius),
          borderSide: const BorderSide(color: Color(0xFFD50000), width: 1.5),
        ),
        enabledBorder: OutlineInputBorder(
          gapPadding: 0.0,
          borderRadius: BorderRadius.circular(outlineRadius),
          borderSide: BorderSide(
            color: _frontendController.darkMode
                ? const Color(0xFFFFFFFF)
                : const Color(0xFFD3D3D3),
            width: 1.5,
          ),
        ),
        contentPadding: const EdgeInsets.all(10),
      ),
      items: list.map((selectedType) {
        return DropdownMenuItem(
          child: CustomText(
            text: selectedType.name.toString(),
            textSize: fontSizeM,
            fontWeight: fontWeightNormal,
          ),
          value: selectedType,
        );
      }).toList(),
      onChanged: (value) => widget.onChange(value!),
    );
  }
}
