import 'package:flutter/material.dart';
import 'package:project14plus/widgets/control_widget/control_widget.dart';

class CustomTextButton extends StatelessWidget {
  const CustomTextButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.isReadMode = false,
  }) : super(key: key);
  final CustomText text;
  final VoidCallback onPressed;
  final bool isReadMode;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: isReadMode
          ? Row(
              children: [
                text,
                const Icon(
                  Icons.navigate_next_rounded,
                )
              ],
            )
          : text,
    );
  }
}
