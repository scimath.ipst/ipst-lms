import 'package:flutter/material.dart';

import '../../constants/constants.dart';
import '../widgets.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.onPressed,
    this.icon,
    required this.text,
    required this.buttonColor,
    this.isTransparent = false,
  }) : super(key: key);

  final CustomText text;
  final VoidCallback onPressed;
  final IconData? icon;
  final Color buttonColor;
  final bool isTransparent;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.0,
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        style: ButtonStyle(
          elevation: MaterialStateProperty.all<double>(0.0),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(outlineRadius),
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
            return Colors.transparent;
          }),
          backgroundColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
            return isTransparent ? Colors.transparent : buttonColor;
          }),
        ),
        onPressed: onPressed,
        child: text,
      ),
    );
  }
}
