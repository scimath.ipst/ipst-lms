import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';

class CustomCoverImage extends StatelessWidget {
  CustomCoverImage({
    Key? key,
    this.image,
    this.height = 150.0,
    this.fit = BoxFit.cover,
    this.width = double.infinity,
  }) : super(key: key);
  final String? image;
  double height;
  double width;
  BoxFit fit;
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(outlineRadius),
        child: image != null && image != ''
            ? image!.contains('http://') || image!.contains('https://')
                ? Image.network(
                    image.toString(),
                    height: height,
                    width: width,
                    fit: fit,
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) return child;
                      return SizedBox(
                        height: height,
                        child: Center(
                          child: CircularProgressIndicator(
                            color: kPrimary,
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                    loadingProgress.expectedTotalBytes!
                                : null,
                          ),
                        ),
                      );
                    },
                  )
                : Image.asset(
                    PATH_DEFAULT,
                    height: height,
                    // color: Colors.red,
                    width: double.infinity,
                    fit: fit,
                  )
            : Image.asset(
                PATH_DEFAULT,
                height: height,
                // color: Colors.red,
                width: double.infinity,
                fit: fit,
              ));
  }
}
