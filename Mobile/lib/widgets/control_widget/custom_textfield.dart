import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/widgets/widgets.dart';
import '../../constants/constants.dart';

class CustomTextFromField extends StatefulWidget {
  bool isPassword;
  String? label;
  String? initialValue;
  bool autofocus;
  TextInputType? inputType;
  double? height;
  Color bgColor;
  bool isMasked;
  Function(String?)? onChanged;
  Function(String?)? onSave;
  String? Function(String?)? validate;
  Function(String?)? onFieldSubmitted;
  FocusNode? focusNode;
  int? minLines;
  int? maxLines;
  bool? isOpacityColor;
  List<TextInputFormatter>? inputFormatter = <TextInputFormatter>[];
  TextEditingController? controller = TextEditingController();
  bool isShowPassEye;
  bool isCenter;
  EdgeInsets? contentPadding;
  bool? enabled;
  int? maxLength;
  bool readOnly;
  TextInputAction? textInputAction;
  bool isRequired;
  bool displaySuffixIcon;
  IconData? suffixIcon;

  CustomTextFromField({
    Key? key,
    this.label,
    this.initialValue,
    this.inputType,
    this.isPassword = false,
    this.onChanged,
    this.isMasked = false,
    this.validate,
    this.onFieldSubmitted,
    this.onSave,
    this.focusNode,
    this.controller,
    this.inputFormatter,
    this.isCenter = true,
    this.bgColor = Colors.grey,
    this.autofocus = false,
    this.isOpacityColor = true,
    this.height = 54.0,
    this.maxLines = 1,
    this.minLines = 1,
    this.isShowPassEye = false,
    this.contentPadding,
    this.enabled,
    this.maxLength,
    this.readOnly = false,
    this.textInputAction = TextInputAction.done,
    this.isRequired = false,
    this.suffixIcon,
    this.displaySuffixIcon = false,
  }) : super(key: key);
  @override
  _CustomTextFromFieldState createState() => _CustomTextFromFieldState();
}

class _CustomTextFromFieldState extends State<CustomTextFromField> {
  final FrontendController _frontendController = Get.find<FrontendController>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          child: widget.isCenter
              ? Center(
                  child: fieldText(),
                )
              : fieldText(),
        ),
      ],
    );
  }

//Dark Mode outline color set to white
  Row fieldText() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: TextFormField(
            readOnly: widget.readOnly,
            minLines: widget.minLines,
            maxLines: widget.maxLines,
            controller: widget.controller,
            autofocus: widget.autofocus,
            focusNode: widget.focusNode,
            validator: widget.validate,
            onChanged: widget.onChanged,
            textInputAction: widget.textInputAction,
            obscureText: widget.isPassword,
            initialValue: widget.initialValue,
            cursorColor: kPrimary,
            style: TextStyle(
              fontSize: fontSizeM,
              color: widget.readOnly == true
                  ? const Color(0xFF999999)
                  : _frontendController.darkMode
                      ? const Color(0xFFFFFFFF)
                      : const Color(0xFF000000),
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: _frontendController.darkMode ? kBlack2 : kWhite,
              alignLabelWithHint: true,
              label: widget.isRequired
                  ? RichText(
                      textAlign: TextAlign.start,
                      textScaleFactor: 0.95,
                      text: TextSpan(
                        style: TextStyle(
                          fontSize: fontSizeM,
                          fontWeight: fontWeightNormal,
                          fontFamily: "Sarabun",
                          color: _frontendController.darkMode ? kWhite : kBlack,
                          decoration: TextDecoration.none,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: widget.label!,
                            style: TextStyle(
                              fontSize: fontSizeM,
                              fontWeight: fontWeightNormal,
                              fontFamily: "Sarabun",
                              color: _frontendController.darkMode
                                  ? kWhite
                                  : kBlack,
                            ),
                          ),
                          const TextSpan(
                            text: '*',
                            style: TextStyle(
                              fontSize: fontSizeM,
                              fontWeight: fontWeightNormal,
                              fontFamily: "Sarabun",
                              color: Color(0xFFD50000),
                            ),
                          ),
                        ],
                      ),
                    )
                  : CustomText(
                      text: widget.label!,
                      textSize: fontSizeM,
                      fontWeight: fontWeightNormal,
                    ),
              labelStyle: TextStyle(
                fontSize: fontSizeM,
                color: _frontendController.darkMode ? kWhite : kBlack,
              ),
              floatingLabelStyle: MaterialStateTextStyle.resolveWith(
                  (Set<MaterialState> states) {
                final Color color = states.contains(MaterialState.focused) &&
                        widget.readOnly == false
                    ? kPrimary
                    : _frontendController.darkMode
                        ? kWhite
                        : const Color(0xFF999999);
                return TextStyle(
                  color: color,
                  letterSpacing: 1.3,
                  fontSize: fontSizeM,
                );
              }),
              contentPadding: const EdgeInsets.only(
                  left: 10.0, right: 10.0, top: 16.0, bottom: 16.0),
              hintStyle: TextStyle(
                color: Colors.grey.withOpacity(0.7),
                fontSize: fontSizeM,
              ),
              suffixIcon: widget.displaySuffixIcon
                  ? widget.isShowPassEye
                      ? IconButton(
                          onPressed: () {
                            setState(() {
                              widget.isPassword = widget.isPassword == false;
                            });
                          },
                          icon: Icon(
                            widget.isPassword
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                          ),
                        )
                      : Icon(widget.suffixIcon)
                  : null,
              focusedBorder: OutlineInputBorder(
                gapPadding: 0.0,
                borderRadius: BorderRadius.circular(outlineRadius),
                borderSide: BorderSide(
                    color: widget.readOnly == false
                        ? kPrimary
                        : _frontendController.darkMode
                            ? kWhite
                            : const Color(0xFF999999),
                    width: 1.5),
              ),
              enabledBorder: OutlineInputBorder(
                gapPadding: 0.0,
                borderRadius: BorderRadius.circular(outlineRadius),
                borderSide: BorderSide(
                    color: _frontendController.darkMode
                        ? kWhite
                        : const Color(0xFFD3D3D3),
                    width: 1.5),
              ),
              errorBorder: OutlineInputBorder(
                gapPadding: 0.0,
                borderRadius: BorderRadius.circular(outlineRadius),
                borderSide:
                    const BorderSide(color: Color(0xFFD50000), width: 1.5),
              ),
              border: OutlineInputBorder(
                gapPadding: 0.0,
                borderRadius: BorderRadius.circular(outlineRadius),
                borderSide: BorderSide(
                    color: _frontendController.darkMode
                        ? kWhite
                        : const Color(0xFF999999),
                    width: 1.5),
              ),
              focusedErrorBorder: OutlineInputBorder(
                gapPadding: 0.0,
                borderRadius: BorderRadius.circular(outlineRadius),
                borderSide: const BorderSide(
                  color: Color(0xFFD50000),
                  width: 1.5,
                ),
              ),
            ),
            inputFormatters: widget.inputFormatter,
            onSaved: widget.onSave,
            onFieldSubmitted: widget.onFieldSubmitted,
            keyboardType: widget.inputType,
            maxLength: widget.maxLength,
          ),
        ),
      ],
    );
  }
}
