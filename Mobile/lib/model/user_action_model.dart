import 'dart:convert';

import 'package:project14plus/model/model.dart';

UserActionModel userActionModelModelFromJson(String str) =>
    UserActionModel.fromJson(json.decode(str));

String userActionModelToJson(UserActionModel data) =>
    json.encode(data.toJson());

class UserActionModel {
  UserActionModel({
    this.watchingSeconds,
    this.scoreType,
    this.questions,
    this.score,
    this.maxScore,
    this.minScore,
    this.status,
    this.id,
    this.user,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  double? watchingSeconds;
  int? scoreType;
  List<QuestionModel>? questions;
  double? score;
  double? maxScore;
  double? minScore;
  int? status;
  String? id;
  UserModel? user;
  Content? content;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;
  factory UserActionModel.fromJson(Map<String, dynamic> json) =>
      UserActionModel(
        watchingSeconds: json["watchingSeconds"] == null
            ? null
            : json["watchingSeconds"]!.toDouble(),
        scoreType:
            json["scoreType"] == null ? null : json["scoreType"]!.toInt(),
        questions: json["questions"] == null
            ? null
            : List<QuestionModel>.from(
                json["questions"].map((x) => QuestionModel.fromJson(x))),
        score: json["score"] == null ? null : json["score"]!.toDouble(),
        maxScore:
            json["maxScore"] == null ? null : json["maxScore"]!.toDouble(),
        minScore:
            json["minScore"] == null ? null : json["minScore"]!.toDouble(),
        status: json["status"] == null ? null : json["status"]!.toInt(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        content:
            json["content"] == null ? null : Content.fromJson(json["content"]),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__V"] == null ? null : json["__v"]!.toInt(),
      );

  Map<String, dynamic> toJson() => {
        "watchingSeconds":
            watchingSeconds == null ? null : watchingSeconds!.toDouble(),
        "scoreType": scoreType == null ? null : scoreType!.toInt(),
        "questions": questions == null
            ? null
            : List<QuestionModel>.from(questions!.map((x) => x.toJson())),
        "score": score == null ? null : score!.toDouble(),
        "maxScore": maxScore == null ? null : maxScore!.toDouble(),
        "minScore": minScore == null ? null : minScore!.toDouble(),
        "status": status == null ? null : status!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "user": user == null ? null : user!.toJson(),
        "content": content == null ? null : content!.toJson(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!.toInt(),
      };
}
