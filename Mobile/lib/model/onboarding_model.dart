// To parse this JSON data, do
//
//     final onboardingModel = onboardingModelFromJson(jsonString);

import 'dart:convert';

OnboardingModel onboardingModelFromJson(String str) =>
    OnboardingModel.fromJson(json.decode(str));

String onboardingModelToJson(OnboardingModel data) =>
    json.encode(data.toJson());

class OnboardingModel {
  OnboardingModel({
    required this.title,
    required this.body,
    required this.image,
  });

  final String title;
  final String body;
  final String image;

  factory OnboardingModel.fromJson(Map<String, dynamic> json) =>
      OnboardingModel(
        title: json["title"],
        body: json["body"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "body": body,
        "image": image,
      };
}
