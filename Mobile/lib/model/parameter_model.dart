class ParameterModel {
  ParameterModel({
    this.contentUrl,
    this.contentId,
    this.isFromPlaylist = false,
    this.playlistUrl,
    this.watchingSeconds,
    this.playlistName,
  });
  String? contentUrl;
  String? contentId;
  bool isFromPlaylist;
  String? playlistUrl;
  double? watchingSeconds;

  String? playlistName;
}
