// ignore_for_file: prefer_null_aware_operators

import 'dart:convert';

import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/utils/date_formatter.dart';

BadgeModel badgeModelFromJson(String str) =>
    BadgeModel.fromJson(json.decode(str));

String badgeModelToJson(BadgeModel data) => json.encode(data.toJson());

class BadgeModel {
  BadgeModel({
    this.watchingSeconds,
    this.scoreType,
    this.questions,
    this.score,
    this.maxScore,
    this.minScore,
    this.status,
    this.id,
    this.user,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final double? watchingSeconds;
  final int? scoreType;
  List<QuestionModel>? questions;
  final int? score;
  final double? maxScore;
  final double? minScore;
  final int? status;
  final String? id;
  final String? user;
  BadgeContent? content;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final int? v;
  factory BadgeModel.fromJson(Map<String, dynamic> json) => BadgeModel(
        watchingSeconds: json["watchingSeconds"] == null
            ? null
            : json["watchingSeconds"].toDouble(),
        scoreType: json["scoreType"] == null ? null : json["scoreType"].toInt(),
        questions: json["questions"] == null
            ? null
            : List<QuestionModel>.from(
                json["questions"].map((x) => QuestionModel.fromJson(x))),
        score: json["score"] == null ? null : json["score"].toInt(),
        maxScore: json["maxScore"] == null ? null : json["maxScore"].toDouble(),
        minScore: json["minScore"] == null ? null : json["minScore"].toDouble(),
        status: json["status"] == null ? null : json["status"].toInt(),
        id: json["_id"] == null ? null : json["_id"].toString(),
        user: json["user"] == null ? null : json["user"].toString(),
        content: json["content"] == null
            ? null
            : BadgeContent.fromJson(json["content"]),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"].toInt(),
      );

  Map<String, dynamic> toJson() => {
        "watchingSeconds":
            watchingSeconds == null ? null : watchingSeconds!.toDouble(),
        "scoreType": scoreType == null ? null : scoreType!.toInt(),
        "questions": questions == null
            ? null
            : List<dynamic>.from(questions!.map((e) => e.toJson())),
        "score": score == null ? null : score!.toInt(),
        "maxScore": maxScore == null ? null : maxScore!.toDouble(),
        "minScore": minScore == null ? null : minScore!.toDouble(),
        "status": status == null ? null : status!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "user": user == null ? null : user!.toString(),
        "content": content == null ? null : content!.toJson(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!.toInt(),
      };

  String displayScoreType() {
    if (scoreType != null) {
      if (scoreType == 0) {
        return "ไม่เก็บคะแนน";
      } else if (scoreType == 1) {
        return "เก็บคะแนนสูงสุด";
      } else {
        return "เก็บคะแนนครั้งล่าสุด";
      }
    }
    return 'ไม่มีข้อมูล';
  }

  String displayDate() {
    if (createdAt != null) {
      return DateFormatter.formatterddMMYYYY(updatedAt!, withHour: false);
    } else {
      return '-';
    }
  }
}

class BadgeContent {
  BadgeContent({
    this.badge,
    this.badgeName,
    this.id,
  });

  final String? badge;
  final String? badgeName;
  final String? id;

  factory BadgeContent.fromJson(Map<String, dynamic> json) => BadgeContent(
        id: json["_id"] == null ? null : json["_id"].toString(),
        badge: json["badge"] == null
            ? null
            : json["badge"] == "/assets/img/default/img.jpg"
                ? PATH_DEFAULT
                : json["badge"].toString(),
        badgeName:
            json["badgeName"] == null ? null : json["badgeName"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id!.toString(),
        "badge": badge == null ? null : badge!.toString(),
        "badgeName": badgeName == null ? null : badgeName!.toString(),
      };
}
// class User {
//   User({
//     this.id,
//     this.externalId,
//     this.role,
//     this.level,
//     this.subjects,
//     this.tags,
//     this.firstname,
//     this.lastname,
//     this.username,
//     this.email,
//     this.avatar,
//     this.status,
//     this.accessToken,
//     this.refreshToken,
//   });
//   final String? id;
//   final int? externalId;
//   Role? role;
//   Level? level;
//   final List<Level>? subjects;
//   final List<Level>? tags;
//   final String? firstname;
//   final String? lastname;
//   final String? username;
//   final String? email;
//   String? avatar;
//   // Detail? detail;
//   final int? status;
//   final String? accessToken;
//   final String? refreshToken;

  // factory User.fromJson(Map<String, dynamic> json) => User(
  //       id: json["_id"] == null ? null : json["_id"] as String,
  //       externalId:
  //           json["externalId"] == null ? null : json["externalId"] as int,
  //       role: json["role"] == null ? null : Role.fromJson(json["role"]),
  //       level: json["level"] == null ? null : Level.fromJson(json["level"]),
  //       subjects: json["subjects"] == null
  //           ? null
  //           : List<Level>.from(json["subjects"].map((x) => Level.fromJson(x))),
  //       tags: json["tags"] == null
  //           ? null
  //           : List<Level>.from(json["tags"].map((x) => Level.fromJson(x))),
  //       firstname:
  //           json["firstname"] == null ? null : json["firstname"] as String,
  //       lastname: json["lastname"] == null ? null : json["lastname"] as String,
  //       username: json["username"] == null ? null : json["username"] as String,
  //       email: json["email"] == null ? null : json["email"] as String,
  //       avatar: json["avatar"] == null
  //           ? null
  //           : json['avatar'] == "/assets/img/avatar/default.jpg"
  //               ? null
  //               : json["avatar"],
  //       status: json["status"] == null ? null : json["status"] as int,
  //       accessToken:
  //           json["accessToken"] == null ? null : json["accessToken"] as String,
  //       refreshToken: json["refreshToken"] == null
  //           ? null
  //           : json["refreshToken"] as String,
  //     );

  // Map<String, dynamic> toJson() => {
  //       "_id": id == null ? null : id as String,
  //       "externalId": externalId == null ? null : externalId as int,
  //       "role": role == null ? null : role!.toJson(),
  //       "level": level == null ? null : level!.toJson(),
  //       "subjects": subjects == null
  //           ? null
  //           : List<dynamic>.from(subjects!.map((e) => e.toJson())),
  //       "tags": tags == null
  //           ? null
  //           : List<dynamic>.from(tags!.map((e) => e.toJson())),
  //       "firstname": firstname == null ? null : firstname as String,
  //       "lastname": lastname == null ? null : lastname as String,
  //       "username": username == null ? null : username as String,
  //       "email": email == null ? null : email as String,
  //       "avatar": avatar == null
  //           ? null
  //           : avatar == "/assets/img/avatar/default.jpg"
  //               ? null
  //               : avatar,
  //       "status": status == null ? null : status as int,
  //       "accessToken": accessToken == null ? null : accessToken as String,
  //       "refreshToken": refreshToken == null ? null : refreshToken as String,
  //     };
// }

