// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:project14plus/model/content/content.dart';

HistoryModel historyModelFromJson(String str) =>
    HistoryModel.fromJson(json.decode(str));
String historyModelToJson(HistoryModel data) => json.encode(data.toJson);

class HistoryModel {
  HistoryModel({
    this.watchingSeconds,
    this.scoreType,
    this.score,
    this.maxScore,
    this.minScore,
    this.status,
    this.id,
    this.user,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  // final double? watchingSeconds;
  // final int? scoreType;
  // // final questions
  // final double? score;
  // final double? maxScore;
  // final double? minScore;
  // final double? status;
  // final String? id;
  // final String? user;
  // Content? content;
  // final DateTime? createdAt;
  // final DateTime? updatedAt;
  // final int? v;
  var watchingSeconds;
  var scoreType;
  // final questions
  var score;
  var maxScore;
  var minScore;
  var status;
  var id;
  var user;
  Content? content;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  var v;

  factory HistoryModel.fromJson(Map<String, dynamic> json) => HistoryModel(
        // watchingSeconds: json["watchingSeconds"] == null
        //     ? null
        //     : double.parse(json["watchingSeconds"]),
        // scoreType:
        //     json["scoreType"] == null ? null : int.parse(json["scoreType"]),
        // score: json["score"] == null ? null : double.parse(json["score"]),
        // maxScore:
        //     json["maxScore"] == null ? null : double.parse(json["maxScore"]),
        // minScore:
        //     json["minScore"] == null ? null : double.parse(json["minScore"]),
        // status: json["status"] == null ? null : double.parse(json["status"]),
        // id: json["_id"] == null ? null : json["_id"]!.toString(),
        // user: json["user"] == null ? null : json["user"]!.toString(),
        // content:
        //     json["content"] == null ? null : Content.fromJson(json["content"]),
        // createdAt: json["createdAt"] == null
        //     ? null
        //     : DateTime.parse(json["createdAt"]),
        // updatedAt: json["updatedAt"] == null
        //     ? null
        //     : DateTime.parse(json["updatedAt"]),
        // v: json["__v"] == null ? null : int.parse(json["__v"]),
        watchingSeconds:
            json["watchingSeconds"] == null ? null : json["watchingSeconds"]!,
        scoreType: json["scoreType"] == null ? null : json["scoreType"]!,
        score: json["score"] == null ? null : json["score"]!,
        maxScore: json["maxScore"] == null ? null : json["maxScore"]!,
        minScore: json["minScore"] == null ? null : json["minScore"]!,
        status: json["status"] == null ? null : json["status"]!,
        id: json["_id"] == null ? null : json["_id"]!,
        user: json["user"] == null ? null : json["user"]!.toString(),
        content:
            json["content"] == null ? null : Content.fromJson(json["content"]),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"]!,
      );

  Map<String, dynamic> toJson() => {
        "watchingSeconds": watchingSeconds == null ? null : watchingSeconds!,
        "scoreType": scoreType == null ? null : scoreType!,
        "score": score == null ? null : score!,
        "maxScore": maxScore == null ? null : maxScore!,
        "minScore": minScore == null ? null : minScore!,
        "status": status == null ? null : status!,
        "_id": id == null ? null : id!,
        "user": user == null ? null : user!,
        "content": content == null ? null : content!.toJson(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!,
      };
}

class Content {
  Content({
    this.description,
    this.metadesc,
    this.image,
    this.youtubeVideoId,
    this.filePath,
    this.scoreType,
    this.minScore = 0,
    this.badge,
    this.badgeName,
    this.approvedStatus,
    this.isFeatured = 0,
    this.visitCount = 0,
    this.order,
    this.status,
    this.id,
    this.user,
    this.playlist,
    this.subject,
    this.name,
    this.url,
    this.type,
    this.approver,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  List<String>? levels;
  List<String>? tags;
  List<String>? keywords;

  String? description;
  String? metadesc;
  String? image;
  String? youtubeVideoId;
  String? filePath;
  List<QuestionModel>? videoQuestions;
  int? scoreType;
  List<QuestionModel>? questions;
  int minScore;
  String? badge;
  String? badgeName;
  int? approvedStatus;
  int? isFeatured;
  int visitCount;
  int? order;
  int? status;

  ///
  String? id;
  String? user;
  String? playlist;
  String? subject;
  String? name;
  String? url;
  int? type;
  String? approver;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  int? v;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        description: json["description"] == null ? null : json["description"]!,
        metadesc:
            json["metadesc"] == null ? null : json["metadesc"]!.toString(),
        image: json["image"] == null ? null : json["image"]!,
        youtubeVideoId:
            json["youtubeVideoId"] == null ? null : json["youtubeVideoId"]!,
        filePath: json["filePath"] == null ? null : json["filePath"]!,
        scoreType: json["scoreType"] == null ? null : json["scoreType"]!,
        minScore: json["minScore"] == null ? null : json["minScore"]!,
        badge: json["badge"] == null ? null : json["badge"]!,
        badgeName: json["badgeName"] == null ? null : json["badgeName"]!,
        approvedStatus:
            json["approvedStatus"] == null ? null : json["approvedStatus"]!,
        isFeatured: json["isFeatured"] == null ? null : json["isFeatured"]!,
        visitCount: json["visitCount"] == null ? 0 : json["visitCount"]!,
        order: json["order"] == null ? null : json["order"]!,
        status: json["status"] == null ? null : json["status"]!,
        id: json["_id"] == null ? null : json["_id"]!,
        user: json["user"] == null ? null : json["user"]!,
        playlist: json["playlist"] == null ? null : json["playlist"]!,
        subject: json["subject"] == null ? null : json["subject"]!,
        name: json["name"] == null ? null : json["name"]!,
        url: json["url"] == null ? null : json["url"]!,
        type: json["type"] == null ? null : json["type"]!,
        approver: json["approver"] == null ? null : json["approver"]!,
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"]!,
      );
  Map<String, dynamic> toJson() => {
        "description": description == null ? null : description!,
        "metadesc": metadesc == null ? null : metadesc!,
        "image": image == null ? null : image!,
        "youtubeVideoId": youtubeVideoId == null ? null : youtubeVideoId!,
        "filePath": filePath == null ? null : filePath!,
        "scoreType": scoreType == null ? null : scoreType!,
        "minScore": minScore,
        "badge": badge == null ? null : badge!,
        "badgeName": badgeName == null ? null : badgeName!,
        "approvedStatus": approvedStatus == null ? null : approvedStatus!,
        "isFeatured": isFeatured == null ? null : isFeatured!,
        "visitCount": visitCount,
        "order": order == null ? null : order!,
        "status": status == null ? null : status!,
        "_id": id == null ? null : id!,
        "user": user == null ? null : user!,
        "playlist": playlist == null ? null : playlist!,
        "subject": subject == null ? null : subject!,
        "name": name == null ? null : name!,
        "url": url == null ? null : url!,
        "type": type == null ? null : type!,
        "approver": approver == null ? null : approver!,
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!,
      };

  String visitCountFormat() {
    if (visitCount != null && visitCount != '') {
      return NumberFormat("##,###", "en_US").format(visitCount);
    }
    return '';
  }
}
