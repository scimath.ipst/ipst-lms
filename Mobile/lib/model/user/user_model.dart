import 'dart:convert';

UserModel userFromJson(String str) => UserModel.fromJson(json.decode(str));

String userToJson(UserModel? data) => json.encode(data?.toJson());

class UserModel {
  UserModel({
    this.id,
    this.externalId,
    this.role,
    this.level,
    this.subjects,
    this.tags,
    this.firstname,
    this.lastname,
    this.username,
    this.email,
    this.avatar,
    this.status,
    this.accessToken,
    this.refreshToken,
    this.fcmToken,
  });
  final String? id;
  final int? externalId;
  Role? role;
  Level? level;
  List<Level>? subjects;
  List<Level>? tags;
  String? firstname;
  String? lastname;
  String? username;
  String? email;
  String? avatar;
  // Detail? detail;
  final int? status;
  final String? accessToken;
  final String? refreshToken;
  String? fcmToken;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        externalId:
            json["externalId"] == null ? null : json["externalId"]!.toInt(),
        role: json["role"] == null ? null : Role.fromJson(json["role"]),
        level: json["level"] == null ? null : Level.fromJson(json["level"]),
        subjects: json["subjects"] == null
            ? null
            : List<Level>.from(json["subjects"].map((x) => Level.fromJson(x))),
        tags: json["tags"] == null
            ? null
            : List<Level>.from(json["tags"].map((x) => Level.fromJson(x))),
        firstname:
            json["firstname"] == null ? null : json["firstname"]!.toString(),
        lastname:
            json["lastname"] == null ? null : json["lastname"]!.toString(),
        username:
            json["username"] == null ? null : json["username"]!.toString(),
        email: json["email"] == null ? null : json["email"]!.toString(),
        avatar: json["avatar"] == null
            ? null
            : json['avatar'] == "/assets/img/avatar/default.jpg" ||
                    json['avatar'] ==
                        "http://sso.ipst.ac.th//assets/img/avatar/default.jpg" ||
                    json['avatar'] ==
                        "http://94.74.118.252:4800/file-1636959589739.jpg"
                ? null
                : json["avatar"],
        status: json["status"] == null ? null : json["status"]!.toInt(),
        accessToken: json["accessToken"] == null
            ? null
            : json["accessToken"]!.toString(),
        refreshToken: json["refreshToken"] == null
            ? null
            : json["refreshToken"]!.toString(),
        fcmToken:
            json["fcmToken"] == null ? null : json["fcmToken"]!.toString(),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id!.toString(),
        "externalId": externalId == null ? null : externalId!.toInt(),
        "role": role == null ? null : role!.toJson(),
        "level": level == null ? null : level!.toJson(),
        "subjects": subjects == null
            ? null
            : List<dynamic>.from(subjects!.map((e) => e.toJson())),
        "tags": tags == null
            ? null
            : List<dynamic>.from(tags!.map((e) => e.toJson())),
        "firstname": firstname == null ? null : firstname!.toString(),
        "lastname": lastname == null ? null : lastname!.toString(),
        "username": username == null ? null : username!.toString(),
        "email": email == null ? null : email!.toString(),
        "avatar": avatar == null
            ? null
            : avatar == "/assets/img/avatar/default.jpg" ||
                    avatar ==
                        "http://sso.ipst.ac.th//assets/img/avatar/default.jpg" ||
                    avatar == "http://94.74.118.252:4800/file-1636959589739.jpg"
                ? null
                : avatar,
        "status": status == null ? null : status!.toInt(),
        "accessToken": accessToken == null ? null : accessToken!.toString(),
        "refreshToken": refreshToken == null ? null : refreshToken!.toString(),
        "fcmToken": fcmToken == null ? null : fcmToken!.toString(),
      };
}

class Role {
  Role({
    this.level,
    this.id,
    this.name,
    this.nameEng,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final int? level;
  final String? id;
  final String? name;
  final String? nameEng;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final int? v;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
        level: json["level"] == null ? null : json["level"]!.toInt(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        name: json["name"] == null ? null : json["name"]!.toString(),
        nameEng: json["nameEng"] == null ? null : json["nameEng"]!.toString(),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"]!.toInt(),
      );

  Map<String, dynamic> toJson() => {
        "level": level == null ? null : level!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "name": name == null ? null : name!.toString(),
        "nameEng": nameEng == null ? null : nameEng!.toString(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!.toInt(),
      };
}

class Level {
  Level({
    this.id,
    this.name,
    this.url,
  });

  final String? id;
  final String? name;
  final String? url;

  factory Level.fromJson(Map<String, dynamic> json) => Level(
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        name: json["name"] == null ? null : json["name"]!.toString(),
        url: json["url"] == null ? null : json["url"]!.toString(),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id!.toString(),
        "name": name == null ? null : name!.toString(),
        "url": url == null ? null : url!.toString(),
      };
}
