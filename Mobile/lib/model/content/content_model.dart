// ignore_for_file: prefer_if_null_operators, prefer_null_aware_operators

import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:project14plus/model/model.dart';

ContentModel contentModelFromJson(String str) =>
    ContentModel.fromJson(json.decode(str));

String contentModelToJson(ContentModel data) => json.encode(data.toJson());

class ContentModel {
  ContentModel({
    this.levels,
    this.tag,
    this.image,
    this.visitCount,
    this.id,
    this.user,
    this.subject,
    this.name,
    this.url,
    this.createdAt,
  });
  List<LevelModel>? levels;
  final String? tag; //List<dynamic>
  final String? image;
  final int? visitCount;
  final String? id;
  UserModel? user;
  Subject? subject;
  final String? name;
  final String? url;
  final DateTime? createdAt;
  factory ContentModel.fromJson(Map<String, dynamic> json) => ContentModel(
        levels: json["levels"] == null
            ? null
            : List<LevelModel>.from(
                json["levels"].map((x) => LevelModel.fromJson(x))),
        tag: json["tag"] == null ? null : json["tag"],
        image: json["image"] == null ||
                json["image"] == "/assets/img/default/img.jpg"
            ? null
            : json["image"],
        visitCount: json["visitCount"] == null ? 0 : json["visitCount"],
        id: json["_id"] == null ? null : json["_id"],
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        subject:
            json["subject"] == null ? null : Subject.fromJson(json["subject"]),
        name: json["name"] == null ? null : json["name"],
        url: json["url"] == null ? null : json["url"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "levels": levels == null
            ? null
            : List<dynamic>.from(levels!.map((e) => e.toJson())),
        "tag": tag == null ? null : tag,
        "image": image == null ? null : image,
        "visitCount": visitCount == null ? 0 : visitCount,
        "_id": id == null ? null : id,
        "user": user == null ? null : user!.toJson(),
        "subject": subject == null ? null : subject!.toJson(),
        "name": name == null ? null : name,
        "url": url == null ? null : url,
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String()
      };

  bool isValid() {
    return id != null ? true : false;
  }

  String visitCountFormat() {
    if (isValid() && visitCount != null && visitCount != '') {
      return NumberFormat("##,###", "en_US").format(visitCount);
    }
    return '';
  }
}

class Subject {
  Subject({
    this.image,
    this.id,
    this.name,
    this.url,
  });
  final String? image;
  final String? id;
  final String? name;
  final String? url;
  factory Subject.fromJson(Map<String, dynamic> json) => Subject(
        image: json["image"] == null ? null : json["image"],
        id: json["_id"] == null ? null : json["_id"],
        name: json["name"] == null ? null : json["name"],
        url: json["url"] == null ? null : json["url"],
      );
  Map<String, dynamic> toJson() => {
        "image": image == null ? null : image,
        "_id": id == null ? null : id,
        "name": name == null ? null : name,
        "url": url == null ? null : url,
      };
}
