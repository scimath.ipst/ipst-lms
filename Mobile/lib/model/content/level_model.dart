import 'dart:convert';

LevelModel levelModelFromJson(String str) =>
    LevelModel.fromJson(json.decode(str));
String levelModelToJson(LevelModel data) => json.encode(data.toJson());

class LevelModel {
  LevelModel({
    this.description,
    this.metadesc,
    this.image,
    this.order,
    this.status,
    this.id,
    this.name,
    this.url,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  final String? description;
  final String? metadesc;
  //  Keywords? keywords;
  final String? image;
  final int? order;
  final int? status;
  final String? id;
  final String? name;
  final String? url;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final int? v;

  factory LevelModel.fromJson(Map<String, dynamic> json) => LevelModel(
        description: json["description"] == null
            ? null
            : json["description"]!.toString(),
        metadesc:
            json["metadesc"] == null ? null : json["metadesc"]!.toString(),
        image: json["image"] == null ? null : json["image"]!.toString(),
        order: json["order"] == null ? null : json["order"]!.toInt(),
        status: json["status"] == null ? null : json["status"]!.toInt(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        name: json["name"] == null ? null : json["name"]!.toString(),
        url: json["url"] == null ? null : json["url"]!.toString(),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"]!.toInt(),
      );

  Map<String, dynamic> toJson() => {
        "description": description == null ? null : description!.toString(),
        "metadesc": metadesc == null ? null : metadesc!.toString(),
        "image": image == null ? null : image!.toString(),
        "order": order == null ? null : order!.toInt(),
        "status": status == null ? null : status!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "name": name == null ? null : name!.toString(),
        "url": url == null ? null : url!.toString(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String,
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String,
        "__v": v == null ? null : v!.toString(),
      };
}
