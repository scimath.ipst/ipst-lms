import 'dart:convert';

import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/home/components/continued_content.dart';

ContinuedModel continuedModelFromJson(String str) =>
    ContinuedModel.fromJson(json.decode(str));

String continuedModelToJson(ContinuedModel data) => json.encode(data.toJson());

class ContinuedModel {
  ContinuedModel({
    this.watchingSeconds,
    this.id,
    this.content,
  });
  final double? watchingSeconds; //List<dynamic>
  final String? id;
  ContentModel? content;

  factory ContinuedModel.fromJson(Map<String, dynamic> json) => ContinuedModel(
        watchingSeconds: json["watchingSeconds"] == null
            ? null
            : json["watchingSeconds"]!.toDouble(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        content: json["content"] == null
            ? null
            : ContentModel.fromJson(json["content"]),
      );

  Map<String, dynamic> toJson() => {
        "watchingSeconds":
            watchingSeconds == null ? null : watchingSeconds!.toDouble(),
        "_id": id == null ? null : id!.toString(),
        "content": content == null ? null : content!.toJson(),
      };

  String displayWatchingSeconds() {
    if (watchingSeconds != null &&
        watchingSeconds != '' &&
        watchingSeconds != 0) {
      return formatTime((watchingSeconds!).round());
    }
    return '00:00:00';
  }
}
