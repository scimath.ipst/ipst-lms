import 'dart:convert';

QuestionModel questionModelFromJson(String str) =>
    QuestionModel.fromJson(json.decode(str));

String questionModelToJson(QuestionModel data) => json.encode(data.toJson());

class QuestionModel {
  QuestionModel({
    this.question,
    this.at = 0,
    this.choices,
    this.score,
    this.selectedChoice,
    this.state,
  });
  final String? question;
  int at;
  List<Choices>? choices;
  int? score;
  String? selectedChoice;
  final int? state;

  factory QuestionModel.fromJson(Map<String, dynamic> json) => QuestionModel(
        question:
            json["question"] == null ? null : json["question"]!.toString(),
        at: json["at"] == null || json["at"] == ''
            ? 0
            : (json["at"] is String)
                ? int.parse(json["at"]!)
                : json["at"]!,
        choices: json["choices"] == null
            ? null
            : List<Choices>.from(
                json["choices"].map((x) => Choices.fromJson(x))),
        score: json["score"] == null ? null : json["score"]!,
        selectedChoice: json["selectedChoice"] == null
            ? null
            : json["selectedChoice"]!.toString(),
        state: json["state"] == null ? null : json["state"]!,
      );

  Map<String, dynamic> toJson() => {
        "question": question == null ? null : question!.toString(),
        "at": at == null || at == '' ? 0 : at,
        "choices": choices == null
            ? null
            : List<dynamic>.from(choices!.map((e) => e.toJson())),
        "score": score == null ? null : score!,
        "selectedChoice": selectedChoice == null ? null : selectedChoice!,
        "state": state == null ? null : state!,
      };
}

class Choices {
  Choices({
    this.text,
    this.selected = false,
    this.isCorrect,
    this.explaination = '',
    this.goTo = 0,
  });
  final String? text; //List<dynamic>
  final bool? isCorrect;
  bool? selected;
  String explaination;
  int goTo;

  factory Choices.fromJson(Map<String, dynamic> json) => Choices(
        text: json["text"] == null ? null : json["text"]!.toString(),
        isCorrect: json["isCorrect"] == null ? null : json["isCorrect"]!,
        explaination: json["explaination"] == null || json["explaination"] == ''
            ? ''
            : json["explaination"]!,
        goTo: json["goTo"] == null || json["goTo"] == '' ? 0 : json["goTo"]!,
      );

  Map<String, dynamic> toJson() => {
        "text": text == null ? null : text!.toString(),
        "isCorrect": isCorrect == null ? null : isCorrect!,
        "explaination": explaination == null ? '' : explaination,
        "goTo": goTo == null || goTo == '' ? 0 : goTo,
      };
}
