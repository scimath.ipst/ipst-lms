
// import 'dart:convert';

// import 'package:project14plus/constants/asset_path.dart';

// ChannelsModel channelsModelFromJson(String str) =>
//     ChannelsModel.fromJson(json.decode(str));

// String channelsModelToJson(ChannelsModel data) => json.encode(data.toJson());

// class ChannelsModel {
//   ChannelsModel({
//     this.description,
//     this.image,
//     this.id,
//     this.department,
//     this.name,
//     this.url,
//     this.createdAt,
//   });

//   final String? description;
//   final String? image;
//   final String? id;
//   Department? department;
//   final String? name;
//   final String? url;
//   final DateTime? createdAt;

//   factory ChannelsModel.fromJson(Map<String, dynamic> json) => ChannelsModel(
//         description:
//             json["description"] == null ? null : json["description"].toString(),
//         image: json["image"] == null
//             ? null
//             : json["image"] == "/assets/img/default/img.jpg"
//                 ? PATH_DEFAULT
//                 : json["image"].toString(),
//         id: json["_id"] == null ? null : json["_id"].toString(),
//         department: json["department"] == null
//             ? null
//             : Department.fromJson(json['department']),
//         name: json["name"] == null ? null : json["name"].toString(),
//         url: json["url"] == null ? null : json["url"].toString(),
//         createdAt: json["createdAt"] == null
//             ? null
//             : DateTime.parse(json["createdAt"]),
//       );
//   Map<String, dynamic> toJson() => {
//         "description": description == null ? null : description.toString(),
//         "image": image == null
//             ? null
//             : image == "/assets/img/default/img.jpg"
//                 ? PATH_DEFAULT
//                 : image.toString(),
//         "_id": id == null ? null : id.toString(),
//         "department": department == null ? null : department!.toJson(),
//         "name": name == null ? null : name.toString(),
//         "url": url == null ? null : url.toString(),
//         "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
//       };
// }
