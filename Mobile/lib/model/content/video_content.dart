import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/utils/date_formatter.dart';

VideoContent videoContentFromJson(String str) =>
    VideoContent.fromJson(json.decode(str));

String videoContentToJson(VideoContent data) => json.encode(data.toJson());

class VideoContent {
  VideoContent({
    this.result,
    this.userAction,
  });
  VideoContentModel? result;
  UserActionModel? userAction;

  factory VideoContent.fromJson(Map<String, dynamic> json) => VideoContent(
        result: json["result"] == null
            ? null
            : VideoContentModel.fromJson(json["result"]),
        userAction: json["userAction"] == null
            ? null
            : UserActionModel.fromJson(json["userAction"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result!.toJson(),
        "userAction": userAction == null ? null : userAction!.toJson(),
      };
}

class VideoContentModel {
  VideoContentModel({
    this.levels,
    this.tags,
    this.description,
    this.metadesc,
    this.image,
    this.youtubeVideoId,
    this.filePath,
    this.videoQuestions,
    this.scoreType,
    this.questions,
    this.minScore,
    this.badge,
    this.badgeName,
    this.approvedStatus,
    this.isFeatured,
    this.visitCount,
    this.order,
    this.status,
    this.id,
    this.user,
    this.playlist,
    this.subject,
    this.name,
    this.url,
    this.type,
    this.approver,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  List<LevelModel>? levels;
  List<LevelModel>? tags;
  String? description;
  String? metadesc;
  // List<Keywords>? keywords;
  String? image;
  String? youtubeVideoId;
  String? filePath;
  List<QuestionModel>? videoQuestions;
  int? scoreType;
  List<QuestionModel>? questions;
  double? minScore;
  String? badge;
  String? badgeName;
  int? approvedStatus;
  int? isFeatured;
  String? visitCount;
  int? order;
  int? status;
  String? id;
  UserModel? user;
  PlayListModel? playlist;
  Subject? subject;
  String? name;
  String? url;
  int? type;
  String? approver;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory VideoContentModel.fromJson(Map<String, dynamic> json) =>
      VideoContentModel(
        levels: json["levels"] == null
            ? null
            : List<LevelModel>.from(
                json["levels"].map((x) => LevelModel.fromJson(x))),
        tags: json["tags"] == null
            ? null
            : List<LevelModel>.from(
                json["tags"].map((x) => LevelModel.fromJson(x))),
        description: json["description"] == null
            ? null
            : json["description"]!.toString(),
        metadesc:
            json["metadesc"] == null ? null : json["metadesc"]!.toString(),
        image: json["image"] == null ? null : json["image"]!.toString(),
        youtubeVideoId: json["youtubeVideoId"] == null
            ? null
            : json["youtubeVideoId"]!.toString(),
        filePath:
            json["filePath"] == null ? null : json["filePath"]!.toString(),
        videoQuestions: json["videoQuestions"] == null
            ? null
            : List<QuestionModel>.from(
                json["videoQuestions"].map((x) => QuestionModel.fromJson(x))),
        scoreType:
            json["scoreType"] == null ? null : json["scoreType"]!.toInt(),
        questions: json["questions"] == null
            ? null
            : List<QuestionModel>.from(
                json["questions"].map((x) => QuestionModel.fromJson(x))),
        minScore:
            json["minScore"] == null ? null : json["minScore"]!.toDouble(),
        badge: json["badge"] == null ? null : json["badge"]!.toString(),
        badgeName:
            json["badgeName"] == null ? null : json["badgeName"]!.toString(),
        approvedStatus: json["approvedStatus"] == null
            ? null
            : json["approvedStatus"]!.toInt(),
        isFeatured:
            json["isFeatured"] == null ? null : json["isFeatured"]!.toInt(),
        visitCount: json["visitCount"] == null
            ? null
            : NumberFormat("##,###", "en_US")
                .format(json["visitCount"]!.toInt()),
        order: json["order"] == null ? null : json["order"]!.toInt(),
        status: json["status"] == null ? null : json["status"]!.toInt(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        playlist: json["playlist"] == null
            ? null
            : PlayListModel.fromJson(json["playlist"]),
        subject:
            json["subject"] == null ? null : Subject.fromJson(json["subject"]),
        name: json["name"] == null ? null : json["name"]!.toString(),
        url: json["url"] == null ? null : json["url"]!.toString().trim(),
        type: json["type"] == null ? null : json["type"]!.toInt(),
        approver:
            json["approver"] == null ? null : json["approver"]!.toString(),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"]!.toInt(),
      );
  Map<String, dynamic> toJson() => {
        "levels": levels == null
            ? null
            : List<LevelModel>.from(levels!.map((x) => x.toJson())),
        "tags":
            tags == null ? null : List<Tags>.from(tags!.map((x) => x.toJson())),
        "description": description == null ? null : description!.toString(),
        "metadesc": metadesc == null ? null : metadesc!.toString(),
        "image": image == null ? null : image!.toString(),
        "youtubeVideoId":
            youtubeVideoId == null ? null : youtubeVideoId!.toString(),
        "filePath": filePath == null ? null : filePath!.toString(),
        "scoreType": scoreType == null ? null : scoreType!.toInt(),
        "questions": questions == null
            ? null
            : List<QuestionModel>.from(questions!.map((x) => x.toJson())),
        "minScore": minScore == null ? null : minScore!.toDouble(),
        "badge": badge == null ? null : badge!.toString(),
        "badgeName": badgeName == null ? null : badgeName!.toString(),
        "approvedStatus":
            approvedStatus == null ? null : approvedStatus!.toInt(),
        "isFeatured": isFeatured == null ? null : isFeatured!.toInt(),
        "visitCount": visitCount == null
            ? null
            : NumberFormat("##,###", "en_US").format(visitCount!),
        "order": order == null ? null : order!.toInt(),
        "status": status == null ? null : status!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "user": user == null ? null : user!.toJson(),
        "playlist": playlist == null ? null : playlist!.toJson(),
        "subject": subject == null ? null : subject!.toJson(),
        "name": name == null ? null : name!.toString(),
        "url": url == null ? null : url!.toString().trim(),
        "type": type == null ? null : type!.toInt(),
        "approver": approver == null ? null : approver!.toString(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!.toInt(),
      };

  bool isValid() {
    return id != null ? true : false;
  }

  String displayVisitCount() {
    if (isValid()) {
      return 'ผู้ชม ${visitCount.toString()} คน \u2022 ${DateFormatter.formatterddMMYYYY(createdAt!, withHour: false)}';
    }
    return '-';
  }
}

class Tags {
  Tags({
    this.id,
    this.name,
    this.status,
  });

  final String? id;
  final String? name;
  final int? status;

  factory Tags.fromJson(Map<String, dynamic> json) => Tags(
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        name: json["name"] == null ? null : json["name"]!.toString(),
        status: json["status"] == null ? null : json["status"]!.toInt(),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id!.toString(),
        "name": name == null ? null : name!.toString(),
        "status": status == null ? null : status!.toInt(),
      };
}

class PlayListModel {
  PlayListModel({
    this.status,
    this.id,
    this.channel,
    this.name,
    this.url,
  });
  int? status;
  String? id;
  Tags? channel;
  String? name;
  String? url;

  factory PlayListModel.fromJson(Map<String, dynamic> json) => PlayListModel(
        status: json["status"] == null ? null : json["status"]!.toInt(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        channel:
            json["channel"] == null ? null : Tags.fromJson(json["channel"]),
        name: json["name"] == null ? null : json["name"]!.toString(),
        url: json["url"] == null ? null : json["url"]!.toString(),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "channel": channel == null ? null : channel!.toJson(),
        "name": name == null ? null : name!.toString(),
        "url": url == null ? null : url!.toString(),
      };
}

class VideoContentListModel {
  VideoContentListModel({
    this.levels,
    this.description,
    this.metadesc,
    this.image,
    this.youtubeVideoId,
    this.filePath,
    this.scoreType,
    this.minScore,
    this.badge,
    this.badgeName,
    this.approvedStatus,
    this.isFeatured,
    this.visitCount,
    this.order,
    this.status,
    this.id,
    this.user,
    this.playlist,
    this.subject,
    this.name,
    this.url,
    this.type,
    this.approver,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  List<LevelModel>? levels;
  String? description;
  String? metadesc;
  // List<Keywords>? keywords;
  String? image;
  String? youtubeVideoId;
  String? filePath;
  // List<VideoQuestions>? videoQuestions;
  int? scoreType;
  // List<Questions>? questions;
  double? minScore;
  String? badge;
  String? badgeName;
  int? approvedStatus;
  int? isFeatured;
  String? visitCount;
  int? order;
  int? status;
  String? id;
  UserModel? user;
  PlayListModel? playlist;
  Subject? subject;
  String? name;
  String? url;
  int? type;
  String? approver;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory VideoContentListModel.fromJson(Map<String, dynamic> json) =>
      VideoContentListModel(
        levels: json["levels"] == null
            ? null
            : List<LevelModel>.from(
                json["levels"].map((x) => LevelModel.fromJson(x))),
        description: json["description"] == null
            ? null
            : json["description"]!.toString(),
        metadesc:
            json["metadesc"] == null ? null : json["metadesc"]!.toString(),
        image: json["image"] == null ? null : json["image"]!.toString(),
        youtubeVideoId: json["youtubeVideoId"] == null
            ? null
            : json["youtubeVideoId"]!.toString(),
        filePath:
            json["filePath"] == null ? null : json["filePath"]!.toString(),
        scoreType:
            json["scoreType"] == null ? null : json["scoreType"]!.toInt(),
        minScore:
            json["minScore"] == null ? null : json["minScore"]!.toDouble(),
        badge: json["badge"] == null ? null : json["badge"]!.toString(),
        badgeName:
            json["badgeName"] == null ? null : json["badgeName"]!.toString(),
        approvedStatus: json["approvedStatus"] == null
            ? null
            : json["approvedStatus"]!.toInt(),
        isFeatured:
            json["isFeatured"] == null ? null : json["isFeatured"]!.toInt(),
        visitCount: json["visitCount"] == null
            ? null
            : NumberFormat("##,###", "en_US")
                .format(json["visitCount"]!.toInt()),
        order: json["order"] == null ? null : json["order"]!.toInt(),
        status: json["status"] == null ? null : json["status"]!.toInt(),
        id: json["_id"] == null ? null : json["_id"]!.toString(),
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        playlist: json["playlist"] == null
            ? null
            : PlayListModel.fromJson(json["playlist"]),
        subject:
            json["subject"] == null ? null : Subject.fromJson(json["subject"]),
        name: json["name"] == null ? null : json["name"]!.toString(),
        url: json["url"] == null ? null : json["url"]!.toString(),
        type: json["type"] == null ? null : json["type"]!.toInt(),
        approver:
            json["approver"] == null ? null : json["approver"]!.toString(),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"]!.toInt(),
      );
  Map<String, dynamic> toJson() => {
        "levels": levels == null
            ? null
            : List<LevelModel>.from(levels!.map((x) => x.toJson())),
        "description": description == null ? null : description!.toString(),
        "metadesc": metadesc == null ? null : metadesc!.toString(),
        "image": image == null ? null : image!.toString(),
        "youtubeVideoId":
            youtubeVideoId == null ? null : youtubeVideoId!.toString(),
        "filePath": filePath == null ? null : filePath!.toString(),
        "scoreType": scoreType == null ? null : scoreType!.toInt(),
        "minScore": minScore == null ? null : minScore!.toDouble(),
        "badge": badge == null ? null : badge!.toString(),
        "badgeName": badgeName == null ? null : badgeName!.toString(),
        "approvedStatus":
            approvedStatus == null ? null : approvedStatus!.toInt(),
        "isFeatured": isFeatured == null ? null : isFeatured!.toInt(),
        "visitCount": visitCount == null
            ? null
            : NumberFormat("##,###", "en_US").format(visitCount!),
        "order": order == null ? null : order!.toInt(),
        "status": status == null ? null : status!.toInt(),
        "_id": id == null ? null : id!.toString(),
        "user": user == null ? null : user!.toJson(),
        "playlist": playlist == null ? null : playlist!.toJson(),
        "subject": subject == null ? null : subject!.toJson(),
        "name": name == null ? null : name!.toString(),
        "url": url == null ? null : url!.toString(),
        "type": type == null ? null : type!.toInt(),
        "approver": approver == null ? null : approver!.toString(),
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "__v": v == null ? null : v!.toInt(),
      };
}
