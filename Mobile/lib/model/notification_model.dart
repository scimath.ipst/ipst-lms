import 'dart:convert';

import 'package:project14plus/utils/utils.dart';

NotificationModel notificationModelFromJson(String str) =>
    NotificationModel.fromJson(json.decode(str));
String notificationModelToJson(NotificationModel model) =>
    json.encode(model.toJson());

class NotificationModel {
  NotificationModel({
    this.id,
    this.title,
    this.subtitle,
    this.body,
    this.isRead = false,
    this.sentTime,
    this.url,
    this.docId,
  });
  final String? id;
  final String? title;
  final String? subtitle;
  final String? body;
  bool? isRead;
  final String? sentTime;
  final String? url;
  final String? docId;
  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        id: json["id"] == null ? null : json["id"]!,
        title: json["title"] == null ? null : json["title"]!,
        subtitle: json["subtitle"] == null ? null : json["subtitle"]!,
        body: json["body"] == null ? null : json["body"]!,
        isRead: json["isRead"] == null ? false : json["isRead"]!,
        sentTime: json["sentTime"] == null ? null : json["sentTime"]!,
        url: json["url"] == null ? null : json["url"]!,
        docId: json["docId"] == null ? null : json["docId"]!,
      );
  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id!,
        "title": title == null ? null : title!,
        "subtitle": subtitle == null ? null : subtitle!,
        "body": body == null ? null : body!,
        "isRead": isRead == null ? false : isRead!,
        "sentTime": sentTime == null ? null : sentTime!,
        "url": url == null ? null : url!,
        "docId": docId == null ? null : docId!,
      };

  static Map<String, dynamic> toMap(NotificationModel model) => {
        "id": model.id,
        "title": model.title ?? '',
        "subtitle": model.subtitle ?? '',
        "body": model.body ?? '',
        "sentTime": model.sentTime ?? '',
        "isRead": model.isRead ?? false,
        "url": model.url ?? '',
        "docId": model.docId ?? '',
      };

  static String encode(List<NotificationModel> lists) => json.encode(
        lists
            .map<Map<String, dynamic>>((item) => NotificationModel.toMap(item))
            .toList(),
      );

  static List<NotificationModel> decode(String lists) =>
      (json.decode(lists) as List<dynamic>)
          .map<NotificationModel>((item) => NotificationModel.fromJson(item))
          .toList();

  String displayDateTime() {
    if (sentTime != null && sentTime != '') {
      return DateFormatter.formatterddMMYYYY(DateTime.parse(sentTime!));
    }
    return '-';
  }
}
