import 'dart:convert';

PdpaModel pdpaModelFromJson(String str) => PdpaModel.fromJson(json.decode(str));

String pdpaModelToJson(PdpaModel data) => json.encode(data.toJson());

class PdpaModel {
  PdpaModel({
    this.result,
    this.date,
  });
  final bool? result;
  final DateTime? date;

  factory PdpaModel.fromJson(Map<String, dynamic> json) => PdpaModel(
        result: json["result"] == null ? null : json["result"]!,
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result!,
        "date": date == null ? null : date!.toIso8601String(),
      };
}
