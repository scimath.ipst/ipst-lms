// ignore: constant_identifier_names
enum DropdownType { SUBJECT, LEVEL, SORTING, SORTING_PLAYLIST, REASON }

extension ParseToString on DropdownType {
  String enumToString() {
    return toString().split('.').last;
  }
}
