// ignore_for_file: constant_identifier_names

export 'asset_path.dart';
export 'value_type.dart';
import 'package:flutter/material.dart';

const String appName = "Project 14+";
const String logName = "Dev";
const PREF_USER = "PREF_USER";
const PREF_THEME = "PREF_THEME";
const PREF_PDPA = "PREF_PDPA";
const PREF_NOTIFICATIONS = "PREF_NOTIFICATIONS";
const PREF_APP_INTRO = "PREF_APP_INTRO";

/*-------------------------------------- FONT SIZE --------------------------------------*/
final data = MediaQueryData.fromWindow(WidgetsBinding.instance!.window);
final bool isMobile = data.size.shortestSide < 600;
double textS = isMobile ? 14 : 16;

double contentListHeight = isMobile ? 200 : 400;
const double fontSizeXS = 12;
const double fontSizeS = 14;
const double fontSizeM = 16;
const double fontSizeL = 18;
const double fontSizeXL = 20; //AppBar Title
const double fontSizeXXL = 22;
const double header = 24;

/*-------------------------------------- COLORS --------------------------------------*/
const Color kPrimary = Color(0xFF595EDC);
const Color kSecondary = Colors.white;

const Color kTextWhite = Colors.white;
const Color kTextBlack = Colors.black;
const Color kWhite = Colors.white;
const Color kBlack = Colors.black;
const Color kBlack2 = Color(0xFF151515);
const Color kBlack3 = Color(0xFF32333A);

const Color kBlackDefault = Color(0xFF626262);
const Color kBlackDefault2 = Color(0xFF767676);

const Color kBlueColor = Color(0xFF338DC9);
const Color kRedColor = Color(0xFFC93333);
const Color kGreenColor = Color(0xFF0ECC5A);

/*-------------------------------------- PADDING --------------------------------------*/
const double kGap = 15.0;
const double kGapS = 10.0;
const double kHalfGap = 7.5;
const double kQuarterGap = 3.75;

const EdgeInsets kPadding = EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0);
const EdgeInsets kButtonPadding = EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 15.0);

/*-------------------------------------- STYLE --------------------------------------*/
const double radius = 15.0;
const double outlineRadius = 4.0;

/*-------------------------------------- FONT WEIGHT --------------------------------------*/

const FontWeight fontWeightNormal = FontWeight.normal;
const FontWeight fontWeightLighter = FontWeight.w100;
const FontWeight fontWeightBold = FontWeight.w600;
const FontWeight fontWeightBolder = FontWeight.bold;
