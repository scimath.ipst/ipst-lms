// ignore_for_file: constant_identifier_names

const String PATH_AUTH_BG = "assets/images/bg.jpg";

const String PATH_AUTH_BG01 = "assets/images/bg-01.jpg";

const String PATH_LEVELS_BANNER = "assets/images/breadcrumb-03.jpg";

const String PATH_LOGO_CONTACT = "assets/images/logo-ipst-project14.png";

const String PATH_CONTACT_LINE = "assets/images/qr-code-line.jpg";

const String PATH_DEFAULT = "assets/images/default.jpg";
const String PATH_AVATAR_DEFAULT = "assets/images/avatar_default.jpg";

const String ABOUT_CONTENT = '''
    Project 14+ เป็นโครงการนำสู่ความปกติใหม่ทางการศึกษาของ สสวท. (New Normal Education) ที่การเรียนรู้ไม่ได้จำเป็นเพียงแค่ในห้องเรียน แต่สามารถเกิดได้ทุกที่ทุกเวลาตามที่ผู้เรียนเลือกหรือกำหนด Project 14+ เป็นบทเรียนออนไลน์ซึ่งประกอบด้วยวีดิทัศน์การสอนเพื่อส่งเสริมวิถีการเรียนรู้ใหม่ที่ผู้เรียนมีแหล่งเรียนรู้ด้านวิทยาศาสตร์ คณิตศาสตร์ และเทคโนโลยีที่ตรงตามหลักสูตร เพื่อใช้ในการศึกษาค้นคว้า เรียนรู้ หรือทบทวนบทเรียน นอกจากนี้ ครูผู้สอนยังสามารถใช้แหล่งเรียนรู้นี้ประกอบการจัดการเรียนรู้ตามปกติในห้องเรียน เพื่อส่งเสริมคุณภาพการเรียนรู้ของผู้เรียน
    
    วีดิทัศน์ที่จัดทำขึ้นใน Project 14+ นี้สอดคล้องกับตัวชี้วัดกลุ่มสาระคณิตศาสตร์ และวิทยาศาสตร์และเทคโนโลยี ตามหลักสูตรแกนกลางการศึกษาขั้นพื้นฐาน พุทธศักราช 2551 (ฉบับปรับปรุง พ.ศ. 2560) ครอบคลุมทุกระดับชั้น ตั้งแต่ชั้นประถมศึกษาปีที่ 1 ถึง มัธยมศึกษาปีที่ 6 ทั้งรายวิชาพื้นฐาน และรายวิชาเพิ่มเติม
    ''';
