import 'dart:developer';

import 'package:intl/intl.dart';

class DateFormatter {
  static formatterddMMYYYY(DateTime? date, {bool withHour = true}) {
    try {
      var formatter;
      Intl.defaultLocale = 'th';

      if (date != null) {
        final dd = DateFormat('dd').format(date);
        final MM = DateFormat('MM').format(date);
        final yyyy = date.year + 543;
        final HH = DateFormat('h').format(date);
        final mm = DateFormat('mm').format(date);

        formatter = withHour ? '$dd/$MM/$yyyy $HH:$mm น.' : '$dd/$MM/$yyyy';
      } else {
        formatter = withHour
            ? "${DateFormat('dd/MM/yyyy HH:mm').format(DateTime.now())} น."
            : DateFormat('dd/MM/yyyy').format(DateTime.now());
      }

      return formatter;
    } catch (e) {
      throw Exception(e);
    }
  }
}
