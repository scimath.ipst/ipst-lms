class AppUtils {
  static String? validateString(String? value, {int? minLength}) {
    if (value == null || value.isEmpty) {
      return 'กรุณาระบุข้อมูล';
    } else if (minLength != null) {
      if (value.length < minLength) {
        return 'ต้องมีความยาวมากกว่า $minLength ตัวอักษร';
      }
    }
    return null;
  }

  static String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return 'กรุณาระบุข้อมูล';
    } else {
      bool emailValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(value);
      if (!emailValid) {
        return 'กรุณระบุอีเมลให้ถูกต้อง';
      } else {
        return null;
      }
    }
  }

  static String? validatePassword(
      String? password, String? password2, int sequence) {
    if (sequence == 1) {
      if (password == null || password.isEmpty) {
        return 'กรุณาระบุรหัสผ่าน';
      } else if (password.length < 6) {
        return 'รหัสผ่านต้องมี 6 หลัก';
      }
      // else
      //  if (!isContainSpecialChar(password)) {
      //   return 'ต้องมี * ! @ # \$ & ? % ^ ( ) อย่างน้อย 1 ตัว';
      // }
    }
    if (sequence == 2) {
      if (password2 == null || password2.isEmpty) {
        return 'กรุณาระบุรหัสผ่าน';
      } else if (password2.length < 6) {
        return 'รหัสผ่านต้องมี 6 หลัก';
      }
      // else
      // if
      // (!isContainSpecialChar(password2)) {
      //   return 'ต้องมี * ! @ # \$ & ? % ^ ( ) อย่างน้อย 1 ตัว';
      // }
    }

    if (password != password2) {
      return 'รหัสผ่านไม่ตรงกัน';
    }

    return null;
  }

// static String? validatePassword(
//       String? password, String? password2, int sequence) {
//     if (sequence == 1) {
//       if (password == null || password.isEmpty) {
//         return 'กรุณาระบุรหัสผ่าน';
//       } else if (password.length < 6) {
//         return 'รหัสผ่านต้องมี 6 หลัก';
//       } else if (!isContainSpecialChar(password)) {
//         return 'ต้องมี * ! @ # \$ & ? % ^ ( ) อย่างน้อย 1 ตัว';
//       }
//     }

//     if (sequence == 2) {
//       if (password2 == null || password2.isEmpty) {
//         return 'กรุณาระบุรหัสผ่าน';
//       } else if (password2.length < 6) {
//         return 'รหัสผ่านต้องมี 6 หลัก';
//       } else if (!isContainSpecialChar(password2)) {
//         return 'ต้องมี * ! @ # \$ & ? % ^ ( ) อย่างน้อย 1 ตัว';
//       }
//     }

//     if (password != password2) {
//       return 'รหัสผ่านไม่ตรงกัน';
//     }
//     return null;
//   }
  static bool hasValue([String? str]) {
    return !['', 'null', null].contains(str);
  }

  static String? validatePhone(String? value) {
    value = value!.replaceAll('-', '');

    if (value == null || value.isEmpty) {
      return 'กรุณาระบุข้อมูล';
    } else if (value.length != 10) {
      return 'หมายเลขโทรศัพท์ต้องมี 10 หลัก !';
    } else if (!isNumeric(value)) {
      return 'เฉพาะตัวเลขเท่านั้น !';
    }
    return null;
  }

  /// return bool เช็คว่าเป็นตัวเลขหรือไม่ [0-9]
  static bool isNumeric(String str) {
    RegExp _numeric = RegExp(r'^-?[0-9]+$');
    return _numeric.hasMatch(str);
  }
}
