import 'package:flutter_easyloading/flutter_easyloading.dart';

class LoadingDialog {
  static void showLoading({bool showTitle = false, String? title}) {
    if (showTitle) {
      EasyLoading.show(status: title);
    } else {
      EasyLoading.show();
    }
  }

  static void showSuccess({required String title}) => EasyLoading.showSuccess(
        title,
        duration: const Duration(milliseconds: 200),
      );

  static void showError({required String title}) => EasyLoading.showError(
        title,
        duration: const Duration(milliseconds: 2000),
      );

  static void showToast({required String title}) => EasyLoading.showToast(
        title,
        duration: const Duration(milliseconds: 2000),
      );

  static void closeDialog({bool withDelay = false}) {
    if (EasyLoading.isShow) {
      if (!withDelay) {
        EasyLoading.dismiss();
      } else {
        Future.delayed(
          const Duration(milliseconds: 2000),
        ).then((value) => LoadingDialog.closeDialog());
      }
    }
  }
}
