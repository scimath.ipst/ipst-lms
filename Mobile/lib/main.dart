import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/dashboards/dashboard_bliding.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/services/services.dart';
import 'package:project14plus/theme.dart';
import 'package:project14plus/utils/utils.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'routes/routes.dart';
import 'package:intl/date_symbol_data_local.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // final pref = await SharedPreferences.getInstance();
  // pref.remove(PREF_NOTIFICATIONS);

  /// init FCM service
  NotificationService.init();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  HttpOverrides.global = MyHttpOverrides();

  /// handle background message
  FirebaseMessaging.onBackgroundMessage(
      NotificationService.handleBackgroundMessage);
  configWebView();
  configLoading();

  initializeDateFormatting();

  runApp(const PageRouteMain());
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

void configWebView() {
  if (Platform.isAndroid) WebView.platform = AndroidWebView();
  if (Platform.isIOS) WebView.platform = CupertinoWebView();
}

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 1500)
    ..indicatorType = EasyLoadingIndicatorType.threeBounce
    ..loadingStyle = EasyLoadingStyle.custom
    ..indicatorSize = 25.0
    ..radius = 10.0
    ..maskType = EasyLoadingMaskType.black
    ..progressColor = kPrimary
    ..backgroundColor = kWhite
    ..indicatorColor = kPrimary
    ..textColor = kBlack
    ..maskColor = kPrimary.withOpacity(0.5)
    ..userInteractions = false
    ..dismissOnTap = false
    ..customAnimation = CustomAnimation()
    ..animationStyle = EasyLoadingAnimationStyle.opacity;
}

class PageRouteMain extends StatelessWidget {
  const PageRouteMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      onInit: () => initialize(),
      initialBinding: DashboardBinding(),
      getPages: AppPages.list,
      debugShowCheckedModeBanner: false,
      darkTheme: AppTheme.dark(),
      theme: AppTheme.light(),
      builder: EasyLoading.init(),
    );
  }

  Future<void> initialize() async {
    var theme = await LocalStorage.getTheme();
    if (theme == ThemeMode.dark) {
      Get.find<FrontendController>().switchDarkMode(true);
      Get.changeThemeMode(ThemeMode.dark);
    } else {
      Get.find<FrontendController>().switchDarkMode(false);
      Get.changeThemeMode(ThemeMode.light);
    }
  }
}
