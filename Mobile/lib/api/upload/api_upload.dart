import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/api/res/res_upload.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/model.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/route_manager.dart';

class ApiUpload {
  static Future<String?> uploadAvatar(
    XFile imageFile,
  ) async {
    try {
      if (imageFile.path == "") return null;
      final formData = FormData.fromMap({
        'file': await MultipartFile.fromFile(
          imageFile.path,
          filename: imageFile.name,
          contentType: MediaType("image", "jpeg"),
        ),
      });

      final res = await Dio(
        BaseOptions(
          headers: {"Authorization": "Bearer $CDN_TOKEN"},
          validateStatus: (_) => true,
          baseUrl: BASE_CDN,
          connectTimeout: 5000,
          receiveTimeout: 5000,
        ),
      ).post(PATH_UPLOAD_AVATAR, data: formData);
      int? status = res.statusCode;

      if (status == 413) {
        return 'Large';
      }
      if ([200, 201, 204].contains(status)) {
        ResUpload resUpload = ResUpload.fromJson(res.data);
        UserModel? user = await LocalStorage.getUser();
        if (user != null) {
          user.avatar = resUpload.result ?? '';
          Get.find<UserController>().updateUser(user);
        }
        return resUpload.result;
      }
      return null;
    } catch (e) {
      return 'E';
    }
  }
}
