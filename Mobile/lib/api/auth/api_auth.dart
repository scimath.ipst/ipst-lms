import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/services/notification_service.dart';
import 'package:project14plus/utils/utils.dart';

class ApiAuth {
  /*----------------------------- SIGN-IN ---------------------------- */
  static Future<bool> signIn(String username, String password,
      {ParameterModel? parameterModel}) async {
    LoadingDialog.showLoading();
    UserController _userController = Get.find<UserController>();
    FrontendController _frontendController = Get.find<FrontendController>();
    try {
      final param = {
        "username": username,
        "password": password,
      };

      final res = await API_SERVICE.post(PATH_SIGNIN, data: param);
      int? status = res.statusCode;

      if ([200, 201, 204].contains(status)) {
        UserModel user = UserModel.fromJson(res.data);
        final auth = FirebaseAuth.instance;
        await auth.signInWithEmailAndPassword(
            email: user.email!, password: password);

        // Store token
        await storage.write(key: ACCESS_TOKEN, value: user.accessToken);
        await storage.write(key: REFRESH_TOKEN, value: user.refreshToken);

        final fcmToken = await NotificationService.getFCMToken();
        if (fcmToken != null) {
          await ApiPersonal.fcmTokenUpdate(fcm: fcmToken, userId: user.id!);
          user.fcmToken = fcmToken;
        }

        // Store User Agent Info
        if (user.id != null) {
          _userController.updateUser(user);
        }
        await _frontendController.refreshValue();
        LoadingDialog.showSuccess(title: "เข้าสู่ระบบสำเร็จ");
        LoadingDialog.closeDialog();
        Get.back();

        return true;
      }

      LoadingDialog.closeDialog();
      return false;
    } on DioError catch (e) {
      var temp = e.response?.data['message'];
      LoadingDialog.showError(title: temp);
      LoadingDialog.closeDialog(withDelay: true);
      Get.back();
      return false;
    }
  }

  /*----------------------------- SIGN-UP ---------------------------- */
  static Future<void> signUp(
      {required String firstname,
      required String lastname,
      required String username,
      required String email,
      required String password,
      required String cPassword,
      ParameterModel? parameterModel}) async {
    LoadingDialog.showLoading();
    try {
      final param = {
        "firstname": firstname,
        "lastname": lastname,
        "username": username,
        "email": email,
        "password": password,
        "confirmPassword": cPassword,
        'ip': '',
      };

      final res = await API_SERVICE.post(PATH_SIGNUP, data: param);
      int? status = res.statusCode;

      if ([200, 201, 204].contains(status)) {
        final FirebaseAuth auth = FirebaseAuth.instance;
        try {
          //  final UserCredential user =
          await auth.createUserWithEmailAndPassword(
              email: email, password: password);

          LoadingDialog.showSuccess(title: "สมัครสมาชิกสำเร็จ");
          LoadingDialog.closeDialog();
        } on FirebaseException {
          var temp = 'เกิดข้อผิดพลาด';
          LoadingDialog.showError(title: temp);
          LoadingDialog.closeDialog(withDelay: true);
        }
        Get.find<FrontendController>().initAuthScreen(3);
      }

      LoadingDialog.closeDialog();
    } on DioError catch (e) {
      var temp = e.response?.data['errors']['email'] ??
          e.response?.data['errors']['username'] ??
          'เกิดข้อผิดพลาด';
      LoadingDialog.showError(title: temp);
      LoadingDialog.closeDialog(withDelay: true);

      throw Exception(e);
    }
  }
}
