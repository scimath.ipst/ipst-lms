import 'dart:convert';
import 'package:project14plus/model/user/user.dart';

ResBadge resBadgeFromJson(String str) => ResBadge.fromJson(json.decode(str));
String resBadgeToJson(ResBadge data) => json.encode(data.toJson());

class ResBadge {
  ResBadge({
    this.result,
  });
  List<BadgeModel>? result;

  factory ResBadge.fromJson(Map<String, dynamic> json) => ResBadge(
        result: json["result"] == null
            ? []
            : List<BadgeModel>.from(
                json["result"].map((x) => BadgeModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<BadgeModel>.from(result!.map((x) => x.toJson())),
      };
}
