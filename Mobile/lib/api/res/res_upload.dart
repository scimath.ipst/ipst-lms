// To parse this JSON data, do
//
//     final resUpload = resUploadFromJson(jsonString);

import 'dart:convert';

ResUpload resUploadFromJson(String str) => ResUpload.fromJson(json.decode(str));

String resUploadToJson(ResUpload data) => json.encode(data.toJson());

class ResUpload {
  ResUpload({
    this.result,
  });

  final String? result;

  factory ResUpload.fromJson(Map<String, dynamic> json) => ResUpload(
        result: json["result"] == null ? null : json["result"]!,
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result!,
      };
}
