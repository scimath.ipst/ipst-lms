import 'dart:convert';

import 'package:project14plus/model/user/user.dart';

ResUserLevel resUserLevelFromJson(String str) =>
    ResUserLevel.fromJson(json.decode(str));
String resUserLevelToJson(ResUserLevel data) => json.encode(data.toJson());

class ResUserLevel {
  ResUserLevel({
    this.result,
  });
  List<Level>? result;

  factory ResUserLevel.fromJson(Map<String, dynamic> json) => ResUserLevel(
        result: json["result"] == null
            ? []
            : List<Level>.from(json["result"].map((x) => Level.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<Level>.from(result!.map((x) => x.toJson())),
      };
}
