import 'dart:convert';

import 'package:project14plus/api/res/res.dart';
import 'package:project14plus/model/model.dart';

ResContinued resContinuedFromJson(String str) =>
    ResContinued.fromJson(json.decode(str));
String resContinuedToJson(ResContinued data) => json.encode(data.toJson());

class ResContinued {
  ResContinued({
    this.result,
    this.paginate,
  });
  List<ContinuedModel>? result;
  Paginate? paginate;

  factory ResContinued.fromJson(Map<String, dynamic> json) => ResContinued(
        result: json["result"] == null
            ? []
            : List<ContinuedModel>.from(
                json["result"].map((x) => ContinuedModel.fromJson(x))),
        paginate: json["paginate"] == null
            ? null
            : Paginate.fromJson(json["paginate"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<ContinuedModel>.from(result!.map((x) => x.toJson())),
        "paginate": paginate == null ? null : paginate!.toJson(),
      };
}
