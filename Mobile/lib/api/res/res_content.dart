import 'dart:convert';

import 'package:project14plus/api/res/res.dart';
import 'package:project14plus/model/model.dart';

ResContent resContentFromJson(String str) =>
    ResContent.fromJson(json.decode(str));
String resContentToJson(ResContent data) => json.encode(data.toJson());

class ResContent {
  ResContent({
    this.result,
    this.paginate,
  });
  List<ContentModel>? result;
  Paginate? paginate;

  factory ResContent.fromJson(Map<String, dynamic> json) => ResContent(
        result: json["result"] == null
            ? []
            : List<ContentModel>.from(
                json["result"].map((x) => ContentModel.fromJson(x))),
        paginate: json["paginate"] == null
            ? null
            : Paginate.fromJson(json["paginate"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<ContentModel>.from(result!.map((x) => x.toJson())),
        "paginate": paginate == null ? null : paginate!.toJson(),
      };
}
