// ignore_for_file: prefer_null_aware_operators

import 'dart:convert';

import 'package:project14plus/model/model.dart';

ResContentChannels resContentChannelsFromJson(String str) =>
    ResContentChannels.fromJson(json.decode(str));
String resContentChannelsToJson(ResContentChannels data) =>
    json.encode(data.toJson());

class ResContentChannels {
  ResContentChannels({
    this.result,
    this.paginate,
  });
  List<ChannelsModel>? result;
  Paginate? paginate;

  factory ResContentChannels.fromJson(Map<String, dynamic> json) =>
      ResContentChannels(
          result: json["result"] == null
              ? []
              : List<ChannelsModel>.from(
                  json["result"].map((x) => ChannelsModel.fromJson(x))),
          paginate: json["paginate"] == null
              ? null
              : Paginate.fromJson(json["paginate"]));

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<ChannelsModel>.from(result!.map((x) => x.toJson())),
        "paginate": paginate == null ? null : paginate!.toJson(),
      };
}

class Paginate {
  Paginate({
    this.page,
    this.pp,
    this.total,
    this.totalPages,
  });
  final int? page;
  final int? pp;
  final int? total;
  final int? totalPages;

  factory Paginate.fromJson(Map<String, dynamic> json) => Paginate(
        page: json["page"] == null ? null : json["page"].toInt(),
        pp: json["pp"] == null ? null : json["pp"].toInt(),
        total: json["total"] == null ? null : json["total"].toInt(),
        totalPages:
            json["totalPages"] == null ? null : json["totalPages"].toInt(),
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page!.toInt(),
        "pp": pp == null ? null : pp!.toInt(),
        "total": total == null ? null : total!.toInt(),
        "totalPages": totalPages == null ? null : totalPages!.toInt(),
      };
}
