import 'dart:convert';

import 'package:project14plus/model/model.dart';

ResVideContent resVideContentFromJson(String str) =>
    ResVideContent.fromJson(json.decode(str));
String resVideContentToJson(ResVideContent data) => json.encode(data.toJson());

class ResVideContent {
  ResVideContent({
    this.result,
    this.userAction,
  });
  VideoContentModel? result;
  UserActionModel? userAction;

  factory ResVideContent.fromJson(Map<String, dynamic> json) => ResVideContent(
        result: json["result"] == null
            ? null
            : VideoContentModel.fromJson(json["result"]),
        userAction: json["userAction"] == null
            ? null
            : UserActionModel.fromJson(json["userAction"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result!.toJson(),
        "userAction": userAction == null ? null : userAction!.toJson(),
      };
}

class ResVideContentList {
  ResVideContentList({
    this.result,
  });
  List<VideoContentListModel>? result;

  factory ResVideContentList.fromJson(Map<String, dynamic> json) =>
      ResVideContentList(
        result: json["result"] == null
            ? null
            : List<VideoContentListModel>.from(
                json["result"].map((x) => VideoContentListModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<VideoContentListModel>.from(result!.map((x) => x.toJson())),
      };
}
