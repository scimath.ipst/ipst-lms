import 'dart:convert';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/model/model.dart';

ResHistory resHistoryFromJson(String str) =>
    ResHistory.fromJson(json.decode(str));
String resHistoryToJson(ResHistory data) => json.encode(data.toJson());

class ResHistory {
  ResHistory({this.result, this.paginate});
  List<HistoryModel>? result;
  Paginate? paginate;
  factory ResHistory.fromJson(Map<String, dynamic> json) => ResHistory(
      result: json["result"] == null
          ? []
          : List<HistoryModel>.from(
              json["result"].map((x) => HistoryModel.fromJson(x))),
      paginate: json["paginate"] == null
          ? null
          : Paginate.fromJson(json["paginate"]));

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<HistoryModel>.from(result!.map((x) => x.toJson())),
        "paginate": paginate == null ? null : paginate!.toJson(),
      };
}
