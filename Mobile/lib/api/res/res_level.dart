import 'dart:convert';
import 'package:project14plus/model/content/level_model.dart';

ResLevel resLevelFromJson(String str) => ResLevel.fromJson(json.decode(str));
String resLevelToJson(ResLevel data) => json.encode(data.toJson());

class ResLevel {
  ResLevel({
    this.result,
  });
  List<LevelModel>? result;

  factory ResLevel.fromJson(Map<String, dynamic> json) => ResLevel(
        result: json["result"] == null
            ? []
            : List<LevelModel>.from(
                json["result"].map((x) => LevelModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null
            ? null
            : List<LevelModel>.from(result!.map((x) => x.toJson())),
      };
}
