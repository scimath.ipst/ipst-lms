// ignore_for_file: avoid_print, constant_identifier_names, non_constant_identifier_names
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/services/logout_service.dart';

// Create storage
const storage = FlutterSecureStorage();

Dio _previous = Dio();
Dio _refreshTokenDio = Dio();

var API_SERVICE = Dio(
  BaseOptions(
    baseUrl: API_URL,
    connectTimeout: 5000,
    receiveTimeout: 5000,
  ),
)..interceptors.add(
    InterceptorsWrapper(
      /// onRequest
      onRequest: (options, handler) async {
        if (kDebugMode) {
          debugPrint('REQUEST[${options.method}] => PATH: ${options.path}');
          debugPrint('PARAMS: ${options.queryParameters}');
          final payload = options.data;
          if (payload is FormData) {
            _printDioFormData(payload);
          } else if (payload != null) {
            debugPrint('PAYLOAD => ${_prettyPrint(options.data)}');
          }
        }

        // Path ที่ไม่ต้องใช้ Token
        if (!PATH_NO_TOKEN_LIST.contains(options.path)) {
          // accessToken
          final accessToken = await storage.read(key: ACCESS_TOKEN);
          debugPrint('ACCESS_TOKEN == $accessToken');
          if ([null, 'null', ''].contains(accessToken)) {
            LogOutService.logOut();
          }
          options.headers['Authorization'] = 'Bearer $accessToken';
        }

        return handler.next(options);
      },

      /// onResponse
      onResponse: (response, handler) {
        if (kDebugMode) {
          debugPrint('RESPONSE[${response.statusCode}]');
          // debugPrint('DATA: ${_prettyPrint(response.data)}');
          // debugPrint('====================================================\n');
        }

        return handler.next(response);
      },

      /// onError
      onError: (DioError e, handler) async {
        if (kDebugMode) {
          debugPrint(
            'ERROR[${e.response?.statusCode}] => PATH: ${e.requestOptions.path}',
          );
          debugPrint('DATA: ${_prettyPrint(e.response?.data)}');
          debugPrint('====================================================\n');
        }

        if (e.response?.statusCode == 401) {
          debugPrint("[Interceptor]=> Access Token was expired ⏳");

          RequestOptions requestOptions = e.requestOptions;

          try {
            final refreshToken = await storage.read(key: REFRESH_TOKEN);
            Response _responseRfToken = await _refreshTokenDio.patch(
              requestOptions.baseUrl + PATH_REFRESH_TOKEN,
              data: {"refreshToken": refreshToken},
            );

            if (_responseRfToken.statusCode == 200) {
              debugPrint("[Interceptor]=> Renew Access Token 🎉");

              final oldToken = await storage.read(key: ACCESS_TOKEN);
              debugPrint('[Interceptor]oldToken: $oldToken');

              final newAccessToken =
                  _responseRfToken.data['data']['accessToken'];
              final newRefreshToken =
                  _responseRfToken.data['data']['refreshToken'];

              // update token
              requestOptions.headers["Authorization"] =
                  "Bearer $newAccessToken";

              // storage token
              await storage.write(key: ACCESS_TOKEN, value: newAccessToken);
              await storage.write(key: REFRESH_TOKEN, value: newRefreshToken);

              final newToken = await storage.read(key: ACCESS_TOKEN);
              debugPrint('[Interceptor]newToken: $newToken');

              // repeat
              debugPrint("[Interceptor]=> Repeat 🔃");
              Response? responseResolve = await _previous.request(
                requestOptions.baseUrl + requestOptions.path,
                data: requestOptions.data,
                queryParameters: requestOptions.queryParameters,
                options: Options(
                  method: requestOptions.method,
                  headers: requestOptions.headers,
                ),
              );
              debugPrint(
                  '[Interceptor]RESOLVE[${responseResolve.statusCode}] => DATA: ${responseResolve.data}');
              return handler.resolve(responseResolve);
            }
          } catch (e) {
            debugPrint(e.toString());
            debugPrint("[Interceptor]=> Refresh Token was expired ❌");

            LogOutService.logOut();
          }
        }

        return handler.next(e);
      },
    ),
  );

void _printDioFormData(FormData formData) {
  try {
    for (var item in formData.fields) {
      debugPrint('[FIELD] = ${item.key}: ${item.value}');
    }
    for (var item in formData.files) {
      debugPrint('[FILE] = ${item.key}: "${item.value.filename}"');
    }
  } catch (e) {
    debugPrint('FormData: $formData');
  }
}

String _prettyPrint(dynamic json) {
  try {
    JsonEncoder encoder = const JsonEncoder.withIndent('  ');
    String pretty = encoder.convert(json);
    return pretty;
  } catch (e) {
    return json.toString();
  }
}
