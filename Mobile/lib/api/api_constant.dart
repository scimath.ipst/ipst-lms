// const String API_URL = "https://api-project14plus.ipst.ac.th/";
// ignore_for_file: constant_identifier_names, non_constant_identifier_names

// const String API_URL = "http://192.168.1.123:4900/";
const String API_URL = "http://192.168.1.123:4900/";

const String BASE_CDN = "https://cdn-project14plus.ipst.ac.th/";
const String CDN_TOKEN =
    "C190935CEB6DA0D96B4ACBH2495AAE8505F4D70EFC6081E202D8F91C6AD70A19";

String ACCESS_TOKEN = "@ACCESS_TOKEN";
String REFRESH_TOKEN = "@REFRESH_TOKEN";

/* -------------------------------  CONTENTS  ------------------------------- */
const String PATH_BANNER = "frontend/banner/list";
const String PATH_SUGGESTED = "frontend/content/suggested-list";
const String PATH_CONTENT_SUGGESTED = "frontend/content/related-list";
const String PATH_NEWEST_CONTENT = "frontend/content/newest-list";
const String PATH_POPULAR_CONTENT = "frontend/content/popular-list";
const String PATH_LEVELS = "frontend/level/list";
const String PATH_LEVEL = "frontend/content/list";
const String PATH_CHANNELS = "frontend/channel/list";
const String PATH_PLAYLIST = "frontend/playlist/list";
const String PATH_RELATED_LIST = "frontend/content/related-list'";
const String PATH_CONTENT = "frontend/content";
const String PATH_CONTENT_LIST = "frontend/content/list";
const String PATH_CONTINUED = "frontend/content/continued-list";
const String PATH_QUESTIONS = "frontend/user-action/result";
const String PATH_USER_ACTION = "frontend/user-action";
const String PATH_UPDATE_FCM = "auth/fcm-token-update";

/*----------------------------------- Auth -----------------------------------*/
const String PATH_SIGNUP = "auth/signup";
const String PATH_SIGNIN = "auth/signin";
const String PATH_REFRESH_TOKEN = "auth/refresh-token";

/*---------------------------------- MEMBER ----------------------------------*/
const String PATH_HISTORY = "member/history/list";
const String PATH_EDIT_PROFILE = "member/profile";
const String PATH_UPDATE_PASSWORD = "member/password";
const String PATH_UPDATE_INTEREST = "member/interest";
const String PATH_USER_LIST = "member/user/list";
const String PATH_LEVEL_LIST = "member/level/list";
const String PATH_SUBJECT_LIST = "frontend/subject/list";
const String PATH_TAG_LIST = "member/tag/list";
const String PATH_BADGE_LIST = "member/badge/list";

/*------------------------------ UPLOAD AVATAR -------------------------------*/
const String PATH_UPLOAD_AVATAR = "upload";
/* -------------------------------------------------------------------------- */
/*                               NO TOKEN PATHS                               */
/* -------------------------------------------------------------------------- */
const List PATH_NO_TOKEN_LIST = [
  PATH_UPDATE_FCM,
  PATH_SIGNUP,
  PATH_SIGNIN,
  PATH_BANNER,
  PATH_NEWEST_CONTENT,
  PATH_POPULAR_CONTENT,
  PATH_LEVELS,
  PATH_CHANNELS,
  PATH_PLAYLIST,
  PATH_LEVEL,
  PATH_SUBJECT_LIST,
];
