import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/utils/utils.dart';

class ApiPersonal {
  /*------------------------------- GET HISTORY ----------------------------- */
  static Future<ResHistory> getHistory(String userId, int page) async {
    try {
      final param = {
        "id": userId,
        "page": page,
        "pp": isMobile ? 10 : 20,
      };
      final res = await API_SERVICE.get(PATH_HISTORY, queryParameters: param);
      final status = res.statusCode;

      ResHistory list = ResHistory();
      if ([200, 201, 204].contains(status)) {
        ResHistory resJson = ResHistory.fromJson(res.data);
        list = resJson;
      }

      return list;
    } catch (e) {
      throw Exception(e);
    }
  }

/*-------------------------------- EDIT PROFILE ----------------------------- */
  static Future<void> editProfile(UserModel userEdit) async {
    LoadingDialog.showLoading();
    try {
      final param = {
        // "id": user.id,
        "email": userEdit.email,
        "username": userEdit.username,
        "firstname": userEdit.firstname,
        "lastname": userEdit.lastname,
        "avatar": userEdit.avatar,
      };

      final res = await API_SERVICE.put(PATH_EDIT_PROFILE, data: param);
      final status = res.statusCode;

      if ([200, 201, 204].contains(status)) {
        LoadingDialog.showSuccess(title: "บันทึกข้อมูลผู้ใช้งานสำเร็จ");

        UserModel? user = await LocalStorage.getUser();
        if (user != null) {
          user.email = userEdit.email;
          user.username = userEdit.username;
          user.firstname = userEdit.firstname;
          user.lastname = userEdit.lastname;
          user.avatar = userEdit.avatar;

          // Update data in GetxController
          Get.find<UserController>().updateUser(user);
        }

        Get.back();
        LoadingDialog.closeDialog();
      }

      LoadingDialog.closeDialog();
    } catch (e) {
      LoadingDialog.showError(title: "เกิดข้อผิดพลาดในการบันทึกข้อมูล");
      LoadingDialog.closeDialog();
      throw Exception(e);
    }
  }

/*------------------------------ UPDATE PASSWORD ---------------------------- */
  static Future<void> updatePassword(
      String id, String pass, String newPass, String newCon) async {
    LoadingDialog.showLoading();

    try {
      final data = {
        // "id": id, //ไม่ต้องส่ง ID ก็ทำงานได้
        "password": pass,
        "newPassword": newPass,
        "confirmPassword": newCon,
      };

      final res = await API_SERVICE.put(PATH_UPDATE_PASSWORD, data: data);
      final status = res.statusCode;

      if ([200, 201, 204].contains(status)) {
        Get.back();
        LoadingDialog.showSuccess(title: 'บันทึกข้อมูลสำเร็จ');
        LoadingDialog.closeDialog();
      }
      LoadingDialog.closeDialog();
    } on DioError catch (e) {
      final temp = e.response?.data['errors']['password'] ??
          e.response?.data['message'] ??
          "เกิดข้อผิดพลาด";

      LoadingDialog.showError(title: temp);
      LoadingDialog.closeDialog();

      throw Exception(e);
    }
  }

/*------------------------------ UPDATE INTEREST ---------------------------- */
  static Future<void> updateInterest(
      {required Level level,
      required List<Level> subject,
      required List<Level> tag}) async {
    LoadingDialog.showLoading();
    try {
      final data = {
        "levelId": level.id,
        "subjects": subject,
        "tags": tag,
      };

      final res = await API_SERVICE.put(PATH_UPDATE_INTEREST, data: data);
      final status = res.statusCode;

      if ([200, 201, 204].contains(status)) {
        LoadingDialog.showSuccess(title: 'บันทึกข้อมูลสำเร็จ');
        UserModel? user = await LocalStorage.getUser();
        if (user != null) {
          user.level = level;
          user.subjects = subject;
          user.tags = tag;

          Get.find<UserController>().updateUser(user);
        }
        LoadingDialog.closeDialog(withDelay: true);
        Get.back();
      }
    } catch (e) {
      LoadingDialog.showError(title: 'เกิดข้อผิดพลาด');
      LoadingDialog.closeDialog(withDelay: true);
      throw Exception(e);
    }
  }

/*----------------------------- GET LEVEL INTEREST -------------------------- */
  static Future<List<Level>> getLevelList() async {
    try {
      final res = await API_SERVICE.get(PATH_LEVEL_LIST);
      final status = res.statusCode;

      List<Level> model = [];

      if ([200, 201, 204].contains(status)) {
        ResUserLevel resJson = ResUserLevel.fromJson(res.data);
        model = resJson.result ?? [];
      }

      return model;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*----------------------------- GET LEVEL INTEREST -------------------------- */
  static Future<List<Level>> getSubjectList() async {
    try {
      final res = await API_SERVICE.get(PATH_SUBJECT_LIST);
      final status = res.statusCode;

      List<Level> model = [];

      if ([200, 201, 204].contains(status)) {
        ResUserLevel resJson = ResUserLevel.fromJson(res.data);
        model = resJson.result ?? [];
      }

      return model;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*----------------------------- GET LEVEL INTEREST -------------------------- */
  static Future<List<Level>> getTagList() async {
    try {
      final res = await API_SERVICE.get(PATH_TAG_LIST);
      final status = res.statusCode;

      List<Level> model = [];

      if ([200, 201, 204].contains(status)) {
        ResUserLevel resJson = ResUserLevel.fromJson(res.data);
        model = resJson.result ?? [];
      }

      return model;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*----------------------------- GET BADGE -------------------------- */
  static Future<List<BadgeModel>> getBadgeList(String userId) async {
    try {
      final param = {
        "userId": userId,
      };

      final res =
          await API_SERVICE.get(PATH_BADGE_LIST, queryParameters: param);
      final status = res.statusCode;

      List<BadgeModel> model = [];

      if ([200, 201, 204].contains(status)) {
        ResBadge resJson = ResBadge.fromJson(res.data);
        model = resJson.result ?? [];
      }

      return model;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*-------------------------------- UPDATE QUESTION ----------------------------- */
  static Future<int?> updateQuestions(Map<String, dynamic> param) async {
    try {
      final res = await API_SERVICE.put(PATH_QUESTIONS, data: param);
      final status = res.statusCode;

      if ([200, 201, 204].contains(status)) {
        if (param["score"] < param["minScore"]) {
          return 1;
        } else {
          return 2;
        }
        // Get.back();
        // LoadingDialog.closeDialog(withDelay: true);
      }

      LoadingDialog.closeDialog(withDelay: true);
    } catch (e) {
      LoadingDialog.showError(title: "เกิดข้อผิดพลาด");
      LoadingDialog.closeDialog();
      return 3;

      // throw Exception(e);
    }
    return null;
  }

  /*-------------------------------- EDIT PROFILE ----------------------------- */
  static Future<int?> userAction(
      {required String id,
      required double watchingSeconds,
      required int status}) async {
    try {
      final param = {
        "id": id,
        "watchingSeconds": watchingSeconds,
        "status": status,
      };
      final res = await API_SERVICE.put(PATH_USER_ACTION, data: param);
      final statusCode = res.statusCode;

      if ([200, 201, 204].contains(statusCode)) {}

      LoadingDialog.closeDialog(withDelay: true);
    } catch (e) {
      LoadingDialog.showError(title: "เกิดข้อผิดพลาด");
      LoadingDialog.closeDialog();
      return 3;

      // throw Exception(e);
    }
    return null;
  }

  /*-------------------------------- EDIT PROFILE ----------------------------- */
  static Future<void> fcmTokenUpdate({
    required String userId,
    required String fcm,
  }) async {
    try {
      final param = {
        "userId": userId,
        "fcmToken": fcm,
      };

      final res = await API_SERVICE.patch(PATH_UPDATE_FCM, data: param);
      final statusCode = res.statusCode;

      if ([200, 201, 204].contains(statusCode)) {}

      LoadingDialog.closeDialog(withDelay: true);
    } catch (e) {
      LoadingDialog.showError(title: "เกิดข้อผิดพลาด");
      LoadingDialog.closeDialog();

      // throw Exception(e);
    }
  }
}
