import 'package:dio/dio.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/utils/easy_loading/easy_loading.dart';

class ApiContent {
  /*---------------------------- VIDEO CONTENT ------------------------------ */
  static Future<List<ContentModel>> getVideoContent(ContentType contentType,
      {String? contentId}) async {
    try {
      String path = "";
      Map<String, dynamic> params = {};
      switch (contentType) {
        case ContentType.BANNER:
          path = PATH_BANNER;
          break;
        case ContentType.CONTINUED:
          path = PATH_CONTINUED;
          break;
        case ContentType.SUGGESTED:
          if (contentId == null) {
            path = PATH_SUGGESTED;
          } else {
            path = PATH_CONTENT_SUGGESTED;
            params = {"contentId": contentId};
          }
          break;
        case ContentType.NEWEST:
          path = PATH_NEWEST_CONTENT;
          break;
        case ContentType.POPULAR:
          path = PATH_POPULAR_CONTENT;
          break;
      }
      final res = await API_SERVICE.get(path, queryParameters: params);

      int? status = res.statusCode;

      List<ContentModel> list = [];
      if ([200, 201, 204].contains(status)) {
        ResContent resJson = ResContent.fromJson(res.data);
        list = resJson.result ?? [];
      }

      return list;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*---------------------------- CONTINUED CONTENTS ---------------------------- */
  static Future<List<ContinuedModel>> getContinuedContents() async {
    try {
      final res = await API_SERVICE.get(PATH_CONTINUED);
      int? status = res.statusCode;
      List<ContinuedModel> list = [];
      if ([200, 201, 204].contains(status)) {
        ResContinued resJson = ResContinued.fromJson(res.data);
        list = resJson.result ?? [];
      }

      return list;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*----------------------------- LEVELS CONTENTS --------------------------- */
  static Future<List<LevelModel>> getLevels() async {
    try {
      final res = await API_SERVICE.get(PATH_LEVELS);
      int? status = res.statusCode;

      List<LevelModel> list = [];
      if ([200, 201, 204].contains(status)) {
        ResLevel resJson = ResLevel.fromJson(res.data);
        list = resJson.result ?? [];
      }

      return list;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*----------------------------- LEVEL CONTENTS ---------------------------- */
  static Future<ResContent> getLevelList(String url, int i,
      {String? subjectUrl, int? sorting, String? keyword}) async {
    try {
      final param = {
        "levelUrl": url,
        "page": i,
        "pp": isMobile ? 10 : 20,
        "subjectUrl": subjectUrl,
        "sorting": sorting,
        "keyword": keyword,
      };
      final res = await API_SERVICE.get(PATH_LEVEL, queryParameters: param);
      int? status = res.statusCode;

      ResContent list = ResContent();
      if ([200, 201, 204].contains(status)) {
        ResContent resJson = ResContent.fromJson(res.data);
        list = resJson;
      }

      return list;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*--------------------------------- CHANNEL ------------------------------- */
  static Future<List<ChannelsModel>> getChannels(
      {int? sorting, String? keyword}) async {
    try {
      final params = {
        "sorting": sorting,
        "keyword": keyword,
      };
      final res = await API_SERVICE.get(PATH_CHANNELS, queryParameters: params);
      int? status = res.statusCode;

      List<ChannelsModel> list = [];
      if ([200, 201, 204].contains(status)) {
        ResContentChannels resJson = ResContentChannels.fromJson(res.data);
        list = resJson.result ?? [];
      }

      return list;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*--------------------------------- CHANNEL ------------------------------- */
  static Future<ResContentChannels> getPlaylist(String url, int i,
      {int? sorting, String? keyword}) async {
    try {
      final params = {
        "channelUrl": url,
        "page": i,
        "pp": isMobile ? 10 : 20,
        "sorting": sorting,
        "keyword": keyword,
      };

      final res = await API_SERVICE.get(PATH_PLAYLIST, queryParameters: params);
      int? status = res.statusCode;

      ResContentChannels list = ResContentChannels();
      if ([200, 201, 204].contains(status)) {
        ResContentChannels resJson = ResContentChannels.fromJson(res.data);
        list = resJson;
      }

      return list;
    } on DioError catch (e) {
      throw Exception(e);
    }
  }

  /*--------------------------------- CONTENT ------------------------------- */
  static Future<VideoContent> getContent(String url) async {
    try {
      final params = {"url": url};
      final res = await API_SERVICE.get(PATH_CONTENT, queryParameters: params);
      int? status = res.statusCode;
      VideoContent model = VideoContent();
      if ([200, 201, 204].contains(status)) {
        ResVideContent resJson = ResVideContent.fromJson(res.data);
        model.result = resJson.result!;
        model.userAction = resJson.userAction!;
      }
      return model;
    } on DioError catch (e) {
      final temp = e.response?.data['message'] ?? 'ไม่พบเนื้อหา';
      LoadingDialog.showError(title: temp);
      LoadingDialog.closeDialog();
      throw Exception(e);
    }
  }

  /*--------------------------------- CONTENT ------------------------------- */
  static Future<List<ContentModel>> getContentPlaylist(
      String playlistUrl) async {
    try {
      final params = {"playlistUrl": playlistUrl};

      final res =
          await API_SERVICE.get(PATH_CONTENT_LIST, queryParameters: params);
      int? status = res.statusCode;

      List<ContentModel> list = [];
      if ([200, 201, 204].contains(status)) {
        ResContent resJson = ResContent.fromJson(res.data);
        list = resJson.result ?? [];
      }

      return list;
    } on DioError catch (e) {
      final temp = e.response?.data['message'] ?? 'ไม่พบเนื้อหา';
      LoadingDialog.showError(title: temp);
      throw Exception(e);
    }
  }

  /*--------------------------------- CONTENT ------------------------------- */
  static Future<VideoContent> getContentPlaylistVideo(
      String playlistUrl, String url) async {
    try {
      final params = {"url": url};
      final res = await API_SERVICE.get(PATH_CONTENT, queryParameters: params);
      int? status = res.statusCode;

      VideoContent model = VideoContent();
      if ([200, 201, 204].contains(status)) {
        ResVideContent resJson = ResVideContent.fromJson(res.data);
        final result = resJson;
        if (result != null) {
          model.result = resJson.result!;
          model.userAction = resJson.userAction!;
        }
      }

      return model;
    } on DioError catch (e) {
      final temp = e.response?.data['message'] ?? 'ไม่พบเนื้อหา';
      LoadingDialog.showError(title: temp);
      throw Exception(e);
    }
  }
}
