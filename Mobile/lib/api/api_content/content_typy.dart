// ประเภทของออเดอร์
// ignore: constant_identifier_names
enum ContentType { BANNER, CONTINUED, SUGGESTED, NEWEST, POPULAR }

extension ParseToString on ContentType {
  String enumToString() {
    return toString().split('.').last;
  }
}
