import 'package:flutter/material.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/model/model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
/*-------------------------------- Theme Mode --------------------------------*/
  static saveTheme(ThemeMode themeMode) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(PREF_THEME, themeMode.toString());
  }

  static Future<ThemeMode> getTheme() async {
    final prefs = await SharedPreferences.getInstance();
    final temp = prefs.getString(PREF_THEME);
    if (temp == 'ThemeMode.dark') {
      return ThemeMode.dark;
    } else {
      return ThemeMode.light;
    }
  }

/*-------------------------------- APP INTRO --------------------------------*/
  static saveAppIntro(bool value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool(PREF_APP_INTRO, value);
  }

  static Future<bool?> getAppIntro() async {
    final prefs = await SharedPreferences.getInstance();
    final temp = prefs.getBool(PREF_APP_INTRO);
    return temp;
  }

/*----------------------------------- User -----------------------------------*/
  static saveUser(UserModel? user) async {
    final prefs = await SharedPreferences.getInstance();
    final String encodedData = userToJson(user);
    await prefs.setString(PREF_USER, encodedData);
  }

  static Future<UserModel?> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    String? str = prefs.getString(PREF_USER);
    if (str != null) {
      UserModel model = userFromJson(str);
      return model;
    }
    return null;
  }

/*----------------------------------- PDPA -----------------------------------*/
  static savePdpa(PdpaModel pdpaModel) async {
    final prefs = await SharedPreferences.getInstance();
    final String encodedData = pdpaModelToJson(pdpaModel);
    await prefs.setString(PREF_PDPA, encodedData);
  }

  static Future<PdpaModel?> getPdpa() async {
    final prefs = await SharedPreferences.getInstance();
    String? str = prefs.getString(PREF_PDPA);
    if (str != null) {
      PdpaModel model = pdpaModelFromJson(str);
      return model;
    }
    return null;
  }
}
