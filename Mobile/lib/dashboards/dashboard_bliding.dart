import 'package:get/get.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import '../controllers/controllers.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<UserController>(UserController());
    Get.put<ContentController>(ContentController());
    Get.put<FrontendController>(FrontendController());
    Get.put<NotificationController>(NotificationController());
  }
}
