import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/model/model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationController extends GetxController {
  bool thereIsNotification = false;
  Stream<QuerySnapshot<Map<String, dynamic>>>? streamNotifications;
  Stream<QuerySnapshot<Map<String, dynamic>>>? streamNotificationsSign;

  final CollectionReference _collection =
      FirebaseFirestore.instance.collection('user_notifications');
  final UserController _userController = Get.find<UserController>();

  Future<void> refreshData() async {
    getNotifications();
    update();
  }

  Future<void> getNotifications() async {
    if (_userController.userModel?.id != '' &&
        _userController.userModel?.id != null) {
      streamNotifications = _collection
          .doc(_userController.userModel?.id)
          .collection("data")
          .orderBy("sentTime", descending: true)
          .snapshots();
      streamNotificationsSign = _collection
          .doc(_userController.userModel?.id)
          .collection("data")
          .where("isRead", isEqualTo: false)
          .snapshots();

      await _collection
          .doc(_userController.userModel?.id)
          .collection("data")
          .where('isRead', isEqualTo: false)
          .get()
          .then((value) {
        if (value.docs.isNotEmpty) {
          thereIsNotification = true;
        } else {
          thereIsNotification = false;
        }
      });
    }
    update();
  }

  Future<void> saveNotifications(NotificationModel model,
      {bool isForeground = false}) async {
    // var userNotification = await _collection
    //     .doc(_userController.userModel?.id ?? '')
    //     .collection('data')
    //     .doc(model.id)
    //     .get();

    // if (userNotification.data() != null) {

    // }
    if (isForeground) {
      Get.snackbar(
        model.title ?? '',
        model.body ?? '',
        snackPosition: SnackPosition.TOP,
        colorText: kWhite,
        backgroundColor: kPrimary,
        borderRadius: 0,
        borderWidth: 4,
        margin: EdgeInsets.zero,
        dismissDirection: DismissDirection.vertical,
        forwardAnimationCurve: Curves.easeIn,
        icon: const Icon(
          Icons.message_rounded,
          color: Colors.white,
        ),
        onTap: (val) {
          const newRouteName = "/NavigationScreen";
          FrontendController _dashboardController =
              Get.find<FrontendController>();
          _dashboardController.changeNavigationIndex(2);
          Get.until((route) => Get.currentRoute == newRouteName);
        },
        snackStyle: SnackStyle.FLOATING,
      );
    }

    getNotifications();
    update();
  }

  Future<void> readNotification(String? docId) async {
    try {
      DocumentSnapshot<Map<String, dynamic>> userNotification =
          await _collection
              .doc(_userController.userModel?.id ?? '')
              .collection('data')
              .doc(docId)
              .get();
      if (userNotification.data() != null) {
        await _collection
            .doc(_userController.userModel?.id ?? '')
            .collection('data')
            .doc(docId)
            .update({"isRead": true});

        await _collection
            .doc(_userController.userModel?.id)
            .collection("data")
            .where('isRead', isEqualTo: false)
            .get()
            .then((value) {
          if (value.docs.isNotEmpty) {
            thereIsNotification = true;
          } else {
            thereIsNotification = false;
          }
        });
      }
    } catch (e) {
      throw Exception(e);
    }

    update();
  }

  void deleteAllNotifications() async {
    if (_userController.userModel?.id != '' &&
        _userController.userModel?.id != null) {
      await _collection.doc(_userController.userModel?.id).delete();
      thereIsNotification = false;
    }
    update();
  }

  void deleteNotification(String docId) async {
    bool prefNoti = false;
    var userNotification = await _collection
        .doc(_userController.userModel?.id ?? '')
        .collection('data')
        .doc(docId)
        .get();
    if (userNotification.data() != null) {
      await _collection
          .doc(_userController.userModel?.id ?? '')
          .collection('data')
          .doc(docId)
          .delete();
      await _collection
          .doc(_userController.userModel?.id)
          .collection("data")
          .where('isRead', isEqualTo: false)
          .get()
          .then((value) {
        if (value.docs.isNotEmpty || prefNoti) {
          thereIsNotification = true;
        } else {
          thereIsNotification = false;
        }
      });
    }
    update();
  }

  clearUser() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(PREF_NOTIFICATIONS);
    thereIsNotification = false;
    streamNotifications = null;
    streamNotificationsSign = null;
    update();
  }
}
