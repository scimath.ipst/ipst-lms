import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/notification_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserController extends GetxController {
  UserModel? userModel;

  initUser() async {
    userModel = await LocalStorage.getUser();
    if (userModel?.id != null) {
      final NotificationController _notificationController =
          Get.find<NotificationController>();
      _notificationController.getNotifications();
    }
    update();
  }

  clearUser() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(PREF_USER);
    userModel = null;
    update();
  }

  updateUser(UserModel? model) async {
    userModel = model;
    await LocalStorage.saveUser(userModel);
    if (userModel?.id != null) {
      final NotificationController _notificationController =
          Get.find<NotificationController>();
      _notificationController.getNotifications();
    }
    update();
  }
}
