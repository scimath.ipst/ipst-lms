import 'package:carousel_slider/carousel_slider.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api_content/api_content.dart';
import 'package:project14plus/api/api_content/content_typy.dart';

class FrontendController extends GetxController {
  bool darkMode = false;
  int pageIndex = 0;

  int navigationIndex = 0;

  var bannerCon = ApiContent.getVideoContent(ContentType.BANNER);
  var carouseCon = CarouselController();

  var continuedCon = ApiContent.getContinuedContents();

  var suggestedCon = ApiContent.getVideoContent(ContentType.SUGGESTED);

  var newestCon = ApiContent.getVideoContent(ContentType.NEWEST);

  var popularCon = ApiContent.getVideoContent(ContentType.POPULAR);

  refreshValue() {
    bannerCon = ApiContent.getVideoContent(ContentType.BANNER);
    continuedCon = ApiContent.getContinuedContents();
    suggestedCon = ApiContent.getVideoContent(ContentType.SUGGESTED);
    newestCon = ApiContent.getVideoContent(ContentType.NEWEST);
    popularCon = ApiContent.getVideoContent(ContentType.POPULAR);
    update();
  }

  setDartMode() {
    darkMode = !darkMode;
    update();
  }

  switchDarkMode(bool value) {
    darkMode = value;
    update();
  }

  initAuthScreen(int index) {
    pageIndex = index;
    update();
  }

  void changeNavigationIndex(int index) {
    navigationIndex = index;
    update();
  }
}
