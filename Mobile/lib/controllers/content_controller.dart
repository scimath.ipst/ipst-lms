import 'package:get/get.dart';
import 'package:project14plus/controllers/frontend_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/model.dart';
import 'package:project14plus/screens/screens.dart';

class ContentController extends GetxController {
  bool desCheck = false;

  bool isFullScreen = false;

  void setDesCheck(bool value) {
    desCheck = value;
    update();
  }

  void updateIsFullScreen(bool value) {
    isFullScreen = value;
    update();
  }

  void getTo(ParameterModel model) {
    Get.to(
      () => VideoContentScreen(
        contentUrl: model.contentUrl.toString(),
        contentId: model.contentId ?? '',
        watchingSeconds: model.watchingSeconds,
      ),
      duration: const Duration(milliseconds: 400),
      transition: Transition.downToUp,
      fullscreenDialog: true,
    );
  }

  void getToPlaylistVideo(ParameterModel model) {
    Get.to(
      () => VideoContentPlaylistScreen(
        playlistUrl: model.playlistUrl ?? '',
        playlistName: model.playlistName ?? '',
      ),
      duration: const Duration(milliseconds: 400),
      transition: Transition.downToUp,
      fullscreenDialog: true,
    );
  }

  Future<void> checkUser(ParameterModel model) async {
    await LocalStorage.getUser().then((user) async {
      if (user == null || user.id == '') {
        dynamic temp;
        await LocalStorage.getPdpa().then((value) async {
          if (value == null) {
            temp = await Get.to(() => PdpaScreen(
                  parameterModel: model,
                  isFromContent: true,
                  isFromLogin: true,
                ));
          } else {
            Get.find<FrontendController>().initAuthScreen(0);
            temp = await Get.to(
              () => LoginScreen(parameterModel: model),
              duration: const Duration(milliseconds: 400),
              transition: Transition.downToUp,
              fullscreenDialog: true,
            );
          }
        });

        if (temp != null && temp['isLogin'] == false) {
          Get.back();
        } else if (temp['isLogin'] == true) {
          if (model.isFromPlaylist) {
            getToPlaylistVideo(model);
          } else {
            getTo(model);
          }
        } else {
          Get.back();
        }
      } else {
        if (model.isFromPlaylist) {
          getToPlaylistVideo(model);
        } else {
          getTo(model);
        }
      }
    });
  }
}
