import 'package:get/get.dart';
import 'package:project14plus/routes/routes.dart';
import 'package:project14plus/screens/splash_screen.dart';
import '../dashboards/dashboards.dart';

class AppPages {
  static var list = [
    GetPage(
      name: AppRoutes.DASHBOARD,
      page: () => SplashScreen(),
    ),
  ];
}
