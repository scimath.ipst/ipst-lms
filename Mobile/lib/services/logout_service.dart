import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/notification_controller.dart';
import 'package:project14plus/controllers/user_controller.dart';
import 'package:project14plus/data/local_storage.dart';
import 'package:project14plus/model/model.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Create storage
const storage = FlutterSecureStorage();

class LogOutService {
  static Future<void> logOut() async {
    UserController _userController = Get.find<UserController>();
    NotificationController _notificationController =
        Get.find<NotificationController>();

    final prefs = await SharedPreferences.getInstance();
    UserModel? user = await LocalStorage.getUser();

    //Delete fcm on web
    if (user != null) {
      await ApiPersonal.fcmTokenUpdate(userId: user.id!, fcm: "");
    }

    // signOut Firebase
    // await AuthService.signOut();
    await FirebaseAuth.instance.signOut();
    // Delete all secure storage
    await storage.delete(key: ACCESS_TOKEN);
    await storage.delete(key: REFRESH_TOKEN);
    await storage.deleteAll();

    await prefs.remove(PREF_USER);
    await prefs.remove(PREF_NOTIFICATIONS);

    _userController.clearUser();
    _notificationController.deleteAllNotifications();
  }
}
