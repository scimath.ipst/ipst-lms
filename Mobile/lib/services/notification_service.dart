import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:project14plus/constants/constants.dart';
import 'package:project14plus/controllers/notification_controller.dart';
import 'package:project14plus/model/notification_model.dart';

final FirebaseMessaging _messaging = FirebaseMessaging.instance;

class NotificationService {
  /// initail Notification Service
  static void init() async {
    _requestPermission();

    /* ---------------------------------------------------------------------- */
    /*                                FCM TOKEN                               */
    /* ---------------------------------------------------------------------- */

    await _messaging.getToken().then((token) => print('FCM TOKEN: $token'));

    // Any time the token refreshes, store this in the database too.
    _messaging.onTokenRefresh.listen(_saveTokenToDatabase);

    /* ---------------------------------------------------------------------- */
    /*                             HANDLE MESSAGE                             */
    /* ---------------------------------------------------------------------- */

    /// Foreground messages
    FirebaseMessaging.onMessage.listen(_handleMessageForeground);

    // แอปถูกเปิดอยู่ใน background
    FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);

    /// แอปถูกปิดอยู่
    final message = await _messaging.getInitialMessage();
    log('message: $message', name: logName);
    await _handleMessage(message);
  }

  /// Background messages
  static Future<void> handleBackgroundMessage(RemoteMessage message) async {
    await Firebase.initializeApp();
    await _handleMessage(message);
  }

  /// Requesting permission (iOS)
  /// Android are not required to request permission.
  static _requestPermission() async {
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    log('User granted permission: ${settings.authorizationStatus}',
        name: logName);
  }

  /// Get FCM TOKEN
  ///
  /// Returns the default FCM token for this device.
  static Future<String?> getFCMToken() async {
    final fcmToken = await _messaging.getToken();
    return fcmToken;
  }

  /// Save FCM Token to Database
  static Future<void> _saveTokenToDatabase(String? token) async {
    if (token != null) {
      // Call APIs :
      // ApiPersonal.updateFCMToken(token);
    }
  }

  static _handleMessage(RemoteMessage? message) async {
    if (message != null && message.notification?.title != '') {
      NotificationModel model = NotificationModel(
        id: message.messageId,
        title: message.notification?.title,
        body: message.notification?.body,
        sentTime: message.sentTime?.toIso8601String() ??
            DateTime.now().toIso8601String(),
        isRead: false,
      );

      // save to local storage
      final NotificationController _notificationController =
          Get.find<NotificationController>();
      _notificationController.saveNotifications(model);
    }
  }

  static _handleMessageForeground(RemoteMessage? message) async {
    Get.lazyPut<NotificationController>(() => NotificationController());
    if (message != null && message.notification?.title != '') {
      NotificationModel model = NotificationModel(
        id: message.messageId,
        title: message.notification?.title,
        body: message.notification?.body,
        sentTime: message.sentTime?.toIso8601String() ??
            DateTime.now().toIso8601String(),
        isRead: false,
      );

      // save to local storage
      final NotificationController _notificationController =
          Get.find<NotificationController>();
      _notificationController.saveNotifications(model, isForeground: true);
    }
  }
}
