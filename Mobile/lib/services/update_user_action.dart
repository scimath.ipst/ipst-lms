import 'package:get/get.dart';
import 'package:project14plus/api/api.dart';
import 'package:project14plus/controllers/frontend_controller.dart';

class UpdateUserAction {
  static updateUserAction(
      {required String id,
      required double watchingSeconds,
      required int status,
      bool isOnEnd = false,
      required int position}) async {
    if (status != 2 && status != null) {
      await ApiPersonal.userAction(
        id: id,
        watchingSeconds: watchingSeconds,
        status: isOnEnd
            ? 2
            : position != 0
                ? 1
                : 0,
      );
      Get.find<FrontendController>().refreshValue();
    }
  }
}
