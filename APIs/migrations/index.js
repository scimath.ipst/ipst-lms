const path = require('path');
const bcrypt = require('bcryptjs');
const db = require('../models');

exports.initial = async () => {
  try { 

    const userRoleCount = await db.UserRole.countDocuments({});
    if (!userRoleCount) {

      // User Roles
      const roleUser = await new db.UserRole({
        name: 'ผู้ใช้', nameEng: 'User', level: 0
      }).save();
      const roleCreator = await new db.UserRole({
        name: 'ผู้สร้างสื่อ', nameEng: 'Creator', level: 1
      }).save();
      const roleManager = await new db.UserRole({
        name: 'ผู้บริหารจัดการสื่อ', nameEng: 'Manager', level: 2
      }).save();
      const roleQCO = await new db.UserRole({
        name: 'ผู้ตรวจสอบคุณภาพ', nameEng: 'Quality Control Officer', level: 3
      }).save();
      const roleSuperAdmin = await new db.UserRole({
        name: 'ผู้ดูแลระบบ', nameEng: 'Super Admin', level: 4
      }).save();


      // Tags
      const tag01 = await new db.Tag({ name: 'วิทยาศาสตร์', status: 1 }).save();
      const tag02 = await new db.Tag({ name: 'คณิตศาสตร์', status: 1 }).save();
      const tag03 = await new db.Tag({ name: 'เทคโนโลยี', status: 1 }).save();
      const tag04 = await new db.Tag({ name: 'วิทยาการคำนวณ', status: 1 }).save();
      const tag05 = await new db.Tag({ name: 'การออกแบบและเทคโนโลยี', status: 1 }).save();
      const tag06 = await new db.Tag({ name: 'ฟิสิกส์', status: 1 }).save();
      const tag07 = await new db.Tag({ name: 'เคมี', status: 1 }).save();
      const tag08 = await new db.Tag({ name: 'ชีววิทยา', status: 1 }).save();
      const tag09 = await new db.Tag({ name: 'ธรณีวิทยา', status: 1 }).save();
      const tag10 = await new db.Tag({ name: 'ดาราศาสตร์', status: 1 }).save();
      const tag11 = await new db.Tag({ name: 'เพิ่มเติม', status: 1 }).save();


      // Levels
      const level01 = await new db.Level({ 
        name: 'ประถมศึกษาปีที่ 1', url: 'grade-1', order: 0, status: 1
      }).save();
      const level02 = await new db.Level({ 
        name: 'ประถมศึกษาปีที่ 2', url: 'grade-2', order: 1, status: 1
      }).save();
      const level03 = await new db.Level({ 
        name: 'ประถมศึกษาปีที่ 3', url: 'grade-3', order: 2, status: 1
      }).save();
      const level04 = await new db.Level({ 
        name: 'ประถมศึกษาปีที่ 4', url: 'grade-4', order: 3, status: 1
      }).save();
      const level05 = await new db.Level({ 
        name: 'ประถมศึกษาปีที่ 5', url: 'grade-5', order: 4, status: 1
      }).save();
      const level06 = await new db.Level({ 
        name: 'ประถมศึกษาปีที่ 6', url: 'grade-6', order: 5, status: 1
      }).save();

      const level07 = await new db.Level({ 
        name: 'มัธยมศึกษาปีที่ 1', url: 'grade-7', order: 6, status: 1
      }).save();
      const level08 = await new db.Level({ 
        name: 'มัธยมศึกษาปีที่ 2', url: 'grade-8', order: 7, status: 1
      }).save();
      const level09 = await new db.Level({ 
        name: 'มัธยมศึกษาปีที่ 3', url: 'grade-9', order: 8, status: 1
      }).save();
      
      const level10 = await new db.Level({ 
        name: 'มัธยมศึกษาปีที่ 4', url: 'grade-10', order: 9, status: 1
      }).save();
      const level11 = await new db.Level({ 
        name: 'มัธยมศึกษาปีที่ 5', url: 'grade-11', order: 10, status: 1
      }).save();
      const level12 = await new db.Level({ 
        name: 'มัธยมศึกษาปีที่ 6', url: 'grade-12', order: 11, status: 1
      }).save();


      // Subjects
      const subject01 = await new db.Subject({ 
        name: 'วิทยาศาสตร์', url: 'science', order: 0, status: 1
      }).save();
      const subject02 = await new db.Subject({ 
        name: 'คณิตศาสตร์', url: 'mathematics', order: 1, status: 1
      }).save();
      const subject03 = await new db.Subject({ 
        name: 'เทคโนโลยี', url: 'technology', order: 2, status: 1
      }).save();
      const subject04 = await new db.Subject({ 
        name: 'วิทยาการคำนวณ', url: 'com-science', order: 3, status: 1
      }).save();
      const subject05 = await new db.Subject({ 
        name: 'การออกแบบและเทคโนโลยี', url: 'design-technology', order: 4, status: 1
      }).save();
      const subject06 = await new db.Subject({ 
        name: 'ฟิสิกส์', url: 'physics', order: 5, status: 1
      }).save();
      const subject07 = await new db.Subject({ 
        name: 'เคมี', url: 'chemistry', order: 6, status: 1
      }).save();
      const subject08 = await new db.Subject({ 
        name: 'ชีววิทยา', url: 'biology', order: 7, status: 1
      }).save();
      const subject09 = await new db.Subject({ 
        name: 'ธรณีวิทยา', url: 'geology', order: 8, status: 1
      }).save();
      const subject10 = await new db.Subject({ 
        name: 'ดาราศาสตร์', url: 'astronomy', order: 9, status: 1
      }).save();

    }

  } catch (err) {
    console.log(err);
  }
}