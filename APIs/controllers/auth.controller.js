const config = require('../config');
const db = require('../models');
const sanitize = require('mongo-sanitize');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mailer = require('nodemailer');
const path = require('path');
const fs = require('fs');
const handlebars = require('handlebars');
const fetch = require('node-fetch');


// Mailer
var smtp = {
  host: config.smtpHost,
  port: Number(config.smtpPort),
  secure: false,
  auth: {
    user: config.smtpUsername,
    pass: config.smtpPassword
  }
};
var smtpConn = mailer.createTransport(smtp);

var readFileHTML = function(path, callback) {
  fs.readFile(path, {encoding: 'utf-8'}, function(err, html) {
    if(err) return callback(err);
    else return callback(null, html);
  });
};


exports.refreshToken = async (req, res) => {
  try {
    const { refreshToken } = req.body;
    if (!refreshToken) {
      return res.status(403).send({message: 'No refresh token provided.'});
    }
    jwt.verify(refreshToken, config.tokenRefreshSecret, async (err, decodedToken) => {
      if (err) {
        return res.status(401).send({message: 'Refresh token expired.'});
      }
      const user = await db.User.findOne({
        _id: sanitize(decodedToken._id), refreshToken: refreshToken
      });
      if (!user) {
        return res.status(401).send({message: 'Refresh token expired.'});
      } else if (!user.externalJWT) {
        return res.status(401).send({message: 'External token expired.'});
      }

      const fetch1 = await fetch(`${config.externalApiUrl}api/user/read`, {
        method: 'GET',
        headers: { 
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${user.externalJWT}`
        },
      });
      if(!fetch1.ok || fetch1.status !== 200) {
        return res.status(401).send({message: 'External token expired.'});
      }
      
      const newAccessToken = jwt.sign(
        { _id: user._id, externalJWT: user.externalJWT }, 
        config.tokenSecret, 
        { expiresIn: config.tokenLife }
      );
      const newRefreshToken = jwt.sign(
        { _id: user._id }, 
        config.tokenRefreshSecret, 
        { expiresIn: config.tokenRefreshLife }
      );
      await user.updateOne({refreshToken: newRefreshToken}, []);
      res.status(200).send({
        user: user,
        accessToken: newAccessToken,
        refreshToken: newRefreshToken
      });
    });
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
}

exports.signin = async (req, res) => {
  try {
    // Check external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/auth/signin`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ...req.body, 
        app_id: config.externalApiId,
        external_app_id: config.externalAppId
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ หรือรหัสผ่านผิด'});
    }
    
    // Check external user info
    const externalJWT = data1.jwt;
    const fetch2 = await fetch(`${config.externalApiUrl}api/user/read`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${externalJWT}`
      },
    });
    const data2 = await fetch2.json();
    if(!data2 || data2.status != 200) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }
    
    // Check internal user
    const userData = data2.data;
    var user = await db.User.findOne({externalId: Number(userData.id)})
      .populate({ path: 'role' })
      .populate({ path: 'department', select: 'name' })
      .populate({ path: 'level', select: 'name' })
      .populate({ path: 'subjects', select: 'name' })
      .populate({ path: 'tags', select: 'name' });
    if(!user) {
      var role;
      if(Number(userData.role.is_super_admin)) {
        role = await db.UserRole.findOne({level: 4});
      } else if(Number(userData.role.is_admin)) {
        role = await db.UserRole.findOne({level: 3});
      } else {
        role = await db.UserRole.findOne({level: 0});
      }
      user = await db.User({
        externalId: Number(userData.id),
        role: role,
        username: userData.username,
        firstname: userData.firstname,
        lastname: userData.lastname,
        avatar: userData.profile,
        externalJWT: externalJWT,
        status: 1
      }).save();
    } else {
      user.username = userData.username;
      user.firstname = userData.firstname;
      user.lastname = userData.lastname;
      user.avatar = userData.profile;
      user.externalJWT = externalJWT;
      user.save();
    }

    // Generate tokens
    const accessToken = jwt.sign(
      { _id: user._id, externalJWT: externalJWT }, 
      config.tokenSecret, 
      { expiresIn: config.tokenLife }
    );
    const refreshToken = jwt.sign(
      { _id: user._id }, 
      config.tokenRefreshSecret, 
      { expiresIn: config.tokenRefreshLife }
    );
    await user.updateOne({ refreshToken: refreshToken }, []);
    if(!user.status) {
      return res.status(401).send({message: 'ยังไม่ได้รับการยืนยันผู้ใช้'});
    }

    return res.status(200).send({
      _id: user._id,
      externalId: user.externalId,
      role: user.role,
      department: user.department,
      level: user.level,
      subjects: user.subjects,
      tags: user.tags,
      firstname: userData.firstname,
      lastname: userData.lastname,
      username: userData.username,
      email: userData.email,
      avatar: userData.profile,
      detail: userData.detail? userData.detail: {},
      status: user.status,
      accessToken: accessToken,
      refreshToken: refreshToken
    });
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.signup = async (req, res) => {
  try {
    // Signup external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/auth/signup`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ...req.body, 
        password_confirm: req.body.confirmPassword,
        app_id: config.externalApiId,
        external_app_id: config.externalAppId
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: data1,
        errors: data1.messages
      });
    }
    
    
    var user = await db.User.findOne({externalId: Number(data1.data.id)});
    if(!user) {
      const role = await db.UserRole.findOne({level: 0});
      user = await db.User({
        externalId: Number(data1.data.id),
        role: role,
        username: req.body.username,
        status: 0
      }).save();
    }
    const verifyToken = jwt.sign(
      { _id: user._id }, 
      config.tokenVerifySecret, 
      { expiresIn: config.tokenVerifyLife }
    );
    
    // Send Email
    readFileHTML(path.join(__dirname, '../emails/signup.html'), function(err, html){
      if(err) console.log(err);
      var template = handlebars.compile(html);
      var mail = {
        from: config.emailFrom,
        to: req.body.email,
        subject: 'เปิดใช้งานบัญชี Project 14+', 
        html: template({
          FRONTEND_URL: config.frontendUrl,
          VERIFY_URL: `${config.frontendUrl}auth/verify/${verifyToken}`
        })
      };
      smtpConn.sendMail(mail, function(err) {
        smtpConn.close();
        if(err) console.log(err);
      });
    });
    
    return res.status(200).send(true);
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.verify = async (req, res) => {
  try {
    if(!req.params.token) {
      return res.status(401).send({message: 'Verify Toekn ไม่ถูกต้อง'});
    }

    const decoded = jwt.verify(req.params.token, config.tokenVerifySecret);
    const user = await db.User.findById(sanitize(decoded._id));
    if(!user) {
      return res.status(401).send({message: 'Verify Toekn ไม่ถูกต้อง'});
    }

    user.status = 1;
    await user.save();
    return res.status(200).send(true);
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.forgetPassword = async (req, res) => {
  try {
    // Forget password external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/auth/forget-password`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ...req.body, 
        app_id: config.externalApiId,
        external_app_id: config.externalAppId
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการขอตั้งรหัสผ่านใหม่',
        errors: data1.messages
      });
    }
    
    // Send Email
    readFileHTML(path.join(__dirname, '../emails/forget-password.html'), function(err, html){
      if(err) console.log(err);
      var template = handlebars.compile(html);
      var mail = {
        from: config.emailFrom,
        to: req.body.email,
        subject: 'ตั้งรหัสผ่านใหม่ในระบบ Project 14+', 
        html: template({
          FRONTEND_URL: config.frontendUrl,
          VERIFY_URL: `${config.frontendUrl}auth/reset-password/${data1.data.salt}`
        })
      };
      smtpConn.sendMail(mail, function(err) {
        smtpConn.close();
        if(err) console.log(err);
      });
    });
    
    return res.status(200).send(true);
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.checkResetPassword = async (req, res) => {
  try {
    const { token } = req.query;
    if(!token) {
      return res.status(401).send({message: 'Reset Password Toekn ไม่ถูกต้อง'});
    }

    const fetch1 = await fetch(`${config.externalApiUrl}api/auth/reset-password/${token}`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการขอตั้งรหัสผ่านใหม่',
        errors: data1.messages
      });
    }

    return res.status(200).send(true);
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.resetPassword = async (req, res) => {
  try {
    // Reset password external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/auth/reset-password`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        salt: req.body.token,
        password_new: req.body.newPassword,
        password_confirm: req.body.confirmNewPassword,
        ip: req.body.ip,
        url: req.body.url,
        app_id: config.externalApiId,
        external_app_id: config.externalAppId
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการตั้งรหัสผ่านใหม่',
        errors: data1.messages
      });
    }
    return res.status(200).send({ message: 'การตั้งรหัสผ่านใหม่สำเร็จแล้ว' });
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


exports.fcmTokenUpdate = async (req, res) => {
  try {
    const { userId, fcmToken } = req.body;
    
    if(!userId) return res.status(401).send({message: 'ระบุ userId'});

    const user = await db.User.findById(sanitize(userId));
    if(!user) return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});

    await user.updateOne({ fcmToken: fcmToken? fcmToken: '' }, []);
    return res.status(200).send({message: 'แก้ไข FCM Token สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

