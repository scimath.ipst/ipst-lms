const config = require('../config');
const db = require('../models');
const sanitize = require('mongo-sanitize');
const fetch = require('node-fetch');


// Dashboard
exports.dashboardCountContents = async (req, res) => {
  try {
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlists = await db.Playlist.find({ 
      channel: channels.map(d => d._id), status: [2, 3]
    }).select('_id');

    const unlisted = await db.Content.countDocuments({ 
      status: 0, user: req.user, playlist: playlists.map(d => d._id)
    });
    const drafted = await db.Content.countDocuments({ 
      status: 1, user: req.user, playlist: playlists.map(d => d._id)
    });
    const private = await db.Content.countDocuments({ 
      status: 2, user: req.user, playlist: playlists.map(d => d._id)
    });
    const public = await db.Content.countDocuments({ 
      status: 3, user: req.user, playlist: playlists.map(d => d._id)
    });
    return res.status(200).send({ 
      result: { unlisted: unlisted, drafted: drafted, private: private, public: public }
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.dashboardMostVisitedContents = async (req, res) => {
  try {
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlists = await db.Playlist.find({ 
      channel: channels.map(d => d._id), status: [2, 3]
    }).select('_id');

    const contents = await db.Content.find({ 
      user: req.user, playlist: playlists.map(d => d._id)
    })
      .select('name url image approvedStatus visitCount status')
      .populate({ 
        path: 'playlist', select: 'name',
        populate: { 
          path: 'channel', select: 'name',
          populate: { path: 'department', select: 'name' }
        }
      })
      .sort({ visitCount: -1 })
      .limit(5);
    return res.status(200).send({ result: contents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.dashboardUsers = async (req, res) => {
  try {
    const users = await db.User.find({ department: req.user.department })
      .sort({ createdAt: -1 })
      .limit(5);
    return res.status(200).send({ result: users });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// User
exports.userList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = { department: req.user.department };
    if(keyword) {
      where['$or'] = [
        { username : new RegExp(keyword, 'i') },
        { firstname : new RegExp(keyword, 'i') },
        { lastname : new RegExp(keyword, 'i') }
      ];
    }

    const users = await db.User.find(where)
      .populate('role')
      .populate('department');
    const result = await Promise.all(
      users.map(async (user) => {
        const fetch1 = await fetch(`${config.externalApiUrl}api/user/user-read/${user.externalId}`, {
          method: 'GET',
          headers: { 
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${req.externalJWT}`
          }
        });
        const data1 = await fetch1.json();
        if(!data1 || data1.status != 200) {
          return {
            _id: user._id,
            externalId: user.externalId,
            role: user.role,
            department: user.department,
            firstname: user.firstname? user.firstname: null,
            lastname: user.lastname? user.lastname: null,
            username: user.username? user.username: null,
            email: user.email? user.email: null,
            avatar: user.profile? user.profile: null,
            status: user.status? user.status: 0
          };
        } else {
          return {
            _id: user._id,
            externalId: user.externalId,
            role: user.role,
            department: user.department,
            firstname: data1.data.firstname? data1.data.firstname: null,
            lastname: data1.data.lastname? data1.data.lastname: null,
            username: data1.data.username? data1.data.username: null,
            email: data1.data.email? data1.data.email: null,
            avatar: data1.data.profile? data1.data.profile: null,
            status: user.status? user.status: 0
          };
        }
      })
    );

    return res.status(200).send({ result: result });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.userRead = async (req, res) => {
  try {
    const {userId} = req.query;
    const user = await db.User.findById(sanitize(userId))
      .populate({ path: 'role' })
      .populate({ path: 'department', select: 'name' })
      .populate({ path: 'level', select: 'name' })
      .populate({ path: 'subjects', select: 'name' })
      .populate({ path: 'tags', select: 'name' });
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    const fetch1 = await fetch(`${config.externalApiUrl}api/user/user-read/${user.externalId}`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      }
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({message: 'External server error.'});
    }
    
    const userData = data1.data;
    user.username = userData.username;
    user.firstname = userData.firstname;
    user.lastname = userData.lastname;
    user.save();

    const result = {
      _id: user._id,
      externalId: user.externalId,
      role: user.role,
      department: user.department,
      level: user.level,
      subjects: user.subjects,
      tags: user.tags,
      firstname: userData.firstname,
      lastname: userData.lastname,
      username: userData.username,
      email: userData.email,
      avatar: userData.profile,
      detail: userData.detail? userData.detail: {},
      status: user.status
    };
    return res.status(200).send({ result: result });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Channel
exports.channelList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = { department: req.user.department, status: [2, 3] };
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const channels = await db.Channel.find(where).sort({'name': 1})
      .populate('department');

    return res.status(200).send({ result: channels });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.channelRead = async (req, res) => {
  try {
    const { channelId } = req.query;
    const channel = await db.Channel.findOne({ 
      _id: sanitize(channelId), department: req.user.department, status: [2,3]
    }).populate('department');
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    return res.status(200).send({ result: channel });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Playlist
exports.playlistList = async (req, res) => {
  try {
    const { keyword } = req.query;

    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3] 
    }).select('_id');
    var where = { channel: channels.map(d => d._id), status: [2, 3] };
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const playlists = await db.Playlist.find(where).sort({'name': 1})
      .populate({ 
        path: 'channel', select: 'name',
        populate: { path: 'department' }
      });
    return res.status(200).send({ result: playlists });
  } catch(err) {
    console.log(err)
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.playlistRead = async (req, res) => {
  try {
    const { playlistId } = req.query;
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(playlistId), channel: channels.map(d => d._id), status: [2, 3]
    }).populate('channel');
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    return res.status(200).send({ result: playlist });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Content
exports.contentList = async (req, res) => {
  try {
    const { keyword, status, approvedStatus } = req.query;
    
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlists = await db.Playlist.find({ 
      channel: channels.map(d => d._id), status: [2, 3]
    }).select('_id');
    var where = { playlist: playlists.map(d => d._id), user: req.user };
    if(keyword) where['name'] = new RegExp(keyword, 'i');
    if(status != undefined && status != '') where['status'] = status;
    if(approvedStatus != undefined && approvedStatus != '') where['approvedStatus'] = approvedStatus;

    const contents = await db.Content.find(where)
      .select('user name url image type approvedStatus order status visitCount isFeatured')
      .populate({ path: 'user', select: 'username firstname lastname' })
      .populate({ 
        path: 'playlist', select: 'name',
        populate: { 
          path: 'channel', select: 'name',
          populate: { path: 'department', select: 'name' }
        }
      })
      .sort({'isFeatured': -1, 'createdAt': -1});

    return res.status(200).send({ result: contents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentRead = async (req, res) => {
  try {
    const { contentId } = req.query;
    
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlists = await db.Playlist.find({
      channel: channels.map(d => d._id), status: [2, 3]
    }).select('_id');

    const content = await db.Content.findOne({
      _id: sanitize(contentId), playlist: playlists.map(d => d._id), user: req.user
    })
      .populate({ path: 'user' })
      .populate({ 
        path: 'playlist',
        populate: { 
          path: 'channel',
          populate: { path: 'department' }
        }
      })
      .populate({ path: 'levels', select: 'name' })
      .populate({ path: 'subject', select: 'name' })
      .populate({ path: 'tags', select: 'name' });
    return res.status(200).send({ result: content });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentAdd = async (req, res) => {
  try {
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(req.body.playlistId), channel: channels.map(d => d._id), status: [2, 3]
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    
    var levelIDs = [];
    if(req.body.levels) levelIDs = req.body.levels.map(d => d._id);
    const levels = await db.Level.find({ _id: { $in: levelIDs } });

    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    const content = await db.Content({
      user: req.user,
      playlist: playlist,
      levels: levels,
      subject: subject,
      tags: tags,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      badge: req.body.badge,
      badgeName: req.body.badgeName,
      type: req.body.type,
      youtubeVideoId: req.body.youtubeVideoId,
      filePath: req.body.filePath,
      videoQuestions: req.body.videoQuestions,
      scoreType: req.body.scoreType,
      questions: req.body.questions,
      approvedStatus: -1,
      order: req.body.order,
      status: req.body.status
    }).save();
    if (!content) {
      return res.status(401).send({message: 'สร้าง Content ไม่สำเร็จ'});
    }

    return res.status(200).send({message: 'สร้าง Content สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ชื่อลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Content ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.contentEdit = async (req, res) => {
  try {
    const channels = await db.Channel.find({ 
      department: req.user.department, status: [2, 3]
    }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(req.body.playlistId), channel: channels.map(d => d._id), status: [2, 3]
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    
    var levelIDs = [];
    if(req.body.levels) levelIDs = req.body.levels.map(d => d._id);
    const levels = await db.Level.find({ _id: { $in: levelIDs } });

    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    const content = await db.Content.findOneAndUpdate({
      _id: sanitize(req.body._id), user: req.user
    }, {
      playlist: playlist,
      levels: levels,
      subject: subject,
      tags: tags,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      badge: req.body.badge,
      badgeName: req.body.badgeName,
      type: req.body.type,
      youtubeVideoId: req.body.youtubeVideoId,
      filePath: req.body.filePath,
      videoQuestions: req.body.videoQuestions,
      scoreType: req.body.scoreType,
      questions: req.body.questions,
      order: req.body.order,
      status: req.body.status
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Content สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Content ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.contentDelete = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const content = await db.Content.findOne({
      _id: sanitize(req.body.contentId), playlist: playlists.map(d => d._id), user: req.user
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }
    content.remove();

    await db.ApprovedContent.deleteMany({ content: content });
    await db.UserAction.deleteMany({ content: content });

    return res.status(200).send({message: 'ลบ Content สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.approvedContentList = async (req, res) => {
  try {
    const { contentId } = req.query;
    const content = await db.Content.findById(sanitize(contentId));
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }

    const approvedContents = await db.ApprovedContent.find({ content: content })
      .sort({'createdAt': 1})
      .populate({ path: 'approver', select: 'username firstname lastname' });

    return res.status(200).send({ result: approvedContents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.approvedContentAdd = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const content = await db.Content.findOneAndUpdate({ 
      _id: sanitize(req.body.contentId), playlist: playlists.map(d => d._id)
    }, {
      approver: req.user,
      approvedStatus: req.body.approvedStatus
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }

    await db.ApprovedContent({
      approver: req.user,
      content: content,
      approvedStatus: req.body.approvedStatus,
      comment: req.body.comment,
    }).save();

    return res.status(200).send({message: 'สร้าง Approved Content สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
