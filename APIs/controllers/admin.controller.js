const config = require('../config');
const db = require('../models');
const sanitize = require('mongo-sanitize');
const fetch = require('node-fetch');


// Department
exports.departmentAdd = async (req, res) => {
  try {
    await db.Department({
      name: req.body.name,
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้างหน่วยงานสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.departmentEdit = async (req, res) => {
  try {
    const department = await db.Department.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      name: req.body.name, 
      status: Number(req.body.status)
    });
    if(!department) {
      return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไขหน่วยงานสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.departmentDelete = async (req, res) => {
  try {
    const department = await db.Department.findById(sanitize(req.body.deptId));
    if(!department) {
      return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
    }
    department.remove();
    return res.status(200).send({message: 'ลบหน่วยงานสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// User
exports.userList = async (req, res) => {
  try {
    const {page, pp, keyword} = req.query;
    const fetch1 = await fetch(`${config.externalApiUrl}api/admin/user-list?page=${page}&pp=${pp}&keyword=${ keyword }`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      }
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({message: 'External server error.'});
    }

    const paginate = {
      page: Number(page), pp: Number(pp),
      total: Number(data1.data.total),
      totalPages: Number(data1.data.total_pages)
    };
    const externalUsers = data1.data.result;
    const userIds = externalUsers.map(d => Number(d.id));
    const users = await db.User.find({ externalId: { $in: userIds} })
      .populate('role').populate('department');
    const result = externalUsers.map(user => {
      let temp = users.filter(d => d.externalId == user.id);
      if(temp.length) temp = temp[0];
      else temp = null;
      return {
        _id: temp? temp._id: null,
        externalId: user.id,
        role: temp? temp.role: null,
        department: temp? temp.department: null,
        firstname: user.firstname,
        lastname: user.lastname,
        username: user.username,
        email: user.email,
        avatar: user.profile,
        status: temp? temp.status: 0
      };
    });

    return res.status(200).send({ paginate: paginate, result: result });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.userRead = async (req, res) => {
  try {
    const { userId } = req.query;
    const user = await db.User.findById(sanitize(userId))
      .populate({ path: 'role' })
      .populate({ path: 'department', select: 'name' })
      .populate({ path: 'level', select: 'name' })
      .populate({ path: 'subjects', select: 'name' })
      .populate({ path: 'tags', select: 'name' });
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    const fetch1 = await fetch(`${config.externalApiUrl}api/admin/user-read/${user.externalId}`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      }
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({message: 'External server error.'});
    }
    
    const userData = data1.data;
    user.username = userData.username;
    user.firstname = userData.firstname;
    user.lastname = userData.lastname;
    user.avatar = userData.profile;
    user.save();

    const result = {
      _id: user._id,
      externalId: user.externalId,
      role: user.role,
      department: user.department,
      level: user.level,
      subjects: user.subjects,
      tags: user.tags,
      firstname: userData.firstname,
      lastname: userData.lastname,
      username: userData.username,
      email: userData.email,
      avatar: userData.profile,
      detail: userData.detail? userData.detail: {},
      status: user.status
    };
    return res.status(200).send({ result: result });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.userAdd = async (req, res) => {
  try {
    const role = await db.UserRole.findById(sanitize(req.body.roleId));
    if(!role) {
      return res.status(401).send({message: 'ไม่พบบทบาทในระบบ'});
    }
    var department;
    if(req.body.deptId) {
      department = await db.Department.findById(sanitize(req.body.deptId));
      if(!department) {
        return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
      }
    }

    var level;
    if(req.body.levelId) {
      level = await db.Level.findById(sanitize(req.body.levelId));
      if(!level) {
        return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
      }
    }
    var subjectIDs = [];
    if(req.body.subjects) subjectIDs = req.body.subjects.map(d => d._id);
    const subjects = await db.Subject.find({ _id: { $in: subjectIDs } });
    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    // Signup external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/admin/user-create`, {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      },
      body: JSON.stringify({
        ...req.body, 
        profile: req.body.avatar,
        password_confirm: req.body.confirmPassword,
        app_id: config.externalApiId,
        external_app_id: config.externalAppId
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการสร้างผู้ใช้',
        errors: data1.messages
      });
    }

    await db.User({
      externalId: Number(data1.data.id),
      role: role,
      status: Number(req.body.status),
      department: department? department: null,
      username: req.body.username,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      avatar: req.body.avatar,
      level: level? level: null,
      subjects: subjects,
      tags: tags,
    }).save();
    return res.status(200).send({message: 'สร้างบัญชีผู้ใช้สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.userEdit = async (req, res) => {
  try {
    const role = await db.UserRole.findById(sanitize(req.body.roleId));
    if(!role) {
      return res.status(401).send({message: 'ไม่พบบทบาทในระบบ'});
    }
    var department;
    if(req.body.deptId) {
      department = await db.Department.findById(sanitize(req.body.deptId));
      if(!department) {
        return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
      }
    }

    var level;
    if(req.body.levelId) {
      level = await db.Level.findById(sanitize(req.body.levelId));
      if(!level) {
        return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
      }
    }
    var subjectIDs = [];
    if(req.body.subjects) subjectIDs = req.body.subjects.map(d => d._id);
    const subjects = await db.Subject.find({ _id: { $in: subjectIDs } });
    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });
    
    const user = await db.User.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      role: role, 
      status: Number(req.body.status),
      department: department? department: null,
      username: req.body.username,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      avatar: req.body.avatar,
      level: level? level: null,
      subjects: subjects,
      tags: tags,
    });
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    // Update external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/admin/user-update`, {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      },
      body: JSON.stringify({
        id: user.externalId,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        profile: req.body.avatar,
        app_id: config.externalApiId,
        external_app_id: config.externalAppId,
        ip: req.body.ip,
        url: req.body.url
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการแก้ไขผู้ใช้',
        errors: data1.messages
      });
    }

    if(req.body.password && req.body.confirmPassword) {
      const fetch2 = await fetch(`${config.externalApiUrl}api/admin/user-update-password`, {
        method: 'POST',
        headers: { 
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${req.externalJWT}`
        },
        body: JSON.stringify({
          id: user.externalId,
          new_password: req.body.password,
          new_password_confirm: req.body.confirmPassword,
          app_id: config.externalApiId,
          external_app_id: config.externalAppId,
          ip: req.body.ip,
          url: req.body.url
        }),
      });
      const data2 = await fetch2.json();
      if(!data2 || data2.status != 200) {
        return res.status(401).send({
          message: 'เกิดข้อผิดพลาดในการแก้ไขผู้ใช้',
          errors: data2.messages
        });
      }
    }

    return res.status(200).send({message: 'แก้ไขบัญชีผู้ใช้สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.userDelete = async (req, res) => {
  try {
    const user = await db.User.findById(sanitize(req.body.userId));
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }
    user.remove();
    return res.status(200).send({message: 'ลบบัญชีผู้ใช้สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.userHistoryList = async (req, res) => {
  try {
    const { userId } = req.query;
    
    const user = await db.User.findById(sanitize(userId));
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    const userActions = await db.UserAction.find({ user: user })
      .populate({ path: 'content' })
      .sort({ updatedAt: -1 });

    return res.status(200).send({ result: userActions });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Role
exports.roleList = async (req, res) => {
  try {
    const roles = await db.UserRole.find().sort({'level': 1});
    return res.status(200).send({ result: roles });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
