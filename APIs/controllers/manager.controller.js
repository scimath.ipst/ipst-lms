const config = require('../config');
const db = require('../models');
const sanitize = require('mongo-sanitize');
const fetch = require('node-fetch');


// Dashboard
exports.dashboardCountContents = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const unlisted = await db.Content.countDocuments({ 
      status: 0, playlist: playlists.map(d => d._id)
    });
    const drafted = await db.Content.countDocuments({ 
      status: 1, playlist: playlists.map(d => d._id) 
    });
    const private = await db.Content.countDocuments({ 
      status: 2, playlist: playlists.map(d => d._id)
    });
    const public = await db.Content.countDocuments({ 
      status: 3, playlist: playlists.map(d => d._id)
    });
    return res.status(200).send({ 
      result: { unlisted: unlisted, drafted: drafted, private: private, public: public }
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.dashboardMostVisitedContents = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const contents = await db.Content.find({ playlist: playlists.map(d => d._id) })
      .select('name url image approvedStatus visitCount status')
      .populate({ 
        path: 'playlist', select: 'name',
        populate: { 
          path: 'channel', select: 'name',
          populate: { path: 'department', select: 'name' }
        }
      })
      .sort({ visitCount: -1 })
      .limit(5);
    return res.status(200).send({ result: contents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.dashboardUsers = async (req, res) => {
  try {
    const users = await db.User.find({ department: req.user.department })
      .sort({ createdAt: -1 })
      .limit(5);
    return res.status(200).send({ result: users });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Channel
exports.channelList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = { department: req.user.department };
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const channels = await db.Channel.find(where).sort({'name': 1})
      .populate('department');

    return res.status(200).send({ result: channels });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.channelRead = async (req, res) => {
  try {
    const { channelId } = req.query;
    const channel = await db.Channel.findOne({ 
      _id: sanitize(channelId), department: req.user.department
    }).populate('department');
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    return res.status(200).send({ result: channel });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.channelAdd = async (req, res) => {
  try {
    await db.Channel({
      department: req.user.department,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้าง Channel สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Channel ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.channelEdit = async (req, res) => {
  try {
    const channel = await db.Channel.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Channel สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Channel ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.channelDelete = async (req, res) => {
  try {
    const channel = await db.Channel.findOne({
      _id: sanitize(req.body.channelId), department: req.user.department
    });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    channel.remove();
    return res.status(200).send({message: 'ลบ Channel สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Playlist
exports.playlistList = async (req, res) => {
  try {
    const { keyword } = req.query;

    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    var where = { channel: channels.map(d => d._id) };
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const playlists = await db.Playlist.find(where).sort({'name': 1})
      .populate({ 
        path: 'channel', select: 'name',
        populate: { path: 'department' }
      });
    return res.status(200).send({ result: playlists });
  } catch(err) {
    console.log(err)
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.playlistRead = async (req, res) => {
  try {
    const { playlistId } = req.query;
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(playlistId), channel: channels.map(d => d._id)
    }).populate('channel');
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    return res.status(200).send({ result: playlist });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.playlistAdd = async (req, res) => {
  try {
    const channel = await db.Channel.findOne({
      _id: sanitize(req.body.channelId), department: req.user.department
    });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }

    await db.Playlist({
      channel: channel,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้าง Playlist สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Playlist ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.playlistEdit = async (req, res) => {
  try {
    const channel = await db.Channel.findOne({
      _id: sanitize(req.body.channelId), department: req.user.department
    });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }

    const playlist = await db.Playlist.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      channel: channel,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Playlist สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Playlist ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.playlistDelete = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(req.body.playlistId), channel: channels.map(d => d._id)
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    playlist.remove();
    return res.status(200).send({message: 'ลบ Playlist สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Content
exports.contentList = async (req, res) => {
  try {
    const { keyword, status, approvedStatus } = req.query;
    
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');
    var where = { playlist: playlists.map(d => d._id) };
    if(keyword) where['name'] = new RegExp(keyword, 'i');
    if(status != undefined && status != '') where['status'] = status;
    if(approvedStatus != undefined && approvedStatus != '') where['approvedStatus'] = approvedStatus;

    const contents = await db.Content.find(where)
      .select('user name url image type approvedStatus order status visitCount isFeatured')
      .populate({ path: 'user', select: 'username firstname lastname' })
      .populate({ 
        path: 'playlist', select: 'name',
        populate: { 
          path: 'channel', select: 'name',
          populate: { path: 'department', select: 'name' }
        }
      })
      .sort({'isFeatured': -1, 'createdAt': -1});

    return res.status(200).send({ result: contents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentRead = async (req, res) => {
  try {
    const { contentId } = req.query;
    
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const content = await db.Content.findOne({
      _id: sanitize(contentId), playlist: playlists.map(d => d._id)
    })
      .populate({ path: 'user' })
      .populate({ 
        path: 'playlist',
        populate: { 
          path: 'channel',
          populate: { path: 'department' }
        }
      })
      .populate({ path: 'levels', select: 'name' })
      .populate({ path: 'subject', select: 'name' })
      .populate({ path: 'tags', select: 'name' });
    return res.status(200).send({ result: content });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentAdd = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(req.body.playlistId), channel: channels.map(d => d._id)
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    
    var levelIDs = [];
    if(req.body.levels) levelIDs = req.body.levels.map(d => d._id);
    const levels = await db.Level.find({ _id: { $in: levelIDs } });

    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    const content = await db.Content({
      user: req.user,
      playlist: playlist,
      levels: levels,
      subject: subject,
      tags: tags,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      badge: req.body.badge,
      badgeName: req.body.badgeName,
      type: req.body.type,
      youtubeVideoId: req.body.youtubeVideoId,
      filePath: req.body.filePath,
      videoQuestions: req.body.videoQuestions,
      scoreType: req.body.scoreType,
      questions: req.body.questions,
      approver: req.user,
      approvedStatus: 1,
      order: req.body.order,
      status: req.body.status
    }).save();
    if (!content) {
      return res.status(401).send({message: 'สร้าง Content ไม่สำเร็จ'});
    }

    await db.ApprovedContent({
      approver: req.user,
      content: content,
      approvedStatus: 1,
      comment: 'สร้างโดย Manager'
    }).save();

    return res.status(200).send({message: 'สร้าง Content สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ชื่อลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Content ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.contentEdit = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlist = await db.Playlist.findOne({
      _id: sanitize(req.body.playlistId), channel: channels.map(d => d._id)
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    
    var levelIDs = [];
    if(req.body.levels) levelIDs = req.body.levels.map(d => d._id);
    const levels = await db.Level.find({ _id: { $in: levelIDs } });

    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    const content = await db.Content.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      playlist: playlist,
      levels: levels,
      subject: subject,
      tags: tags,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      badge: req.body.badge,
      badgeName: req.body.badgeName,
      type: req.body.type,
      youtubeVideoId: req.body.youtubeVideoId,
      filePath: req.body.filePath,
      videoQuestions: req.body.videoQuestions,
      scoreType: req.body.scoreType,
      questions: req.body.questions,
      order: req.body.order,
      status: req.body.status
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Content สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Content ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.contentDelete = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const content = await db.Content.findOne({
      _id: sanitize(req.body.contentId), playlist: playlists.map(d => d._id)
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }
    content.remove();

    await db.ApprovedContent.deleteMany({ content: content });
    await db.UserAction.deleteMany({ content: content });

    return res.status(200).send({message: 'ลบ Content สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.approvedContentAdd = async (req, res) => {
  try {
    const channels = await db.Channel.find({ department: req.user.department }).select('_id');
    const playlists = await db.Playlist.find({ channel: channels.map(d => d._id) }).select('_id');

    const content = await db.Content.findOneAndUpdate({ 
      _id: sanitize(req.body.contentId), playlist: playlists.map(d => d._id)
    }, {
      approver: req.user,
      approvedStatus: req.body.approvedStatus
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }

    await db.ApprovedContent({
      approver: req.user,
      content: content,
      approvedStatus: req.body.approvedStatus,
      comment: req.body.comment,
    }).save();

    return res.status(200).send({message: 'สร้าง Approved Content สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Department
exports.departmentEdit = async (req, res) => {
  try {
    const department = await db.Department.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      name: req.body.name
    });
    if(!department) {
      return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
    }
    department.name = req.body.name;
    return res.status(200).send({message: 'แก้ไขหน่วยงานสำเร็จ', result: department});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
