const config = require('../config');
const db = require('../models');
const sanitize = require('mongo-sanitize');
const fetch = require('node-fetch');


// Global Variables
var availableChannels = [];
var availablePlaylists = [];

const generateAvailableChannels = async () => {
  availableChannels = await db.Channel
    .find({ status: 3 })
    .select('name url image status')
    .populate({ path: 'department', select: 'name status' })
    .sort({ order: 1, createdAt: -1 });
  availableChannels = availableChannels.filter(d => d.department && d.department.status == 1);
};
const generateAvailablePlaylists = async () => {
  if(!availableChannels.length) {
    availablePlaylists = [];
  } else {
    availablePlaylists = await db.Playlist
      .find({ status: 3, channel: { $in: availableChannels } })
      .sort({ order: 1, createdAt: -1 });
  }
};


// Verify
exports.verifySignIn = async (req, res) => {
  try {
    if(!req.user) {
      return res.status(401).send({ result: false });
    }
    return res.status(200).send({ result: true });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Traffic
exports.trafficCreate = async (req, res) => {
  try {
    const { ip, url } = req.body;

    await fetch(`${config.externalApiUrl}api/auth/traffic-create`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        app_id: config.externalApiId,
        external_app_id: config.externalAppId,
        ip: ip? ip: '127.0.0.1',
        url: url? url: 'https://project14plus.ipst.ac.th/'
      }),
    });

    return res.status(200).send(true);
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Banner
exports.bannerList = async (req, res) => {
  try {
    await generateAvailableChannels();
    await generateAvailablePlaylists();
    if(!availablePlaylists.length) {
      return res.status(200).send({ result: [] });
    } else {
      var contents;
      if(!req.user) {
        contents = await db.Content
          .find({ 
            status: 3, approvedStatus: 1,
            playlist: { $in: availablePlaylists }
          })
          .select('_id name url image visitCount tags createdAt')
          .populate({ path: 'user', select: 'firstname lastname username avatar' })
          .populate({ path: 'subject', select: 'name url image' })
          .populate({ path: 'levels', select: 'name url image' })
          .sort({ isFeatured: -1, createdAt: -1 }).limit(7);
      } else {
        const userActions = await db.UserAction
          .find({ user: req.user }).select('_id')
          .populate({ path: 'content', select: '_id' });
        contents = await db.Content
          .find({ 
            status: 3, approvedStatus: 1, 
            _id: { $nin: userActions.map(d => d.content._id) },
            playlist: { $in: availablePlaylists }
          })
          .select('_id name url image visitCount tags createdAt')
          .populate({ path: 'user', select: 'firstname lastname username avatar' })
          .populate({ path: 'subject', select: 'name url image' })
          .populate({ path: 'levels', select: 'name url image' })
          .sort({ isFeatured: -1, createdAt: -1 }).limit(7);
        if(contents.length < 7) {
          const moreContents = await db.Content
            .find({ 
              status: 3, approvedStatus: 1, 
              _id: { $nin: contents.map(d => d._id) },
              playlist: { $in: availablePlaylists }
            })
            .select('_id name url image visitCount tags createdAt')
            .populate({ path: 'user', select: 'firstname lastname username avatar' })
            .populate({ path: 'subject', select: 'name url image' })
            .populate({ path: 'levels', select: 'name url image' })
            .sort({ isFeatured: -1, createdAt: -1 }).limit(7 - contents.length);
          contents = contents.concat(moreContents);
        }
      }
      return res.status(200).send({ result: contents });
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Content
exports.continuedContentList = async (req, res) => {
  try {
    if(!req.user) {
      return res.status(200).send({ result: [] })
    } else {
      await generateAvailableChannels();
      await generateAvailablePlaylists();
      if(!availablePlaylists.length) {
        return res.status(200).send({ result: [] });
      } else {
        const userActions = await db.UserAction
          .find({ user: req.user, status: 1 }).select('_id watchingSeconds')
          .populate({ 
            path: 'content', select: '_id name url image visitCount tags createdAt',
            populate: [
              { path: 'user', select: 'firstname lastname username avatar' },
              { path: 'subject', select: 'name url image' },
              { path: 'levels', select: 'name url image' }
            ]
          })
          .sort({ updatedAt: -1 }).limit(10);
        return res.status(200).send({ result: userActions });
      }
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.suggestedContentList = async (req, res) => {
  try {
    await generateAvailableChannels();
    await generateAvailablePlaylists();
    if(!availablePlaylists.length) {
      return res.status(200).send({ result: [] });
    } else {
      var contents;
      if(!req.user) {
        contents = await db.Content
          .find({ 
            status: 3, approvedStatus: 1,
            playlist: { $in: availablePlaylists }
          })
          .select('_id name url image visitCount tags createdAt')
          .populate({ path: 'user', select: 'firstname lastname username avatar' })
          .populate({ path: 'subject', select: 'name url image' })
          .populate({ path: 'levels', select: 'name url image' })
          .sort({ createdAt: -1 }).limit(10);
        return res.status(200).send({ result: contents });
      } else {
        const userActions = await db.UserAction
          .find({ user: req.user }).select('_id')
          .populate({ path: 'content', select: '_id' });
        const totalUsers = await db.User.countDocuments({});
        contents = await db.Content
          .find({
            status: 3, approvedStatus: 1,
            playlist: { $in: availablePlaylists }
          })
          .select('_id name url image visitCount tags createdAt')
          .populate({ path: 'user', select: 'firstname lastname username avatar' })
          .populate({ path: 'subject', select: 'name url image' })
          .populate({ path: 'levels', select: 'name url image' })
          .exec(function(err, instances) {
            let sorted = suggestedSort(
              instances, req.user, 
              userActions.map(d => `${d.content._id}`), totalUsers
            );
            return res.status(200).send({ result: sorted.slice(0, 10) });
          });
      }
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.newestContentList = async (req, res) => {
  try {
    await generateAvailableChannels();
    await generateAvailablePlaylists();
    if(!availablePlaylists.length) {
      return res.status(200).send({ result: [] });
    } else {
      const contents = await db.Content
        .find({ 
          status: 3, approvedStatus: 1,
          playlist: { $in: availablePlaylists }
        })
        .select('_id name url image visitCount tags createdAt')
        .populate({ path: 'user', select: 'firstname lastname username avatar' })
        .populate({ path: 'subject', select: 'name url image' })
        .populate({ path: 'levels', select: 'name url image' })
        .sort({ createdAt: -1 }).limit(10);
      return res.status(200).send({ result: contents });
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.popularContentList = async (req, res) => {
  try {
    await generateAvailableChannels();
    await generateAvailablePlaylists();
    if(!availablePlaylists.length) {
      return res.status(200).send({ result: [] });
    } else {
      const contents = await db.Content
        .find({ 
          status: 3, approvedStatus: 1,
          playlist: { $in: availablePlaylists }
        })
        .select('_id name url image visitCount tags createdAt')
        .populate({ path: 'user', select: 'firstname lastname username avatar' })
        .populate({ path: 'subject', select: 'name url image' })
        .populate({ path: 'levels', select: 'name url image' })
        .sort({ visitCount: -1, createdAt: -1 }).limit(10);
      return res.status(200).send({ result: contents });
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.contentList = async (req, res) => {
  try {
    var { playlistUrl, levelUrl, subjectUrl, sorting, keyword, page, pp } = req.query;
    page = parseInt(page);
    pp = parseInt(pp);

    await generateAvailableChannels();
    await generateAvailablePlaylists();
    if(!availablePlaylists.length) {
      return res.status(200).send({ 
        result: contents, 
        paginate: {
          page: page, pp: pp, total: 0,
          totalPages: Math.ceil(0 / pp)
        }
      });
    } else {
      var whereQuery = { 
        status: 3, approvedStatus: 1,
        playlist: { $in: availablePlaylists }
      };
      if(playlistUrl){
        const playlist = await db.Playlist.findOne({ url: playlistUrl });
        if(!playlistUrl) {
          return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
        }
        whereQuery.playlist = playlist;
      }
      if(levelUrl){
        const level = await db.Level.findOne({ url: levelUrl });
        if(!level) {
          return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
        }
        whereQuery.levels = { $in: level };
      }
      if(subjectUrl){
        const subject = await db.Subject.findOne({ url: subjectUrl });
        if(!subject) {
          return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
        }
        whereQuery.subject = subject;
      }
      if(keyword){
        whereQuery['$or'] = [
          { name : new RegExp(keyword, 'i') },
          { description : new RegExp(keyword, 'i') }
        ];
      }
  
      var sortingQuery = { visitCount: -1, createdAt: -1 };
      if(playlistUrl){
        sortingQuery = { order: 1, createdAt: -1 }
      }else if(sorting){
        if(sorting == 1) sortingQuery = { createdAt: -1 };
        else if(sorting == 2) sortingQuery = { createdAt: 1 };
        else if(sorting == 3) sortingQuery = { visitCount: -1, createdAt: -1 };
        else if(sorting == 4) sortingQuery = { name: 1, createdAt: -1 };
        else if(sorting == 5) sortingQuery = { name: -1, createdAt: -1 };
      }
      
      const contents = await db.Content.find(whereQuery)
        .select('_id name url image visitCount tags createdAt')
        .populate({ path: 'user', select: 'firstname lastname username avatar' })
        .populate({ path: 'subject', select: 'name url image' })
        .populate({ path: 'levels', select: 'name url image' })
        .sort(sortingQuery)
        .skip((page - 1) * pp).limit(pp);
      const total = await db.Content.countDocuments(whereQuery);
      return res.status(200).send({ 
        result: contents, 
        paginate: {
          page: page, pp: pp, total: total,
          totalPages: Math.ceil(total / pp)
        }
      });
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentRead = async (req, res) => {
  try {
    const { url } = req.query;
    var whereQuery = { url: url, status: 3, approvedStatus: 1 };
    const content = await db.Content.findOne(whereQuery)
      .populate({ path: 'user', select: 'firstname lastname username avatar' })
      .populate({ path: 'subject', select: 'name url image' })
      .populate({ path: 'levels', select: 'name url image' })
      .populate({ path: 'tags', select: 'name status', match: { status: 1 } })
      .populate({ 
        path: 'playlist', select: 'name url status',
        populate: {
          path: 'channel', select: 'name status'
        }
      });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบเนื้อหาในระบบ'});
    } else if(!content.playlist || content.playlist.status != 3) {
      return res.status(401).send({message: 'ไม่พบเนื้อหาในระบบ'});
    } else if(!content.playlist.channel || content.playlist.channel.status != 3) {
      return res.status(401).send({message: 'ไม่พบเนื้อหาในระบบ'});
    }

    var userAction = await db.UserAction.findOne({ user: req.user, content: content })
      .populate({ path: 'user', select: 'firstname lastname username avatar' })
      .populate({ path: 'content' });
    if(!userAction){
      await db.UserAction({
        user: req.user,
        content: content
      }).save();
      userAction = await db.UserAction.findOne({ user: req.userId, content: content })
        .populate({ path: 'user', select: 'firstname lastname username avatar' })
        .populate({ path: 'content' });
    }
        
    content.visitCount = content.visitCount + 1;
    content.save();

    return res.status(200).send({ result: content, userAction: userAction });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.relatedContentList = async (req, res) => {
  try {
    await generateAvailableChannels();
    await generateAvailablePlaylists();
    if(!availablePlaylists.length) {
      return res.status(200).send({ result: [] });
    } else {
      const { contentId } = req.query;
      const content = await db.Content.findById(sanitize(contentId));
      const userActions = await db.UserAction
        .find({ user: req.user }).select('_id')
        .populate({ path: 'content', select: '_id' });
      const totalUsers = await db.User.countDocuments({});

      await db.Content
        .find({ 
          status: 3, approvedStatus: 1,
          playlist: { $in: availablePlaylists },
          _id: { $ne: sanitize(contentId) }
        })
        .select('_id name url image visitCount tags createdAt')
        .populate({ path: 'user', select: 'firstname lastname username avatar' })
        .populate({ path: 'subject', select: 'name url image' })
        .populate({ path: 'levels', select: 'name url image' })
        .exec(function(err, instances) {
          let sorted = relatedSort(
            instances, content, req.user, 
            userActions.map(d => `${d.content._id}`), totalUsers
          );
          return res.status(200).send({ result: sorted.slice(0, 10) });
        });
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// User Action
exports.userActionEdit = async (req, res) => {
  try {
    const { id, watchingSeconds, status } = req.body;

    var userAction = await db.UserAction.findOne({ _id: sanitize(id), user: req.user });
    if(!userAction) {
      return res.status(401).send({message: 'ไม่พบการกระทำของผู้ใช้ในระบบ'});
    }

    userAction.watchingSeconds = watchingSeconds;
    userAction.status = Math.max(userAction.status, status);
    userAction.save();

    return res.status(200).send({
      message: 'แก้ไขการกระทำของผู้ใช้สำเร็จ',
      result: userAction
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.userActionResultEdit = async (req, res) => {
  try {
    var { id, scoreType, questions, score, maxScore, minScore } = req.body;
    scoreType = parseInt(scoreType);
    score = parseInt(score);
    maxScore = parseInt(maxScore);

    var userAction = await db.UserAction.findOne({ _id: sanitize(id), user: req.user });
    if(!userAction) {
      return res.status(401).send({message: 'ไม่พบการกระทำของผู้ใช้ในระบบ'});
    }

    userAction.scoreType = scoreType;
    userAction.questions = questions;
    userAction.minScore = minScore;
    if(scoreType == 0 || scoreType == 2) {
      userAction.score = score;
      userAction.maxScore = maxScore;
    } else if(scoreType == 1) {
      if(userAction.maxScore != maxScore) {
        userAction.score = score;
        userAction.maxScore = maxScore;
      } else if(score > userAction.score) {
        userAction.score = score;
      }
    }
    userAction.status = 3;
    userAction.save();

    return res.status(200).send({
      message: 'แก้ไขการกระทำของผู้ใช้สำเร็จ',
      result: userAction
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Level
exports.levelList = async (req, res) => {
  try {
    var whereQuery = { status: 1 };
    const levels = await db.Level.find(whereQuery).sort({ order: 1 });
    return res.status(200).send({ result: levels });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.levelRead = async (req, res) => {
  try {
    const { url } = req.query;
    const level = await db.Level.findOne({ url: url });
    if(!level) {
      return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
    }
    return res.status(200).send({ result: level });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

// Subject
exports.subjectList = async (req, res) => {
  try {
    var whereQuery = { status: 1 };
    const subjects = await db.Subject.find(whereQuery).sort({ order: 1 });
    return res.status(200).send({ result: subjects });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

// Tag
exports.tagList = async (req, res) => {
  try {
    var whereQuery = { status: 1 };
    const tags = await db.Tag.find(whereQuery).sort({ order: 1 });
    return res.status(200).send({ result: tags });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Channel
exports.channelList = async (req, res) => {
  try {
    const activeDepartments = await db.Department.find({ status: 1 });

    var { sorting, keyword, page, pp } = req.query;
    page = parseInt(page);
    pp = parseInt(pp);

    var whereQuery = { status: 3, department: { $in: activeDepartments } };
    if(keyword){
      whereQuery['$or'] = [
        { name : new RegExp(keyword, 'i') },
        { description : new RegExp(keyword, 'i') }
      ];
    }
    var sortingQuery = { order: 1, createdAt: -1 };
    if(sorting){
      if(sorting == 1) sortingQuery = { createdAt: -1 };
      else if(sorting == 2) sortingQuery = { createdAt: 1 };
      else if(sorting == 3) sortingQuery = { name: 1, createdAt: -1 };
      else if(sorting == 4) sortingQuery = { name: -1, createdAt: -1 };
    }
    
    const channels = await db.Channel.find(whereQuery)
      .select('_id name url description image createdAt')
      .populate({ path: 'department', select: 'name status' })
      .sort(sortingQuery)
      .skip((page - 1) * pp).limit(pp);
    const total = await db.Channel.countDocuments(whereQuery);
    return res.status(200).send({ 
      result: channels, 
      paginate: {
        page: page, pp: pp, total: total,
        totalPages: Math.ceil(total / pp)
      }
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.channelRead = async (req, res) => {
  try {
    const { url } = req.query;
    var whereQuery = { url: url, status: 3 };
    const channel = await db.Channel.findOne(whereQuery)
      .populate({ path: 'department', select: 'name status' });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบช่องในระบบ'});
    } else if(!channel.department || channel.department.status != 1) {
      return res.status(401).send({message: 'ไม่พบช่องในระบบ'});
    }

    return res.status(200).send({ result: channel });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

// Playlist
exports.playlistList = async (req, res) => {
  try {
    var { channelUrl, sorting, keyword, page, pp } = req.query;
    page = parseInt(page);
    pp = parseInt(pp);

    const channel = await db.Channel.findOne({ url: channelUrl })
      .populate({ path: 'department', select: 'name status' });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบช่องในระบบ'});
    } else if(!channel.department || channel.department.status != 1) {
      return res.status(401).send({message: 'ไม่พบช่องในระบบ'});
    }

    var whereQuery = { status: 3, channel: channel };
    if(keyword){
      whereQuery['$or'] = [
        { name : new RegExp(keyword, 'i') },
        { description : new RegExp(keyword, 'i') }
      ];
    }
    var sortingQuery = { order: 1, createdAt: -1 };
    if(sorting){
      if(sorting == 1) sortingQuery = { createdAt: -1 };
      else if(sorting == 2) sortingQuery = { createdAt: 1 };
      else if(sorting == 3) sortingQuery = { name: 1, createdAt: -1 };
      else if(sorting == 4) sortingQuery = { name: -1, createdAt: -1 };
    }
    
    const playlists = await db.Playlist.find(whereQuery)
      .select('_id name url description image createdAt')
      .sort(sortingQuery)
      .skip((page - 1) * pp).limit(pp);
    const total = await db.Playlist.countDocuments(whereQuery);

    const contents = await db.Content.aggregate([
      {
        $match: {
          playlist: { $in: playlists.map(d => sanitize(d._id)) },
          status: 3, approvedStatus: 1
        },
      }, {
        $sort: { order: 1, createdAt: -1 }
      }, {
        $group: {
          _id: '$playlist',
          contentUrl: { $first: '$url' }
        }
      }
    ]);
    const finalPlaylists = playlists.map(d => {
      let temp = contents.filter(k => k._id.toString() == d._id.toString());
      return {
        ...d._doc,
        firstContentUrl: temp.length? temp[0]['contentUrl']: null,
      };
    });

    return res.status(200).send({ 
      result: finalPlaylists, 
      paginate: {
        page: page, pp: pp, total: total,
        totalPages: Math.ceil(total / pp)
      }
    });
  } catch(err) {
    console.log(err)
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.playlistRead = async (req, res) => {
  try {
    const { url } = req.query;
    var whereQuery = { url: url, status: 3 };
    const playlist = await db.Playlist.findOne(whereQuery)
      .populate({ 
        path: 'channel', select: 'name status',
        populate: {
          path: 'department', select: 'name status'
        }
      });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    } else if(!playlist.channel || playlist.channel.status != 3) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    } else if(!playlist.channel.department || playlist.channel.department.status != 1) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }

    return res.status(200).send({ result: playlist });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Algorithm Functions
function suggestedSort(array, user, watchedIds, totalUsers) {
  array.sort(function (a, b) {
    try{
      let scoreA = 2 * (a.levels.map(d => `${d._id}`).indexOf(`${user.level}`) > -1? 1: 0) 
        + 1 * (user.subjects && user.subjects.length && user.subjects.indexOf(a.subject._id) > -1? 1: 0) 
        + 1 * a.tags.map(t => user.tags.indexOf(t) > -1? 1: 0).reduce((a, b) => a + b, 0) 
          / Math.max(1, a.tags.length) 
        - 5 * (watchedIds.indexOf(`${a._id}`) > -1? 1: 0) 
        + 0 * a.visitCount / totalUsers;
      let scoreB = 2 * (b.levels.map(d => `${d._id}`).indexOf(`${user.level}`) > -1? 1: 0) 
        + 1 * (user.subjects && user.subjects.length && user.subjects.indexOf(b.subject._id) > -1? 1: 0) 
        + 1 * b.tags.map(t => user.tags.indexOf(t) > -1? 1: 0).reduce((a, b) => a + b, 0) 
          / Math.max(1, b.tags.length) 
        - 5 * (watchedIds.indexOf(`${b._id}`) > -1? 1: 0) 
        + 0 * b.visitCount / totalUsers;

      if(scoreA > scoreB) return -1;
      else if(scoreA < scoreB) return 1;
      else return 0;
    } catch(err) {
      return 0;
    }
  });
  return array;
}
function relatedSort(array, content, user, watchedIds, totalUsers) {
  array.sort(function (a, b) {
    try{
      let scoreA = 2 * (a.levels.map(d => `${d._id}`).indexOf(`${user.level}`) > -1? 1: 0) 
        + 1 * (user.subjects && user.subjects.length && user.subjects.indexOf(a.subject._id) > -1? 1: 0) 
        + 1 * a.tags.map(t => user.tags.indexOf(t) > -1? 1: 0).reduce((a, b) => a + b, 0) 
          / Math.max(1, a.tags.length) 
        - 5 * (watchedIds.indexOf(`${a._id}`) > -1? 1: 0) 
        + 0 * a.visitCount / totalUsers 
        + 1 * a.levels.map(t => content.levels.indexOf(t) > -1? 1: 0).reduce((a, b) => a + b, 0) 
          / Math.max(1, a.levels.length) 
        + 1 * (a.subject._id === content.subject? 1: 0);
      let scoreB = 2 * (b.levels.map(d => `${d._id}`).indexOf(`${user.level}`) > -1? 1: 0) 
        + 1 * (user.subjects && user.subjects.length && user.subjects.indexOf(b.subject._id) > -1? 1: 0) 
        + 1 * b.tags.map(t => user.tags.indexOf(t) > -1? 1: 0).reduce((a, b) => a + b, 0) 
          / Math.max(1, b.tags.length) 
        - 5 * (watchedIds.indexOf(`${b._id}`) > -1? 1: 0) 
        + 0 * b.visitCount / totalUsers 
        + 1 * b.levels.map(t => content.levels.indexOf(t) > -1? 1: 0).reduce((a, b) => a + b, 0) 
          / Math.max(1, b.levels.length) 
        + 1 * (b.subject._id === content.subject? 1: 0);

      if(scoreA > scoreB) return -1;
      else if(scoreA < scoreB) return 1;
      else return 0;
    } catch(err) {
      return 0;
    }
  });
  return array;
}
