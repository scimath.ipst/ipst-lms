const config = require('../config');
const db = require('../models');
const sanitize = require('mongo-sanitize');
const fetch = require('node-fetch');


// Traffic
exports.trafficCreate = async (req, res) => {
  try {
    const { ip, url } = req.body;

    await fetch(`${config.externalApiUrl}api/auth/traffic-create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      },
      body: JSON.stringify({
        app_id: config.externalApiId,
        external_app_id: config.externalAppId,
        ip: ip? ip: '127.0.0.1',
        url: url? url: 'https://project14plus.ipst.ac.th/'
      }),
    });

    return res.status(200).send(true);
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Profile
exports.profileEdit = async (req, res) => {
  try {
    var department;
    if(req.body.deptId) {
      department = await db.Department.findById(sanitize(req.body.deptId));
      if(!department) {
        return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
      }
    }
    
    const user = await db.User.findOneAndUpdate({ _id: sanitize(req.user._id) }, {
      department: department? department: null,
      username: req.body.username,
      firstname: req.body.firstname,
      lastname: req.body.lastname
    });
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    // Update external user
    const fetch1 = await fetch(`${config.externalApiUrl}api/user/update`, {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      },
      body: JSON.stringify({
        id: user.externalId,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        profile: req.body.avatar,
        app_id: config.externalApiId,
        external_app_id: config.externalAppId,
        ip: req.body.ip,
        url: req.body.url
      }),
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการแก้ไขผู้ใช้',
        errors: data1.messages
      });
    }

    return res.status(200).send({
      message: 'แก้ไขบัญชีผู้ใช้สำเร็จ',
      result: {
        department: department,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        username: req.body.username,
        email: req.body.email,
        avatar: req.body.avatar
      }
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.passwordEdit = async (req, res) => {
  try {
    const fetch2 = await fetch(`${config.externalApiUrl}api/user/update-password`, {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      },
      body: JSON.stringify({
        id: req.user.externalId,
        password: req.body.password,
        new_password: req.body.newPassword,
        new_password_confirm: req.body.confirmPassword,
        app_id: config.externalApiId,
        external_app_id: config.externalAppId,
        ip: req.body.ip,
        url: req.body.url
      }),
    });
    const data2 = await fetch2.json();
    if(!data2 || data2.status != 200) {
      return res.status(401).send({
        message: 'เกิดข้อผิดพลาดในการแก้ไขรหัสผ่าน',
        errors: data2.messages
      });
    }

    return res.status(200).send({message: 'แก้ไขรหัสผ่านสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.interestEdit = async (req, res) => {
  try {
    var level;
    if(req.body.levelId) {
      level = await db.Level.findById(sanitize(req.body.levelId));
      if(!level) {
        return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
      }
    }
    var subjectIDs = [];
    if(req.body.subjects) subjectIDs = req.body.subjects.map(d => d._id);
    const subjects = await db.Subject.find({ _id: { $in: subjectIDs } });
    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    const user = await db.User.findOneAndUpdate({ _id: sanitize(req.user._id) }, {
      level: level? level: null,
      subjects: subjects,
      tags: tags
    });
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    return res.status(200).send({
      message: 'แก้ไขบัญชีผู้ใช้สำเร็จ',
      result: {
        level: level,
        subjects: subjects,
        tags: tags
      }
    });
  } catch(err) {
    console.log(err)
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.historyList = async (req, res) => {
  try {
    var { page, pp } = req.query;

    if(page !== undefined && pp !== undefined){
      page = parseInt(page);
      pp = parseInt(pp);
  
      const userActions = await db.UserAction.find({ user: req.user })
        .populate({ path: 'content' })
        .sort({ updatedAt: -1 })
        .skip((page - 1) * pp).limit(pp);
  
      const total = await db.UserAction.countDocuments({ user: req.user });
      return res.status(200).send({
        result: userActions, 
        paginate: {
        page: page, pp: pp, total: total,
        totalPages: Math.ceil(total / pp)
      }});
    }else{
      const userActions = await db.UserAction.find({ user: req.user })
        .populate({ path: 'content' })
        .sort({ updatedAt: -1 });
      return res.status(200).send({ result: userActions });
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// User
exports.userList = async (req, res) => {
  try {
    const {page, pp, keyword} = req.query;
    const fetch1 = await fetch(`${config.externalApiUrl}api/user/user-list?page=${page}&pp=${pp}&keyword=${ keyword }`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${req.externalJWT}`
      }
    });
    const data1 = await fetch1.json();
    if(!data1 || data1.status != 200) {
      return res.status(401).send({message: 'External server error.'});
    }

    const paginate = {
      page: Number(page), pp: Number(pp),
      total: Number(data1.data.total),
      totalPages: Number(data1.data.total_pages)
    };
    const externalUsers = data1.data.result;
    const userIds = externalUsers.map(d => Number(d.id));
    const users = await db.User.find({ externalId: { $in: userIds} })
      .populate('role').populate('department');
    const result = externalUsers.map(user => {
      let temp = users.filter(d => d.externalId == user.id);
      if(temp.length) temp = temp[0];
      else temp = null;
      return {
        _id: temp? temp._id: null,
        externalId: user.id,
        role: temp? temp.role: null,
        department: temp? temp.department: null,
        firstname: user.firstname,
        lastname: user.lastname,
        username: user.username,
        email: user.email,
        avatar: user.profile,
        status: temp? temp.status: 0
      };
    });

    return res.status(200).send({ paginate: paginate, result: result });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Tag
exports.tagList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = { status: 1 };
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const tags = await db.Tag.find(where).sort({'name': 1});
    
    return res.status(200).send({ result: tags });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
// Level
exports.levelList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = { status: 1 };
    if(keyword) {
      where['$or'] = [
        { name : new RegExp(keyword, 'i') },
        { url : new RegExp(keyword, 'i') }
      ];
    }

    const levels = await db.Level.find(where).sort({'order': 1});
    
    return res.status(200).send({ result: levels });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
// Subject
exports.subjectList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = { status: 1 };
    if(keyword) {
      where['$or'] = [
        { name : new RegExp(keyword, 'i') },
        { url : new RegExp(keyword, 'i') }
      ];
    }

    const subjects = await db.Subject.find(where).sort({'order': 1});
    
    return res.status(200).send({ result: subjects });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Badge
exports.badgeList = async (req, res) => {
  try {
    const { userId } = req.query;
    
    const user = await db.User.findById(sanitize(userId));
    if(!user) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    var badges = await db.UserAction
      .find({ user: user, status: 3 })
      .populate({ path: 'content', select: 'badge badgeName' })
      .sort({ updatedAt: -1 });
    badges = badges.filter(d => d.minScore != undefined && d.score >= d.minScore);
    
    return res.status(200).send({ result: badges });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
