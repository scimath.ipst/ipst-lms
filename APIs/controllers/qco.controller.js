const db = require('../models');
const sanitize = require('mongo-sanitize');
const { firebaseDb, sendPushNotification } = require('../config/firebase-service');
const { doc, getDoc, setDoc, updateDoc , collection , addDoc  } = require('firebase/firestore');

// Dashboard
exports.dashboardCountContents = async (req, res) => {
  try {
    const unlisted = await db.Content.countDocuments({ status: 0 });
    const drafted = await db.Content.countDocuments({ status: 1 });
    const private = await db.Content.countDocuments({ status: 2 });
    const public = await db.Content.countDocuments({ status: 3 });
    return res.status(200).send({ 
      result: { unlisted: unlisted, drafted: drafted, private: private, public: public }
    });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.dashboardMostVisitedContents = async (req, res) => {
  try {
    const contents = await db.Content.find()
      .select('name url image approvedStatus visitCount status')
      .populate({ 
        path: 'playlist', select: 'name',
        populate: { 
          path: 'channel', select: 'name',
          populate: { path: 'department', select: 'name' }
        }
      })
      .sort({ visitCount: -1 })
      .limit(5);
    return res.status(200).send({ result: contents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.dashboardDepartments = async (req, res) => {
  try {
    const departments = await db.Department.find()
      .sort({ name: 1 })
      .limit(5);
    return res.status(200).send({ result: departments });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Channel
exports.channelList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = {};
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const channels = await db.Channel.find(where).sort({'name': 1})
      .populate('department');

    return res.status(200).send({ result: channels });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.channelRead = async (req, res) => {
  try {
    const { channelId } = req.query;
    const channel = await db.Channel.findById(sanitize(channelId))
      .populate('department');
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    return res.status(200).send({ result: channel });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.channelAdd = async (req, res) => {
  try {
    const department = await db.Department.findById(sanitize(req.body.deptId));
    if(!department) {
      return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
    }

    await db.Channel({
      department: department,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้าง Channel สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Channel ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.channelEdit = async (req, res) => {
  try {
    const department = await db.Department.findById(sanitize(req.body.deptId));
    if(!department) {
      return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
    }

    const channel = await db.Channel.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      department: department,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    });
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Channel สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Channel ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.channelDelete = async (req, res) => {
  try {
    const channel = await db.Channel.findById(sanitize(req.body.channelId));
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }
    channel.remove();
    return res.status(200).send({message: 'ลบ Channel สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Playlist
exports.playlistList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = {};
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const playlists = await db.Playlist.find(where).sort({'name': 1})
      .populate({ 
        path: 'channel', select: 'name',
        populate: { path: 'department', select: 'name' }
      });
        
    return res.status(200).send({ result: playlists });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.playlistRead = async (req, res) => {
  try {
    const { playlistId } = req.query;
    const playlist = await db.Playlist.findById(sanitize(playlistId))
      .populate('channel');
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    return res.status(200).send({ result: playlist });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.playlistAdd = async (req, res) => {
  try {
    const channel = await db.Channel.findById(sanitize(req.body.channelId));
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }

    await db.Playlist({
      channel: channel,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้าง Playlist สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Playlist ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.playlistEdit = async (req, res) => {
  try {
    const channel = await db.Channel.findById(sanitize(req.body.channelId));
    if(!channel) {
      return res.status(401).send({message: 'ไม่พบ Channel ในระบบ'});
    }

    const playlist = await db.Playlist.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      channel: channel,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    });
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Playlist สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Playlist ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.playlistDelete = async (req, res) => {
  try {
    const playlist = await db.Playlist.findById(sanitize(req.body.playlistId));
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    playlist.remove();
    return res.status(200).send({message: 'ลบ Playlist สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Content
exports.contentList = async (req, res) => {
  try {
    const { keyword, status, approvedStatus } = req.query;

    var where = {};
    if(keyword) where['name'] = new RegExp(keyword, 'i');
    if(status != undefined && status != '') where['status'] = status;
    if(approvedStatus != undefined && approvedStatus != '') where['approvedStatus'] = approvedStatus;

    const contents = await db.Content.find(where)
      .select('user name url image type approvedStatus order status visitCount isFeatured')
      .populate({ path: 'user', select: 'username firstname lastname' })
      .populate({ 
        path: 'playlist', select: 'name',
        populate: { 
          path: 'channel', select: 'name',
          populate: { path: 'department', select: 'name' }
        }
      })
      .sort({'isFeatured': -1, 'createdAt': -1});

    return res.status(200).send({ result: contents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentRead = async (req, res) => {
  try {
    const { contentId } = req.query;
    const content = await db.Content.findById(sanitize(contentId))
      .populate({ path: 'user' })
      .populate({ 
        path: 'playlist',
        populate: { 
          path: 'channel',
          populate: { path: 'department' }
        }
      })
      .populate({ path: 'levels', select: 'name' })
      .populate({ path: 'subject', select: 'name' })
      .populate({ path: 'tags', select: 'name' });
    return res.status(200).send({ result: content });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.contentAdd = async (req, res) => {
  try {
    const playlist = await db.Playlist.findById(sanitize(req.body.playlistId));
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    
    var levelIDs = [];
    if(req.body.levels) levelIDs = req.body.levels.map(d => d._id);
    const levels = await db.Level.find({ _id: { $in: levelIDs } });

    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    var user = req.user;
    if(req.body.userId){
      var temp = await db.User.findById(sanitize(req.body.userId));
      if(temp) user = temp;
    }

    const content = await db.Content({
      user: user,
      playlist: playlist,
      levels: levels,
      subject: subject,
      tags: tags,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      badge: req.body.badge,
      badgeName: req.body.badgeName,
      type: req.body.type,
      youtubeVideoId: req.body.youtubeVideoId,
      filePath: req.body.filePath,
      videoQuestions: req.body.videoQuestions,
      scoreType: req.body.scoreType,
      questions: req.body.questions,
      approver: req.user,
      approvedStatus: 1,
      order: req.body.order,
      status: req.body.status,
      isFeatured: req.body.isFeatured,
      minScore: req.body.minScore
    }).save();
    if (!content) {
      return res.status(401).send({message: 'สร้าง Content ไม่สำเร็จ'});
    }

    await db.ApprovedContent({
      approver: req.user,
      content: content,
      approvedStatus: 1,
      comment: 'สร้างโดย Admin or Quality Control Officer'
    }).save();

    var orConditions = [];
    if(tagIDs.length) orConditions.push({tags: {$in: tagIDs}});
    if(levelIDs.length) orConditions.push({level : {$in : levelIDs}});
    if(subject) orConditions.push({subjects: {$in: subject}});
    var users = await db.User.find({ $or:orConditions, fcmToken: { $ne: '' }}).select('fcmToken');

    const fcmTokens = users.map(d => d.fcmToken).filter(d => !!d);
    const userIds = users.map(d => d).filter(d => !!d.fcmToken);

    console.log(fcmTokens);
    console.log(userIds);

    if(fcmTokens.length){
      var title = 'มีเนื้อหาใหม่สำหรับคุณ';
      var message = req.body.name;

      sendPushNotification(fcmTokens, title, message);

      let sentTime = new Date().toISOString();

      var fbData = {
        title : title,
        body : req.body.name,
        id : content.id ,
        url : req.body.url,
        isRead : false,
        sentTime : sentTime,
      };

      userIds.forEach(async e => {
        addDoc(collection(
          firebaseDb, 'user_notifications',
          `${e._id}`, 'data'
        ), fbData);      
      });
      
    }
    
    // const users = await db.User.find({ fcmToken: { $ne: '' } })
    //   .select('fcmToken');
    // const fcmTokens = users.map(d => d.fcmToken).filter(d => !!d);
    
    // if(fcmTokens.length){
    //   sendPushNotification(fcmTokens, 'มีเนื้อหาใหม่สำหรับคุณ', req.body.name);
    // }
    
    return res.status(200).send({message: 'สร้าง Content สำเร็จ'});
  } catch(err) {
    console.log(err)
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ชื่อลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้าง Content ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.contentEdit = async (req, res) => {
  try {
    const playlist = await db.Playlist.findById(sanitize(req.body.playlistId));
    if(!playlist) {
      return res.status(401).send({message: 'ไม่พบ Playlist ในระบบ'});
    }
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    
    var levelIDs = [];
    if(req.body.levels) levelIDs = req.body.levels.map(d => d._id);
    const levels = await db.Level.find({ _id: { $in: levelIDs } });

    var tagIDs = [];
    if(req.body.tags) tagIDs = req.body.tags.map(d => d._id);
    const tags = await db.Tag.find({ _id: { $in: tagIDs } });

    const content = await db.Content.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      playlist: playlist,
      levels: levels,
      subject: subject,
      tags: tags,
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      badge: req.body.badge,
      badgeName: req.body.badgeName,
      type: req.body.type,
      youtubeVideoId: req.body.youtubeVideoId,
      filePath: req.body.filePath,
      videoQuestions: req.body.videoQuestions,
      scoreType: req.body.scoreType,
      questions: req.body.questions,
      order: req.body.order,
      status: req.body.status,
      isFeatured: req.body.isFeatured,
      minScore: req.body.minScore
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไข Content สำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('url') > -1) errors['url'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไข Content ได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.contentDelete = async (req, res) => {
  try {
    const content = await db.Content.findById(sanitize(req.body.contentId));
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }
    content.remove();

    await db.ApprovedContent.deleteMany({ content: content });
    await db.UserAction.deleteMany({ content: content });

    return res.status(200).send({message: 'ลบ Content สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};

exports.approvedContentList = async (req, res) => {
  try {
    const { contentId } = req.query;
    const content = await db.Content.findById(sanitize(contentId));
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }

    const approvedContents = await db.ApprovedContent.find({ content: content })
      .sort({'createdAt': 1})
      .populate({ path: 'approver', select: 'username firstname lastname' });

    return res.status(200).send({ result: approvedContents });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.approvedContentAdd = async (req, res) => {
  try {
    const content = await db.Content.findOneAndUpdate({ _id: sanitize(req.body.contentId) }, {
      approver: req.user,
      approvedStatus: req.body.approvedStatus
    });
    if(!content) {
      return res.status(401).send({message: 'ไม่พบ Content ในระบบ'});
    }

    await db.ApprovedContent({
      approver: req.user,
      content: content,
      approvedStatus: req.body.approvedStatus,
      comment: req.body.comment,
    }).save();

    return res.status(200).send({message: 'สร้าง Approved Content สำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Tag
exports.tagList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = {};
    if(keyword) where['name'] = new RegExp(keyword, 'i');

    const tags = await db.Tag.find(where).sort({'name': 1});
    
    return res.status(200).send({ result: tags });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.tagRead = async (req, res) => {
  try {
    const {tagId} = req.query;
    const tag = await db.Tag.findById(sanitize(tagId));
    if(!tag) {
      return res.status(401).send({message: 'ไม่พบแท็กในระบบ'});
    }
    return res.status(200).send({ result: tag });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.tagAdd = async (req, res) => {
  try {
    await db.Tag({
      name: req.body.name,
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้างแท็กสำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('name') > -1) errors['name'] = 'ชื่อแท็กซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้างแท็กได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.tagEdit = async (req, res) => {
  try {
    const tag = await db.Tag.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      name: req.body.name, 
      status: Number(req.body.status)
    });
    if(!tag) {
      return res.status(401).send({message: 'ไม่พบแท็กในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไขแท็กสำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('name') > -1) errors['name'] = 'ชื่อแท็กซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไขแท็กได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.tagDelete = async (req, res) => {
  try {
    const tag = await db.Tag.findById(sanitize(req.body.tagId));
    if(!tag) {
      return res.status(401).send({message: 'ไม่พบแท็กในระบบ'});
    }
    tag.remove();
    return res.status(200).send({message: 'ลบแท็กสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Level
exports.levelList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = {};
    if(keyword) {
      where['$or'] = [
        { name : new RegExp(keyword, 'i') },
        { url : new RegExp(keyword, 'i') }
      ];
    }

    const levels = await db.Level.find(where).sort({'order': 1});
    
    return res.status(200).send({ result: levels });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.levelRead = async (req, res) => {
  try {
    const {levelId} = req.query;
    const level = await db.Level.findById(sanitize(levelId));
    if(!level) {
      return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
    }
    return res.status(200).send({ result: level });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.levelAdd = async (req, res) => {
  try {
    await db.Level({
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้างระดับชั้นสำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('name') > -1) errors['name'] = 'ชื่อระดับชั้นซ้ำ';
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้างระดับชั้นได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.levelEdit = async (req, res) => {
  try {
    const level = await db.Level.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    });
    if(!level) {
      return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไขระดับชั้นสำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('name') > -1) errors['name'] = 'ชื่อระดับชั้นซ้ำ';
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไขระดับชั้นได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.levelDelete = async (req, res) => {
  try {
    const level = await db.Level.findById(sanitize(req.body.levelId));
    if(!level) {
      return res.status(401).send({message: 'ไม่พบระดับชั้นในระบบ'});
    }
    level.remove();
    return res.status(200).send({message: 'ลบระดับชั้นสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Subject
exports.subjectList = async (req, res) => {
  try {
    const { keyword } = req.query;
    
    var where = {};
    if(keyword) {
      where['$or'] = [
        { name : new RegExp(keyword, 'i') },
        { url : new RegExp(keyword, 'i') }
      ];
    }

    const subjects = await db.Subject.find(where).sort({'order': 1});
    
    return res.status(200).send({ result: subjects });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.subjectRead = async (req, res) => {
  try {
    const {subjectId} = req.query;
    const subject = await db.Subject.findById(sanitize(subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    return res.status(200).send({ result: subject });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.subjectAdd = async (req, res) => {
  try {
    await db.Subject({
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    }).save();
    return res.status(200).send({message: 'สร้างวิชาสำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('name') > -1) errors['name'] = 'ชื่อวิชาซ้ำ';
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถสร้างวิชาได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.subjectEdit = async (req, res) => {
  try {
    const subject = await db.Subject.findOneAndUpdate({ _id: sanitize(req.body._id) }, {
      name: req.body.name,
      url: req.body.url,
      description: req.body.description,
      metadesc: req.body.metadesc,
      keywords: req.body.keywords,
      image: req.body.image,
      order: Number(req.body.order),
      status: Number(req.body.status)
    });
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    return res.status(200).send({message: 'แก้ไขวิชาสำเร็จ'});
  } catch(err) {
    if(err.code == 11000) {
      let keys = Object.keys(err.keyPattern);
      let errors = {};
      if(keys.indexOf('name') > -1) errors['name'] = 'ชื่อวิชาซ้ำ';
      if(keys.indexOf('url') > -1) errors['name'] = 'ลิงค์ซ้ำ';
      return res.status(500).send({message: 'ไม่สามารถแก้ไขวิชาได้', errors: errors});
    } else {
      return res.status(500).send({message: 'Internal server error.'});
    }
  }
};
exports.subjectDelete = async (req, res) => {
  try {
    const subject = await db.Subject.findById(sanitize(req.body.subjectId));
    if(!subject) {
      return res.status(401).send({message: 'ไม่พบวิชาในระบบ'});
    }
    subject.remove();
    return res.status(200).send({message: 'ลบวิชาสำเร็จ'});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Department
exports.departmentList = async (req, res) => {
  try {
    const { keyword } = req.query;
    var departments;
    if(!keyword) {
      departments = await db.Department.aggregate([
        {
          $lookup: {
            from: 'users',
            localField: '_id',
            foreignField: 'department',
            as: 'members'
          }
        }, 
        {
          $project: {
            _id: 1, status: 1, name: 1,
            totalMembers: { 
              $cond: { if: { $isArray: '$members' }, then: { $size: '$members' }, else: 0 } 
            }
          }
        }
      ]).sort({'name': 1});
    } else {
      departments = await db.Department.aggregate([
        { $match: { name : new RegExp(keyword, 'i') } },
        {
          $lookup: {
            from: 'users',
            localField: '_id',
            foreignField: 'department',
            as: 'members'
          }
        }, 
        {
          $project: {
            _id: 1, status: 1, name: 1,
            totalMembers: { 
              $cond: { if: { $isArray: '$members' }, then: { $size: '$members' }, else: 0 } 
            }
          }
        }
      ]).sort({'name': 1});
    }
    return res.status(200).send({ result: departments });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
exports.departmentRead = async (req, res) => {
  try {
    const {deptId} = req.query;
    const department = await db.Department.findById(sanitize(deptId));
    if(!department) {
      return res.status(401).send({message: 'ไม่พบหน่วยงานในระบบ'});
    }
    
    const members = await db.User.find({department: sanitize(deptId)})
      .populate('role');

    return res.status(200).send({ result: department, members: members });
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
