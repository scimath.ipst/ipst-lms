const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    0 = Visited
    1 = Watching
    2 = Watched
    3 = Completed
  scoreType :
    0 = ไม่เก็บคะแนน
    1 = เก็บคะแนนสูงสุด
    2 = เก็บคะแนนครั้งล่าสุด
*/

const UserAction = mongoose.model(
  'user_action',
  new mongoose.Schema({
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    content: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'content'
    },
    watchingSeconds: { type: Number, default: 0 },
    scoreType: { type: Number, default: 0 },
    questions: { type: Array, default: [] },
    score: { type: Number, default: 0 },
    maxScore: { type: Number, default: 0 },
    minScore: { type: Number, default: 0 },
    status: { type: Number, default: 0 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = UserAction;