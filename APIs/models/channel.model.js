const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    0 = Unlisted
    1 = Drafted
    2 = Private
    3 = Public
*/

const Channel = mongoose.model(
  'channel',
  new mongoose.Schema({
    department: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'department'
    },
    name: { type: String },
    url: { type: String, unique: true },
    description: { type: String, default: '' },
    metadesc: { type: String, default: '' },
    keywords: [ { type: String } ],
    image: { type: String, default: '' },
    order: { type: Number, default: 0 },
    status: { type: Number, default: 1 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = Channel;