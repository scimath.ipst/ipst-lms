const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  0 = User : ผู้ใช้
      สามารถเข้าใช้สื่อประเภทต่างๆ ที่ตนเองต้องการได้
  1 = Creator : ผู้สร้างสื่อ
      สาขาวิชาสามารถนำเข้า แก้ไข ลบ สื่อประเภทต่างๆ ของตนเองได้
      ซึ่งสามารถเลือกได้ว่าสื่อประเภทใดจะต้องผ่านการตรวจสอบโดยผู้ตรวจสอบคุณภาพ
  2 = Manager : ผู้บริหารจัดการสื่อ
      สาขาวิชาสามารถนำเข้า แก้ไข ลบ สื่อประเภทต่างๆ ของตนเองได้
      และสามารถบริหารจัดการสื่อภายใต้สาขาเดียวกันได้
  3 = Quality Control Officer : ผู้ตรวจสอบคุณภาพ
      สามารถตรวจสอบและให้ความเห็น รวมทั้งพิจารณาอนุมัติ/ไม่อนุมัติ สื่อประเภทต่างๆ ได้
  4 = Super Admin : ผู้ดูแลระบบ
      สาขาวิชาสามารถนำเข้า แก้ไข ลบ สื่อประเภทต่างๆ รวมถึงการตั้งค่าต่างๆ บนระบบได้ทั้งหมด
*/

const UserRole = mongoose.model(
  'user_role',
  new mongoose.Schema({
    name: { type: String },
    nameEng: { type: String },
    level: { type: Number, default: 0 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = UserRole;