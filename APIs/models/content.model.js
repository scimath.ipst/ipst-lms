const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    0 = Unlisted
    1 = Drafted
    2 = Private
    3 = Public
  type :
    1 = Youtube video : need to provide youtubeVideoId
    2 = Video MP4 : need to provide OneDrive/GoogleDrive Url
    3 = PDF : need to provide OneDrive/GoogleDrive Url
    4 = iFrame : need to provide filePath
  scoreType :
    0 = ไม่เก็บคะแนน
    1 = เก็บคะแนนสูงสุด
    2 = เก็บคะแนนครั้งล่าสุด
  approver : Map with Quality Control Office
  approvedStatus : Every content need to be approved first!
    -1 = Pending
    0  = Not approved
    1  = Approved
*/

const Content = mongoose.model(
  'content',
  new mongoose.Schema({
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    playlist: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'playlist'
    },

    levels: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'level'
    }],
    subject: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'subject'
    },
    tags: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'tag'
    }],

    name: { type: String },
    url: { type: String, unique: true },
    description: { type: String, default: '' },
    metadesc: { type: String, default: '' },
    keywords: [ { type: String } ],
    image: { type: String, default: '' },

    type: { type: Number },
    youtubeVideoId: { type: String, default: '' },
    filePath: { type: String, default: '' },

    videoQuestions: { type: Array, default: [] },
    
    scoreType: { type: Number, default: 0 },
    questions: { type: Array, default: [] },
    minScore: { type: Number, default: 0 },
    badge: { type: String, default: '' },
    badgeName: { type: String, default: '' },
    
    approver: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    approvedStatus: { type: Number, default: -1 },

    isFeatured: { type: Number, default: 0 },
    visitCount: { type: Number, default: 0 },
    order: { type: Number, default: 0 },
    status: { type: Number, default: 1 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = Content;