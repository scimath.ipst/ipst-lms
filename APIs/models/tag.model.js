const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    -1 = Suggested
    0 = Inactive
    1 = Active
*/

const Tag = mongoose.model(
  'tag',
  new mongoose.Schema({
    name: { type: String, unique: true },
    status: { type: Number, default: 0 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = Tag;