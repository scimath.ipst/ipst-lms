const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    0 = Inactive
    1 = Active
*/

const Subject = mongoose.model(
  'subject',
  new mongoose.Schema({
    name: { type: String, unique: true },
    url: { type: String, unique: true },
    description: { type: String, default: '' },
    metadesc: { type: String, default: '' },
    keywords: [ { type: String } ],
    image: { type: String, default: '' },
    order: { type: Number, default: 0 },
    status: { type: Number, default: 0 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = Subject;