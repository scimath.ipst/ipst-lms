const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;


db.UserRole = require('./user-role.model');
db.Department = require('./department.model');
db.User = require('./user.model');

db.Tag = require('./tag.model');

db.Level = require('./level.model');
db.Subject = require('./subject.model');

db.Channel = require('./channel.model');
db.Playlist = require('./playlist.model');
db.Content = require('./content.model');
db.ApprovedContent = require('./approved-content.model');

db.UserAction = require('./user-action.model');


module.exports = db;