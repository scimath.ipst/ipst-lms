const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    0 = Inactive
    1 = Active
*/

const User = mongoose.model(
  'user',
  new mongoose.Schema({
    externalId: { type: Number },
    role: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user_role'
    },
    department: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'department'
    },
    username: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    avatar: { type: String, default: '' },
    status: { type: Number, default: 0 },

    refreshToken: { type: String, default: '' },
    externalJWT: { type: String, default: '' },
    
    fcmToken: { type: String, default: '' },

    level: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'level'
    },
    subjects: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'subject'
    }],
    tags: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'tag'
    }]
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = User;