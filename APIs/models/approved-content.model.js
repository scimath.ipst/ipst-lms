const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  approver : Map with Quality Control Office
  approvedStatus :
    0 = Not approved
    1 = Approved
*/

const ApprovedContent = mongoose.model(
  'approved_content',
  new mongoose.Schema({
    approver: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    content: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'content'
    },
    approvedStatus: { type: Number, default: 0 },
    comment: { type: String, default: '' }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = ApprovedContent;