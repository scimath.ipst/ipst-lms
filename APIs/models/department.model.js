const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');

/*
  status :
    0 = Inactive
    1 = Active
*/

const Department = mongoose.model(
  'department',
  new mongoose.Schema({
    name: { type: String },
    status: { type: Number, default: 0 }
  }, { timestamps: true }).plugin(sanitizerPlugin)
);

module.exports = Department;