const jwt = require('jsonwebtoken');
const sanitize = require('mongo-sanitize');
const config = require('../config');
const db = require('../models');


// Verify JWT
verifyToken = async (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'].split(' ');
    if(authHeader.length != 2 || authHeader[0] != 'Bearer') {
      return res.status(403).send({message: 'No bearer token provided.'});
    }
    
    const token = authHeader[1];
    jwt.verify(token, config.tokenSecret, (err, decoded) => {
      if(err) {
        return res.status(500).send({message: 'Internal server error.'});
      }
      req.userId = decoded._id;
      req.externalJWT = decoded.externalJWT;
      next();
    })
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Check Permission
isMember = async (req, res, next) => {
  try {
    const user = await db.User.findById(req.userId)
      .populate('role')
      .populate('department');
    if(user.role && user.role.level >= 0) {
      req.user = user;
      return next();
    }
    return res.status(403).send({message: 'Required a member account.'});
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
isCreator = async (req, res, next) => {
  try {
    const user = await db.User.findById(req.userId)
      .populate('role')
      .populate('department');
    if(!user.department) {
      return res.status(403).send({message: 'Required a department.'});
    } else if(user.role && user.role.level >= 1) {
      req.user = user;
      return next();
    }
    return res.status(403).send({message: 'Required a creator account.'});
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
isManager = async (req, res, next) => {
  try {
    const user = await db.User.findById(req.userId)
      .populate('role')
      .populate('department');
    if(!user.department) {
      return res.status(403).send({message: 'Required a department.'});
    } else if(user.role && user.role.level >= 2) {
      req.user = user;
      return next();
    }
    return res.status(403).send({message: 'Required a manager account.'});
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
isQCO = async (req, res, next) => {
  try {
    const user = await db.User.findById(req.userId)
      .populate('role')
      .populate('department');
    if(user.role && user.role.level >= 3) {
      req.user = user;
      return next();
    }
    return res.status(403).send({message: 'Required a quality control officer account.'});
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};
isAdmin = async (req, res, next) => {
  try {
    const user = await db.User.findById(req.userId)
      .populate('role')
      .populate('department');
    if(user.role && user.role.level >= 4) {
      req.user = user;
      return next();
    }
    return res.status(403).send({message: 'Required an admin account.'});
  } catch (err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


// Check
checkMember = async (req, res, next) => {
  try {
    if(req.headers['authorization']) {
      const authHeader = req.headers['authorization'].split(' ');
      if(authHeader.length == 2 && authHeader[0] == 'Bearer') {
        const token = authHeader[1];
        jwt.verify(token, config.tokenSecret, async (err, decoded) => {
          if(err) return next();

          req.userId = decoded._id;
          req.externalJWT = decoded.externalJWT;
          
          const user = await db.User.findById(req.userId)
            .populate('role');
          if(user.role && user.role.level >= 0) {
            req.user = user;
          }
          return next();
        });
      } else {
        return next();
      }
    } else {
      return next();
    }
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


const authJwt = {
  verifyToken,
  isMember,
  isCreator,
  isManager,
  isQCO,
  isAdmin,
  checkMember
};

module.exports = authJwt;
