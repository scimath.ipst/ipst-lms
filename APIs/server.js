const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const cors = require('cors');
const path = require('path');
const db = require('./models');
const migrations = require('./migrations');
require('dotenv').config();


const app = express();

// Enable helmet security
app.use(helmet());

// Give permission for fetch resource
const frontendDomain = process.env.FRONTEND_URL.replace(/http:\/\/|https:\/\/|\//g, '');
const backendDomain = process.env.BACKEND_URL.replace(/http:\/\/|https:\/\/|\//g, '');
const corsOptions = {
  origin: [ 
    new RegExp(`${frontendDomain}$`),
    new RegExp(`${backendDomain}$`)
  ],
  credentials: true,
  optionsSuccessStatus: 200
};
app.use(cors(corsOptions));

// Parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(cookieParser());

// Parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));


// Routes
require('./routes/auth.routes')(app);
require('./routes/member.routes')(app);
require('./routes/creator.routes')(app);
require('./routes/manager.routes')(app);
require('./routes/qco.routes')(app);
require('./routes/admin.routes')(app);

require('./routes/frontend.routes')(app);


// Set port listening for requests
const PORT = process.env.SERVER_PORT;
server = app.listen(PORT, () => {
  console.log(`APIs is running on port ${PORT}.`);
});


// Connect to database
db.mongoose.connect(process.env.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
}).then(() => {
  console.log('Successfully connect to MongoDB.');
  migrations.initial();
}).catch(err => {
  console.error('Connection error', err);
  process.exit();
});


// Initiate app
module.exports = app;