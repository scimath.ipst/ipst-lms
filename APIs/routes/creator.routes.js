module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const CreatorController = require('../controllers/creator.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });


  // Dashboard
  router.get(
    '/dashboard/count-contents',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.dashboardCountContents
  );
  router.get(
    '/dashboard/most-visited-contents',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.dashboardMostVisitedContents
  );
  router.get(
    '/dashboard/users',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.dashboardUsers
  );


  // User
  router.get(
    '/user/list',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.userList
  );
  router.get(
    '/user',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.userRead
  );


  // Channel
  router.get(
    '/channel/list',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.channelList
  );
  router.get(
    '/channel',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.channelRead
  );


  // Playlist
  router.get(
    '/playlist/list',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.playlistList
  );
  router.get(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.playlistRead
  );


  // Content
  router.get(
    '/content/list',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.contentList
  );
  router.get(
    '/content',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.contentRead
  );
  router.post(
    '/content',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.contentAdd
  );
  router.put(
    '/content',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.contentEdit
  );
  router.delete(
    '/content',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.contentDelete
  );

  router.get(
    '/approved-content/list',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.approvedContentList
  );
  router.post(
    '/approved-content',
    [ authJwt.verifyToken, authJwt.isCreator ],
    CreatorController.approvedContentAdd
  );

  
  app.use('/creator', router);
};