module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const MemberController = require('../controllers/member.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  
  // Traffic
  router.post(
    '/traffic-create',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.trafficCreate
  );


  // Profile
  router.put(
    '/profile',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.profileEdit
  );
  router.put(
    '/password',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.passwordEdit
  );
  router.put(
    '/interest',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.interestEdit
  );
  
  router.get(
    '/history/list',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.historyList
  );
  

  // User
  router.get(
    '/user/list',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.userList
  );

  
  // Tag
  router.get(
    '/tag/list',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.tagList
  );
  // Level
  router.get(
    '/level/list',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.levelList
  );
  // Subject
  router.get(
    '/subject/list',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.subjectList
  );
  
  // Badge
  router.get(
    '/badge/list',
    [ authJwt.verifyToken, authJwt.isMember ],
    MemberController.badgeList
  );

  
  app.use('/member', router);
};