module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const ManagerController = require('../controllers/manager.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });


  // Dashboard
  router.get(
    '/dashboard/count-contents',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.dashboardCountContents
  );
  router.get(
    '/dashboard/most-visited-contents',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.dashboardMostVisitedContents
  );
  router.get(
    '/dashboard/users',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.dashboardUsers
  );


  // Channel
  router.get(
    '/channel/list',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.channelList
  );
  router.get(
    '/channel',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.channelRead
  );
  router.post(
    '/channel',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.channelAdd
  );
  router.put(
    '/channel',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.channelEdit
  );
  router.delete(
    '/channel',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.channelDelete
  );


  // Playlist
  router.get(
    '/playlist/list',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.playlistList
  );
  router.get(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.playlistRead
  );
  router.post(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.playlistAdd
  );
  router.put(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.playlistEdit
  );
  router.delete(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.playlistDelete
  );


  // Content
  router.get(
    '/content/list',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.contentList
  );
  router.get(
    '/content',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.contentRead
  );
  router.post(
    '/content',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.contentAdd
  );
  router.put(
    '/content',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.contentEdit
  );
  router.delete(
    '/content',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.contentDelete
  );
  
  router.post(
    '/approved-content',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.approvedContentAdd
  );


  // Department
  router.put(
    '/department',
    [ authJwt.verifyToken, authJwt.isManager ],
    ManagerController.departmentEdit
  );

  
  app.use('/manager', router);
};