module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const AdminController = require('../controllers/admin.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });
  

  // Department
  router.post(
    '/department',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.departmentAdd
  );
  router.put(
    '/department',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.departmentEdit
  );
  router.delete(
    '/department',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.departmentDelete
  );


  // User
  router.get(
    '/user/list',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.userList
  );
  router.get(
    '/user',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.userRead
  );
  router.post(
    '/user',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.userAdd
  );
  router.put(
    '/user',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.userEdit
  );
  router.delete(
    '/user',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.userDelete
  );
  
  router.get(
    '/user/history/list',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.userHistoryList
  );

  
  // Role
  router.get(
    '/role/list',
    [ authJwt.verifyToken, authJwt.isAdmin ],
    AdminController.roleList
  );

  
  app.use('/admin', router);
};