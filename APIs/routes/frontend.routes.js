module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const FrontendController = require('../controllers/frontend.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  
  // Verify
  router.get(
    '/verify-signin',
    [ authJwt.checkMember ],
    FrontendController.verifySignIn
  );

  
  // Traffic
  router.post(
    '/traffic-create',
    FrontendController.trafficCreate
  );

  
  // Banner
  router.get(
    '/banner/list',
    [ authJwt.checkMember ],
    FrontendController.bannerList
  );


  // Content
  router.get(
    '/content/continued-list',
    [ authJwt.checkMember ],
    FrontendController.continuedContentList
  );
  router.get(
    '/content/suggested-list',
    [ authJwt.checkMember ],
    FrontendController.suggestedContentList
  );
  router.get(
    '/content/newest-list',
    FrontendController.newestContentList
  );
  router.get(
    '/content/popular-list',
    [ authJwt.checkMember ],
    FrontendController.popularContentList
  );
  
  router.get(
    '/content/list',
    FrontendController.contentList
  );
  router.get(
    '/content',
    [ authJwt.verifyToken, authJwt.isMember ],
    FrontendController.contentRead
  );
  
  router.get(
    '/content/related-list',
    [ authJwt.verifyToken, authJwt.isMember ],
    FrontendController.relatedContentList
  );


  // User Action
  router.put(
    '/user-action',
    [ authJwt.verifyToken, authJwt.isMember ],
    FrontendController.userActionEdit
  );
  router.put(
    '/user-action/result',
    [ authJwt.verifyToken, authJwt.isMember ],
    FrontendController.userActionResultEdit
  );


  // Level
  router.get(
    '/level/list',
    FrontendController.levelList
  );
  router.get(
    '/level',
    FrontendController.levelRead
  );

  // Subject
  router.get(
    '/subject/list',
    FrontendController.subjectList
  );
  
  // Tag
  router.get(
    '/tag/list',
    FrontendController.tagList
  );


  // Channel
  router.get(
    '/channel/list',
    FrontendController.channelList
  );
  router.get(
    '/channel',
    FrontendController.channelRead
  );

  // Playlist
  router.get(
    '/playlist/list',
    FrontendController.playlistList
  );
  router.get(
    '/playlist',
    FrontendController.playlistRead
  );

  
  app.use('/frontend', router);
};