module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const QcoController = require('../controllers/qco.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });


  // Dashboard
  router.get(
    '/dashboard/count-contents',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.dashboardCountContents
  );
  router.get(
    '/dashboard/most-visited-contents',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.dashboardMostVisitedContents
  );
  router.get(
    '/dashboard/departments',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.dashboardDepartments
  );


  // Channel
  router.get(
    '/channel/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.channelList
  );
  router.get(
    '/channel',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.channelRead
  );
  router.post(
    '/channel',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.channelAdd
  );
  router.put(
    '/channel',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.channelEdit
  );
  router.delete(
    '/channel',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.channelDelete
  );


  // Playlist
  router.get(
    '/playlist/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.playlistList
  );
  router.get(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.playlistRead
  );
  router.post(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.playlistAdd
  );
  router.put(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.playlistEdit
  );
  router.delete(
    '/playlist',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.playlistDelete
  );


  // Content
  router.get(
    '/content/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.contentList
  );
  router.get(
    '/content',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.contentRead
  );
  router.post(
    '/content',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.contentAdd
  );
  router.put(
    '/content',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.contentEdit
  );
  router.delete(
    '/content',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.contentDelete
  );
  
  router.get(
    '/approved-content/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.approvedContentList
  );
  router.post(
    '/approved-content',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.approvedContentAdd
  );


  // Tag
  router.get(
    '/tag/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.tagList
  );
  router.get(
    '/tag',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.tagRead
  );
  router.post(
    '/tag',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.tagAdd
  );
  router.put(
    '/tag',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.tagEdit
  );
  router.delete(
    '/tag',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.tagDelete
  );


  // Level
  router.get(
    '/level/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.levelList
  );
  router.get(
    '/level',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.levelRead
  );
  router.post(
    '/level',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.levelAdd
  );
  router.put(
    '/level',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.levelEdit
  );
  router.delete(
    '/level',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.levelDelete
  );


  // Subject
  router.get(
    '/subject/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.subjectList
  );
  router.get(
    '/subject',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.subjectRead
  );
  router.post(
    '/subject',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.subjectAdd
  );
  router.put(
    '/subject',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.subjectEdit
  );
  router.delete(
    '/subject',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.subjectDelete
  );


  // Department
  router.get(
    '/department/list',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.departmentList
  );
  router.get(
    '/department',
    [ authJwt.verifyToken, authJwt.isQCO ],
    QcoController.departmentRead
  );


  app.use('/qco', router);
};