const config = require('./');
const firebaseAdmin = require('firebase-admin');
const firebaseServiceAccount = require('./firebase-service-account.json');
const { initializeApp } = require('firebase/app');
const { getFirestore } = require('firebase/firestore');

firebaseAdmin.initializeApp({ credential: firebaseAdmin.credential.cert(firebaseServiceAccount) });
const firebaseApp = initializeApp(config.firebaseConfig);

module.exports = {

  sendPushNotification: async (fcmTokens, title='', message='') => {
    try {
      await firebaseAdmin.messaging().sendToDevice(fcmTokens, {
        notification: {
          title: title,
          body: message,
        },
        data: { click_action: 'FLUTTER_NOTIFICATION_CLICK' }
      }) 
    } catch (err) {
      console.log(err);
    }
  },

  firebaseDb: getFirestore(firebaseApp)

};
