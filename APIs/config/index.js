module.exports = {
  frontendUrl: process.env.FRONTEND_URL,
  backUrl: process.env.BACKEND_URL,

  externalApiUrl: process.env.EXTERNAL_API_URL,
  externalApiId: process.env.EXTERNAL_API_ID,
  externalAppId: process.env.EXTERNAL_APP_ID,
  
  tokenSecret: process.env.TOKEN_SECRET,
  tokenLife: process.env.TOKEN_LIFE,
  
  tokenRefreshSecret: process.env.TOKEN_REFRESH_SECRET,
  tokenRefreshLife: process.env.TOKEN_REFRESH_LIFE,
  
  tokenVerifySecret: process.env.TOKEN_VERIFY_SECRET,
  tokenVerifyLife: process.env.TOKEN_VERIFY_LIFE,
  
  tokenPasswordSecret: process.env.TOKEN_PASSWORD_SECRET,
  tokenPasswordLife: process.env.TOKEN_PASSWORD_LIFE,

  emailFrom: process.env.EMAIL_FROM,
  smtpHost: process.env.SMTP_HOST,
  smtpPort: process.env.SMTP_PORT,
  smtpUsername: process.env.SMTP_USERNAME,
  smtpPassword: process.env.SMTP_PASSWORD,

  firebaseConfig: {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASUREMENT_ID
  }
};