
// Environtment Variables
export const TOKEN_KEY = process.env.REACT_APP_TOKEN_KEY;
export const REFRESH_KEY = process.env.REACT_APP_REFRESH_KEY;


// Alert
export const ALERT_CHANGE = 'alert/change';
export const ALERT_DISAPPEAR = 'alert/disappear';
export const ALERT_LOADING = 'alert/loading';

// Redirect
export const FRONTEND_REDIRECT = 'frontend/redirect';

// Theme
export const THEME_CHANGE = 'theme/change';

// User
export const USER_SIGNIN = 'user/signin';
export const USER_SIGNOUT = 'user/signout';
export const USER_UPDATE = 'user/update';


// Tonav
export const TOPNAV_ACTIVE_INDEX = 'frontend/topnav/active-index';

// Banner
export const BANNER_LIST = 'frontend/banner/list';

// Content
export const CONTENT_CONTINUED_LIST = 'frontend/content/continued-list';
export const CONTENT_SUGGESTED_LIST = 'frontend/content/suggested-list';
export const CONTENT_NEWEST_LIST = 'frontend/content/newest-list';
export const CONTENT_POPULAR_LIST = 'frontend/content/popular-list';
export const CONTENT_LIST = 'frontend/content/list';
export const CONTENT_READ = 'frontend/content/read';
export const CONTENT_RELATED_LIST = 'frontend/content/related-list';
export const CONTENT_RELEARN_LIST = 'frontend/content/relearn-list';

// User Action
export const USER_ACTION_UPDATE = 'frontend/user-action/update';

// Level
export const LEVEL_LIST = 'frontend/level/list';
export const LEVEL_READ = 'frontend/level/read';

// Subject
export const SUBJECT_LIST = 'frontend/subject/list';

// Tag
export const TAG_LIST = 'frontend/tag/list';

// Channel
export const CHANNEL_LIST = 'frontend/channel/list';
export const CHANNEL_READ = 'frontend/channel/read';

// Playlist
export const PLAYLIST_LIST = 'frontend/playlist/list';
export const PLAYLIST_READ = 'frontend/playlist/read';
