import { THEME_CHANGE } from './types';

export const themeChange = (type) => (dispatch) => {
  dispatch({
    type: THEME_CHANGE,
    payload: type
  });
};
