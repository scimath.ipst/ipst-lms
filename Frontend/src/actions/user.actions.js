import { USER_SIGNIN, USER_SIGNOUT, USER_UPDATE } from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader, apiHeaderFormData } from '../helpers/header';
import { UserActionModel, BadgeModel } from '../models';

import CryptoJS from 'crypto-js';
import { TOKEN_KEY, REFRESH_KEY } from '../actions/types';


export const userSignin = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/signin`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    data1.accessToken = CryptoJS.AES.encrypt(data1.accessToken, TOKEN_KEY).toString();
    data1.refreshToken = CryptoJS.AES.encrypt(data1.refreshToken, REFRESH_KEY).toString();
      
    dispatch(alertChange('Info', 'เข้าสู่ระบบสำเร็จแล้ว'));
    dispatch({ type: USER_SIGNIN, payload: data1 });
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};

export const userSignup = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/signup`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors));
      return false;
    }
    
    dispatch(alertChange('Info', 'สมัครสมาชิกสำเร็จแล้ว'));
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};

export const userSignout = () => (dispatch) => {
  dispatch(alertChange('Info', 'ออกจากระบบสำเร็จแล้ว'));
  dispatch({ type: USER_SIGNOUT, payload: true });
  return true;
};

export const userVerify = (token) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/verify/${token}`, {
      method: 'GET',
      headers: apiHeader()
    });
    const data1 = await fetch1.json();
    dispatch(alertLoading(false));
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};

export const userForgetPassword = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/forget-password`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors));
      return false;
    }
    
    dispatch(alertChange('Info', 'ขอการตั้งรหัสผ่านใหม่สำเร็จแล้ว'));
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userCheckResetPassword = (token) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      dispatch(alertLoading(true));
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/check-reset-password?token=${token}`, {
        method: 'GET',
        headers: apiHeader()
      });
      dispatch(alertLoading(false));
      if(!fetch1.ok || fetch1.status !== 200) {
        resolve(false);
      } else {
        resolve(true);
      }
    } catch (error) {
      dispatch(alertLoading(false));
      console.log(error);
    }
  });
};
export const userResetPassword = (input) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      dispatch(alertLoading(true));
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/reset-password`, {
        method: 'POST',
        headers: apiHeader(),
        body: JSON.stringify(input)
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) {
        dispatch(alertChange('Warning', data1.message, data1.errors));
        resolve(false);
      } else {
        dispatch(alertChange('Info', 'การตั้งรหัสผ่านใหม่สำเร็จแล้ว'));
        resolve(true);
      }
    } catch (error) {
      dispatch(alertLoading(false));
      console.log(error);
    }
  });
};


// User Functions
export const userUploadFile = (file) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    
    const formData = new FormData();
    formData.append('file', file);
    
    const fetch1 = await fetch(`${process.env.REACT_APP_CDN_URL}upload`, {
      method: 'POST',
      headers: apiHeaderFormData(),
      body: formData
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    return data1.result;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};

export const userHistoryList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/history/list`, {
        method: 'GET',
        headers: apiHeader()
      });
      var data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) reject(data1);

      var result = data1.result.map(d => new UserActionModel(d));
      resolve(result);
    } catch (error) {
      dispatch(alertLoading(false));
      console.log(error);
    }
  });
};
export const userBadgeList = ({ id }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/badge/list?userId=${id}`, {
        method: 'GET',
        headers: apiHeader()
      });
      var data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) reject(data1);
      
      var result = data1.result.map(d => new BadgeModel(d));
      resolve(result);
    } catch (error) {
      dispatch(alertLoading(false));
      console.log(error);
    }
  });
};

export const userProfileEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/profile`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({ type: USER_UPDATE, payload: data1.result });
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userPasswordEdit = (input) => async (dispatch) => {
  try {
    if(input.newPassword !== input.confirmPassword) {
      dispatch(alertChange('Warning', 'รหัสผ่านใหม่ไม่ตรงกัน'));
      return false;
    }

    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/password`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};
export const userInterestEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/interest`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({ type: USER_UPDATE, payload: data1.result });
    return true;
  } catch (error) {
    dispatch(alertLoading(false));
    console.log(error);
  }
};

export const userTrafficCreate = (input) => async (dispatch) => {
  try {
    await fetch(`${process.env.REACT_APP_API_URL}member/traffic-create`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
  } catch (error) {
    console.log(error);
  }
};
