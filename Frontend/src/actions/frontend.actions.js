import {
  FRONTEND_REDIRECT, TOPNAV_ACTIVE_INDEX, BANNER_LIST, 
  CONTENT_CONTINUED_LIST, CONTENT_SUGGESTED_LIST, 
  CONTENT_NEWEST_LIST, CONTENT_POPULAR_LIST,
  CONTENT_LIST, CONTENT_READ, 
  CONTENT_RELATED_LIST, CONTENT_RELEARN_LIST,
  USER_ACTION_UPDATE,
  LEVEL_LIST, LEVEL_READ, SUBJECT_LIST, TAG_LIST,
  CHANNEL_LIST, CHANNEL_READ,
  PLAYLIST_LIST, PLAYLIST_READ
} from './types';
import { apiHeader } from '../helpers/header';
import { 
  ContentModel, LevelModel, SubjectModel, TagModel, UserActionModel,
  ChannelModel, PlaylistModel
} from '../models';


// Redirect
export const setTopnavActiveIndex2 = (input) => (dispatch) => {
  dispatch({
    type: FRONTEND_REDIRECT,
    payload: input
  });
  return true;
};

// Traffic
export const frontendTrafficCreate = (input) => async (dispatch) => {
  try {
    await fetch(`${process.env.REACT_APP_API_URL}frontend/traffic-create`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
  } catch (error) {
    console.log(error);
  }
};


// Topnav
export const setTopnavActiveIndex = (index) => (dispatch) => {
  dispatch({
    type: TOPNAV_ACTIVE_INDEX,
    payload: index
  });
  return true;
};


// Banner
export const frontendBannerList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/banner/list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = data1.result.map(data => new ContentModel(data));
      dispatch({
        type: BANNER_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};


// Contents
export const frontendContinuedContentList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/continued-list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = data1.result.map(data => new ContentModel(data.content, data.watchingSeconds));
      dispatch({
        type: CONTENT_CONTINUED_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendSuggestedContentList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/suggested-list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = data1.result.map(data => new ContentModel(data));
      dispatch({
        type: CONTENT_SUGGESTED_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendNewestContentList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/newest-list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) {
        return false;
      }

      const result = data1.result.map(data => new ContentModel(data));
      dispatch({
        type: CONTENT_NEWEST_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendPopularContentList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/popular-list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }
      
      const result = data1.result.map(data => new ContentModel(data));
      dispatch({
        type: CONTENT_POPULAR_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};

export const frontendContentList = ({ 
  playlistUrl, levelUrl, subjectUrl, sorting, keyword, paginate 
}) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const options = [
        `playlistUrl=${playlistUrl? playlistUrl: ''}`,
        `levelUrl=${levelUrl? levelUrl: ''}`,
        `subjectUrl=${subjectUrl? subjectUrl: ''}`,
        `sorting=${sorting? sorting: ''}`,
        `keyword=${keyword? keyword: ''}`,
        `page=${paginate.page}`,
        `pp=${paginate.pp}`
      ];
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/list?${options.join('&')}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }
      
      const result = data1.result.map(data => new ContentModel(data));
      dispatch({
        type: CONTENT_LIST,
        payload: result
      });
      resolve({ result: result, paginate: data1.paginate });
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendContentRead = ({ url }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      dispatch({ type: USER_ACTION_UPDATE, payload: new UserActionModel({}) });
      
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content?url=${url}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }
      
      const result = new ContentModel(data1.result);
      dispatch({ type: CONTENT_READ, payload: result });
      
      const userAction = new UserActionModel(data1.userAction);
      dispatch({ type: USER_ACTION_UPDATE, payload: userAction });

      resolve({ content: result, userAction: userAction });
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendContentClear = () => async (dispatch) => {
  dispatch({
    type: CONTENT_READ,
    payload: new ContentModel({})
  });
  return true;
};

export const frontendRelatedContentList = ({ id, isPlaylist, playlistUrl }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      var fetch1;
      if(isPlaylist){
        const options = [
          `playlistUrl=${playlistUrl? playlistUrl: ''}`,
          `page=1`,
          `pp=9999`
        ];
        fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/list?${options.join('&')}`, {
          method: 'GET',
          headers: apiHeader()
        });
      }else{
        fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content/related-list?contentId=${id}`, {
          method: 'GET',
          headers: apiHeader()
        });
      }
      
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }
      
      const result = data1.result.map(data => new ContentModel(data));
      dispatch({
        type: CONTENT_RELATED_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendRelatedContentListClear = () => async (dispatch) => {
  dispatch({
    type: CONTENT_RELATED_LIST,
    payload: []
  });
  return true;
};

export const frontendRelearnContentList = ({ id }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      var result = [];

      if(id){
        if(id === '61daf1c4fc6ae02c506d5bd5'){
          const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content?url=${'RZ_WacwUEpE'}`, {
            method: 'GET',
            headers: apiHeader()
          });
          const data1 = await fetch1.json();
          if(fetch1.ok && fetch1.status === 200){
            result.push(new ContentModel(data1.result));
          }
        }else if(id === '61dbd4879f7d442cb0cde79d'){
          const fetch2 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content?url=${'GJeB_6_OV6Q'}`, {
            method: 'GET',
            headers: apiHeader()
          });
          const data2 = await fetch2.json();
          if(fetch2.ok && fetch2.status === 200){
            result.push(new ContentModel(data2.result));
          }
          const fetch3 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content?url=${'klCC4BLmCC4'}`, {
            method: 'GET',
            headers: apiHeader()
          });
          const data3 = await fetch3.json();
          if(fetch3.ok && fetch3.status === 200){
            result.push(new ContentModel(data3.result));
          }
          const fetch4 = await fetch(`${process.env.REACT_APP_API_URL}frontend/content?url=${'ryLvpsozn1A'}`, {
            method: 'GET',
            headers: apiHeader()
          });
          const data4 = await fetch4.json();
          if(fetch4.ok && fetch4.status === 200){
            result.push(new ContentModel(data4.result));
          }
        }
      }

      dispatch({
        type: CONTENT_RELEARN_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendRelearnContentListClear = () => async (dispatch) => {
  dispatch({
    type: CONTENT_RELEARN_LIST,
    payload: []
  });
  return true;
};


// User Action
export const frontendUserActionUpdate = (input) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/user-action`, {
        method: 'PUT',
        headers: apiHeader(),
        body: JSON.stringify(input),
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = new UserActionModel(data1.result);
      dispatch({
        type: USER_ACTION_UPDATE,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendUserActionResultUpdate = (input) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/user-action/result`, {
        method: 'PUT',
        headers: apiHeader(),
        body: JSON.stringify(input),
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = new UserActionModel(data1.result);
      dispatch({
        type: USER_ACTION_UPDATE,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};


// Level
export const frontendLevelList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/level/list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = data1.result.map(data => new LevelModel(data));
      dispatch({
        type: LEVEL_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendLevelRead = ({ url }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/level?url=${url}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = new LevelModel(data1.result);
      dispatch({
        type: LEVEL_READ,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendLevelClear = () => async (dispatch) => {
  dispatch({
    type: LEVEL_READ,
    payload: new LevelModel({})
  });
  return true;
};

// Subject
export const frontendSubjectList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/subject/list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = data1.result.map(data => new SubjectModel(data));
      dispatch({
        type: SUBJECT_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};

// Tag
export const frontendTagList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/tag/list`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = data1.result.map(data => new TagModel(data));
      dispatch({
        type: TAG_LIST,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};

// Channel
export const frontendChannelList = ({ sorting, keyword, paginate }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const options = [
        `sorting=${sorting? sorting: ''}`,
        `keyword=${keyword? keyword: ''}`,
        `page=${paginate.page}`,
        `pp=${paginate.pp}`
      ];
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/channel/list?${options.join('&')}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }
      
      const result = data1.result.map(data => new ChannelModel(data));
      dispatch({
        type: CHANNEL_LIST,
        payload: result
      });
      resolve({ result: result, paginate: data1.paginate });
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendChannelRead = ({ url }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/channel?url=${url}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = new ChannelModel(data1.result);
      dispatch({
        type: CHANNEL_READ,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendChannelClear = () => async (dispatch) => {
  dispatch({
    type: CHANNEL_READ,
    payload: new ChannelModel({})
  });
  return true;
};

// Playlist
export const frontendPlaylistList = ({ channelUrl, sorting, keyword, paginate }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const options = [
        `channelUrl=${channelUrl? channelUrl: ''}`,
        `sorting=${sorting? sorting: ''}`,
        `keyword=${keyword? keyword: ''}`,
        `page=${paginate.page}`,
        `pp=${paginate.pp}`
      ];
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/playlist/list?${options.join('&')}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }
      
      const result = data1.result.map(data => new PlaylistModel(data));
      dispatch({
        type: PLAYLIST_LIST,
        payload: result
      });
      resolve({ result: result, paginate: data1.paginate });
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendPlaylistRead = ({ url }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}frontend/playlist?url=${url}`, {
        method: 'GET',
        headers: apiHeader()
      });
      const data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200){ reject(data1); return false; }

      const result = new PlaylistModel(data1.result);
      dispatch({
        type: PLAYLIST_READ,
        payload: result
      });
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
export const frontendPlaylistClear = () => async (dispatch) => {
  dispatch({
    type: PLAYLIST_READ,
    payload: new PlaylistModel({})
  });
  return true;
};


// OTIMS
export const frontendProcessOtims = ({ email }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_OTIMS_API_URL}ml?email=${email}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': `${process.env.REACT_APP_OTIMS_KEY}`
        }
      });
      if(!fetch1.ok || fetch1.status !== 200){
        resolve([]);
      }else{
        const data1 = await fetch1.json();
        resolve(data1);
      }
    } catch (error) {
      console.log(error);
    }
  });
};
