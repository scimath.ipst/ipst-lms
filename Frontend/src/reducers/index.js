import { combineReducers } from 'redux';
import alertReducer from './alert.reducer';
import themeReducer from './theme.reducer';
import userReducer from './user.reducer';
import frontendReducer from './frontend.reducer';

export default combineReducers({
  alert: alertReducer,
  theme: themeReducer,
  user: userReducer,
  frontend: frontendReducer
});