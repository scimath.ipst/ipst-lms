import {
  TOPNAV_ACTIVE_INDEX, BANNER_LIST, 
  CONTENT_CONTINUED_LIST, CONTENT_SUGGESTED_LIST, 
  CONTENT_NEWEST_LIST, CONTENT_POPULAR_LIST,
  CONTENT_LIST, CONTENT_READ, 
  CONTENT_RELATED_LIST, CONTENT_RELEARN_LIST,
  USER_ACTION_UPDATE,
  LEVEL_LIST, LEVEL_READ, SUBJECT_LIST, TAG_LIST,
  CHANNEL_LIST, CHANNEL_READ,
  PLAYLIST_LIST, PLAYLIST_READ
} from '../actions/types';
import { 
  ContentModel, LevelModel, UserActionModel, ChannelModel, PlaylistModel
} from '../models';

const initialState = {
  topnavActiveIndex: 0,
  banners: [],
  continuedContents: [],
  suggestedContents: [],
  newestContents: [],
  popularContents: [],
  contents: [],
  content: new ContentModel({}),
  relatedContents: [],
  relearnContents: [],
  userAction: new UserActionModel({}),
  levels: [],
  level: new LevelModel({}),
  subjects: [],
  tags: [],
  channels: [],
  channel: new ChannelModel({}),
  playlists: [],
  playlist: new PlaylistModel({})
};

const frontendReducer = (state = initialState, action) => {
  switch(action.type) {
    
    case TOPNAV_ACTIVE_INDEX:
      return {...state, topnavActiveIndex: action.payload };
    
    case BANNER_LIST:
      return {...state, banners: action.payload };
      
    case CONTENT_CONTINUED_LIST:
      return {...state, continuedContents: action.payload };
    case CONTENT_SUGGESTED_LIST:
      return {...state, suggestedContents: action.payload };
    case CONTENT_NEWEST_LIST:
      return {...state, newestContents: action.payload };
    case CONTENT_POPULAR_LIST:
      return {...state, popularContents: action.payload };

    case CONTENT_LIST:
      return {...state, contents: action.payload };
    case CONTENT_READ:
      return {...state, content: action.payload };

    case CONTENT_RELATED_LIST:
      return {...state, relatedContents: action.payload };
    case CONTENT_RELEARN_LIST:
      return {...state, relearnContents: action.payload };
      
    case USER_ACTION_UPDATE:
      return {...state, userAction: action.payload };

    case LEVEL_LIST:
      return {...state, levels: action.payload };
    case LEVEL_READ:
      return {...state, level: action.payload };

    case SUBJECT_LIST:
      return {...state, subjects: action.payload };

    case TAG_LIST:
      return {...state, tags: action.payload };

    case CHANNEL_LIST:
      return {...state, channels: action.payload };
    case CHANNEL_READ:
      return {...state, channel: action.payload };

    case PLAYLIST_LIST:
      return {...state, playlists: action.payload };
    case PLAYLIST_READ:
      return {...state, playlist: action.payload };

    default:
      return state;
  }
};

export default frontendReducer;