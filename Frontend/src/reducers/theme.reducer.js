import { ThemeModel } from '../models';
import { THEME_CHANGE } from '../actions/types';

const theme = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_THEME`);
const initialState = new ThemeModel({ type: theme? theme: 'day' });

const themeReducer = (state = initialState, action) => {
  switch(action.type) {

    case THEME_CHANGE:
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_THEME`, action.payload);
      return { ...state, ...new ThemeModel({ type: action.payload }) };

    default:
      return state;
  }
};

export default themeReducer;