import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { UserModel } from './models';
import Topnav from './components/Topnav';
import Footer from './components/Footer';
import AlertPopup from './components/AlertPopup';

import CryptoJS from 'crypto-js';
import { TOKEN_KEY, REFRESH_KEY } from './actions/types';

import { 
  SignInPage, SignUpPage, VerifyPage, ForgetPasswordPage, ResetPasswordPage, Page404 
} from './views/auth';
import { 
  HomePage, AboutPage, ContactPage, LevelsPage, ContentPage, ProfilePage,
  ChannelsPage, ChannelPage, PlaylistPage
} from './views/frontends';

function App() {
  VerifySignedIn();
  return (
    <BrowserRouter>
      <Topnav />
      <Switch>

        <Route exact path="/" component={HomePage} />
        <Route exact path="/about" component={AboutPage} />
        <Route exact path="/contact" component={ContactPage} />
        <Route exact path="/levels/:level?" component={LevelsPage} />

        <Route exact path="/channels" component={ChannelsPage} />
        <Route exact path="/channel/:url" component={ChannelPage} />
        <Route exact path="/playlist/:url" component={PlaylistPage} />
        
        <Route exact path="/content/:url/:playlistUrl?" component={ContentPage} />
        <ProtectedRoute 
          auth={GuardMember()} redirect="/auth/signin" 
          exact path="/profile" component={ProfilePage} 
        />

        <NotSignedInRoute exact path="/auth/signin/:redirect?" component={SignInPage} />
        <NotSignedInRoute exact path="/auth/signup" component={SignUpPage} />
        <NotSignedInRoute exact path="/auth/verify/:token" component={VerifyPage} />
        <NotSignedInRoute exact path="/auth/forget-password" component={ForgetPasswordPage} />
        <NotSignedInRoute exact path="/auth/reset-password/:token" component={ResetPasswordPage} />

        <Route path="*" component={Page404} />

      </Switch>
      <Footer />
      <AlertPopup />
    </BrowserRouter>
  );
}


// Verify
const VerifySignedIn = async () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return true;

  user = JSON.parse(user);
  if(!user.refreshToken) return true;
  
  let bytes = CryptoJS.AES.decrypt(user.refreshToken, REFRESH_KEY);
  let refreshToken = bytes.toString(CryptoJS.enc.Utf8);
  const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/refresh-token`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ refreshToken: refreshToken })
  });
  if(!fetch1.ok || fetch1.status !== 200){
    localStorage.removeItem(`${process.env.REACT_APP_PREFIX}_USER`);
    window.location.reload();
    return false;
  }else{
    let data1 = await fetch1.json();
    user.accessToken = CryptoJS.AES.encrypt(data1.accessToken, TOKEN_KEY).toString();
    user.refreshToken = CryptoJS.AES.encrypt(data1.refreshToken, REFRESH_KEY).toString();
    localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(user));
  }
  
  return true;
};


// Routes
const ProtectedRoute = ({ component: Component, auth, redirect="/", ...rest }) => {
  if(auth) {
    return (<Route {...rest} render={(props) => (<Component {...props} />)} />);
  } else {
    return <Redirect to={redirect} />;
  }
};
const NotSignedInRoute = ({ component: Component, ...rest }) => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return (<Route {...rest} render={(props) => (<Component {...props} />)} />);

  user = new UserModel(JSON.parse(user));
  if(!user.isSignedIn()) return (<Route {...rest} render={(props) => (<Component {...props} />)} />);

  return <Redirect to="/" />;
};


// Guards
const GuardMember = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isSignedIn()) {
    return false;
  }
  return true;
};


export default App;
