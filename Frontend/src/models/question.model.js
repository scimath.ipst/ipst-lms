export class QuestionModel {
  constructor(data) {
    this.question = data.question? data.question: null;
    this.choices = data.choices
      ? data.choices.map(d => {
        return {
          text: d.text? d.text: null,
          isCorrect: d.isCorrect? d.isCorrect: false
        };
      }): [];
    this.score = data.score? parseInt(data.score): 1;
    this.selectedChoice = '';
    this.state = 0;
  }

  setState(state) {
    this.state = state;
  }
  setSelectedChoice(selectedChoice) {
    this.selectedChoice = selectedChoice;
  }
  isCorrect() {
    if(this.selectedChoice !== ''){
      return this.choices[this.selectedChoice].isCorrect;
    }
    return false;
  }
  getScore() {
    if(this.isCorrect()) return this.score;
    return 0;
  }
}
