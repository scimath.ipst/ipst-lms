export class ThemeModel {
  constructor(data) {
    this.type = data.type? data.type: 'day';
    if(this.type === 'day') this.path = '/assets/css/color-0.css';
    else this.path = '/assets/css/color-1.css';
  }
  
  isValid() { return this._id !== null; }
}
