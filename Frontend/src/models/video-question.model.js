export class VideoQuestionModel {
  constructor(data) {
    this.question = data.question? data.question: null;
    this.at = data.at? parseInt(data.at): null;
    this.choices = data.choices
      ? data.choices.map(d => {
        return {
          text: d.text? d.text: null,
          isCorrect: d.isCorrect? d.isCorrect: false,
          explaination: d.explaination? d.explaination: null,
          goTo: d.goTo? parseInt(d.goTo): null
        };
      }): [];
    this.selectedChoice = '';
    this.state = 0;
  }

  setState(state) {
    this.state = state;
  }
  setSelectedChoice(selectedChoice) {
    this.selectedChoice = selectedChoice;
  }
  selectedExplanation() {
    if(this.selectedChoice !== ''){
      return this.choices[this.selectedChoice].explaination;
    }
    return null;
  }
  isCorrect() {
    if(this.selectedChoice !== ''){
      return this.choices[this.selectedChoice].isCorrect;
    }
    return false;
  }
  goTo() {
    if(this.selectedChoice !== ''){
      return this.choices[this.selectedChoice].goTo;
    }
    return null;
  }
}
