import { ContentModel } from '.';

export class BadgeModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.content = new ContentModel(data.content? data.content: {});
    this.createdAt = data.createdAt? data.createdAt: null;
    this.updatedAt = data.updatedAt? data.updatedAt: null;
  }

  getName() {
    return this.content.badgeName;
  }
  getImage() {
    return this.content.badge;
  }
}
