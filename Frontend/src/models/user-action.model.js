import { UserModel, ContentModel } from '.';

export class UserActionModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.user = new UserModel(data.user? data.user: {});
    this.content = new ContentModel(data.content? data.content: {});

    this.watchingSeconds = data.watchingSeconds? parseFloat(data.watchingSeconds): 0;
    this.scoreType = data.scoreType? parseInt(data.scoreType): 0;
    this.questions = data.questions? data.questions: [];
    this.score = data.score? parseFloat(data.score): 0;
    this.maxScore = data.maxScore? parseFloat(data.maxScore): 0;
    this.minScore = data.minScore? parseFloat(data.minScore): 0;
    
    this.status = data.status? parseInt(data.status): 0;
    
    this.createdAt = data.createdAt? data.createdAt: null;
    this.updatedAt = data.updatedAt? data.updatedAt: null;
  }
  
  isValid() { return this._id !== null; }

  isWatching() { return this.isValid() && this.status >= 1; }
  isWatched() { return this.isValid() && this.status >= 2; }
  isCompleted() { return this.isValid() && this.status >= 3; }
}
