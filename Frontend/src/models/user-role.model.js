export class UserRoleModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.name = data.name? data.name: null;
    this.nameEng = data.nameEng? data.nameEng: null;
    this.level = data.level? data.level: 0;
  }
  
  isValid() { return this._id !== null; }
}
