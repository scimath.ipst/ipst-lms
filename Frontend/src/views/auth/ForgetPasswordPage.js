import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { TextField, Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex } from '../../actions/frontend.actions';
import { alertChange } from '../../actions/alert.actions';
import { userForgetPassword } from '../../actions/user.actions';

function ForgetPasswordPage(props) {
  const [process, setProcess] = useState(false);
  const [values, setValues] = useState({
    email: '',
    ip: '',
    url: window.location.href
  });
  const onChangeInput = (key, fromInput=true) => (event) => {
    if(fromInput) setValues({ ...values, [key]: event.target.value });
    else setValues({ ...values, [key]: event });
  };
  const onSubmit = async (e) => {
    e.preventDefault();
    var res = await props.processForgetPassword(values);
    setProcess(res);
  };

  /* eslint-disable */
  useEffect(() => {
    onMounted(onChangeInput('ip', false), true);
    props.setTopnavActiveIndex(0);
  }, []);
  /* eslint-enable */

  return (
    <section className="auth-01">
      <div className="wrapper">

        <div className="bg-container">
          <div className="wrapper">
            <div 
              className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
              data-aos="fade-up" data-aos-delay="450"
            ></div>
            <div className="hide-mobile">
              <h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
                นำสู่ความปกติใหม่ทางการศึกษา <br />
                (New Normal Education)
              </h4>
            </div>
          </div>
        </div>

        {process? (
          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper lg">
              <h4 className="fw-600 text-center">
                ขอตั้งรหัสผ่านใหม่สำเร็จ
              </h4>
              <p className="fw-400 color-black text-center mt-2">
                คุณได้ทำการขอตั้งรหัสผ่านใหม่เรียบร้อยแล้ว <br /> 
                กรุณาตรวจสอบอีเมลของคุณเพื่อทำตามขั้นตอนต่อไป
              </p>
              <div className="text-center mt-5">
                <Button 
                  component={Link} to="/auth/signin" variant="contained" 
                  color="primary" size="large"
                >
                  เข้าสู่ระบบ
                </Button>
              </div>
            </div>
          </div>
        ): (
          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper">
              <h4 className="fw-600 text-center">
                ลืมรหัสผ่าน
              </h4>
							<p className="fw-400 color-black text-center mt-2">
								ใส่อีเมลของคุณเพื่อขอการตั้งรหัสผ่านใหม่
							</p>
              <form onSubmit={onSubmit} autoComplete="off">
                <div className="grids">
                  <div className="grid sm-100">
                    <TextField 
                      required={true} type="email" label="อีเมล" 
                      variant="outlined" fullWidth={true} 
                      value={values.email} onChange={onChangeInput('email')} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <Button 
                      type="submit" variant="contained" size="large" 
                      color="primary" fullWidth={true}
                    >
                      ลืมรหัสผ่าน
                    </Button>
                  </div>
                </div>
              </form>
              <div className="d-flex jc-space-between fw-wrap mt-4">
                <Link to="/auth/signin" className="p fw-500 h-color-p ws-nowrap mt-2 mr-4">
                  มีบัญชีผู้ใช้แล้ว เข้าสู่ระบบ
                </Link>
              </div>
            </div>
          </div>
        )}

      </div>
    </section>
  );
}

ForgetPasswordPage.defaultProps = {
	
};
ForgetPasswordPage.propTypes = {
  alertChange: PropTypes.func.isRequired,
	processForgetPassword: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  alertChange: alertChange,
  setTopnavActiveIndex: setTopnavActiveIndex,
  processForgetPassword: userForgetPassword
})(ForgetPasswordPage);