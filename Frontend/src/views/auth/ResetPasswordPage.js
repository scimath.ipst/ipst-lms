import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { TextField, Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex } from '../../actions/frontend.actions';
import { alertChange } from '../../actions/alert.actions';
import { userCheckResetPassword, userResetPassword } from '../../actions/user.actions';

function ResetPasswordPage(props) {
  const { token } = props.match.params;
  const [process, setProcess] = useState(0);
  const [values, setValues] = useState({
    newPassword: '',
    confirmNewPassword: '',
    ip: '',
    url: window.location.href
  });
  const onChangeInput = (key, fromInput=true) => (event) => {
    if(fromInput) setValues({ ...values, [key]: event.target.value });
    else setValues({ ...values, [key]: event });
  };
  const onSubmit = async (e) => {
    e.preventDefault();
    if(values.newPassword !== values.confirmNewPassword) {
      props.alertChange('Warning', 'ยืนยันรหัสผ่านใหม่ไม่ตรง');
    } else {
      await props.processResetPassword({ ...values, token: token }).then(d => {
        if(d) setProcess(2);
      });
    }
  };

  /* eslint-disable */
  useEffect(() => {
    onMounted(onChangeInput('ip', false), true);
    props.setTopnavActiveIndex(0);
  }, []);
  useEffect(() => {
    if(token) {
      props.processCheckResetPassword(token).then(d => {
        if(d) setProcess(1);
        else setProcess(-1);
      });
    } else {
      setProcess(0);
    }
  }, [token]);
  /* eslint-enable */

  return (
    <section className="auth-01">
      <div className="wrapper">

        <div className="bg-container">
          <div className="wrapper">
            <div 
              className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
              data-aos="fade-up" data-aos-delay="450"
            ></div>
            <div className="hide-mobile">
              <h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
                นำสู่ความปกติใหม่ทางการศึกษา <br />
                (New Normal Education)
              </h4>
            </div>
          </div>
        </div>

        {process === -1? (
          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper lg">
              <h4 className="fw-600 text-center">
                ขอตั้งรหัสผ่านใหม่ผิดพลาด
              </h4>
              <p className="fw-400 color-black text-center mt-2">
                การขอตั้งรหัสผ่านใหม่ผิดพลาด เนื่องจาก Token ที่ได้รับหมดอายุ <br /> 
                หรือถูกใช้งานแล้ว หรือไม่พบผู้ใช้งานในระบบ
              </p>
              <div className="text-center mt-5">
                <Button 
                  component={Link} to="/auth/signin" variant="contained" 
                  color="primary" size="large"
                >
                  เข้าสู่ระบบ
                </Button>
              </div>
            </div>
          </div>
        ): process === 1? (
          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper">
              <h4 className="fw-600 text-center">
                ตั้งรหัสผ่านใหม่
              </h4>
              <form onSubmit={onSubmit} autoComplete="off">
                <div className="grids">
                  <div className="grid sm-100">
                    <TextField 
                      required={true} type="password" label="รหัสผ่าน" 
                      variant="outlined" fullWidth={true} 
                      value={values.newPassword} onChange={onChangeInput('newPassword')} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <TextField 
                      required={true} type="password" label="ยืนยันรหัสผ่าน" 
                      variant="outlined" fullWidth={true} 
                      value={values.confirmNewPassword} 
                      onChange={onChangeInput('confirmNewPassword')} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <Button 
                      type="submit" variant="contained" size="large" 
                      color="primary" fullWidth={true}
                    >
                      ตั้งรหัสผ่านใหม่
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        ): process === 2? (
          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper lg">
              <h4 className="fw-600 text-center">
                ตั้งรหัสผ่านใหม่สำเร็จ
              </h4>
              <p className="fw-400 color-black text-center mt-2">
                คุณได้ทำการตั้งรหัสผ่านใหม่เรียบร้อยแล้ว <br /> 
                คุณสามารถเข้าสู่ระบบได้ในขณะนี้
              </p>
              <div className="text-center mt-5">
                <Button 
                  component={Link} to="/auth/signin" variant="contained" 
                  color="primary" size="large"
                >
                  เข้าสู่ระบบ
                </Button>
              </div>
            </div>
          </div>
        ): (<></>)}

      </div>
    </section>
  );
}

ResetPasswordPage.defaultProps = {
	
};
ResetPasswordPage.propTypes = {
  alertChange: PropTypes.func.isRequired,
	processCheckResetPassword: PropTypes.func.isRequired,
  processResetPassword: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  alertChange: alertChange,
  setTopnavActiveIndex: setTopnavActiveIndex,
  processCheckResetPassword: userCheckResetPassword,
  processResetPassword: userResetPassword
})(ResetPasswordPage);