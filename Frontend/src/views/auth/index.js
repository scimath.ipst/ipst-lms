export {default as SignInPage} from './SignInPage';
export {default as SignUpPage} from './SignUpPage';
export {default as VerifyPage} from './VerifyPage';
export {default as ForgetPasswordPage} from './ForgetPasswordPage';
export {default as ResetPasswordPage} from './ResetPasswordPage';

export {default as Page404} from './404';
