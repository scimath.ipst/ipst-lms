import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex } from '../../actions/frontend.actions';
import { userVerify } from '../../actions/user.actions';

function VerifyPage(props) {
  const [process, setProcess] = useState(0);
	const checkToken = async (token) => {
		var result = await props.processVerify(token);
		if(result) setProcess(1);
		else setProcess(-1);
	};

  /* eslint-disable */
  useEffect(() => {
		onMounted(); checkToken(props.match.params.token);
    props.setTopnavActiveIndex(0);
	}, []);
  /* eslint-enable */

	return (
		<section className="auth-01">
			<div className="wrapper">

				<div className="bg-container">
					<div className="wrapper">
						<div 
							className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
							data-aos="fade-up" data-aos-delay="450"
						></div>
						<div className="hide-mobile">
							<h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
								นำสู่ความปกติใหม่ทางการศึกษา <br />
								(New Normal Education)
							</h4>
						</div>
					</div>
				</div>

				{process === 0? (
					<div className="auth-container" data-aos="fade-up" data-aos-delay="0">
						<div className="auth-wrapper lg">
							<h4 className="fw-600 text-center">
								กำลังเปิดใช้งานบัญชี
							</h4>
							<p className="fw-400 color-black text-center mt-2">
								กำลังทำการยืนยันข้อมูล เพื่อทำการเปิดใช้งานบัญชีของคุณ
							</p>
						</div>
					</div>
				): process === -1 ? (
					<div className="auth-container" data-aos="fade-up" data-aos-delay="0">
						<div className="auth-wrapper lg">
							<h4 className="fw-600 text-center">
								เปิดใช้งานบัญชีล้มเหลว
							</h4>
							<p className="fw-400 color-black text-center mt-2">
								การเปิดใช้งานบัญชีของคุณล้มเหลว กรุณาเช็คข้อมูลของคุณอีกครั้ง
							</p>
							<div className="text-center mt-5">
								<Button 
									component={Link} to="/auth/signin" variant="contained" 
									color="primary" size="large"
								>
									เข้าสู่ระบบ
								</Button>
							</div>
						</div>
					</div>
				): (
					<div className="auth-container" data-aos="fade-up" data-aos-delay="0">
						<div className="auth-wrapper lg">
							<h4 className="fw-600 text-center">
								เปิดใช้งานบัญชีสำเร็จ
							</h4>
							<p className="fw-400 color-black text-center mt-2">
								การเปิดใช้งานบัญชีของคุณสำเร็จแล้ว คุณสามารถเข้าสู่ระบบได้ในขณะนี้
							</p>
							<div className="text-center mt-5">
								<Button 
									component={Link} to="/auth/signin" variant="contained" 
									color="primary" size="large"
								>
									เข้าสู่ระบบ
								</Button>
							</div>
						</div>
					</div>
				)}

			</div>
		</section>
	);
}

VerifyPage.defaultProps = {
	
};
VerifyPage.propTypes = {
	processVerify: PropTypes.func.isRequired,
	setTopnavActiveIndex: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
	processVerify: userVerify,
	setTopnavActiveIndex: setTopnavActiveIndex
})(VerifyPage);