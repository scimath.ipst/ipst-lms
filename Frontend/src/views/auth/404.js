
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex } from '../../actions/frontend.actions';

function Page404(props) {
  
  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(0);
  }, []);
  /* eslint-enable */

  return (
    <section className="auth-01">
      <div className="wrapper">

        <div className="bg-container">
          <div className="wrapper">
            <div 
              className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
              data-aos="fade-up" data-aos-delay="450"
            ></div>
            <div className="hide-mobile">
              <h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
                นำสู่ความปกติใหม่ทางการศึกษา <br />
                (New Normal Education)
              </h4>
            </div>
          </div>
        </div>

        <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
          <div className="auth-wrapper">
            <h4 className="fw-600 text-center">
              404
            </h4>
            <p className="fw-400 color-black text-center mt-2">
              ไม่พบหน้าที่คุณค้นหา กรุณาลองใหม่อีกครั้ง
            </p>
            <div className="text-center mt-5">
              <Button 
                component={Link} to="/" variant="contained" color="primary" size="large"
              >
                กลับสู่หน้าแรก
              </Button>
            </div>
          </div>
        </div>

      </div>
    </section>
  );
}

Page404.defaultProps = {

};
Page404.propTypes = {
	setTopnavActiveIndex: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
	setTopnavActiveIndex: setTopnavActiveIndex
})(Page404);