import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { onMounted } from '../../helpers/frontend';
import Breadcrumb from '../../components/Breadcrumb';

import { connect } from 'react-redux';
import {
  setTopnavActiveIndex, frontendSubjectList,
  frontendPlaylistRead, frontendPlaylistClear, frontendContentList
} from '../../actions/frontend.actions';
import { alertLoading } from '../../actions/alert.actions';
import { PaginateModel } from '../../models';

function PlaylistPage(props) {
  const history = useHistory();

  const { url } = props.match.params;
  const [loading, setLoading] = useState(false);
  const [loadCount, setLoadCount] = useState(0);

  const pp = 15;
  const [paginate, setPaginate] = useState(new PaginateModel({ pp: pp }));
  
  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(5);
    if(!props.subjects.length) props.processSubjectList();
  }, []);
  useEffect(() => {
    props.processClear();
    props.processRead({ url: url });
    setLoadCount(loadCount + 1);
  }, []);
  useEffect(() => {
    if(!loading){
      setLoading(true);
      props.alertLoading(true);
      props.processContentList({ 
        playlistUrl: url, paginate: paginate, 
        subjectUrl: '', sorting: '', keyword: ''
      }).then(d => {
        setLoading(false);
        props.alertLoading(false);
        setPaginate(d.paginate);
        if(d.result && d.result.length){
          history.push('/content/'+d.result[0].url+'/'+url);
        }
      }).catch(() => {
        setLoading(false);
        props.alertLoading(false);
      });
    }
  }, [loadCount]);
  /* eslint-enable */

  return props.playlist._id? (
    <>
      <Breadcrumb 
        classer="style-profile" 
        title={`Playlist ${props.playlist.name}`} 
        subtitle={`โดย ${props.playlist.displayDeptChannel()}`} 
        background="/assets/img/bg/breadcrumb-03.jpg" 
      />
    </>
  ): (<></>);
}

PlaylistPage.defaultProps = {
	
};
PlaylistPage.propTypes = {
  alertLoading: PropTypes.func.isRequired,
	setTopnavActiveIndex: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processClear: PropTypes.func.isRequired,
  processSubjectList: PropTypes.func.isRequired,
  processContentList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  playlist: state.frontend.playlist,
  subjects: state.frontend.subjects,
  contents: state.frontend.contents
});

export default connect(mapStateToProps, {
  alertLoading: alertLoading,
	setTopnavActiveIndex: setTopnavActiveIndex,
  processRead: frontendPlaylistRead,
  processClear: frontendPlaylistClear,
  processSubjectList: frontendSubjectList,
  processContentList: frontendContentList
})(PlaylistPage);