import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { onMounted, formatNumber, formatDate } from '../../helpers/frontend';
import { Button, Chip, Divider, Avatar } from '@material-ui/core';
import ContentCard03 from '../../components/ContentCard03';
import YoutubeVideo from '../../components/YoutubeVideo';
import ContentQuestions from '../../components/ContentQuestions';

import { connect } from 'react-redux';
import { UserModel } from '../../models';
import { 
  setTopnavActiveIndex, frontendContentRead, frontendContentClear,
  frontendRelatedContentList, frontendRelatedContentListClear,
  frontendRelearnContentList, frontendRelearnContentListClear,
  frontendPlaylistClear, frontendPlaylistRead
} from '../../actions/frontend.actions';
import { alertLoading } from '../../actions/alert.actions';

function ContentPage(props) {
	const user = new UserModel(props.user);
  const history = useHistory();

  const { url, playlistUrl } = props.match.params;
  const [loading, setLoading] = useState(false);
  const [foundContent, setFoundContent] = useState(null);
  const [loadingRelated, setLoadingRelated] = useState(true);
  
  const [userAction, setUserAction] = useState({});

  const [clickQuestion, setClickQuestion] = useState(false);
  const onClickQuestion = (e) => {
    e.preventDefault();
    setClickQuestion(true);
  };

  const onTriggerRelearn = (content) => {
    props.processRelearnList({ id: content._id });
  };

  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(0);
  }, []);
  useEffect(() => {
    if(user.isSignedIn()){
      props.processClear();
      props.processRelatedListClear();
      props.processRelearnListClear();
      setFoundContent(null);
      if(url && !loading){
        window.scrollTo(0, 0);
        setLoading(true);
        setLoadingRelated(true);
        props.alertLoading(true);
        props.processRead({ url: url }).then(d => {
          setUserAction(d.userAction);
          setLoading(false);
          props.alertLoading(false);
          setFoundContent(true);
          if(playlistUrl){
            props.processPlaylistClear();
            props.processPlaylistRead({ url: playlistUrl });
            props.processRelatedList({ 
              id: d.content._id, isPlaylist: true, playlistUrl: playlistUrl
            }).then(() => {
              setLoadingRelated(false);
            });
          }else{
            props.processRelatedList({ id: d.content._id }).then(() => {
              setLoadingRelated(false);
            });
          }
        }).catch(() => {
          setLoading(false);
          props.alertLoading(false);
          setFoundContent(false);
        });
      }
    }else{
      history.replace(`/auth/signin/${url}${playlistUrl? '_P14P_'+playlistUrl: ''}`);
    }
  }, [url]);
  /* eslint-enable */

  return (
    <>
      <div className="topnav-spacer"></div>
      {loading? (
        <section className="content-01"></section>
      ): (
        foundContent !== null? (
          foundContent? (
            <section className="content-01 section-padding pt-6">
              <div className="container">
                <div className="grids">
                  
                  <div className="grid xl-70 lg-2-3 sm-100">
  
                    {props.content.type === 1? (
                      // Youtube Video
                      <YoutubeVideo 
                        content={props.content} userAction={userAction} 
                        playlistUrl={playlistUrl} onClickQuestion={onClickQuestion} 
                      />
                    ): props.content.type === 2? (
                      <div className="video-container">
                        <div className="wrapper">
                          <iframe src={props.content.filePath} title="Content MP4"></iframe>
                        </div>
                      </div>
                    ): props.content.type === 3? (
                      <div className="pdf-container">
                        <div className="wrapper">
                          <iframe src={props.content.filePath} title="Content PDF"></iframe>
                        </div>
                      </div>
                    ): props.content.type === 4? (
                      // iFrame
                      <div className="iframe-container">
                        <div className="wrapper">
                          <iframe src={props.content.filePath} title="Content iFrame"></iframe>
                        </div>
                      </div>
                    ): (<></>)}

                    <ContentQuestions 
                      content={props.content} clickQuestion={clickQuestion} 
                      onTriggerRelearn={onTriggerRelearn} 
                    />
  
                    <div className="pt-4 pb-5">
                      <div className="d-flex fw-wrap">
                        {props.content.levels.map((level, j) => (
                          <div className="mt-1 mr-1" key={`level-${j}`}>
                            <Chip color="primary" label={level.name} />
                          </div>
                        ))}
                        {props.content.subject.name? (
                          <div className="mt-1">
                            <Chip color="primary" label={props.content.subject.name} />
                          </div>
                        ): (<></>)}
                      </div>
                      <h5 className="fw-600 mt-3">{props.content.name}</h5>
                      <p className="xs fw-400 op-80 mt-2">
                        ผู้ชม {formatNumber(props.content.visitCount, 0)} คน 
                        <span className="p-dot ml-3 mr-3"></span> 
                        {formatDate(props.content.createdAt)}
                      </p>
                      <div className="d-flex ai-center mt-5">
                        <Avatar src={props.content.user.avatar} className={`sm`} />
                        <p className="fw-300 pl-3">{props.content.user.displayName()}</p>
                      </div>
                      {props.content.description? (
                        <p className="fw-400 lh-md mt-5">{props.content.description}</p>
                      ): (<></>)}
                      {props.content.tags.length? (
                        <p className="fw-500 mt-5">
                          <span className="mr-2">แท็ก :</span> 
                          {props.content.tags.map((tag, i) => (
                            <span className="color-p mr-2" key={i}>#{tag.name}</span>
                          ))}
                        </p>
                      ): (<></>)}
                    </div>
                    <Divider />
  
                  </div>
  
                  <div className="grid xl-30 lg-1-3 sm-100" data-aos="fade-up" data-aos-delay="150">
                    {props.relearnContents && props.relearnContents.length? (
                      <>
                        <h6 className="ss-title stripe-success fw-700 ls-1">
                          เนื้อหาทบทวน
                        </h6>
                        <div className="r-container mt-2 mb-4">
                          <div className="r-blocks">
                            {props.relearnContents.map((c, i) => (
                              <div className="r-block" key={`relearn-${i}`}>
                                <ContentCard03 content={c} />
                              </div>
                            ))}
                          </div>
                        </div>
                      </>
                    ): (<></>)}

                    {playlistUrl? (
                      <h6 className="ss-title fw-700 ls-1">
                        รายการ {props.playlist.channel.name}
                      </h6>
                    ): (
                      <h6 className="ss-title fw-700 ls-1">วิดีโอแนะนำ</h6>
                    )}
                    <div className="r-container mt-2">
                      {playlistUrl? (
                        <div className="r-header">
                          <p className="fw-500">Playlist {props.playlist.name}</p>
                        </div>
                      ): (<></>)}
                      <div className="r-blocks">
                        {!loadingRelated? (
                            props.relatedContents.map((c, i) => (
                              <div className={`r-block ${c.url === url? 'active': ''}`} key={`related-${i}`}>
                                <ContentCard03 content={c} playlistUrl={playlistUrl? playlistUrl: ''} />
                              </div>
                            ))
                          ): (
                            [0,1,2,3,4,5,6,7,8,9].map((i) => (
                              <div className="r-block" key={i}>
                                <ContentCard03 loading={true} />
                              </div>
                            ))
                          )}
                      </div>
                    </div>
                  </div>
  
                </div>
              </div>
            </section>
          ): (
            <section className="section-padding">
              <div className="wrapper">
                <h4 className="sm text-center fw-500" data-aos="fade-up" data-aos-delay="150">
                  ไม่พบเนื้อหาในระบบ
                </h4>
                <div className="text-center mt-4" data-aos="fade-up" data-aos-delay="300">
                  <Button 
                    component={Link} to="/" variant="contained" color="primary" size="large"
                  >
                    กลับสู่หน้าแรก
                  </Button>
                </div>
              </div>
            </section>
          )
        ): (<></>)
      )}
    </>
  );
}

ContentPage.defaultProps = {
	
};
ContentPage.propTypes = {
  alertLoading: PropTypes.func.isRequired,
	setTopnavActiveIndex: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processClear: PropTypes.func.isRequired,
  processRelatedListClear: PropTypes.func.isRequired,
  processRelatedList: PropTypes.func.isRequired,
  processRelearnListClear: PropTypes.func,
  processRelearnList: PropTypes.func,
  processPlaylistClear: PropTypes.func.isRequired,
  processPlaylistRead: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
	content: state.frontend.content,
  relatedContents: state.frontend.relatedContents,
  relearnContents: state.frontend.relearnContents,
  playlist: state.frontend.playlist,
});

export default connect(mapStateToProps, {
  alertLoading: alertLoading,
	setTopnavActiveIndex: setTopnavActiveIndex,
  processRead: frontendContentRead,
  processClear: frontendContentClear,
  processRelatedListClear: frontendRelatedContentListClear,
  processRelatedList: frontendRelatedContentList,
  processRelearnListClear: frontendRelearnContentListClear,
  processRelearnList: frontendRelearnContentList,
  processPlaylistClear: frontendPlaylistClear,
  processPlaylistRead: frontendPlaylistRead,
})(ContentPage);