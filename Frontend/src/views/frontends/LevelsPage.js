import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { onMounted } from '../../helpers/frontend';
import Breadcrumb from '../../components/Breadcrumb';
import ContentCard01 from '../../components/ContentCard01';
import ContentCard02 from '../../components/ContentCard02';
import { 
  FormControl, InputLabel, Select, MenuItem, TextField
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';

import { connect } from 'react-redux';
import {
  setTopnavActiveIndex,
  frontendLevelList, frontendLevelRead, frontendLevelClear, 
  frontendSubjectList, frontendContentList
} from '../../actions/frontend.actions';
import { alertLoading } from '../../actions/alert.actions';
import { PaginateModel } from '../../models';

function LevelsPage(props) {
  const { level } = props.match.params;
	const [oldLevel, setOldLevel] = useState('');
  const [loading, setLoading] = useState(false);
  const [loadCount, setLoadCount] = useState(0);

  const pp = 15;
  const [subject, setSubject] = useState('');
  const [sorting, setSorting] = useState('');
  const [keyword, setKeyword] = useState('');
  const [paginate, setPaginate] = useState(new PaginateModel({ pp: pp }));
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage });
    setLoadCount(loadCount + 1);
  };
  const onSubjectChange = (val) => {
    setSubject(val);
    setPaginate(new PaginateModel({ pp: pp }));
    setLoadCount(loadCount + 1);
  };
  const onSortingChange = (val) => {
    setSorting(val);
    setPaginate(new PaginateModel({ pp: pp }));
    setLoadCount(loadCount + 1);
  };
  const onKeywordSubmit = (e) => {
    e.preventDefault();
    setPaginate(new PaginateModel({ pp: pp }));
    setLoadCount(loadCount + 1);
  };
  
  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(2);
    if(!props.levels.length) props.processList();
    if(!props.subjects.length) props.processSubjectList();
  }, []);
  useEffect(() => {
    props.processClear();
    setPaginate(new PaginateModel({ pp: pp }));
    setSubject(''); setSorting(''); setKeyword('');
    if(level && level !== oldLevel){
      setOldLevel(level);
      props.processRead({ url: level });
      setLoadCount(loadCount + 1);
    }
  }, [level]);
  useEffect(() => {
    if(level && !loading){
      setLoading(true);
      props.alertLoading(true);
      props.processContentList({ 
        levelUrl: level, subjectUrl: subject, sorting: sorting, 
        keyword: keyword, paginate: paginate
      }).then(d => {
        setLoading(false);
        props.alertLoading(false);
        setPaginate(d.paginate);
      }).catch(() => {
        setLoading(false);
        props.alertLoading(false);
      });
    }
  }, [loadCount]);
  /* eslint-enable */

  return !level? (
    <>
      <Breadcrumb title="ระดับชั้น" background="/assets/img/bg/breadcrumb-03.jpg" />
      {props.levels.length && (
        <section className="section-padding">
          <div className="container">
            <div className="grids jc-center">
              {props.levels.map((level, i) => (
                <div className="grid xl-20 lg-25 md-1-3 sm-50" key={i}>
                  <ContentCard02 level={level} />
                </div>
              ))}
            </div>
          </div>
        </section>
      )}
    </>
  ): (
    <>
      <Breadcrumb title={props.level.name} background="/assets/img/bg/breadcrumb-03.jpg" />
      <section className="section-padding">
        <div className="container">
          <div className="grids">
            <div className="grid xl-25 lg-30 md-1-3 mt-0 mb-2">
              <FormControl variant="outlined" fullWidth={true}>
                <InputLabel id="input-subject">เลือกวิชา</InputLabel>
                <Select
                  labelId="input-subject" label="เลือกวิชา" 
                  value={subject ?? ''} onChange={e => onSubjectChange(e.target.value)} 
                >
                  <MenuItem value="">&nbsp;</MenuItem>
                  {props.subjects.map((s, i) => (
                    <MenuItem value={s.url} key={i}>{s.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
            <div className="grid xl-25 lg-30 md-1-3 mt-0 mb-2">
              <FormControl variant="outlined" fullWidth={true}>
                <InputLabel id="input-sorting">เรียงลำดับ</InputLabel>
                <Select
                  labelId="input-sorting" label="เรียงลำดับ" 
                  value={sorting ?? ''} onChange={e => onSortingChange(e.target.value)} 
                >
                  <MenuItem value="">&nbsp;</MenuItem>
                  <MenuItem value={1}>ใหม่ที่สุด</MenuItem>
                  <MenuItem value={2}>เก่าที่สุด</MenuItem>
                  <MenuItem value={3}>เป็นที่นิยม</MenuItem>
                  <MenuItem value={4}>ชื่อ ก-ฮ</MenuItem>
                  <MenuItem value={5}>ชื่อ ฮ-ก</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="grid xl-25 lg-10 md-0 sm-100 mt-0"></div>
            <div className="grid xl-25 lg-30 md-1-3 sm-100 mt-0 mb-2">
              <form onSubmit={onKeywordSubmit}>
                <TextField 
                  label="ค้นหา" variant="outlined" fullWidth={true} 
                  value={keyword} onChange={e => setKeyword(e.target.value)} 
                />
              </form>
            </div>
          </div>
          {!props.contents.length? (
            <div className="mt-6 pt-4 pb-2">
              <h4 className="sm fw-500 text-center">ไม่พบเนื้อหาในระบบ</h4>
            </div>
          ): (
            <div className="grids jc-center mt-4">
              {props.contents.map((content, i) => (
                <div className="grid xl-20 lg-25 md-1-3 sm-50" key={i}>
                  <ContentCard01 content={content} />
                </div>
              ))}
            </div>
          )}
          <div className="d-flex jc-center mt-6 pt-2">
            <Pagination 
              size="large" color="primary" 
              count={paginate.totalPages ?? 0} 
              page={paginate.page ?? 1} 
              onChange={onChangePage} 
            />
          </div>
        </div>
      </section>
    </>
  );
}

LevelsPage.defaultProps = {
	
};
LevelsPage.propTypes = {
  alertLoading: PropTypes.func.isRequired,
	setTopnavActiveIndex: PropTypes.func.isRequired,
  processList: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processClear: PropTypes.func.isRequired,
  processSubjectList: PropTypes.func.isRequired,
  processContentList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	levels: state.frontend.levels,
  level: state.frontend.level,
  subjects: state.frontend.subjects,
  contents: state.frontend.contents
});

export default connect(mapStateToProps, {
  alertLoading: alertLoading,
	setTopnavActiveIndex: setTopnavActiveIndex,
  processList: frontendLevelList,
  processRead: frontendLevelRead,
  processClear: frontendLevelClear,
  processSubjectList: frontendSubjectList,
  processContentList: frontendContentList
})(LevelsPage);