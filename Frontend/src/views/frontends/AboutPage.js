import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex, frontendTrafficCreate } from '../../actions/frontend.actions';
import { userTrafficCreate } from '../../actions/user.actions';
import { UserModel } from '../../models';

function AboutPage(props) {
  const user = new UserModel(props.user);
  
  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(3);
  }, []);
  useEffect(() => {
    if(user.isSignedIn()){
      props.userTrafficCreate({ url: 'https://project14plus.ipst.ac.th/about' });
    }else{
      props.frontendTrafficCreate({ url: 'https://project14plus.ipst.ac.th/about' });
    }
  }, []);
  /* eslint-enable */

  return (
    <>
      <section className="auth-01">
        <div className="wrapper">

          <div className="bg-container">
            <div className="wrapper">
              <div 
                className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
                data-aos="fade-up" data-aos-delay="450"
              ></div>
              <div className="hide-mobile">
                <h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
                  นำสู่ความปกติใหม่ทางการศึกษา <br />
                  (New Normal Education)
                </h4>
              </div>
            </div>
          </div>

          <div className="auth-container md" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper">
              <img className="img-content" src="/assets/img/logo-ipst-project14.png" alt="Logo" />
              <h5 className="fw-600 text-center mt-6">
                โครงการสอนออนไลน์ Project 14+
              </h5>
              <p className="mt-4">
                Project 14+ เป็นโครงการนำสู่ความปกติใหม่ทางการศึกษาของ สสวท. (New Normal Education) 
                ที่การเรียนรู้ไม่ได้จำเป็นเพียงแค่ในห้องเรียน แต่สามารถเกิดได้ทุกที่ทุกเวลาตามที่ผู้เรียนเลือกหรือกำหนด 
                Project 14+ เป็นบทเรียนออนไลน์ซึ่งประกอบด้วยวีดิทัศน์การสอนเพื่อส่งเสริมวิถีการเรียนรู้ใหม่ที่ผู้เรียนมีแหล่งเรียนรู้ด้านวิทยาศาสตร์ 
                คณิตศาสตร์ และเทคโนโลยีที่ตรงตามหลักสูตร เพื่อใช้ในการศึกษาค้นคว้า เรียนรู้ หรือทบทวนบทเรียน นอกจากนี้ 
                ครูผู้สอนยังสามารถใช้แหล่งเรียนรู้นี้ประกอบการจัดการเรียนรู้ตามปกติในห้องเรียน เพื่อส่งเสริมคุณภาพการเรียนรู้ของผู้เรียน
                <br /><br />
                วีดิทัศน์ที่จัดทำขึ้นใน Project 14+ นี้สอดคล้องกับตัวชี้วัดกลุ่มสาระคณิตศาสตร์ และวิทยาศาสตร์และเทคโนโลยี 
                ตามหลักสูตรแกนกลางการศึกษาขั้นพื้นฐาน พุทธศักราช 2551 (ฉบับปรับปรุง พ.ศ. 2560) ครอบคลุมทุกระดับชั้น 
                ตั้งแต่ชั้นประถมศึกษาปีที่ 1 ถึง มัธยมศึกษาปีที่ 6 ทั้งรายวิชาพื้นฐาน และรายวิชาเพิ่มเติม
              </p>
            </div>
          </div>

        </div>
      </section>
    </>
  );
}

AboutPage.defaultProps = {
	
};
AboutPage.propTypes = {
	setTopnavActiveIndex: PropTypes.func.isRequired,
  frontendTrafficCreate: PropTypes.func.isRequired,
  userTrafficCreate: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {
	setTopnavActiveIndex: setTopnavActiveIndex,
  frontendTrafficCreate: frontendTrafficCreate,
  userTrafficCreate: userTrafficCreate
})(AboutPage);