import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Divider } from '@material-ui/core';
import Banner from '../../components/Banner';
import SectionContinuedContents from '../../components/SectionContinuedContents';
import SectionSuggestedContents from '../../components/SectionSuggestedContents';
import SectionNewestContents from '../../components/SectionNewestContents';
import SectionPopularContents from '../../components/SectionPopularContents';
import SectionLevelGroups from '../../components/SectionLevelGroups';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex } from '../../actions/frontend.actions';

function HomePage(props) {

  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(1);
  }, []);
  /* eslint-enable */

  return (
    <>
      <Banner />
      <SectionContinuedContents />
      <SectionSuggestedContents />
      <Divider />
      <SectionNewestContents />
      <SectionPopularContents />
      <SectionLevelGroups />
    </>
  );
}

HomePage.defaultProps = {
	
};
HomePage.propTypes = {
	setTopnavActiveIndex: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
	setTopnavActiveIndex: setTopnavActiveIndex
})(HomePage);