import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { onMounted } from '../../helpers/frontend';
import Breadcrumb from '../../components/Breadcrumb';
import ContentCard04 from '../../components/ContentCard04';
import { 
  FormControl, InputLabel, Select, MenuItem, TextField
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';

import { connect } from 'react-redux';
import { setTopnavActiveIndex, frontendChannelList } from '../../actions/frontend.actions';
import { alertLoading } from '../../actions/alert.actions';
import { PaginateModel } from '../../models';

function ChannelsPage(props) {
  const [loading, setLoading] = useState(false);
  const [loadCount, setLoadCount] = useState(0);

  const pp = 15;
  const [sorting, setSorting] = useState('');
  const [keyword, setKeyword] = useState('');
  const [paginate, setPaginate] = useState(new PaginateModel({ pp: pp }));
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage });
    setLoadCount(loadCount + 1);
  };
  const onSortingChange = (val) => {
    setSorting(val);
    setPaginate(new PaginateModel({ pp: pp }));
    setLoadCount(loadCount + 1);
  };
  const onKeywordSubmit = (e) => {
    e.preventDefault();
    setPaginate(new PaginateModel({ pp: pp }));
    setLoadCount(loadCount + 1);
  };
  
  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(5);
  }, []);
  useEffect(() => {
    if(!loading){
      setLoading(true);
      props.alertLoading(true);
      props.processList({ 
        sorting: sorting, keyword: keyword, paginate: paginate
      }).then(d => {
        setLoading(false);
        props.alertLoading(false);
        setPaginate(d.paginate);
      }).catch(() => {
        setLoading(false);
        props.alertLoading(false);
      });
    }
  }, [loadCount]);
  /* eslint-enable */

  return (
    <>
      <Breadcrumb title="เลือกรายการ" background="/assets/img/bg/breadcrumb-03.jpg" />
      <section className="section-padding bg-01">
        <div className="container">
          <div className="grids jc-center">
            <div className="grid xl-25 lg-30 md-1-3 mt-0 mb-2">
              <FormControl variant="outlined" fullWidth={true}>
                <InputLabel id="input-sorting">เรียงลำดับ</InputLabel>
                <Select
                  labelId="input-sorting" label="เรียงลำดับ" 
                  value={sorting ?? ''} onChange={e => onSortingChange(e.target.value)} 
                >
                  <MenuItem value="">&nbsp;</MenuItem>
                  <MenuItem value={1}>ใหม่ที่สุด</MenuItem>
                  <MenuItem value={2}>เก่าที่สุด</MenuItem>
                  <MenuItem value={3}>ชื่อ ก-ฮ</MenuItem>
                  <MenuItem value={4}>ชื่อ ฮ-ก</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="grid xl-25 lg-30 md-1-3 sm-100 mt-0 mb-2">
              <form onSubmit={onKeywordSubmit}>
                <TextField 
                  label="ค้นหา" variant="outlined" fullWidth={true} 
                  value={keyword} onChange={e => setKeyword(e.target.value)} 
                />
              </form>
            </div>
          </div>
          {!props.channels.length? (
            <div className="mt-6 pt-4 pb-2">
              <h4 className="sm fw-500 text-center">ไม่พบรายการในระบบ</h4>
            </div>
          ): (
            <div className="grids jc-center mt-4">
              {props.channels.map((d, i) => (
                <div className="grid xl-20 lg-25 md-1-3 sm-50" key={i}>
                  <ContentCard04 channel={d} />
                </div>
              ))}
            </div>
          )}
          <div className="d-flex jc-center mt-6 pt-2">
            <Pagination 
              size="large" color="primary" 
              count={paginate.totalPages ?? 0} 
              page={paginate.page ?? 1} 
              onChange={onChangePage} 
            />
          </div>
        </div>
      </section>
    </>
  );
}

ChannelsPage.defaultProps = {
	
};
ChannelsPage.propTypes = {
  alertLoading: PropTypes.func.isRequired,
	setTopnavActiveIndex: PropTypes.func.isRequired,
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	channels: state.frontend.channels
});

export default connect(mapStateToProps, {
  alertLoading: alertLoading,
	setTopnavActiveIndex: setTopnavActiveIndex,
  processList: frontendChannelList
})(ChannelsPage);