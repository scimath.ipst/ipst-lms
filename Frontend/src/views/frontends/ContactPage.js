import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { setTopnavActiveIndex } from '../../actions/frontend.actions';

function ContactPage(props) {
  
  /* eslint-disable */
  useEffect(() => {
    onMounted(); props.setTopnavActiveIndex(4);
  }, []);
  /* eslint-enable */

  return (
    <section className="auth-01">
      <div className="wrapper">

        <div className="bg-container">
          <div className="wrapper">
            <div 
              className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
              data-aos="fade-up" data-aos-delay="450"
            ></div>
            <div className="hide-mobile">
              <h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
                นำสู่ความปกติใหม่ทางการศึกษา <br />
                (New Normal Education)
              </h4>
            </div>
          </div>
        </div>

        <div className="auth-container md" data-aos="fade-up" data-aos-delay="0">
          <div className="auth-wrapper text-center">
            <img className="img-content" src="/assets/img/logo-ipst-project14.png" alt="Logo" />
            <h5 className="fw-600 mt-6">
              ติดต่อเรา Project 14+
            </h5>
            <p className="lg fw-600 mt-4 mb-3">
              Line Openchat Project 14
            </p>
            <a href="https://line.me/ti/g2/Chl46ZROFBbb3CyuQfSDEw" target="_blank" rel="noreferrer">
							<img className="img-qr-code" src="/assets/img/qr-code-line.jpg" alt="Line QR Code" />
            </a>
            <p className="lg fw-600 mt-3">
              อีเมล <a href="mailto:project14plus@ipst.ac.th" className="color-p">
                <u>project14plus@ipst.ac.th</u>
              </a> <br />
              เพื่อติดตามข่าวสาร สอบถาม และแจ้งปัญหาการใช้งาน
            </p>
          </div>
        </div>

      </div>
    </section>
  );
}

ContactPage.defaultProps = {
	
};
ContactPage.propTypes = {
	setTopnavActiveIndex: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
	setTopnavActiveIndex: setTopnavActiveIndex
})(ContactPage);