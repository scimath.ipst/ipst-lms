import { Link } from 'react-router-dom';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

function ContentCard04(props) {
  return props.channel? (
    <div className="ss-card ss-card-04">
      <Link className="ss-img c-pointer" to={`/channel/${props.channel.url}`}>
        <div className="img-bg" style={{ backgroundImage: `url('${props.channel.image}')` }}></div>
        <div className="hover-container">
          <div className="icon-play">
            <PlayArrowIcon style={{ fontSize: 40 }} />
          </div>
        </div>
      </Link>
      <div className="text-container">
        <Link className="title p lg fw-600 c-pointer" to={`/channel/${props.channel.url}`}>
          {props.channel.name}
        </Link>
        {props.channel.description? (
          <p className="desc sm fw-400 mt-1">
            {props.channel.description}
          </p>
        ): (<></>)}
        <div className="p xs fw-600 mt-2">
          <span className="fw-500 op-80 mr-1">โดย</span> 
          {props.channel.displayDept()} 
        </div>
      </div>
    </div>
  ): props.playlist? (
    <div className="ss-card ss-card-04">
      <Link 
        className="ss-img c-pointer" 
        to={props.playlist.firstContentUrl
          ? `/content/${props.playlist.firstContentUrl}/${props.playlist.url}`
          : `/playlist/${props.playlist.url}`} 
      >
        <div className="img-bg" style={{ backgroundImage: `url('${props.playlist.image}')` }}></div>
        <div className="hover-container">
          <div className="icon-play">
            <PlayArrowIcon style={{ fontSize: 40 }} />
          </div>
        </div>
      </Link>
      <div className="text-container">
        <Link 
          className="title p lg fw-600 c-pointer" 
          to={props.playlist.firstContentUrl
            ? `/content/${props.playlist.firstContentUrl}/${props.playlist.url}`
            : `/playlist/${props.playlist.url}`} 
        >
          {props.playlist.name}
        </Link>
        {props.playlist.description? (
          <p className="desc sm fw-400 mt-1">
            {props.playlist.description}
          </p>
        ): (<></>)}
      </div>
    </div>
  ): (<></>);
}

ContentCard04.defaultProps = {
  channel: false,
  playlist: false
};
ContentCard04.propTypes = {
  
};

export default ContentCard04;