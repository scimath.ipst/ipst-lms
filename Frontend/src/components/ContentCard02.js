import PropTypes from 'prop-types';
import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import AddIcon from '@material-ui/icons/Add';

function ContentCard02(props) {
  const history = useHistory();
  const [mouseState, setMouseState] = useState(false);
  const handleClick = (url) => {
    if(!mouseState) history.push(url);
  };

  return props.forSlider? (
    <div className="ss-card ss-card-02">
      <div 
        className="ss-img bradius c-pointer" 
        onMouseMove={() => setMouseState(true)} 
        onMouseDown={() => setMouseState(false)} 
        onClick={() => handleClick(`/levels/${props.level.url}`)} 
      >
        <div className="img-bg" style={{ backgroundImage: `url('${props.level.image}')` }}></div>
        <div className="hover-container colorful">
          <div className="icon">
            <AddIcon style={{ fontSize: 60 }} />
          </div>
        </div>
      </div>
      <div className="text-center mt-2">
        <div 
          className="p fw-500 c-pointer" 
          onMouseMove={() => setMouseState(true)} 
          onMouseDown={() => setMouseState(false)} 
          onClick={() => handleClick(`/levels/${props.level.url}`)} 
        >
          {props.level.name}
        </div>
      </div>
    </div>
  ): (
    <div className="ss-card ss-card-02">
      <Link className="ss-img bradius" to={`/levels/${props.level.url}`}>
        <div className="img-bg" style={{ backgroundImage: `url('${props.level.image}')` }}></div>
        <div className="hover-container colorful">
          <div className="icon">
            <AddIcon style={{ fontSize: 60 }} />
          </div>
        </div>
      </Link>
      <div className="text-center mt-2">
        <Link className="p fw-500" to={`/levels/${props.level.url}`}>
          {props.level.name}
        </Link>
      </div>
    </div>
  );
}

ContentCard02.defaultProps = {
  forSlider: false
};
ContentCard02.propTypes = {
  forSlider: PropTypes.bool,
	level: PropTypes.object.isRequired
};

export default ContentCard02;