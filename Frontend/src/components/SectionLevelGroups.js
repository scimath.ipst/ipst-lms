import PropTypes from 'prop-types';
import { useEffect } from 'react';
import Slider from 'react-slick';
import ContentCard02 from './ContentCard02';

import { connect } from 'react-redux';
import { frontendLevelList } from '../actions/frontend.actions';

function SectionLevelGroups(props) {
  const sliderSettings = {
    dots: true, arrows: false, infinite: false, speed: 600, autoplaySpeed: 3000,
    slidesToShow: 5, slidesToScroll: 1, initialSlide: 0, swipeToSlide: true,
    responsive: [
      { breakpoint: 1299.98, settings: { slidesToShow: 4 } },
      { breakpoint: 991.98, settings: { slidesToShow: 3 } },
      { breakpoint: 767.98, settings: { slidesToShow: 2 } },
      { breakpoint: 575.98, settings: { slidesToShow: 1 } },
    ]
  };
  
  /* eslint-disable */
  useEffect(() => {
		if(!props.levels.length) props.processList();
  }, []);
  /* eslint-enable */

  return props.levels.length? (
    <section className="intro-01 section-padding pos-relative">
      <div className="container">
        <h4 className="ss-title stripe-success fw-700 ls-1" data-aos="fade-up" data-aos-delay="150">
          ดูตามระดับชั้น
        </h4>
        <div className="grids" data-aos="fade-up" data-aos-delay="300">
          <div className="grid sm-100">
            <div className="slide-container">
              <Slider {...sliderSettings}>
                {props.levels.map((level, i) => (
                  <div className="slide" key={i}>
                    <div className="wrapper">
                      <ContentCard02 forSlider={true} level={level} />
                    </div>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </section>
  ): (<></>);
}

SectionLevelGroups.defaultProps = {
	
};
SectionLevelGroups.propTypes = {
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	levels: state.frontend.levels
});

export default connect(mapStateToProps, {
  processList: frontendLevelList
})(SectionLevelGroups);