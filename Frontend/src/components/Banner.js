import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Slider from 'react-slick';
import { CircularProgress, Avatar, Chip } from '@material-ui/core';

import { connect } from 'react-redux';
import { frontendBannerList } from '../actions/frontend.actions';

function Banner(props) {
  const history = useHistory();
  const [mouseState, setMouseState] = useState(false);

  const handleClick = (url) => {
    if(!mouseState) history.push(url);
  };

  const sliderSettings = {
    dots: true, arrows: true, infinite: true, speed: 600, autoplaySpeed: 3000,
    slidesToShow: 1, slidesToScroll: 1, initialSlide: 0, fade: true
  };

  /* eslint-disable */
  useEffect(() => {
		// if(!props.banners.length) props.processList();
    props.processList();
  }, []);
  /* eslint-enable */

  return (
    <section className="banner-01 v2">
      {!props.banners.length? (
        <div className="loading"><CircularProgress size="48px" thickness={4.4} /></div>
      ): (
        <div className="slide-container">
          <Slider {...sliderSettings}>
            {props.banners.map((c, i) => (
              <div className="slide" key={i}>
                <div className="wrapper">
                  <div className="img-bg" style={{ backgroundImage: `url('${c.image}')` }}></div>
                  <div className="overlay"></div>
                  <div className="container">
                    <div className="d-flex fw-wrap" style={{ maxWidth: 750 }}>
                      {c.levels.map((level, j) => (
                        <div className="mt-1 mr-1" key={`level-${i}-${j}`}>
                          <Chip color="primary" label={level.name} />
                        </div>
                      ))}
                      {c.subject.name? (
                        <div className="mt-1">
                          <Chip color="primary" label={c.subject.name} />
                        </div>
                      ): (<></>)}
                    </div>
                    <div 
                      className="text-wrapper c-pointer" 
                      onMouseMove={() => setMouseState(true)} 
                      onMouseDown={() => setMouseState(false)} 
                      onClick={() => handleClick(`/content/${c.url}`)} 
                    >
                      <h4 className="title fw-600 mt-1">{c.name}</h4>
                      {c.description && <p>{c.description}</p>}
                      <div className="avatar-wrapper mt-2">
                        <Avatar src={c.user.avatar} />
                        <div className="name">{c.user.displayName()}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      )}
    </section>
  );
}

Banner.defaultProps = {
	
};
Banner.propTypes = {
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  banners: state.frontend.banners
});

export default connect(mapStateToProps, {
  processList: frontendBannerList
})(Banner);