import PropTypes from 'prop-types';
import { connect } from 'react-redux';

function Breadcrumb(props) {
  return (
    <section className={`breadcrumb-01 ${props.classer}`}>
      <div className="img-bg img-theme" style={{ 'backgroundImage': `url('${props.background}')` }}></div>
      <div className="container">
        <div className="wrapper">
          {props.title? (
            <p className="title h1 fw-600" data-aos="fade-up" data-aos-delay="0">
              { props.title }
            </p>
          ): (<></>)}
          {props.subtitle? (
            <p className="h6 lg fw-500 text-center mt-1" data-aos="fade-up" data-aos-delay="0">
              { props.subtitle }
            </p>
          ): (<></>)}
        </div>
      </div>
    </section>
  );
}

Breadcrumb.defaultProps = {
  classer: '',
  background: '',
  title: '',
  subtitle: ''
};
Breadcrumb.propTypes = {
	classer: PropTypes.string,
	background: PropTypes.string.isRequired,
	title: PropTypes.string
};

const mapStateToProps = (state) => ({
	
});

export default connect(mapStateToProps, {})(Breadcrumb);