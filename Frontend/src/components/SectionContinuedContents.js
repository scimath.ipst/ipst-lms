import PropTypes from 'prop-types';
import { useEffect } from 'react';
import Slider from 'react-slick';
import ContentCard01 from './ContentCard01';
import { Divider } from '@material-ui/core';

import { connect } from 'react-redux';
import { UserModel } from '../models';
import { frontendContinuedContentList } from '../actions/frontend.actions';

function SectionContinuedContents(props) {
	const user = new UserModel(props.user);

  const sliderSettings = {
    dots: true, arrows: false, infinite: false, speed: 600, autoplaySpeed: 3000,
    slidesToShow: 5, slidesToScroll: 1, initialSlide: 0, swipeToSlide: true,
    responsive: [
      { breakpoint: 1299.98, settings: { slidesToShow: 4 } },
      { breakpoint: 991.98, settings: { slidesToShow: 3 } },
      { breakpoint: 767.98, settings: { slidesToShow: 2 } },
      { breakpoint: 575.98, settings: { slidesToShow: 1 } },
    ]
  };
  
  /* eslint-disable */
  useEffect(() => {
		// if(!props.contents.length) props.processList();
    props.processList();
  }, []);
  /* eslint-enable */

  return user.isSignedIn() && props.contents.length? (
    <>
      <section className="intro-01 section-padding pos-relative">
        <div className="container">
          <h4 className="ss-title stripe-success fw-700 ls-1" data-aos="fade-up" data-aos-delay="150">
            ดูต่อสำหรับคุณ <span className="color-p">{user.firstname} {user.lastname}</span>
          </h4>
          <div className="grids" data-aos="fade-up" data-aos-delay="300">
            <div className="grid sm-100">
              <div className="slide-container">
                <Slider {...sliderSettings}>
                  {props.contents.map((c, i) => (
                    <div className="slide" key={i}>
                      <div className="wrapper">
                        <ContentCard01 forSlider={true} content={c} />
                      </div>
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Divider />
    </>
  ): (<></>);
}

SectionContinuedContents.defaultProps = {
	
};
SectionContinuedContents.propTypes = {
  processList: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user,
	contents: state.frontend.continuedContents
});

export default connect(mapStateToProps, {
  processList: frontendContinuedContentList
})(SectionContinuedContents);