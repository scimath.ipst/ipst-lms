import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { formatNumber, formatDate } from '../helpers/frontend';
import { CircularProgress } from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

function ContentCard03(props) {
  return props.loading? (
    <div className="ss-card ss-card-03">
      <div className="img-container">
        <div className="ss-img adaptive">
          <div className="loading">
            <div className="loading"><CircularProgress size="32px" thickness={4.4} /></div>
          </div>
        </div>
      </div>
      <div className="text-container">
        <h6 className="title p sm fw-500 lh-sm loading-text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
          Sed lacinia lacinia nisi eget tempus.
        </h6>
        <p className="xxs fw-400 mt-1 loading-text">Lorem ipsum</p>
        <p className="xxs fw-400 ws-nowrap">
          <span className="loading-text">Lorem ipsum</span> 
          <span className="text-dot ml-1 mr-1"></span> 
          <span className="loading-text">Loremip</span>
        </p>
      </div>
    </div>
  ): (
    <div className="ss-card ss-card-03">
      <div className="img-container">
        <Link 
          className="ss-img adaptive" 
          to={`/content/${props.content.url}${props.playlistUrl? '/'+props.playlistUrl: ''}`} 
        >
          <div className="img-bg" style={{ backgroundImage: `url('${props.content.image}')` }}></div>
          <div className="hover-container colorful">
            <div className="icon">
              <PlayArrowIcon style={{ fontSize: 40 }} />
            </div>
          </div>
        </Link>
      </div>
      <div className="text-container">
        <Link 
          className="title p sm fw-500 lh-sm" 
          to={`/content/${props.content.url}${props.playlistUrl? '/'+props.playlistUrl: ''}`} 
        >
          {props.content.name}
        </Link>
        <p className="xxs fw-400 mt-1">{props.content.user.displayName()}</p>
        <p className="xxs fw-400 ws-nowrap">
          <span>ผู้ชม {formatNumber(props.content.visitCount, 0)}</span> 
          <span className="text-dot ml-1 mr-1"></span> 
          <span>{formatDate(props.content.createdAt)}</span>
        </p>
      </div>
    </div>
  );
}

ContentCard03.defaultProps = {
  loading: false,
  content: {},
  playlistUrl: ''
};
ContentCard03.propTypes = {
  loading: PropTypes.bool,
	content: PropTypes.object,
  playlistUrl: PropTypes.string
};

export default ContentCard03;