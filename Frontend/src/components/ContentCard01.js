import PropTypes from 'prop-types';
import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { formatNumber, formatSeconds } from '../helpers/frontend';
import { Avatar } from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import WatchLaterIcon from '@material-ui/icons/WatchLater';

function ContentCard01(props) {
  const history = useHistory();
  const [mouseState, setMouseState] = useState(false);
  const handleClick = (url) => {
    if(!mouseState) history.push(url);
  };

  return props.forSlider? (
    <div className="ss-card ss-card-01">
      <div 
        className="ss-img bradius c-pointer" 
        onMouseMove={() => setMouseState(true)} 
        onMouseDown={() => setMouseState(false)} 
        onClick={() => handleClick(`/content/${props.content.url}`)} 
      >
        <div className="img-bg" style={{ backgroundImage: `url('${props.content.image}')` }}></div>
        <div className="hover-container">
          <div className="icon-play">
            <PlayArrowIcon style={{ fontSize: 40 }} />
          </div>
        </div>
        {props.content.watchingSeconds? (
          <div className="continued-time">
            <WatchLaterIcon className="mr-1" style={{ fontSize: 16 }} />
            {formatSeconds(props.content.watchingSeconds)}
          </div>
        ): (<></>)}
      </div>
      <div className="text-container">
        <div className="avatar-container">
          <Avatar src={props.content.user.avatar} className={`sm`} />
        </div>
        <h6 
          className="title p fw-600 c-pointer" 
          onMouseMove={() => setMouseState(true)} 
          onMouseDown={() => setMouseState(false)} 
          onClick={() => handleClick(`/content/${props.content.url}`)} 
        >
          {props.content.name}
        </h6>
        <div className="p xxs fw-500 d-flex ai-center op-80 mt-2">
          {props.content.user.displayName()} 
          <div className="text-dot ml-2 mr-2"></div> 
          ผู้ชม {formatNumber(props.content.visitCount, 0)} คน
        </div>
      </div>
    </div>
  ): (
    <div className="ss-card ss-card-01">
      <Link className="ss-img bradius c-pointer" to={`/content/${props.content.url}`}>
        <div className="img-bg" style={{ backgroundImage: `url('${props.content.image}')` }}></div>
        <div className="hover-container">
          <div className="icon-play">
            <PlayArrowIcon style={{ fontSize: 40 }} />
          </div>
        </div>
        {props.content.watchingSeconds? (
          <div className="continued-time">
            <WatchLaterIcon className="mr-1" style={{ fontSize: 16 }} />
            {formatSeconds(props.content.watchingSeconds)}
          </div>
        ): (<></>)}
      </Link>
      <div className="text-container">
        <div className="avatar-container">
          <Avatar src={props.content.user.avatar} className={`sm`} />
        </div>
        <Link className="title p fw-600 c-pointer" to={`/content/${props.content.url}`}>
          {props.content.name}
        </Link>
        <div className="p xxs fw-500 d-flex ai-center op-80 mt-2">
          {props.content.user.displayName()} 
          <div className="text-dot ml-2 mr-2"></div> 
          ผู้ชม {props.content.visitCount} คน
        </div>
      </div>
    </div>
  );
}

ContentCard01.defaultProps = {
  forSlider: false
};
ContentCard01.propTypes = {
  forSlider: PropTypes.bool,
	content: PropTypes.object.isRequired
};

export default ContentCard01;