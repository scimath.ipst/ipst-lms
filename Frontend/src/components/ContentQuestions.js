import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import {
  Button, Divider, FormControl, RadioGroup, FormControlLabel, Radio
} from '@material-ui/core';

import { connect } from 'react-redux';
import { frontendUserActionResultUpdate } from '../actions/frontend.actions';

function ContentQuestions(props) {
  const [state, setState] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [score, setScore] = useState(0);
  const [maxScore, setMaxScore] = useState(0);
  const [counter, setCounter] = useState(0);

  const onStateChange = (e, state) => {
    e.preventDefault();
    setState(state);
  };
  const onSubmit = (e) => {
    e.preventDefault();
    if(props.userAction.isValid()){
      let temp1 = questions.map(d => d.getScore()).reduce((a, b) => a + b, 0);
      setScore(temp1);
      let temp2 = questions.map(d => d.score).reduce((a, b) => a + b, 0);
      setMaxScore(temp2);
      props.userActionResultUpdate({
        id: props.userAction._id,
        scoreType: props.content.scoreType,
        questions: questions,
        score: temp1,
        maxScore: temp2,
        minScore: props.content.minScore
      });

      if(temp1 < props.content.minScore){
        props.onTriggerRelearn(props.content);
      }
    }
    setState(2);
  };

  const onChoiceChange = (e, i) => {
    e.preventDefault();
    questions[i].setSelectedChoice(parseInt(e.target.value));
    setQuestions(questions);
    setCounter(counter + 1);
  };

  /* eslint-disable */
  useEffect(() => {
    setQuestions(props.content.questions);
  }, [props.content]);
  useEffect(() => {
    if(props.clickQuestion){
      if(state===0) setState(1);
    }
  }, [props.clickQuestion]);
  /* eslint-enable */

  return questions && questions.length 
    && (
      (props.content.isYoutubeVideo() && props.userAction.isWatched()) 
      || !props.content.isYoutubeVideo() 
    )? (
    <>
      {state >= 3? (
        score >= props.content.minScore? (
          <div className="content-footer completed">
            <div className="option">
              <div className="text-wrapper">
                <p className="fw-500">คุณผ่านแบบทดสอบและได้รับ</p>
                {props.content.badgeName? (
                  <p className="h6 fw-500 color-p">{props.content.badgeName}</p>
                ): (<></>)}
              </div>
              <div className="badge-container">
                <img src={props.content.badge} alt="Content Badge" />
              </div>
            </div>
          </div>
        ): (
          <div className="content-footer completed">
            <div className="option">
              <div className="text-wrapper">
                <h6 className="fw-500 pt-6 pb-6 mt-1 mb-1">
                  นักเรียนอาจจะยังไม่ค่อยเข้าใจนะ ลองทบทวนเนื้อหาดูอีกครั้ง
                </h6>
              </div>
            </div>
          </div>
        )
      ): (
        <div className="content-footer">
          <div className="option">
            <Button 
              variant="contained" color="primary" size="large" 
              onClick={e => onStateChange(e, 1)}
            >
              ทำแบบทดสอบ
            </Button>
          </div>
          <div className="option">
            <div className="text-wrapper">
              <p className="fw-500">เมื่อคุณผ่านแบบทดสอบจะได้รับ</p>
              {props.content.badgeName? (
                <p className="h6 fw-500 color-p">{props.content.badgeName}</p>
              ): (<></>)}
            </div>
            <div className="badge-container">
              <img src={props.content.badge} alt="Content Badge" />
            </div>
          </div>
        </div>
      )}
      <Divider />

      <div className={`popup-container ${state === 1 || state === 2? 'active': ''}`}>
        <div className="wrapper">
          <div className="popup-box xxl">
            {state === 1? (
              <>
                <h4 className="sm fw-600 color-p text-center">แบบทดสอบ</h4>
                <form onSubmit={onSubmit}>
                  <div className="question-scroll">
                    {questions.map((q, i) => (
                      <div className="mt-6" key={`quesion-${i}-${q.selectedChoice}`}>
                        <h6 className="fw-500">
                          {q.question} 
                          <span className="text-2xs ml-3">({q.score} คะแนน)</span>
                        </h6>
                        <div className="mt-2 pl-5">
                          <FormControl component="fieldset">
                            <RadioGroup 
                              value={q.selectedChoice} onChange={e => onChoiceChange(e, i)} 
                            >
                              {q.choices.map((choice, j) => (
                                <FormControlLabel 
                                  key={`choice-${i}-${j}`} value={j} 
                                  control={<Radio required={true} />} label={choice.text} 
                                />
                              ))}
                            </RadioGroup>
                          </FormControl>
                        </div>
                      </div>
                    ))}
                  </div>
                  <div className="text-center mt-6">
                    <Button type="submit" variant="contained" color="primary" size="large">
                      ส่งแบบทดสอบ
                    </Button>
                  </div>
                </form>
              </>
            ): (
              <>
                <h4 className="sm fw-600 text-center">
                  คุณได้คะแนน 
                  <span className="color-p ml-3">{score}</span> จาก 
                  <span className="color-p ml-3">{maxScore}</span> คะแนน
                </h4>
                {score >= props.content.minScore? (
                  <h6 className="fw-400 text-center mt-2">
                    ยินดีด้วย คุณผ่านแบบทดสอบ
                  </h6>
                ): (
                  <h6 className="fw-400 text-center sm-no-br mt-2">
                    นักเรียนอาจจะยังไม่ค่อยเข้าใจนะ ลองทบทวนเนื้อหาดูอีกครั้ง <br />
                    แล้วทำให้ได้อย่างน้อย {props.content.minScore} คะแนน
                  </h6>
                )}
                <div className="text-center mt-5">
                  <Button 
                    variant="contained" color="primary" size="large" 
                    onClick={e => onStateChange(e, 3)}
                  >
                    ตกลง
                  </Button>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  ): (<></>);
}

ContentQuestions.defaultProps = {
  clickQuestion: false
};
ContentQuestions.propTypes = {
	content: PropTypes.object.isRequired,
  userActionResultUpdate: PropTypes.func.isRequired,
  clickQuestion: PropTypes.bool,
  onTriggerRelearn: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  userAction: state.frontend.userAction
});

export default connect(mapStateToProps, {
  userActionResultUpdate: frontendUserActionResultUpdate
})(ContentQuestions);