import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Avatar, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import HomeIcon from '@material-ui/icons/Home';
import LayersIcon from '@material-ui/icons/Layers';
import InfoIcon from '@material-ui/icons/Info';
import PhoneIcon from '@material-ui/icons/Phone';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';

import { connect } from 'react-redux';
import { userSignout } from '../actions/user.actions';
import { themeChange } from '../actions/theme.actions';
import { UserModel } from '../models';

function Topnav(props) {
  const history = useHistory();
	const user = new UserModel(props.user);

	const showAlert = true;
	const alertText = 'ขออภัยในความไม่สะดวก เว็บไซต์ปิดปรับปรุงระหว่างวันที่ 10 ตุลาคม พ.ศ.2565';

	const [sidenavActive, setSidenavStatus] = useState(false);
	const [offsetY, setOffsetY] = useState(0);
	const [allMenu, setAllMenu] = useState([]);
	const [classer, setClasser] = useState('');

	function onSignout() {
		props.processSignout();
		history.push('/auth/signin');
	};
	function onMenuClick(e, url) {
		e.preventDefault();
		history.push(url);
		setSidenavStatus(false);
		setClasser('collapse');
		setTimeout(() => setClasser(''), 750);
	};

	const backToTop = () => {
		window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
	};

  /* eslint-disable */
	useEffect(() => {
    const unlisten = history.listen(() => {
      window.scrollTo(0, 0);
    });
		return async () => {
      unlisten();
    };
  }, []);
	useEffect(() => {
		setOffsetY(window.pageYOffset)
		const handleScroll = () => {
			setOffsetY(window.pageYOffset)
		}
		window.addEventListener('scroll', handleScroll)
		return function cleanup() {
      window.removeEventListener('scroll', handleScroll)
    };
  }, []);
  useEffect(async () => {
		setAllMenu([
			{ name: 'หน้าแรก', to: '/', icon: <HomeIcon />, activeIndex: 1 },
			{ name: 'ระดับชั้น', to: '/levels', icon: <LayersIcon />, activeIndex: 2 },
			{ name: 'เลือกรายการ', to: '/channels', icon: <OndemandVideoIcon />, activeIndex: 5 },
			{ name: 'เกี่ยวกับเรา', to: '/about', icon: <InfoIcon />, activeIndex: 3 },
			{ name: 'ติดต่อเรา', to: '/contact', icon: <PhoneIcon />, activeIndex: 4 },
		]);
  }, []);
  /* eslint-enable */
	
  return (
		<>
			<link rel="stylesheet" type="text/css" href={props.theme.path} />
			
			<nav className={`topnav ${offsetY > 50? 'sticky': ''}`}>
				<div className="container">
					<div className="options">

						<div className="option">

							<div className="menu-option show-mobile" onClick={() => setSidenavStatus(true)}>
								<div className={`hamburger ${sidenavActive? 'active': ''}`}>
									<div></div><div></div><div></div>
								</div>
							</div>

							<div className="logo c-pointer" onClick={e => onMenuClick(e, '/')}>
								<h6>Project 14+</h6>
								<p>นำสู่ความปกติใหม่ทางการศึกษา</p>
							</div>

							<div className="menu-container hide-tablet">
								{allMenu.map((menu, i) => (
									!menu.children
									? (<div className={`menu ${menu.activeIndex === props.activeIndex? 'active': ''}`} key={i}>
										<div onClick={e => onMenuClick(e, menu.to)}>
											{menu.name}
											</div>
									</div>)
									: (<div className={`menu has-children ${menu.activeIndex === props.activeIndex? 'active': ''}`} key={i}>
										<div className="c-pointer" onClick={e => onMenuClick(e, menu.to)}>
											{menu.name} 
											<div className="chev">
												<KeyboardArrowDownIcon style={{ fontSize: 17 }} />
											</div>
										</div>
										<div className={`submenu-container ${classer}`}>
											{menu.children.map((submenu, j) => (
												<div className="submenu" key={i+'_'+j}>
													<div className="c-pointer" onClick={e => onMenuClick(e, submenu.to)}>
														{submenu.name}
													</div>
												</div>
											))}
										</div>
									</div>)
								))}
							</div>
						
						</div>

						<div className="option">
							<div className="theme-toggle mr-2 hide-mobile">
								<input 
									className="toggle-input" type="checkbox" checked={props.theme.type === 'night'} 
									onChange={(event) => props.processThemeChange(event.target.checked? 'night': 'day') }
								/>
								<div className="toggle-bg"></div>
								<div className="toggle-switch">
									<div className="toggle-switch-figure"></div>
									<div className="toggle-switch-figure-alt"></div>
								</div>  
							</div>
							{user.isSignedIn()? (
								<div className="avatar-container">
									<Avatar src={user.avatar} className="sm" />
									<div className="dropdown">
										<List>
											<ListItem className="d-block">
												<p className="text-line fw-500">{user.displayName()}</p>
												<p className="text-line xs fw-400">{user.email}</p>
											</ListItem>
											<ListItem className="c-pointer" button onClick={e => onMenuClick(e, '/profile')}>
												<ListItemIcon>
													<AccountCircleIcon />
												</ListItemIcon>
												<ListItemText primary="ข้อมูลส่วนตัว" />
											</ListItem>
											<ListItem button onClick={onSignout}>
												<ListItemIcon>
													<ExitToAppIcon />
												</ListItemIcon>
												<ListItemText primary="ออกจากระบบ" />
											</ListItem>
										</List>
									</div>
								</div>
							): (
								<div className="btn btn-action c-pointer" onClick={e => onMenuClick(e, '/auth/signin')}>
									เข้าสู่ระบบ
								</div>
							)}
						</div>

					</div>
				</div>
			</nav>
			{showAlert && alertText? (
				<div className="alert-area">
					<div className="container">
						<p className="fw-400">{alertText}</p>
					</div>
				</div>
			): (<></>)}
			
			<nav className={`sidenav ${sidenavActive? 'active': ''}`}>
				<div className="wrapper">
					<div className="options">
						<div className="theme-toggle">
							<input 
								className="toggle-input" type="checkbox" checked={props.theme.type === 'night'} 
								onChange={(event) => props.processThemeChange(event.target.checked? 'night': 'day') }
							/>
							<div className="toggle-bg"></div>
							<div className="toggle-switch">
								<div className="toggle-switch-figure"></div>
								<div className="toggle-switch-figure-alt"></div>
							</div>  
						</div>
						<div className="menu-option" onClick={() => setSidenavStatus(false)}>
							<div className={`hamburger ${sidenavActive? 'active': ''}`}>
								<div></div><div></div><div></div>
							</div>
						</div>
					</div>
					<div className="scroll-wrapper" data-simplebar>
						<div className="menu-container hide-tablet">
							{allMenu.map((menu, i) => (
								!menu.children
								? (<div className={`menu ${menu.activeIndex === props.activeIndex? 'active': ''}`} key={i}>
									<div onClick={e => onMenuClick(e, menu.to)}>
										<div className="icon">{menu.icon}</div>
										{menu.name}
									</div>
								</div>)
								: (<div className={`menu has-children ${menu.activeIndex === props.activeIndex? 'active': ''}`} key={i}>
									<div onClick={e => onMenuClick(e, menu.to)}>
										<div className="icon">{menu.icon}</div>
										{menu.name} 
										<div className="chev">
											<KeyboardArrowDownIcon style={{ fontSize: 18 }} />
										</div>
									</div>
									<div className="submenu-container">
										{menu.children.map((submenu, j) => (
											<div className="submenu" key={i+'_'+j}>
												<div className="c-pointer" onClick={e => onMenuClick(e, submenu.to)}>
													{submenu.name}
												</div>
											</div>
										))}
									</div>
								</div>)
							))}
						</div>
					</div>
				</div>
			</nav>
			<div className="sidenav-filter" onClick={() => setSidenavStatus(false)}></div>
		
			<div className={`back-to-top ${offsetY > 50? 'active': ''}`} onClick={backToTop}>
				<KeyboardArrowUpIcon style={{ fontSize: 32 }} />
			</div>
		</>
  );
}

Topnav.defaultProps = {
	
};
Topnav.propTypes = {
	user: PropTypes.object.isRequired,
	processSignout: PropTypes.func.isRequired,
	theme: PropTypes.object.isRequired,
	processThemeChange: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user,
	theme: state.theme,
	activeIndex: state.frontend.topnavActiveIndex,
	levels: state.frontend.levels,
});

export default connect(mapStateToProps, {
	processSignout: userSignout, 
	processThemeChange: themeChange
})(Topnav);
