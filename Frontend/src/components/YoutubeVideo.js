import PropTypes from 'prop-types';
import { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button, FormControl, RadioGroup, FormControlLabel, Radio, LinearProgress
} from '@material-ui/core';
import YouTube from 'react-youtube';

import { connect } from 'react-redux';
import { frontendUserActionUpdate } from '../actions/frontend.actions';

function useInterval(callback, delay) {
  const intervalRef = useRef();
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  useEffect(() => {
    if(typeof delay === 'number'){
      intervalRef.current = window.setInterval(() => callbackRef.current(), delay);
      return () => window.clearInterval(intervalRef.current);
    }
  }, [delay]);
  
  return intervalRef;
}

function YoutubeVideo(props) {
  const history = useHistory();
  
  // https://developers.google.com/youtube/player_parameters
  const options = {
    playerVars: {
      autoplay: false
    },
  };
  
  const [isValid, setIsValid] = useState(false);
  const [isContinue, setIsContinue] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [Player, setPlayer] = useState(null);
  const [setup, setSetup] = useState(true);

  const playerRef = useRef();
  var playerState;
  var currentTime = 0;
  var isEnded = false;
  var questioning = false;

  const goingNextRef = useRef();
  const [goingNext, setGoingNext] = useState(false);

  const goingNextQuestionRef = useRef();
  const [goingNextQuestion, setGoingNextQuestion] = useState(false);

  const onContinue = (e) => {
    e.preventDefault();
    if(playerRef && playerRef.current && playerRef.current.internalPlayer){
      setIsContinue(0);
      playerRef.current.internalPlayer.playVideo();
    }
  };
  const onRewatch = (e) => {
    e.preventDefault();
    if(playerRef && playerRef.current && playerRef.current.internalPlayer){
      setIsContinue(0);
      playerRef.current.internalPlayer.seekTo(0);
      playerRef.current.internalPlayer.playVideo();
    }
  };

  const onGoingNextCancel = (e) => {
    e.preventDefault();
    setGoingNext(false);
  };
  const onGoingNextQuestionCancel = (e) => {
    e.preventDefault();
    setGoingNextQuestion(false);
  };
  const onGoingNextQuestion = (e) => {
    e.preventDefault();
    setGoingNextQuestion(false);
    if(props.onClickQuestion) props.onClickQuestion(e);
  };
  const onEnd = async () => {
    if(!isEnded){
      isEnded = true;
      if(playerRef && playerRef.current && playerRef.current.internalPlayer){
        var duration = await playerRef.current.internalPlayer.getDuration();
        if(props.userAction.isValid() && !props.userAction.isWatched()){
          props.userActionUpdate({
            id: props.userAction._id,
            watchingSeconds: duration,
            status: 2
          });
        }
      }

      if(props.relatedContents && props.relatedContents.length){
        var nextIndex = 0;
        if(!props.content.questions || !props.content.questions.length){
          if(props.playlistUrl){
            props.relatedContents.forEach((d, i) => {
              if(d._id === props.content._id){
                nextIndex = i + 1;
              }
            });
            if(nextIndex < props.relatedContents.length){
              setGoingNext(true);
              setTimeout(function(){
                if(goingNextRef.current.className.indexOf('active') > -1){
                  history.push('/content/'+props.relatedContents[nextIndex].url+'/'+props.playlistUrl);
                }
              }, 4000);
            }
          }else{
            setGoingNext(true);
            setTimeout(function(){
              if(goingNextRef.current.className.indexOf('active') > -1){
                history.push('/content/'+props.relatedContents[0].url);
              }
            }, 4000);
          }
        }else{
          if(props.playlistUrl){
            props.relatedContents.forEach((d, i) => {
              if(d._id === props.content._id){
                nextIndex = i + 1;
              }
            });
            if(nextIndex < props.relatedContents.length){
              setGoingNextQuestion(true);
              setTimeout(function(){
                if(goingNextQuestionRef.current.className.indexOf('active') > -1){
                  history.push('/content/'+props.relatedContents[nextIndex].url+'/'+props.playlistUrl);
                }
              }, 10000);
            }
          }else{
            setGoingNextQuestion(true);
            setTimeout(function(){
              if(goingNextQuestionRef.current.className.indexOf('active') > -1){
                history.push('/content/'+props.relatedContents[0].url);
              }
            }, 10000);
          }
        }
      }
    }
  };

  const onUnload = async () => {
    if(props.userAction.isValid() && !props.userAction.isWatched() && Player){
      let watchingSeconds = await Player.getCurrentTime();
      if(props.userAction.isValid() && !props.userAction.isWatched()){
        props.userActionUpdate({
          id: props.userAction._id,
          watchingSeconds: watchingSeconds,
          status: 1
        });
      }
    }
  };

  const onChoiceChange = (e, i) => {
    e.preventDefault();
    questions[i].setSelectedChoice(parseInt(e.target.value));
    setQuestions(questions);
    setCounter(counter + 1);
  };
  const onQuestionSubmit = (e, i) => {
    e.preventDefault();
    questions[i].setState(2);
    setQuestions(questions);
    setCounter(counter + 1);
  };
  const onQuestionComplete = async (e, i) => {
    e.preventDefault();
    questions[i].setState(3);
    setQuestions(questions);
    let goTo = questions[i].goTo();
    if(goTo){
      let t = await playerRef.current.internalPlayer.getCurrentTime();
      if(goTo > t) playerRef.current.internalPlayer.seekTo(goTo);
    }
    playerRef.current.internalPlayer.playVideo();
    setCounter(counter + 1);
  };

  // Video Logic
  const [counter, setCounter] = useState(0);
  useInterval(async () => {
    setCounter(counter + 1);
    if(!Player){
      if(playerRef && playerRef.current && playerRef.current.internalPlayer){
        setPlayer(playerRef.current.internalPlayer);
      }
    }else{
      playerState = await Player.getPlayerState();
      currentTime = await Player.getCurrentTime();

      if(props.userAction.isValid() && !props.userAction.isWatched()){
        if(setup){
          setSetup(false);
          if(props.userAction.isWatching()){
            await Player.seekTo(props.userAction.watchingSeconds);
            await Player.pauseVideo();
          }
        }

        if(playerState === 1 && !questioning && questions.length){
          for(let i=0; i<questions.length; i++){
            if(
              questions[i]['state'] === 0 
              && currentTime > questions[i]['at'] 
              && currentTime < questions[i]['at']+5 
            ){
              questioning = true;
              Player.pauseVideo();

              questions[i].setState(1);
              setQuestions(questions);

              break;
            }
          }
        }
      }
    }
  }, 500);

  /* eslint-disable */
  useEffect(() => {
    if(!isValid && props.content.isValid() && props.content.youtubeVideoId 
    && props.userAction.isValid()){
      setIsValid(true);
      currentTime = props.userAction.watchingSeconds;
      setQuestions(props.content.videoQuestions);
      if(props.userAction.isWatched()){
        setIsContinue(2);
      }else if(props.userAction.isWatching()){
        setIsContinue(1);
      }
    }
  }, [props.content, props.userAction]);
  useEffect(() => {
    window.removeEventListener('beforeunload', onUnload);
    window.addEventListener('beforeunload', onUnload);

    return async () => {
      if(props.userAction.isValid() && !props.userAction.isWatched() && Player){
        let watchingSeconds = await Player.getCurrentTime();
        props.userActionUpdate({
          id: props.userAction._id,
          watchingSeconds: watchingSeconds,
          status: 1
        });
      }
      window.removeEventListener('beforeunload', onUnload);
    };
  }, [Player]);
  /* eslint-enable */

  return (
    <>
      {isValid? (
        <>
          <div className="video-container">
            <div className="wrapper">
              <YouTube 
                ref={playerRef} containerClassName="youtube-video" 
                videoId={props.content.youtubeVideoId} 
                opts={options} onEnd={onEnd} 
              />
              {isContinue === 1? (
                <div className="continue-container" style={{ backgroundImage: `url('${props.content.image}')` }}>
                  <div className="filter">
                    <Button variant="contained" color="primary" size="large" onClick={onContinue}>
                      ดูวิดีโอต่อ
                    </Button>
                  </div>
                </div>
              ): isContinue === 2? (
                <div className="continue-container" style={{ backgroundImage: `url('${props.content.image}')` }}>
                  <div className="filter">
                    <Button variant="contained" color="primary" size="large" onClick={onRewatch}>
                      ดูวิดีโอใหม่
                    </Button>
                  </div>
                </div>
              ): (<></>)}
            </div>
          </div>
          {questions.map((q, i) => (
            <div 
              className={`popup-container ${q.state === 1 || q.state === 2? 'active': ''}`} 
              key={`question-${i}`}
            >
              <div className="wrapper">
                <div className="popup-box xl">
                  {q.state === 1? (
                    <>
                      <h5 className="fw-500">{q.question}</h5>
                      <div className="mt-2 pl-5">
                        <form onSubmit={e => onQuestionSubmit(e, i)}>
                          <FormControl component="fieldset">
                            <RadioGroup 
                              value={q.selectedChoice} onChange={e => onChoiceChange(e, i)} 
                            >
                              {q.choices.map((choice, j) => (
                                <FormControlLabel 
                                  key={`choice-${i}-${j}`} value={j} 
                                  control={<Radio required={true} />} label={choice.text} 
                                />
                              ))}
                            </RadioGroup>
                          </FormControl>
                          <div className="mt-5">
                            <Button type="submit" variant="contained" color="primary" size="large">
                              ตอบคำถาม
                            </Button>
                          </div>
                        </form>
                      </div>
                    </>
                  ): (
                    <>
                      <h5 className="fw-600">
                        {q.isCorrect()? 'คุณตอบถูก': 'คุณตอบผิด'}
                      </h5>
                      {q.selectedExplanation()? (
                        <p className="h6 mt-3">{q.selectedExplanation()}</p>
                      ): (<></>)}
                      <div className="mt-6">
                        <Button 
                          variant="contained" color="primary" size="large" 
                          onClick={e => onQuestionComplete(e, i)} 
                        >
                          ดูวิดีโอต่อ
                        </Button>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
          ))}
        </>
      ): (<></>)}

      {/* Auto Play Without Questions */}
      <div ref={goingNextRef} className={`popup-container ${goingNext? 'active': ''}`}>
        <div className="wrapper">
          <div className="popup-box md">
            <h6 className="fw-600 text-center">
              กำลังที่จะเล่นเนื้อหาต่อไป
            </h6>
            <div className="mt-3">
              <LinearProgress variant="determinate" value={0} style={{ '--loading': '4s' }} />
            </div>
            <div className="text-center mt-6">
              <Button 
                variant="outlined" color="primary" size="large" 
                onClick={e => onGoingNextCancel(e)} 
              >
                ยกเลิก
              </Button>
            </div>
          </div>
        </div>
      </div>

      {/* Auto Play With Questions */}
      <div ref={goingNextQuestionRef} className={`popup-container ${goingNextQuestion? 'active': ''}`}>
        <div className="wrapper">
          <div className="popup-box md">
            <h6 className="fw-600 text-center">
              กดปุ่มเพื่อเริ่มทำแบบทดสอบ <br />
              หรือรอเพื่อเล่นเนื้อหาต่อไป
            </h6>
            <div className="text-center mt-3">
              <Button 
                variant="contained" color="primary" size="large" className="mr-3" 
                onClick={e => onGoingNextQuestion(e)} 
              >
                เริ่มทำแบบทดสอบ
              </Button>
              <Button 
                variant="outlined" color="primary" size="large" 
                onClick={e => onGoingNextQuestionCancel(e)} 
              >
                ยกเลิก
              </Button>
            </div>
            <div className="mt-6">
              <LinearProgress variant="determinate" value={0} style={{ '--loading': '10s' }} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

YoutubeVideo.defaultProps = {
  playlistUrl: ''
};
YoutubeVideo.propTypes = {
  content: PropTypes.object.isRequired,
  userAction: PropTypes.object.isRequired,
  userActionUpdate: PropTypes.func.isRequired,
  playlistUrl: PropTypes.string,
  onClickQuestion: PropTypes.func
};

const mapStateToProps = (state) => ({
  relatedContents: state.frontend.relatedContents
});

export default connect(mapStateToProps, {
  userActionUpdate: frontendUserActionUpdate
})(YoutubeVideo);