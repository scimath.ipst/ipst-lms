require('dotenv').config();


verifyPublicKey = async (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'].split(' ');
    
    if(authHeader.length != 2 || authHeader[0] != 'Bearer') {
      return res.status(403).send({message: 'No public key provided.'});
    }else if(authHeader[1] != process.env.PUBLIC_KEY) {
      return res.status(500).send({message: 'Invalid public key.'});
    }
    return next();
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


const auth = {
  verifyPublicKey
};

module.exports = auth;