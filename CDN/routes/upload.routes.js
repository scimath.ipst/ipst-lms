module.exports = function(app) {
  var router = require('express').Router();
  const { auth } = require('../middlewares');
  const UploadController = require('../controllers/upload.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    req.header('Content-Type', 'multipart/form-data');
    next();
  });


  router.post(
    '/',
    [ auth.verifyPublicKey ],
    UploadController.uploadFile
  );
  router.delete(
    '/',
    [ auth.verifyPublicKey ],
    UploadController.deleteFile
  );


  app.use('/upload', router);
};