const util = require('util');
const multer = require('multer');
const path = require('path');
const fs = require('fs');

require('dotenv').config();


// Upload Service
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, process.env.UPLOAD_DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
    cb(null, fileName)
  },
});
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if(file.mimetype && ['image/png', 'image/jpg', 'image/jpeg'].indexOf(file.mimetype) > -1) {
      cb(null, true);
    } else {
      cb(null, false);
      req.validationError = true;
      return cb(null, false, 'Forbidden file extension.');
    }
  }
}).single('file');
const UploadService = util.promisify(upload);


// Routes
exports.uploadFile = async (req, res) => {
  try {
    await UploadService(req, res);
    if(!req.file) {
      return res.status(403).send({message: 'Failed to upload.'});
    }
    if(req.validationError) {
      return res.status(403).send({message: 'Forbidden file extension.'});
    };

    const imgUrl = `${process.env.SERVER_URL}${req.file.filename}`;
    return res.status(200).send({result: imgUrl});
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  };
};

exports.deleteFile = async (req, res) => {
  const { files } = req.body;
	try {
		files.map(file => {
			if (file.indexOf(process.env.API_URL) > -1) {
				let path = file.replace(process.env.API_URL, '');
				try {
					fs.unlinkSync(`public/${path}`);
				} catch (err) {
					console.log(err);
				}
			}
		});
    return res.status(200).send({result: true});
	} catch (error) {
    return res.status(500).send({message: 'Internal server error.'});
	}
};
