import { 
  UserModel, PlaylistModel, TagModel, LevelModel, SubjectModel, 
  VideoQuestionModel, QuestionModel 
} from '.';

export class ContentModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.user = new UserModel(data.user? data.user: {});
    this.playlist = new PlaylistModel(data.playlist? data.playlist: {});

    this.levels = data.levels && data.levels.length? data.levels.map(d => new LevelModel(d)): [];
    this.subject = new SubjectModel(data.subject? data.subject: {});
    this.tags = data.tags && data.tags.length ? data.tags.map(d => new TagModel(d)): [];

    this.name = data.name? data.name: null;
    this.url = data.url? data.url: null;
    this.description = data.description? data.description: null;
    this.metadesc = data.metadesc? data.metadesc: null;
    this.keywords = data.keywords? data.keywords: [];
    this.image = data.image? data.image: '/assets/img/default/img.jpg';
    
    this.type = data.type? data.type: 1;
    this.youtubeVideoId = data.youtubeVideoId? data.youtubeVideoId: null;
    this.filePath = data.filePath? data.filePath: null;

    // this.videoQuestions = data.videoQuestions? data.videoQuestions: [];
    this.videoQuestions = data.videoQuestions
      ? data.videoQuestions.map(d => new VideoQuestionModel(d)): [];

    this.scoreType = data.scoreType? Number(data.scoreType): 0;
    // this.questions = data.questions? data.questions: [];
    this.questions = data.questions
      ? data.questions.map(d => new QuestionModel(d)): [];
    this.minScore = data.minScore? Number(data.minScore): 0;
    this.badgeName = data.badgeName? data.badgeName: null;
    this.badge = data.badge? data.badge: '/assets/img/default/img.jpg';

    this.approver = new UserModel(data.approver? data.approver: {});
    this.approvedStatus = data.approvedStatus? Number(data.approvedStatus): 0;

    this.isFeatured = data.isFeatured? Number(data.isFeatured): 0;
    this.visitCount = data.visitCount? Number(data.visitCount): 0;
    this.order = data.order? data.order: 0;
    this.status = data.status? data.status: 0;
    
    this.createdAt = data.createdAt? data.createdAt: null;
    this.updatedAt = data.updatedAt? data.updatedAt: null;
  }
  
  isValid() { return this._id !== null; }

  displayStatus(){
    if(this.status === 3) return 'Public';
    else if(this.status === 2) return 'Private';
    else if(this.status === 1) return 'Drafted';
    else return 'Unlisted';
  }

  displayPlaylist(){
    return this.playlist.name;
  }
  displayChannel(){
    if(this.playlist.channel) return this.playlist.channel.name;
    else return '';
  }
  displayDept(){
    if(this.playlist.channel && this.playlist.channel.department){
      return this.playlist.channel.department.name;
    }else return '';
  }
}
