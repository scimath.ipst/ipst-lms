import { UserModel, ContentModel } from '.';

export class ApprovedContentModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.approver = new UserModel(data.approver? data.approver: {});
    this.content = new ContentModel(data.content? data.content: {});
    this.approvedStatus = data.approvedStatus? Number(data.approvedStatus): 0;
    this.comment = data.comment? data.comment: null;
    this.createdAt = data.createdAt? data.createdAt: null;
  }
}
