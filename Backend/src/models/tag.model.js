export class TagModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.name = data.name? data.name: null;
    this.status = data.status? data.status: 0;
  }
}
