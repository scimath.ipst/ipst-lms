export class DepartmentModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.name = data.name? data.name: null;
    this.status = data.status? data.status: 0;
    this.members = data.members? data.members: [];
    this.totalMembers = data.totalMembers? data.totalMembers: 0;
  }
}
