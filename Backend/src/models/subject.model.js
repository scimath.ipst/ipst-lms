export class SubjectModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.name = data.name? data.name: null;
    this.url = data.url? data.url: null;
    this.description = data.description? data.description: null;
    this.metadesc = data.metadesc? data.metadesc: null;
    this.keywords = data.keywords? data.keywords: [];
    this.image = data.image? data.image: '/assets/img/default/img.jpg';
    this.order = data.order? data.order: 0;
    this.status = data.status? data.status: 0;
  }
}
