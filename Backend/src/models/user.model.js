import { UserRoleModel, DepartmentModel, TagModel, LevelModel, SubjectModel } from '.';

export class UserModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.externalId = data.externalId? data.externalId: null;
    
    this.firstname = data.firstname? data.firstname: null;
    this.lastname = data.lastname? data.lastname: null;
    this.username = data.username? data.username: null;
    this.email = data.email? data.email: null;
    this.avatar = data.avatar? data.avatar: '/assets/img/avatar/default.jpg';
    this.detail = data.detail? data.detail: {};

    this.role = new UserRoleModel(data.role? data.role: {});
    this.department = new DepartmentModel(data.department? data.department: {});
    this.status = data.status? data.status: 0;

    this.level = new LevelModel(data.level? data.level: {});
    this.subjects = data.subjects && data.subjects.length 
      ? data.subjects.map(d => new SubjectModel(d)): [];
    this.tags = data.tags && data.tags.length ? data.tags.map(d => new TagModel(d)): [];
    
    this.accessToken = data.accessToken? data.accessToken: null;
    this.refreshToken = data.refreshToken? data.refreshToken: null;
  }

  displayName() {
    if(this.firstname || this.lastname) {
      return this.firstname + ' ' + this.lastname;
    } else if (this.username) {
      return this.username;
    } else {
      return 'General User';
    }
  }
  displayRole() {
    if(this.role && this.role.name) {
      return this.role.name;
    } else {
      return null;
    }
  }
  displayDepartment() {
    if(this.department && this.department.name) {
      return this.department.name;
    } else {
      return null;
    }
  }

  isSignedIn() {
    return this._id && this.accessToken && this.refreshToken? true: false;
  }
  isRegistered() {
    return this._id? true: false;
  }

  isCreator() {
    return this.role && this.role.level >= 1? true: false;
  }
  isManager() {
    return this.role && this.role.level >= 2? true: false;
  }
  isQCO() {
    return this.role && this.role.level >= 3? true: false;
  }
  isAdmin() {
    return this.role && this.role.level >= 4? true: false;
  }
}
