export class PaginateModel {
  constructor(data) {
    this.page = data.page? Number(data.page): 1;
    this.pp = data.pp? Number(data.pp): 10;
    this.keyword = data.keyword? data.keyword: '';
    this.total = data.total? Number(data.total): 0;
    this.totalPages = data.totalPages? Number(data.totalPages): 0;
  }

  anchorId() {
    return (this.page - 1) * this.pp + 1;
  }
}
