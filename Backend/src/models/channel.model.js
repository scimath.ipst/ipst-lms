import { DepartmentModel } from '.';

export class ChannelModel {
  constructor(data) {
    this._id = data._id? data._id: null;
    this.department = new DepartmentModel(data.department? data.department: {});
    this.name = data.name? data.name: null;
    this.url = data.url? data.url: null;
    this.description = data.description? data.description: null;
    this.metadesc = data.metadesc? data.metadesc: null;
    this.keywords = data.keywords? data.keywords: [];
    this.image = data.image? data.image: '/assets/img/default/img.jpg';
    this.order = data.order? data.order: 0;
    this.status = data.status? data.status: 0;
  }

  displayStatus(){
    if (this.status === 3) return 'Public';
    else if (this.status === 2) return 'Private';
    else if (this.status === 1) return 'Drafted';
    else return 'Unlisted';
  }

  displayName() {
    return this.name? this.name: '';
  }
  displayDeptChannel() {
    if (this.department.name) {
      return `${this.department.name} - ${this.name}`;
    } else return this.name;
  }
}
