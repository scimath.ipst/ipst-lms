import CryptoJS from 'crypto-js';
import { TOKEN_KEY } from '../actions/types';

export function apiHeader() {
  let user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(user) {
    user = JSON.parse(user);
    if(user.accessToken) {
      let bytes = CryptoJS.AES.decrypt(user.accessToken, TOKEN_KEY);
      let accessToken = bytes.toString(CryptoJS.enc.Utf8);
      return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      };
    } else {
      return { 'Content-Type': 'application/json' };
    }
  } else {
    return { 'Content-Type': 'application/json' };
  }
}

export function apiHeaderFormData(){
  let user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(user) {
    user = JSON.parse(user);
    return {
      'Authorization': `Bearer ${process.env.REACT_APP_CDN_PUBLIC_KEY}`
    };
  } else {
    return {};
  }
}
