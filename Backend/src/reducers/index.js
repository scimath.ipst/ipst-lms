import { combineReducers } from 'redux';
import alertReducer from './alert.reducer';
import userReducer from './user.reducer';
import generalReducer from './general.reducer';
import adminReducer from './admin.reducer';

export default combineReducers({
  alert: alertReducer,
  user: userReducer,
  general: generalReducer,
  admin: adminReducer
});