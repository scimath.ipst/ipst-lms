import { USER_SIGNIN, USER_SIGNOUT, USER_UPDATE, USER_UPDATE_DEPT } from '../actions/types';
import { 
  UserModel, DepartmentModel, LevelModel, SubjectModel, TagModel 
} from '../models';

const tempUser = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
const initialState = new UserModel(tempUser? JSON.parse(tempUser): {});
var temp;

const userReducer = (state = initialState, action) => {
  switch(action.type) {

    case USER_SIGNIN:
      temp = new UserModel(action.payload);
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(temp));
      return { ...state, ...temp };

    case USER_SIGNOUT:
      temp = new UserModel({});
      localStorage.removeItem(`${process.env.REACT_APP_PREFIX}_USER`);
      return { ...state, ...temp };

    case USER_UPDATE:
      temp = JSON.parse(localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`));
      if(action.payload.department) temp.department = new DepartmentModel(action.payload.department);
      if(action.payload.firstname) temp.firstname = action.payload.firstname;
      if(action.payload.lastname) temp.lastname = action.payload.lastname;
      if(action.payload.username) temp.username = action.payload.username;
      if(action.payload.email) temp.email = action.payload.email;
      if(action.payload.avatar) temp.avatar = action.payload.avatar;
      if(action.payload.level) temp.level = new LevelModel(action.payload.level);
      if(action.payload.subjects) {
        temp.subjects = action.payload.subjects.map(d => new SubjectModel(d));
      }
      if(action.payload.tags) {
        temp.tags = action.payload.tags.map(d => new TagModel(d));
      }
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(temp));
      return { ...state, ...temp };

    case USER_UPDATE_DEPT:
      temp = JSON.parse(localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`));
      temp.department = new DepartmentModel(action.payload);
      localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(temp));
      return { ...state, ...temp };

    default:
      return state;
  }
};

export default userReducer;