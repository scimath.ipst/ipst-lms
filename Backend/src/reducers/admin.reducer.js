import { ADMIN_USER_LIST, ADMIN_USER_READ, ADMIN_ROLE_LIST } from '../actions/types';
import { PaginateModel, UserModel } from '../models';

const initialState = {
  users: {
    paginate: new PaginateModel({}),
    result: []
  },
  user: new UserModel({}),
  roles: []
};

const adminReducer = (state = initialState, action) => {
  switch(action.type) {

    case ADMIN_USER_LIST:
      return {...state, users: action.payload };
    case ADMIN_USER_READ:
      return {...state, user: action.payload };

    case ADMIN_ROLE_LIST:
      return {...state, roles: action.payload };

    default:
      return state;
  }
};

export default adminReducer;