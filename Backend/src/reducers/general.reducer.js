import {
  USER_LIST, USER_READ,
  CHANNEL_LIST, CHANNEL_READ,
  PLAYLIST_LIST, PLAYLIST_READ,
  CONTENT_LIST, CONTENT_PAGINATE, 
  CONTENT_READ, APPROVED_CONTENT_LIST,
  TAG_LIST, TAG_READ, 
  LEVEL_LIST, LEVEL_READ,
  SUBJECT_LIST, SUBJECT_READ,
  DEPT_LIST, DEPT_READ 
} from '../actions/types';
import { 
  UserModel, ChannelModel, PlaylistModel, ContentModel,
  TagModel, LevelModel, SubjectModel, DepartmentModel 
} from '../models';

const initialState = {
  users: [],
  user: new UserModel({}),
  channels: [],
  channel: new ChannelModel({}),
  playlists: [],
  playlist: new PlaylistModel({}),
  contents: [],
  content: new ContentModel({}),
  contentPaginate: {},
  approvedContents: [],
  tags: [],
  tag: new TagModel({}),
  levels: [],
  level: new LevelModel({}),
  subjects: [],
  subject: new SubjectModel({}),
  departments: [],
  department: new DepartmentModel({})
};

const generalReducer = (state = initialState, action) => {
  switch(action.type) {

    case USER_LIST:
      return {...state, users: action.payload };
    case USER_READ:
      return {...state, user: action.payload };

    case CHANNEL_LIST:
      return {...state, channels: action.payload };
    case CHANNEL_READ:
      return {...state, channel: action.payload };

    case PLAYLIST_LIST:
      return {...state, playlists: action.payload };
    case PLAYLIST_READ:
      return {...state, playlist: action.payload };

    case CONTENT_LIST:
      return {...state, contents: action.payload };
    case CONTENT_PAGINATE:
      return {...state, contentPaginate: action.payload };
    case CONTENT_READ:
      return {...state, content: action.payload };
    case APPROVED_CONTENT_LIST:
      return {...state, approvedContents: action.payload };

    case TAG_LIST:
      return {...state, tags: action.payload };
    case TAG_READ:
      return {...state, tag: action.payload };

    case LEVEL_LIST:
      return {...state, levels: action.payload };
    case LEVEL_READ:
      return {...state, level: action.payload };

    case SUBJECT_LIST:
      return {...state, subjects: action.payload };
    case SUBJECT_READ:
      return {...state, subject: action.payload };

    case DEPT_LIST:
      return {...state, departments: action.payload };
    case DEPT_READ:
      return {...state, department: action.payload };

    default:
      return state;
  }
};

export default generalReducer;