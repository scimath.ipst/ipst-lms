import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { onMounted, formatNumber } from '../../helpers/frontend';

import {
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Button
} from '@material-ui/core';

import { connect } from 'react-redux';
import { ContentModel, UserModel } from '../../models';
import { 
  managerDashboardCountContents, managerDashboardMostVisitedContents, managerDashboardUsers
} from '../../actions/manager.actions';

function ManagerDashboardPage(props) {
  const [countContents, setCountContents] = useState({ 
    unlisted: 0, drafted: 0, private: 0, public: 0 
  });
  const [mostVisitedContents, setMostVisitedContents] = useState([]);
  const [users, setUsers] = useState([]);

  /* eslint-disable */
  useEffect(async () => {
    onMounted();
    let result1 = await props.processCountContents();
    if(result1) setCountContents(result1);
    let result2 = await props.processMostVisitedContents();
    if(result2) setMostVisitedContents(result2.map(d => new ContentModel(d)));
    let result3 = await props.processUsers();
    if(result3) setUsers(result3.map(d => new UserModel(d)));
  }, []);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="Dashboard" />

        <div className="grids">
          <div className="grid xl-25 sm-50 mt-0">
            <div className="app-card" data-aos="fade-up" data-aos-delay="150">
              <h3 className="fw-600"># Unlisted Contents</h3>
              <h2 className="fw-500 lh-3xs text-right mt-3">
                {formatNumber(countContents.unlisted, 0)}
              </h2>
            </div>
          </div>
          <div className="grid xl-25 sm-50 xl-mt-0 lg-mt-0 md-mt-0 sm-mt-0">
            <div className="app-card" data-aos="fade-up" data-aos-delay="150">
              <h3 className="fw-600"># Drafted Contents</h3>
              <h2 className="fw-500 lh-3xs text-right mt-3">
                {formatNumber(countContents.drafted, 0)}
              </h2>
            </div>
          </div>
          <div className="grid xl-25 sm-50 xl-mt-0">
            <div className="app-card" data-aos="fade-up" data-aos-delay="300">
              <h3 className="fw-600"># Private Contents</h3>
              <h2 className="fw-500 lh-3xs text-right mt-3">
                {formatNumber(countContents.private, 0)}
              </h2>
            </div>
          </div>
          <div className="grid xl-25 sm-50 xl-mt-0">
            <div className="app-card" data-aos="fade-up" data-aos-delay="450">
              <h3 className="fw-600"># Public Contents</h3>
              <h2 className="fw-500 lh-3xs text-right mt-3">
                {formatNumber(countContents.public, 0)}
              </h2>
            </div>
          </div>

        </div>

        <div className="grids">
          <div className="grid xl-75 md-60 sm-100">
            <div className="app-card" style={{height: '100%'}} data-aos="fade-up" data-aos-delay="600">
              <h3 className="fw-600">Contents ในหน่วยงาน</h3>
              <TableContainer>
                <Table size="medium">
                  <TableHead>
                    <TableRow>
                      <TableCell className="ws-nowrap" align="center" padding="none">
                        ID
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        รูปภาพ
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        Content
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        ลิงค์
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        ผู้ชม
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!mostVisitedContents.length? (
                      <TableRow>
                        <TableCell colSpan={5} align="center">
                          ไม่พบข้อมูลในระบบ
                        </TableCell>
                      </TableRow>
                    ): mostVisitedContents.map((d, i) => {
                      return (
                        <TableRow key={`most-visited-content-${i}`} hover>
                          <TableCell align="center">
                            {i+1}
                          </TableCell>
                          <TableCell align="left">
                            <div className="img-preview">
                              <div className="img-bg" style={{ backgroundImage: `url('${d.image}')` }}></div>
                            </div>
                          </TableCell>
                          <TableCell align="left">
                            {d.name}
                          </TableCell>
                          <TableCell align="left" className="ws-nowrap">
                            {d.url}
                          </TableCell>
                          <TableCell align="left">
                            {formatNumber(d.visitCount, 0)}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              <div className="text-right mt-4">
                <Button 
                  component={Link} to="/manager/contents" variant="outlined" 
                  color="primary" size="medium" 
                >ดูทั้งหมด</Button>
              </div>
            </div>
          </div>
          <div className="grid xl-25 md-40 sm-100">
            <div className="app-card" style={{height: '100%'}} data-aos="fade-up" data-aos-delay="600">
              <h3 className="fw-600">บัญชีผู้ใช้</h3>
              <TableContainer>
                <Table size="medium">
                  <TableHead>
                    <TableRow>
                      <TableCell className="ws-nowrap" align="center" padding="none">
                        ID
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        ชื่อผู้ใช้
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!users.length? (
                      <TableRow>
                        <TableCell colSpan={2} align="center">
                          ไม่พบข้อมูลในระบบ
                        </TableCell>
                      </TableRow>
                    ): users.map((d, i) => {
                      return (
                        <TableRow key={`most-visited-content-${i}`} hover>
                          <TableCell align="center">
                            {i+1}
                          </TableCell>
                          <TableCell align="left">
                            {d.displayName()}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              <div className="text-right mt-4">
                <Button 
                  component={Link} to="/manager/users" variant="outlined" 
                  color="primary" size="medium" 
                >ดูทั้งหมด</Button>
              </div>
            </div>
          </div>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ManagerDashboardPage.defaultProps = {
	activeIndex: 0
};
ManagerDashboardPage.propTypes = {
	activeIndex: PropTypes.number,
  processCountContents: PropTypes.func.isRequired,
  processMostVisitedContents: PropTypes.func.isRequired,
  processUsers: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  processCountContents: managerDashboardCountContents,
  processMostVisitedContents: managerDashboardMostVisitedContents,
  processUsers: managerDashboardUsers
})(ManagerDashboardPage);