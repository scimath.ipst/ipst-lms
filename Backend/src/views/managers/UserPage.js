import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Avatar, Tabs, Tab, Chip
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { onMounted, formatDate } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { UserModel } from '../../models';
import { creatorUserRead } from '../../actions/creator.actions';
import { 
  memberTagList, memberLevelList, memberSubjectList, memberBadgeList
} from '../../actions/member.actions';

function ManagerUserPage(props) {
  const history = useHistory();
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  const [tabValue, setTabValue] = useState(0);

  const [values, setValues] = useState(new UserModel({status: 1}));
  const [levelId, setLevelId] = useState('');
  const [badges, setBadges] = useState([]);

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(process !== 'view') history.push('/manager/users');
    if(dataId) props.processRead({id: dataId});
    props.tagList({ keyword: '' });
    props.levelList({ keyword: '' });
    props.subjectList({ keyword: '' });
  }, []);
  useEffect(() => {
    if(dataId && ['view'].indexOf(process) > -1) {
      setValues(props.single);
      if(props.single.level._id) setLevelId(props.single.level._id);
      props.badgeList({ id: dataId }).then(d => setBadges(d));
    }
  }, [props.single]);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}บัญชีผู้ใช้`} 
          back="/manager/users" 
        />

        <div className="app-card app-card-header" data-aos="fade-up" data-aos-delay="150">
          <Tabs
            value={tabValue} onChange={(e, val) => setTabValue(val)} 
            indicatorColor="primary" textColor="primary" variant="scrollable" 
            scrollButtons="auto" aria-label="scrollable auto tabs example" 
          >
            <Tab label="บัญชีผู้ใช้" id="tab-0" aria-controls="tabpanel-0" />
            <Tab label="ความสนใจ" id="tab-1" aria-controls="tabpanel-1" />
            {process === 'view' && (
              <Tab label="เหรียญที่ได้รับ" id="tab-2" aria-controls="tabpanel-2" />
            )}
          </Tabs>
        </div>
        <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">

          {/* User Tab 1 */}
          {tabValue === 0? (
            <div className="grids ai-center">
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="ชื่อจริง" variant="outlined" fullWidth={true} 
                  value={values.firstname? values.firstname: ''} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="นามสกุล" variant="outlined" fullWidth={true} 
                  value={values.lastname? values.lastname: ''} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="sep"></div>
              
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="ชื่อผู้ใช้" variant="outlined" fullWidth={true} 
                  value={values.username? values.username: ''} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="อีเมล" variant="outlined" fullWidth={true} 
                  value={values.email? values.email: ''} type="email" 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="sep"></div>

              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="บทบาท" variant="outlined" fullWidth={true} 
                  value={values.displayRole()? values.displayRole(): ''} type="text" 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="หน่วยงาน" variant="outlined" fullWidth={true} 
                  value={values.displayDepartment()? values.displayDepartment(): ''} type="text" 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="sep"></div>
              
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="สถานะ" variant="outlined" fullWidth={true} 
                  value={values.status? 'เปิดใช้งาน': 'ปิดใช้งาน'} type="text" 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <Button 
                  variant="outlined" component="label" color="primary" 
                  disabled={process === 'view'} 
                >
                  <Avatar src={values.avatar} />
                  <span className="ml-3">เปลี่ยนรูปโปรไฟล์</span>
                </Button>
              </div>
            </div>
          ): (<></>)}

          {/* User Tab 2 */}
          {tabValue === 1? (
            <div className="grids">
              <div className="grid xl-2-3 lg-90 md-80 sm-100">
                <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                  <InputLabel id="input-level">ระดับชั้น</InputLabel>
                  <Select
                    labelId="input-level" label="ระดับชั้น" 
                    value={props.levels.length && levelId? levelId: ''} 
                  >
                    {props.levels.map((d, i) => (
                      <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
              <div className="grid xl-2-3 lg-90 md-80 sm-100">
                <Autocomplete
                  multiple id="input-subjects" 
                  value={values.subjects.length? values.subjects: []} 
                  options={props.subjects} filterSelectedOptions 
                  getOptionLabel={(option) => option.name} 
                  getOptionSelected={(option, value) => option._id === value._id}
                  fullWidth={true} disabled={process === 'view'} 
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="วิชาที่สนใจ" />
                  )}
                />
              </div>
              <div className="grid xl-2-3 lg-90 md-80 sm-100">
                <Autocomplete
                  multiple id="input-tags" 
                  value={values.tags.length? values.tags: []} 
                  options={props.tags} filterSelectedOptions 
                  getOptionLabel={(option) => option.name} 
                  getOptionSelected={(option, value) => option._id === value._id}
                  fullWidth={true} disabled={process === 'view'} 
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="แท็กที่สนใจ" />
                  )}
                />
              </div>
            </div>
          ): (<></>)}

          {/* User Tab 3 */}
          {process === 'view' && tabValue === 2? (
            <div className="grids">
              {!badges.length? (
                <div className="grid sm-100">
                  <p className="h6 fw-500">ไม่พบเหรียญที่ได้รับ</p>
                </div>
              ): (
                badges.map((b, i) => (
                  <div className="grid xl-20 lg-25 md-1-3 sm-50 xs-50" key={i}>
                    <div className="badge-container">
                      <div className="wrapper">
                        <div className="inner-wrapper">
                          <img src={b.getImage()} alt="Badge Icon" />
                        </div>
                      </div>
                    </div>
                    <p className="color-p fw-500 text-center mt-2">
                      {b.getName()}
                    </p>
                    <p className="xxs fw-500 text-center">
                      {formatDate(b.updatedAt)}
                    </p>
                  </div>
                ))
              )}
            </div>
          ): (<></>)}
          
          {/* User Buttons */}
          <div className="grids">
            <div className="grid sm-100">
              <div className="mt-2">
                <Button 
                  component={Link} to="/manager/users" 
                  variant="outlined" color="primary" size="large"
                >
                  ย้อนกลับ
                </Button>
              </div>
            </div>
          </div>

        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ManagerUserPage.defaultProps = {
	activeIndex: 5
};
ManagerUserPage.propTypes = {
	activeIndex: PropTypes.number,
  tagList: PropTypes.func.isRequired,
  levelList: PropTypes.func.isRequired,
  subjectList: PropTypes.func.isRequired,
  badgeList: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  single: state.general.user,
  tags: state.general.tags,
  levels: state.general.levels,
  subjects: state.general.subjects
});

export default connect(mapStateToProps, {
  tagList: memberTagList,
  levelList: memberLevelList,
  subjectList: memberSubjectList,
  badgeList: memberBadgeList,
  processRead: creatorUserRead
})(ManagerUserPage);