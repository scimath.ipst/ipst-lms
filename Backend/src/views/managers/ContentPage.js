import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import YoutubeVideo from '../../components/YoutubeVideo';
import { 
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Chip, Tabs, Tab,
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination, 
  InputAdornment, OutlinedInput, Switch, Dialog, DialogActions, DialogContent, DialogTitle
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { onMounted, formatDate, formatSeconds } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { FRONTEND_URL } from '../../actions/types';
import { ContentModel } from '../../models';
import { memberUploadFile } from '../../actions/member.actions';
import { memberTagList, memberLevelList, memberSubjectList } from '../../actions/member.actions';
import { creatorApprovedContentList } from '../../actions/creator.actions';
import { 
  managerPlaylistList, 
  managerContentRead, managerContentAdd, managerContentEdit,
  managerApprovedContentAdd
} from '../../actions/manager.actions';

function ManagerContentPage(props) {
  const history = useHistory();
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [tabValue, setTabValue] = useState(0);
  
  const [values, setValues] = useState(new ContentModel({status: 1}));
  const [playlistId, setPlaylistId] = useState('');
  const [subjectId, setSubjectId] = useState('');
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };
  
  const onFileChange = (key) => async (e) => {
    let temp = await props.uploadFile(e.target.files[0]);
    if(temp) onChangeInput(key, temp);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(!values.name || !values.url || !values.levels.length || !playlistId || !subjectId) setTabValue(0);
    else if(
      !values.type || (values.type === 1 && !values.youtubeVideoId) 
      || (values.type !== 1 && !values.filePath)
    ) setTabValue(1);
    else {
      if(process === 'add') {
        let result = await props.processAdd({
          ...values, playlistId: playlistId, subjectId: subjectId
        });
        if(result) history.push('/manager/contents');
      } else if(process === 'edit') {
        await props.processEdit({
          ...values, playlistId: playlistId, subjectId: subjectId
        });
        // if(result) history.push('/manager/contents');
      }
    }
  };

  // Video Question Process
  const onAddVideoQuestion = (e) => {
    e.preventDefault();
    let temp = values.videoQuestions;
    temp.push({ 
      question: '', at: 0, 
      choices: [ { text: '', isCorrect: false, goTo: undefined, explaination: '' } ] 
    });
    setValues({ ...values, videoQuestions: temp });
  };
  const onDeleteVideoQuestion = (e, key) => {
    e.preventDefault();
    let temp = values.videoQuestions;
    temp.splice(key, 1);
    setValues({ ...values, videoQuestions: temp });
  };
  const onChangeVideoQuestion = (key, attr, val) => {
    let temp = values.videoQuestions;
    temp[key][attr] = val;
    setValues({ ...values, videoQuestions: temp });
  };
  const onAddChoiceVideoQuestion = (key) => {
    let temp = values.videoQuestions;
    temp[key].choices.push({ text: '', isCorrect: false, goTo: undefined, explaination: '' });
    setValues({ ...values, videoQuestions: temp });
  };
  const onDeleteChoiceVideoQuestion = (e, key, key2) => {
    e.preventDefault();
    let temp = values.videoQuestions;
    temp[key]['choices'].splice(key2, 1);
    setValues({ ...values, videoQuestions: temp });
  };
  const onChangeChoiceVideoQuestion = (key, key2, attr, val) => {
    let temp = values.videoQuestions;
    temp[key]['choices'][key2][attr] = val;
    setValues({ ...values, videoQuestions: temp });
  };

  // Question Process
  const onAddQuestion = (e) => {
    e.preventDefault();
    let temp = values.questions;
    temp.push({ 
      question: '', score: 1, 
      choices: [ { text: '', isCorrect: false } ] 
    });
    setValues({ ...values, questions: temp });
  };
  const onDeleteQuestion = (e, key) => {
    e.preventDefault();
    let temp = values.questions;
    temp.splice(key, 1);
    setValues({ ...values, questions: temp });
  };
  const onChangeQuestion = (key, attr, val) => {
    let temp = values.questions;
    temp[key][attr] = val;
    setValues({ ...values, questions: temp });
  };
  const onAddChoiceQuestion = (key) => {
    let temp = values.questions;
    temp[key].choices.push({ text: '', isCorrect: false });
    setValues({ ...values, questions: temp });
  };
  const onDeleteChoiceQuestion = (e, key, key2) => {
    e.preventDefault();
    let temp = values.questions;
    temp[key]['choices'].splice(key2, 1);
    setValues({ ...values, questions: temp });
  };
  const onChangeChoiceQuestion = (key, key2, attr, val) => {
    let temp = values.questions;
    temp[key]['choices'][key2][attr] = val;
    setValues({ ...values, questions: temp });
  };

  // Contetn Quality
  const [paginate, setPaginate] = useState({ page: 1, pp: 3 });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const [modalQualityOpen, setModalQualityOpen] = useState(false);
  const [approvedStatus, setApprovedStatus] = useState('');
  const [approvedComment, setApprovedComment] = useState('');
  const onModalQualityOpen = (e) => {
    if(e) e.preventDefault(); 
    setModalQualityOpen(true);
    setApprovedStatus(''); setApprovedComment('');
    props.approvedContentList({ id: props.single._id });
  };
  const onModalQualityClose = (e) => {
    if(e) e.preventDefault(); 
    setModalQualityOpen(false);
    setApprovedStatus(''); setApprovedComment('');
  };
  const onQualitySubmit = async (e) => {
    e.preventDefault();
    let result = await props.approvedContentAdd({
      contentId: props.single._id,
      approvedStatus: approvedStatus,
      comment: approvedComment
    });
    if(result) onModalQualityClose();
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(dataId) props.processRead({id: dataId});
    props.playlistList({ keyword: '' });
    props.tagList({ keyword: '' });
    props.levelList({ keyword: '' });
    props.subjectList({ keyword: '' });
  }, []);
  useEffect(() => {
    if(dataId && ['view', 'edit'].indexOf(process) > -1) {
      setValues(props.single);
      if(props.single.playlist._id) setPlaylistId(props.single.playlist._id);
      if(props.single.subject._id) setSubjectId(props.single.subject._id);
    }
  }, [props.single]);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'} Content`} 
          back="/manager/contents" 
        />

        <div className="app-card app-card-header" data-aos="fade-up" data-aos-delay="150">
          <Tabs
            value={tabValue} onChange={(e, val) => setTabValue(val)} 
            indicatorColor="primary" textColor="primary" variant="scrollable" 
            scrollButtons="auto" aria-label="scrollable auto tabs example" 
          >
            <Tab label="ทั่วไป" id="tab-0" aria-controls="tabpanel-0" />
            <Tab label="เนื้อหา" id="tab-1" aria-controls="tabpanel-1" />
            <Tab label="คำถามท้ายเนื้อหา" id="tab-2" aria-controls="tabpanel-2" />
          </Tabs>
        </div>
        <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>

            {/* Content Tab 1 */}
            {tabValue === 0? (
              <div className="grids">
                <div className="grid xl-2-3 md-70 sm-100 mt-0">
                  <div className="grids">
                    <div className="grid sm-100">
                      <TextField 
                        required={true} label="ชื่อ Content" variant="outlined" fullWidth={true} 
                        value={values.name? values.name: ''} 
                        onChange={e => onChangeInput('name', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid sm-100">
                      <FormControl fullWidth variant="outlined">
                        <InputLabel htmlFor="input-url">ลิงค์ *</InputLabel>
                        <OutlinedInput
                          id="input-url"
                          required={true} label="ลิงค์ *" variant="outlined" fullWidth={true} 
                          value={values.url? values.url: ''} 
                          onChange={e => onChangeInput('url', e.target.value)} 
                          disabled={process === 'view'} 
                          startAdornment={
                            <InputAdornment position="start" className="op-60">
                              {FRONTEND_URL}content/
                            </InputAdornment>
                          } 
                        />
                      </FormControl>
                    </div>
                    <div className="sep"></div>

                    <div className="grid sm-100">
                      <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                        <InputLabel id="input-playlist">Playlist *</InputLabel>
                        <Select
                          required={true} labelId="input-playlist" label="Playlist *" 
                          value={props.playlists.length? playlistId: ''} 
                          onChange={e => setPlaylistId(e.target.value)} 
                        >
                          {props.playlists.map((d, i) => (
                            <MenuItem key={i} value={d._id}>{d.displayChannelPlaylist()}</MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </div>
                    <div className="sep"></div>

                    <div className="grid sm-100">
                      <Autocomplete
                        multiple id="input-levels" 
                        value={values.levels.length? values.levels: []} 
                        options={props.levels} filterSelectedOptions 
                        getOptionLabel={(option) => option.name} 
                        getOptionSelected={(option, value) => option._id === value._id}
                        onChange={(e, val) => onChangeInput('levels', val)} 
                        fullWidth={true} disabled={process === 'view'} 
                        renderTags={(value, getTagProps) =>
                          value.map((option, index) => (
                            <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                          ))
                        }
                        renderInput={(params) => (
                          <TextField 
                            {...params} variant="outlined" label="ระดับชั้น" 
                            required={values.levels.length === 0} 
                            InputLabelProps={{ required: true }} 
                          />
                        )}
                      />
                    </div>
                    <div className="grid sm-100">
                      <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                        <InputLabel id="input-subject">วิชา *</InputLabel>
                        <Select
                          required={true} labelId="input-subject" label="วิชา *" 
                          value={props.subjects.length && subjectId? subjectId: ''} 
                          onChange={e => setSubjectId(e.target.value)} 
                        >
                          {props.subjects.map((d, i) => (
                            <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </div>
                    <div className="grid sm-100">
                      <Autocomplete
                        multiple id="input-tags" 
                        value={values.tags.length? values.tags: []} 
                        options={props.tags} filterSelectedOptions 
                        getOptionLabel={(option) => option.name} 
                        getOptionSelected={(option, value) => option._id === value._id}
                        onChange={(e, val) => onChangeInput('tags', val)} 
                        fullWidth={true} disabled={process === 'view'} 
                        renderTags={(value, getTagProps) =>
                          value.map((option, index) => (
                            <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                          ))
                        }
                        renderInput={(params) => (
                          <TextField {...params} variant="outlined" label="แท็ก" />
                        )}
                      />
                    </div>
                    <div className="sep"></div>
                    
                    <div className="grid sm-100">
                      <TextField 
                        label="คำบรรยาย" variant="outlined" fullWidth={true} 
                        multiline={true} minRows={2} maxRows={4} 
                        value={values.description? values.description: ''} 
                        onChange={e => onChangeInput('description', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <TextField 
                        label="คำบรรยาย Meta" variant="outlined" fullWidth={true} 
                        multiline={true} minRows={2} maxRows={4} 
                        value={values.metadesc? values.metadesc: ''} 
                        onChange={e => onChangeInput('metadesc', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    <div className="grid sm-100">
                      <Autocomplete
                        multiple freeSolo id="input-keywords" 
                        value={values.keywords} options={[]} 
                        onChange={(e, val) => onChangeInput('keywords', val)} 
                        fullWidth={true} disabled={process === 'view'} 
                        renderTags={(value, getTagProps) =>
                          value.map((option, index) => (
                            <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                          ))
                        }
                        renderInput={(params) => (
                          <TextField {...params} variant="outlined" label="คำค้นหา" />
                        )}
                      />
                    </div>
                    <div className="sep"></div>

                    <div className="grid sm-50">
                      <TextField 
                        label="ลำดับ" variant="outlined" fullWidth={true} 
                        value={values.order? values.order: 0} type="number" 
                        onChange={e => onChangeInput('order', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid sm-50">
                      <FormControl 
                        variant="outlined" fullWidth={true} 
                        disabled={process === 'view'} 
                      >
                        <InputLabel id="input-status">สถานะ</InputLabel>
                        <Select
                          labelId="input-status" label="สถานะ" 
                          value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                        >
                          <MenuItem value={0}>Unlisted</MenuItem>
                          <MenuItem value={1}>Drafted</MenuItem>
                          <MenuItem value={2}>Private</MenuItem>
                          <MenuItem value={3}>Public</MenuItem>
                        </Select>
                      </FormControl>
                    </div>
                  </div>
                </div>

                <div className="grid xl-25 md-30 sm-45 mt-0">
                  <div className="grids">
                    <div className="grid sm-100">
                      <div className="input-upload">
                        <div className="wrapper">
                          <div className="img-wrapper">
                            {values.image? (
                              <div className="img-bg" style={{ backgroundImage: `url('${values.image}')` }}></div>
                            ): (
                              <div className="img-bg" style={{ backgroundImage: `url('/assets/img/default/img.jpg')` }}></div>
                            )}
                          </div>
                          {process === 'view'? <template></template>: (
                            <p className="fw-400 mt-3">คลิกเพื่อ Upload รูปภาพ</p>
                          )}
                        </div>
                        {process === 'view'? <template></template>: (
                          <input 
                            type="file" accept="image/png, image/gif, image/jpeg" 
                            onChange={onFileChange('image')} 
                          />
                        )}
                      </div>
                    </div>
                    
                    <div className="grid sm-100">
                      <TextField 
                        label="ชื่อเหรียญ" variant="outlined" fullWidth={true} 
                        value={values.badgeName? values.badgeName: ''} 
                        onChange={e => onChangeInput('badgeName', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>

                    <div className="grid sm-100">
                      <div className="input-upload">
                        <div className="wrapper">
                          <div className="img-wrapper">
                            {values.badge? (
                              <div className="img-bg" style={{ backgroundImage: `url('${values.badge}')` }}></div>
                            ): (
                              <div className="img-bg" style={{ backgroundImage: `url('/assets/img/default/img.jpg')` }}></div>
                            )}
                          </div>
                          {process === 'view'? <template></template>: (
                            <p className="fw-400 mt-3">คลิกเพื่อ Upload รูปเหรียญ</p>
                          )}
                        </div>
                        {process === 'view'? <template></template>: (
                          <input 
                            type="file" accept="image/png, image/gif, image/jpeg" 
                            onChange={onFileChange('badge')} 
                          />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ): (<></>)}

            {/* Content Tab 2 */}
            {tabValue === 1? (
              <div className="grids">
                <div className="grid xl-1-3 lg-40 md-50 sm-75">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="input-type">ประเภท *</InputLabel>
                    <Select
                      required={true} labelId="input-type" label="ประเภท *" 
                      value={values.type} onChange={e => onChangeInput('type', e.target.value)} 
                    >
                      <MenuItem value={1}>Youtube video</MenuItem>
                      <MenuItem value={2}>Video MP4</MenuItem>
                      <MenuItem value={3}>PDF</MenuItem>
                      <MenuItem value={4}>iFrame</MenuItem>
                    </Select>
                  </FormControl>
                </div>

                {values.type === 1? (
                  <>
                    <div className="grid xl-2-3 lg-60 sm-100">
                      <TextField 
                        required={true} label="Youtube Video ID" variant="outlined" 
                        type="text" fullWidth={true} 
                        value={values.youtubeVideoId? values.youtubeVideoId: ''} 
                        onChange={e => onChangeInput('youtubeVideoId', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    {values.youtubeVideoId? (
                      <div className="grid xl-2-3 lg-60 md-60 sm-80">
                        <YoutubeVideo videoId={values.youtubeVideoId}></YoutubeVideo>
                      </div>
                    ): (<></>)}
                    <div className="sep"></div>

                    <div className="sep mt-4"></div>
                    <div className="grid xl-2-3 lg-80 sm-100">
                      <div className="d-flex ai-center">
                        <h6 className="fw-600 mr-4">คำถามวิดีโอ</h6>
                        {['add', 'edit'].indexOf(process) > -1? (
                          <Button 
                            variant="contained" color="primary" size="small" startIcon={<AddIcon />} 
                            onClick={onAddVideoQuestion} 
                          >เพิ่มคำถาม</Button>
                        ): (<></>)}
                      </div>
                    </div>
                    {values.videoQuestions.map((d, i) => (
                      <div className="grid sm-100" key={`video-question-${i}`}>
                        <TableContainer>
                          <table className="table table-question">
                            <tbody>
                              <tr>
                                <td className="num-q" rowSpan="4">{i+1}</td>
                                <td className="col-title">คำถาม</td>
                                <td colSpan="2">
                                  <TextField 
                                    required={true} variant="outlined" type="text" fullWidth={true} 
                                    value={d.question} size="small" disabled={process === 'view'} 
                                    onChange={e => onChangeVideoQuestion(i, 'question', e.target.value)} 
                                  />
                                </td>
                                <td className="pr-4" align="right" width="100">
                                  {['add', 'edit'].indexOf(process) > -1? (
                                    <Button 
                                      variant="contained" color="secondary" size="small" startIcon={<DeleteIcon />} 
                                      onClick={e => onDeleteVideoQuestion(e, i)} 
                                    >ลบ</Button>
                                  ): (<></>)}
                                </td>
                              </tr>
                              <tr>
                                <td className="col-title">เมื่อเวลา</td>
                                <td>
                                  <div className="d-flex ai-center">
                                    <TextField 
                                      required={true} variant="outlined" type="number" fullWidth={false} 
                                      value={d.at} size="small" disabled={process === 'view'}  
                                      onChange={e => onChangeVideoQuestion(i, 'at', e.target.value)} 
                                      InputProps={{
                                        endAdornment: <InputAdornment position="end">วินาที</InputAdornment>,
                                      }}
                                    />
                                    <p className="fw-500 ml-3">
                                      {formatSeconds(values.videoQuestions[i]['at'])}
                                    </p>
                                  </div>
                                </td>
                                <td colSpan="2"></td>
                              </tr>
                              {['add', 'edit'].indexOf(process) > -1? (
                                <tr>
                                  <td className="col-title">คำตอบ</td>
                                  <td className="pt-1 pb-1">
                                    <Button 
                                      variant="contained" color="primary" size="small" startIcon={<AddIcon />} 
                                      onClick={() => onAddChoiceVideoQuestion(i)} 
                                    >เพิ่มคำตอบ</Button>
                                  </td>
                                  <td colSpan="2"></td>
                                </tr>
                              ): (<></>)}
                              <tr>
                                <td colSpan="4">
                                  <TableContainer>
                                    <Table size="small" className={`table-choice`}>
                                      <TableHead>
                                        <TableRow>
                                          <TableCell>#</TableCell>
                                          <TableCell style={{paddingRight: 0}}>คำตอบ *</TableCell>
                                          <TableCell style={{paddingRight: 0}} width="90">คำตอบถูก</TableCell>
                                          <TableCell style={{paddingRight: 0}} width="150">ข้ามไปที่</TableCell>
                                          <TableCell style={{paddingRight: 0}}>คำเฉลย</TableCell>
                                          <TableCell width="100"></TableCell>
                                        </TableRow>
                                      </TableHead>
                                      <TableBody>
                                        {d.choices.map((k, j) => (
                                          <TableRow key={`video-question-choice-${i}-${j}`}>
                                            <TableCell>{j+1}</TableCell>
                                            <TableCell style={{paddingRight: 0}}>
                                              <TextField 
                                                required={true} variant="outlined" type="text" fullWidth={true} 
                                                value={k.text} size="small" disabled={process === 'view'} 
                                                onChange={e => onChangeChoiceVideoQuestion(i, j, 'text', e.target.value)} 
                                              />
                                            </TableCell>
                                            <TableCell style={{paddingRight: 0}}>
                                              <Switch 
                                                checked={k.isCorrect} color="primary" 
                                                onChange={
                                                  (e) => {
                                                    if(['add', 'edit'].indexOf(process) > -1) {
                                                      return onChangeChoiceVideoQuestion(i, j, 'isCorrect', e.target.checked);
                                                    } else {
                                                      return true;
                                                    }
                                                  }
                                                } 
                                              />
                                            </TableCell>
                                            <TableCell style={{paddingRight: 0}}>
                                              <TextField 
                                                variant="outlined" type="number" fullWidth={false} 
                                                value={k.goTo} size="small" disabled={process === 'view'} 
                                                onChange={e => onChangeChoiceVideoQuestion(i, j, 'goTo', e.target.value)} 
                                                InputProps={{
                                                  endAdornment: <InputAdornment position="end">วินาที</InputAdornment>,
                                                }}
                                              />
                                              <p className="fw-400 mt-1">
                                                {formatSeconds(values.videoQuestions[i]['choices'][j]['goTo'])}
                                              </p>
                                            </TableCell>
                                            <TableCell style={{paddingRight: 0}}>
                                              <TextField 
                                                variant="outlined" type="text" fullWidth={true} 
                                                value={k.explaination} size="small" disabled={process === 'view'} 
                                                onChange={e => onChangeChoiceVideoQuestion(i, j, 'explaination', e.target.value)} 
                                                multiline rows={2} 
                                              />
                                            </TableCell>
                                            <TableCell align="right">
                                              {['add', 'edit'].indexOf(process) > -1? (
                                                <Button 
                                                  variant="contained" color="secondary" size="small" startIcon={<DeleteIcon />} 
                                                  onClick={e => onDeleteChoiceVideoQuestion(e, i, j)} 
                                                >ลบ</Button>
                                              ): (<></>)}
                                            </TableCell>
                                          </TableRow>
                                        ))}
                                      </TableBody>
                                    </Table>
                                  </TableContainer>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </TableContainer>
                      </div>
                    ))}
                    <div className="sep mb-4"></div>
                  </>
                ): (
                  <>
                    <div className="grid xl-2-3 lg-60 sm-100">
                      <TextField 
                        required={true} label="Content URL" variant="outlined" 
                        type="text" fullWidth={true} 
                        value={values.filePath? values.filePath: ''} 
                        onChange={e => onChangeInput('filePath', e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                    {values.filePath && (values.filePath.indexOf('http://')>-1 || values.filePath.indexOf('https://')>-1)? (
                      <>
                        <div className="grid xl-2-3 lg-60 md-60 sm-80">
                          <div className="video-container">
                            <div className="wrapper">
                              <iframe src={values.filePath} title="Content iFrame"></iframe>
                            </div>
                          </div>
                        </div>
                        <div className="sep"></div>
                      </>
                    ): (<></>)}
                  </>
                )}

              </div>
            ): (<></>)}

            {/* Content Tab 3 */}
            {tabValue === 2? (
              <div className="grids">
                <div className="grid xl-1-3 lg-40 md-50 sm-75">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="input-score-type">การเก็บคะแนน</InputLabel>
                    <Select
                      required={true} labelId="input-score-type" label="การเก็บคะแนน" 
                      value={values.scoreType} onChange={e => onChangeInput('scoreType', e.target.value)} 
                    >
                      <MenuItem value={0}>ไม่เก็บคะแนน</MenuItem>
                      <MenuItem value={1}>เก็บคะแนนสูงสุด</MenuItem>
                      <MenuItem value={2}>เก็บคะแนนครั้งล่าสุด</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-1-3 lg-40 md-50 sm-75">
                  <TextField 
                    label="คะแนนขั้นต่ำที่ผ่าน" variant="outlined" fullWidth={true} 
                    value={values.minScore? values.minScore: 0} type="number" 
                    onChange={e => onChangeInput('minScore', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>

                <div className="sep mt-4"></div>
                <div className="grid xl-2-3 lg-80 sm-100">
                  <div className="d-flex ai-center">
                    <h6 className="fw-600 mr-4">คำถามท้ายเนื้อหา</h6>
                    {['add', 'edit'].indexOf(process) > -1? (
                      <Button 
                        variant="contained" color="primary" size="small" startIcon={<AddIcon />} 
                        onClick={onAddQuestion} 
                      >เพิ่มคำถาม</Button>
                    ): (<></>)}
                  </div>
                </div>
                {values.questions.map((d, i) => (
                  <div className="grid sm-100" key={`question-${i}`}>
                    <TableContainer>
                      <table className="table table-question">
                        <tbody>
                          <tr>
                            <td className="num-q" rowSpan="4">{i+1}</td>
                            <td className="col-title">คำถาม</td>
                            <td colSpan="2">
                              <TextField 
                                required={true} variant="outlined" type="text" fullWidth={true} 
                                value={d.question} size="small" disabled={process === 'view'} 
                                onChange={e => onChangeQuestion(i, 'question', e.target.value)} 
                              />
                            </td>
                            <td className="pr-4" align="right" width="100">
                              {['add', 'edit'].indexOf(process) > -1? (
                                <Button 
                                  variant="contained" color="secondary" size="small" startIcon={<DeleteIcon />} 
                                  onClick={e => onDeleteQuestion(e, i)} 
                                >ลบ</Button>
                              ): (<></>)}
                            </td>
                          </tr>
                          <tr>
                            <td className="col-title">คะแนน</td>
                            <td>
                              <TextField 
                                required={true} variant="outlined" type="number" fullWidth={false} 
                                value={d.score} size="small" disabled={process === 'view'}  
                                onChange={e => onChangeQuestion(i, 'score', e.target.value)} 
                              />
                            </td>
                            <td colSpan="2"></td>
                          </tr>
                          {['add', 'edit'].indexOf(process) > -1? (
                            <tr>
                              <td className="col-title">คำตอบ</td>
                              <td className="pt-1 pb-1">
                                <Button 
                                  variant="contained" color="primary" size="small" startIcon={<AddIcon />} 
                                  onClick={() => onAddChoiceQuestion(i)} 
                                >เพิ่มคำตอบ</Button>
                              </td>
                              <td colSpan="2"></td>
                            </tr>
                          ): (<></>)}
                          <tr>
                            <td colSpan="4">
                              <TableContainer>
                                <Table size="small" className={`table-choice`}>
                                  <TableHead>
                                    <TableRow>
                                      <TableCell>#</TableCell>
                                      <TableCell style={{paddingRight: 0}}>คำตอบ *</TableCell>
                                      <TableCell style={{paddingRight: 0}} width="90">คำตอบถูก</TableCell>
                                      <TableCell width="100"></TableCell>
                                    </TableRow>
                                  </TableHead>
                                  <TableBody>
                                    {d.choices.map((k, j) => (
                                      <TableRow key={`question-choice-${i}-${j}`}>
                                        <TableCell>{j+1}</TableCell>
                                        <TableCell style={{paddingRight: 0}}>
                                          <TextField 
                                            required={true} variant="outlined" type="text" fullWidth={true} 
                                            value={k.text} size="small" disabled={process === 'view'} 
                                            onChange={e => onChangeChoiceQuestion(i, j, 'text', e.target.value)} 
                                          />
                                        </TableCell>
                                        <TableCell style={{paddingRight: 0}}>
                                          <Switch 
                                            checked={k.isCorrect} color="primary" 
                                            onChange={
                                              (e) => {
                                                if(['add', 'edit'].indexOf(process) > -1) {
                                                  return onChangeChoiceQuestion(i, j, 'isCorrect', e.target.checked);
                                                } else {
                                                  return true;
                                                }
                                              }
                                            } 
                                          />
                                        </TableCell>
                                        <TableCell align="right">
                                          {['add', 'edit'].indexOf(process) > -1? (
                                            <Button 
                                              variant="contained" color="secondary" size="small" startIcon={<DeleteIcon />} 
                                              onClick={e => onDeleteChoiceQuestion(e, i, j)} 
                                            >ลบ</Button>
                                          ): (<></>)}
                                        </TableCell>
                                      </TableRow>
                                    ))}
                                  </TableBody>
                                </Table>
                              </TableContainer>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </TableContainer>
                  </div>
                ))}
                <div className="sep mb-4"></div>
              </div>
            ): (<></>)}

            {/* Content Buttons */}
            <div className="grids">
              <div className="grid sm-100">
                <div className="mt-2">
                  {process === 'view'? (
                    <Button 
                      variant="contained" color="primary" size="large" className={`mr-2`} 
                      onClick={onModalQualityOpen} 
                    >การตรวจสอบ</Button>
                  ): (<></>)}
                  {process === 'add'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<AddIcon />} className={`mr-2`} 
                    >สร้าง</Button>
                  ): process === 'edit'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >บันทึก</Button>
                  ): ('')}
                  <Button 
                    component={Link} to="/manager/contents" className={`mr-2`} 
                    variant="outlined" color="primary" size="large"
                  >ย้อนกลับ</Button>
                  {['edit', 'view'].indexOf(process) > -1? (
                    <Button 
                      component={Link} to={`/manager/content-preview/${process}/${dataId}`} className={`mr-2`} 
                      variant="outlined" color="primary" size="large" 
                    >ดูตัวอย่าง</Button>
                  ): (<></>)}
                </div>
              </div>
            </div>
          
          </form>
        </div>

        <FooterAdmin />
      </div>

      {/* Content Quality Modal */}
      <Dialog fullWidth={true} maxWidth={`md`} open={modalQualityOpen} onClose={onModalQualityClose}>
        <DialogTitle>การตรวจสอบ Content</DialogTitle>
        <form onSubmit={onQualitySubmit}>
          <DialogContent className="pt-0">
            <TableContainer>
              <Table size="medium">
                <TableHead>
                  <TableRow>
                    <TableCell className="ws-nowrap" align="center" padding="none">
                      ID
                    </TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">
                      ผู้ตรวจสอบ
                    </TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">
                      สถานะตรวจสอบ
                    </TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">
                      เหตุผล
                    </TableCell>
                    <TableCell className="ws-nowrap" align="left" padding="normal">
                      วันที่
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {!props.approvedContents.length? (
                    <TableRow>
                      <TableCell colSpan={5} align="center">
                        ไม่พบสถานะตรวจสอบ
                      </TableCell>
                    </TableRow>
                  ): props.approvedContents
                  .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                  .map((d, i) => {
                    let index = (paginate.page-1)*paginate.pp + i + 1;
                    return (
                      <TableRow key={`approved-content-${index}`} hover>
                        <TableCell align="center">
                          {index}
                        </TableCell>
                        <TableCell align="left">
                          {d.approver.displayName()}
                        </TableCell>
                        <TableCell align="left">
                          {d.approvedStatus === -1? (
                            <Chip label="Pending" size="small" />
                          ): d.approvedStatus === 0? (
                            <Chip label="Not approved" size="small" color="secondary" />
                          ): (
                            <Chip label="Approved" size="small" color="primary" />
                          )}
                        </TableCell>
                        <TableCell align="left" className="ws-nowrap">
                          {d.comment}
                        </TableCell>
                        <TableCell align="left">
                          {formatDate(d.createdAt, 'MM/DD/YYYY HH:mm:ss')}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              labelRowsPerPage="แสดง" 
              rowsPerPageOptions={[3]} 
              component="div" 
              count={props.approvedContents.length} 
              rowsPerPage={paginate.pp} 
              page={paginate.page - 1} 
              onPageChange={onChangePage} 
            />
            <div className="grids">
              <div className="grid md-40 sm-50">
                <FormControl 
                  required={true} variant="outlined" fullWidth={true} disabled={process !== 'view'} 
                >
                  <InputLabel id="input-approved-status">สถานะตรวจสอบ</InputLabel>
                  <Select
                    labelId="input-approved-status" label="สถานะตรวจสอบ *" 
                    value={approvedStatus} onChange={e => setApprovedStatus(e.target.value)} 
                  >
                    <MenuItem value={0}>Not approved</MenuItem>
                    <MenuItem value={1}>Approved</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="grid md-60 sm-100">
                <TextField 
                  required={true} label="ความคิดเห็น" variant="outlined" fullWidth={true} 
                  value={approvedComment} onChange={e => setApprovedComment(e.target.value)} 
                  disabled={process !== 'view'} 
                />
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={onModalQualityClose} color="primary">
              ปิด
            </Button>
            <Button type="submit" color="primary">
              ยืนยันการตรวจสอบ
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
}

ManagerContentPage.defaultProps = {
	activeIndex: 3
};
ManagerContentPage.propTypes = {
	activeIndex: PropTypes.number,
  uploadFile: PropTypes.func.isRequired,
  playlistList: PropTypes.func.isRequired,
  tagList: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processAdd: PropTypes.func.isRequired,
  processEdit: PropTypes.func.isRequired,
  approvedContentList: PropTypes.func,
  approvedContentAdd: PropTypes.func
};

const mapStateToProps = (state) => ({
  user: state.user,
  single: state.general.content,
  playlists: state.general.playlists,
  tags: state.general.tags,
  levels: state.general.levels,
  subjects: state.general.subjects,
  approvedContents: state.general.approvedContents
});

export default connect(mapStateToProps, {
  uploadFile: memberUploadFile, 
  playlistList: managerPlaylistList,
  tagList: memberTagList,
  levelList: memberLevelList,
  subjectList: memberSubjectList,
  processRead: managerContentRead, 
  processAdd: managerContentAdd, 
  processEdit: managerContentEdit,
  approvedContentList: creatorApprovedContentList,
  approvedContentAdd: managerApprovedContentAdd
})(ManagerContentPage);