import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { TextField, Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { DepartmentModel, UserModel } from '../../models';
import { managerDepartmentEdit } from '../../actions/manager.actions';

function ManagerDepartmentPage(props) {
	const user = new UserModel(props.user);
  
  const [values, setValues] = useState(new DepartmentModel(user.department));
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    await props.processEdit(values);
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title={`หน่วยงาน`} back="/manager" />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>
            <div className="grids ai-center">
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="ชื่อหน่วยงาน" variant="outlined" fullWidth={true} 
                  value={values.name? values.name: ''} 
                  onChange={e => onChangeInput('name', e.target.value)} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  label="สถานะ" variant="outlined" fullWidth={true} disabled={true} 
                  value={values.status? 'เปิดใช้งาน': 'ปิดใช้งาน'} 
                />
              </div>
              <div className="sep"></div>

              <div className="grid sm-100">
                <div className="mt-2">
                  <Button 
                    type="submit" variant="contained" color="primary" 
                    size="large" startIcon={<EditIcon />} className={`mr-2`} 
                  >
                    แก้ไข
                  </Button>
                  <Button 
                    component={Link} to="/manager" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ManagerDepartmentPage.defaultProps = {
	activeIndex: 4
};
ManagerDepartmentPage.propTypes = {
	activeIndex: PropTypes.number,
	user: PropTypes.object.isRequired,
  processEdit: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {
  processEdit: managerDepartmentEdit
})(ManagerDepartmentPage);