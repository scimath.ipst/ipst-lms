export {default as ManagerDashboardPage} from './DashboardPage';
export {default as ManagerProfilePage} from './ProfilePage';

export {default as ManagerChannelsPage} from './ChannelsPage';
export {default as ManagerChannelPage} from './ChannelPage';
export {default as ManagerPlaylistsPage} from './PlaylistsPage';
export {default as ManagerPlaylistPage} from './PlaylistPage';
export {default as ManagerContentsPage} from './ContentsPage';
export {default as ManagerContentPage} from './ContentPage';
export {default as ManagerContentPreviewPage} from './ContentPreviewPage';

export {default as ManagerDepartmentPage} from './DepartmentPage';
export {default as ManagerUsersPage} from './UsersPage';
export {default as ManagerUserPage} from './UserPage';
