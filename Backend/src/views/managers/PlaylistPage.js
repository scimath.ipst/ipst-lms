import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Chip,
  OutlinedInput, InputAdornment
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { FRONTEND_URL } from '../../actions/types';
import { PlaylistModel } from '../../models';
import { memberUploadFile } from '../../actions/member.actions';
import { 
  managerPlaylistRead, managerPlaylistAdd, managerPlaylistEdit, managerChannelList 
} from '../../actions/manager.actions';

function ManagerPlaylistPage(props) {
  const history = useHistory();
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new PlaylistModel({status: 1}));
  const [channelId, setChannelId] = useState('');
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };
  
  const onFileChange = (key) => async (e) => {
    let temp = await props.uploadFile(e.target.files[0]);
    if(temp) onChangeInput(key, temp);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(process === 'add') {
      let result = await props.processAdd({...values, channelId: channelId});
      if(result) history.push('/manager/playlists');
    } else if(process === 'edit') {
      let result = await props.processEdit({...values, channelId: channelId});
      if(result) history.push('/manager/playlists');
    }
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(dataId) props.processRead({id: dataId});
    props.channelList({ keyword: '' });
  }, []);
  useEffect(() => {
    if(dataId && ['view', 'edit'].indexOf(process) > -1) {
      setValues(props.single);
      if(props.single.channel._id) setChannelId(props.single.channel._id);
    }
  }, [props.single]);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'} Playlist`} 
          back="/manager/playlists" 
        />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>
            <div className="grids">

              <div className="grid xl-2-3 md-70 sm-100 mt-0">
                <div className="grids">
                  <div className="grid sm-100">
                    <TextField 
                      required={true} label="ชื่อ Playlist" variant="outlined" fullWidth={true} 
                      value={values.name? values.name: ''} 
                      onChange={e => onChangeInput('name', e.target.value)} 
                      disabled={process === 'view'} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <FormControl fullWidth variant="outlined">
                      <InputLabel htmlFor="input-url">ลิงค์ *</InputLabel>
                      <OutlinedInput
                        id="input-url"
                        required={true} label="ลิงค์ *" variant="outlined" fullWidth={true} 
                        value={values.url? values.url: ''} 
                        onChange={e => onChangeInput('url', e.target.value)} 
                        disabled={process === 'view'} 
                        startAdornment={
                          <InputAdornment position="start" className="op-60">
                            {FRONTEND_URL}playlist/
                          </InputAdornment>
                        } 
                      />
                    </FormControl>
                  </div>

                  <div className="grid sm-100">
                    <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                      <InputLabel id="input-channel">Channel *</InputLabel>
                      <Select
                        required={true} labelId="input-channel" label="Channel *" 
                        value={props.channels.length? channelId: ''} 
                        onChange={e => setChannelId(e.target.value)} 
                      >
                        {props.channels.map((d, i) => (
                          <MenuItem key={i} value={d._id}>{d.displayName()}</MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                  <div className="sep"></div>
                  
                  <div className="grid sm-100">
                    <TextField 
                      label="คำบรรยาย" variant="outlined" fullWidth={true} 
                      value={values.description? values.description: ''} 
                      onChange={e => onChangeInput('description', e.target.value)} 
                      disabled={process === 'view'} 
                    />
                  </div>
                  <div className="sep"></div>

                  <div className="grid sm-100">
                    <TextField 
                      label="คำบรรยาย Meta" variant="outlined" fullWidth={true} 
                      value={values.metadesc? values.metadesc: ''} 
                      onChange={e => onChangeInput('metadesc', e.target.value)} 
                      disabled={process === 'view'} 
                    />
                  </div>
                  <div className="sep"></div>

                  <div className="grid sm-100">
                    <Autocomplete
                      multiple freeSolo id="input-keywords" 
                      value={values.keywords} options={[]} 
                      onChange={(e, val) => onChangeInput('keywords', val)} 
                      fullWidth={true} disabled={process === 'view'} 
                      renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                          <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                        ))
                      }
                      renderInput={(params) => (
                        <TextField {...params} variant="outlined" label="คำค้นหา" />
                      )}
                    />
                  </div>
                  <div className="sep"></div>

                  <div className="grid sm-50">
                    <TextField 
                      label="ลำดับ" variant="outlined" fullWidth={true} 
                      value={values.order? values.order: 0} type="number" 
                      onChange={e => onChangeInput('order', e.target.value)} 
                      disabled={process === 'view'} 
                    />
                  </div>
                  <div className="grid sm-50">
                    <FormControl 
                      variant="outlined" fullWidth={true} 
                      disabled={process === 'view'} 
                    >
                      <InputLabel id="input-status">สถานะ</InputLabel>
                      <Select
                        labelId="input-status" label="สถานะ" 
                        value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                      >
                        <MenuItem value={0}>Unlisted</MenuItem>
                        <MenuItem value={1}>Drafted</MenuItem>
                        <MenuItem value={2}>Private</MenuItem>
                        <MenuItem value={3}>Public</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                </div>
              </div>

              <div className="grid xl-1-3 md-30">
                <div className="input-upload">
                  <div className="wrapper">
                    <div className="img-wrapper">
                      {values.image? (
                        <div className="img-bg" style={{ backgroundImage: `url('${values.image}')` }}></div>
                      ): (
                        <div className="img-bg" style={{ backgroundImage: `url('/assets/img/default/img.jpg')` }}></div>
                      )}
                    </div>
                    {process === 'view'? <template></template>: (
                      <p className="fw-400 mt-3">คลิกเพื่อ Upload รูปภาพ</p>
                    )}
                  </div>
                  {process === 'view'? <template></template>: (
                    <input 
                      type="file" accept="image/png, image/gif, image/jpeg" 
                      onChange={onFileChange('image')} 
                    />
                  )}
                </div>
              </div>

              <div className="grid sm-100">
                <div className="mt-2">
                  {process === 'add'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<AddIcon />} className={`mr-2`} 
                    >
                      สร้าง
                    </Button>
                  ): process === 'edit'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      แก้ไข
                    </Button>
                  ): ('')}
                  <Button 
                    component={Link} to="/manager/playlists" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>

            </div>
          </form>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ManagerPlaylistPage.defaultProps = {
	activeIndex: 2
};
ManagerPlaylistPage.propTypes = {
	activeIndex: PropTypes.number,
  uploadFile: PropTypes.func.isRequired,
  channelList: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processAdd: PropTypes.func.isRequired,
  processEdit: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  single: state.general.playlist,
  channels: state.general.channels
});

export default connect(mapStateToProps, {
  uploadFile: memberUploadFile, 
  channelList: managerChannelList,
  processRead: managerPlaylistRead, 
  processAdd: managerPlaylistAdd, 
  processEdit: managerPlaylistEdit
})(ManagerPlaylistPage);