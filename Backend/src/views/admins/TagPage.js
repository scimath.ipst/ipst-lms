import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TextField, FormControl, InputLabel, Select, MenuItem, Button
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { TagModel } from '../../models';
import { qcoTagRead, qcoTagAdd, qcoTagEdit } from '../../actions/qco.actions';

function TagPage(props) {
  const history = useHistory();
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new TagModel({status: 1}));
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(process === 'add') {
      let result = await props.processAdd(values);
      if(result) history.push('/admin/tags');
    } else if(process === 'edit') {
      let result = await props.processEdit(values);
      if(result) history.push('/admin/tags');
    }
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(dataId) props.processRead({id: dataId});
  }, []);
  useEffect(() => {
    if(dataId && ['view', 'edit'].indexOf(process) > -1) {
      setValues(props.single);
    }
  }, [props.single]);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}แท็ก`} 
          back="/admin/tags" 
        />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>
            <div className="grids ai-center">
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="ชื่อแท็ก" variant="outlined" fullWidth={true} 
                  value={values.name? values.name: ''} 
                  onChange={e => onChangeInput('name', e.target.value)} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <FormControl 
                  variant="outlined" fullWidth={true} 
                  disabled={process === 'view'} 
                >
                  <InputLabel id="input-status">สถานะ</InputLabel>
                  <Select
                    labelId="input-status" label="สถานะ" 
                    value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                  >
                    <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                    <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                    <MenuItem value={-1}>เสนอแนะ</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="sep"></div>

              <div className="grid sm-100">
                <div className="mt-2">
                  {process === 'add'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<AddIcon />} className={`mr-2`} 
                    >
                      สร้าง
                    </Button>
                  ): process === 'edit'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      บันทึก
                    </Button>
                  ): ('')}
                  <Button 
                    component={Link} to="/admin/tags" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

TagPage.defaultProps = {
	activeIndex: 6
};
TagPage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired,
  processAdd: PropTypes.func.isRequired,
  processEdit: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  single: state.general.tag
});

export default connect(mapStateToProps, {
  processRead: qcoTagRead, 
  processAdd: qcoTagAdd, 
  processEdit: qcoTagEdit
})(TagPage);