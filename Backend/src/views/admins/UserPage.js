import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Avatar, Tabs, Tab, Chip
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import StarIcon from '@material-ui/icons/Star';
import { onMounted, formatDate } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { UserModel } from '../../models';
import { memberUploadFile } from '../../actions/member.actions';
import { qcoDepartmentList } from '../../actions/qco.actions';
import { 
  adminRoleList, adminUserRead, adminUserAdd, adminUserEdit, adminUserHistoryList
} from '../../actions/admin.actions';
import { qcoTagList, qcoLevelList, qcoSubjectList } from '../../actions/qco.actions';
import { memberBadgeList } from '../../actions/member.actions';
import { userProcessOtims } from '../../actions/user.actions';

function UserPage(props) {
  const history = useHistory();
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;

  const [ip, setIp] = useState('');
  const url = window.location.href;
  const [tabValue, setTabValue] = useState(0);
  
  const [values, setValues] = useState(new UserModel({status: 1}));
  const [deptId, setDeptId] = useState('');
  const [levelId, setLevelId] = useState('');
  const [roleId, setRoleId] = useState('');
  const [requiredDepartment, setRequiredDepartment] = useState(false);
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [badges, setBadges] = useState([]);
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  const [userActions, setUserActions] = useState([]);
  const [paginate, setPaginate] = useState({ page: 1, pp: 10 });
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'รูปภาพ', numeric: false, noPadding: false },
    { label: 'Content', numeric: false, noPadding: false },
    { label: 'สถานะ', numeric: false, noPadding: false },
    { label: 'คะแนน', numeric: true, noPadding: false },
    { label: 'วันที่', numeric: true, noPadding: true },
  ];
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };

  const onFileChange = (key) => async (e) => {
    let temp = await props.uploadFile(e.target.files[0]);
    if(temp) onChangeInput(key, temp);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(process === 'add') {
      if(
        !values.firstname || !values.lastname || !values.username || !values.email 
        || !password || !confirmPassword || !roleId
      ) {
        setTabValue(0);
      } else {
        let result = await props.processAdd({
          ...values, ip: ip, url: url, 
          roleId: roleId, deptId: deptId, levelId: levelId, 
          password: password, confirmPassword: confirmPassword
        });
        if(result) history.push('/admin/users');
      }
    } else if(process === 'edit') {
      if(
        !values.firstname || !values.lastname || !values.username || !values.email || !roleId
      ) {
        setTabValue(0);
      } else {
        await props.processEdit({
          ...values, ip: ip, url: url, 
          roleId: roleId, deptId: deptId, levelId: levelId, 
          password: password, confirmPassword: confirmPassword
        });
        // if(result) history.push('/admin/users');
      }
    }
  };

  // OTIMS
  const [otims, setOtims] = useState([]);
  const [paginateOtims, setPaginateOtims] = useState({ page: 1, pp: 10 });
  const onChangePageOtims = (e, newPage) => {
    setPaginateOtims({ ...paginateOtims, page: newPage+1 });
  };
  const onChangePerPageOtims = (e) => {
    setPaginateOtims({ ...paginateOtims, page: 1, pp: e.target.value });
  };

  /* eslint-disable */
  useEffect(() => onMounted(setIp, true), []);
  useEffect(() => {
    props.roleList();
    props.departmentList({ keyword: '' });
    props.tagList({ keyword: '' });
    props.levelList({ keyword: '' });
    props.subjectList({ keyword: '' });
    if(dataId) props.processRead({id: dataId});
  }, []);
  useEffect(() => {
    if(dataId && ['view', 'edit'].indexOf(process) > -1) {
      setValues(props.single);
      if(props.single.role._id) setRoleId(props.single.role._id);
      if(props.single.department._id) setDeptId(props.single.department._id);
      if(props.single.level._id) setLevelId(props.single.level._id);
    }
    if(dataId && ['view'].indexOf(process) > -1) {
      props.badgeList({ id: dataId }).then(d => setBadges(d));
      props.historyList({ id: dataId }).then(d => setUserActions(d));
      if(props.single.email) {
        props.processOtims({ email: props.single.email }).then(d => setOtims(d));
      }
    }
  }, [props.single]);
  useEffect(() => {
    setRequiredDepartment(false);
    let temp = props.roles.filter(d => d._id === roleId);
    if(temp.length) {
      if(temp[0].level > 0 && temp[0].level <= 2) {
        setRequiredDepartment(true);
      }
    }
  }, [roleId]);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}บัญชีผู้ใช้`} 
          back="/admin/users" 
        />

        <div className="app-card app-card-header" data-aos="fade-up" data-aos-delay="150">
          <Tabs
            value={tabValue} onChange={(e, val) => setTabValue(val)} 
            indicatorColor="primary" textColor="primary" variant="scrollable" 
            scrollButtons="auto" aria-label="scrollable auto tabs example" 
          >
            <Tab label="บัญชีผู้ใช้" id="tab-0" aria-controls="tabpanel-0" />
            <Tab label="ความสนใจ" id="tab-1" aria-controls="tabpanel-1" />
            {process === 'view' && (
              <Tab label="ประวัติการชม" id="tab-2" aria-controls="tabpanel-2" />
            )}
            {process === 'view' && (
              <Tab label="เหรียญที่ได้รับ" id="tab-3" aria-controls="tabpanel-3" />
            )}
            {process === 'view' && (
              <Tab label="การสอบออนไลน์" id="tab-4" aria-controls="tabpanel-4" />
            )}
          </Tabs>
        </div>
        <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>
            
            {/* User Tab 1 */}
            {tabValue === 0? (
              <div className="grids ai-center">
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="ชื่อจริง" variant="outlined" fullWidth={true} 
                    value={values.firstname? values.firstname: ''} 
                    onChange={e => onChangeInput('firstname', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="นามสกุล" variant="outlined" fullWidth={true} 
                    value={values.lastname? values.lastname: ''} 
                    onChange={e => onChangeInput('lastname', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="ชื่อผู้ใช้" variant="outlined" fullWidth={true} 
                    value={values.username? values.username: ''} 
                    onChange={e => onChangeInput('username', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="อีเมล" variant="outlined" fullWidth={true} 
                    value={values.email? values.email: ''} type="email" 
                    onChange={e => onChangeInput('email', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>
                
                {['add', 'edit'].indexOf(process) > -1? (
                  <>
                    <div className="grid xl-1-3 lg-45 md-40">
                      <TextField 
                        required={process === 'add'} label="รหัสผ่าน" variant="outlined" 
                        value={password} type="password" fullWidth={true} 
                        onChange={e => setPassword(e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="grid xl-1-3 lg-45 md-40">
                      <TextField 
                        required={process === 'add'} label="ยืนยันรหัสผ่าน" variant="outlined" 
                        value={confirmPassword} type="password" fullWidth={true} 
                        onChange={e => setConfirmPassword(e.target.value)} 
                        disabled={process === 'view'} 
                      />
                    </div>
                    <div className="sep"></div>
                  </>
                ): (<></>)}

                <div className="grid xl-1-3 lg-45 md-40">
                  <FormControl 
                    variant="outlined" fullWidth={true} required={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="input-role">บทบาท</InputLabel>
                    <Select
                      labelId="input-role" label="บทบาท *" 
                      value={roleId} onChange={e => setRoleId(e.target.value)} 
                    >
                      {props.roles.map((d, i) => (
                        <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                    <InputLabel id="input-dept">
                      หน่วยงาน {requiredDepartment? '*': ''}
                    </InputLabel>
                    <Select
                      labelId="input-dept" label={`หน่วยงาน ${requiredDepartment? '*': ''}`} 
                      required={requiredDepartment} 
                      value={deptId} onChange={e => setDeptId(e.target.value)} 
                    >
                      <MenuItem value="">ไม่มีหน่วยงาน</MenuItem>
                      {props.departments.map((d, i) => (
                        <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="sep"></div>
                
                <div className="grid xl-1-3 lg-45 md-40">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="input-status">สถานะ</InputLabel>
                    <Select
                      labelId="input-status" label="สถานะ" 
                      value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                    >
                      <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                      <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <Button 
                    variant="outlined" component="label" color="primary" 
                    disabled={process === 'view'} 
                  >
                    <Avatar src={values.avatar} />
                    <span className="ml-3">เปลี่ยนรูปโปรไฟล์</span>
                    <input 
                      hidden type="file" accept="image/png, image/gif, image/jpeg" 
                      onChange={onFileChange('avatar')} 
                    />
                  </Button>
                </div>
              </div>
            ): (<></>)}

            {/* User Tab 2 */}
            {tabValue === 1? (
              <div className="grids">
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                    <InputLabel id="input-level">ระดับชั้น</InputLabel>
                    <Select
                      labelId="input-level" label="ระดับชั้น" 
                      value={props.levels.length && levelId? levelId: ''} 
                      onChange={e => setLevelId(e.target.value)} 
                    >
                      {props.levels.map((d, i) => (
                        <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <Autocomplete
                    multiple id="input-subjects" 
                    value={values.subjects.length? values.subjects: []} 
                    options={props.subjects} filterSelectedOptions 
                    getOptionLabel={(option) => option.name} 
                    getOptionSelected={(option, value) => option._id === value._id}
                    onChange={(e, val) => onChangeInput('subjects', val)} 
                    fullWidth={true} disabled={process === 'view'} 
                    renderTags={(value, getTagProps) =>
                      value.map((option, index) => (
                        <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                      ))
                    }
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" label="วิชาที่สนใจ" />
                    )}
                  />
                </div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <Autocomplete
                    multiple id="input-tags" 
                    value={values.tags.length? values.tags: []} 
                    options={props.tags} filterSelectedOptions 
                    getOptionLabel={(option) => option.name} 
                    getOptionSelected={(option, value) => option._id === value._id}
                    onChange={(e, val) => onChangeInput('tags', val)} 
                    fullWidth={true} disabled={process === 'view'} 
                    renderTags={(value, getTagProps) =>
                      value.map((option, index) => (
                        <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                      ))
                    }
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" label="แท็กที่สนใจ" />
                    )}
                  />
                </div>
              </div>
            ): (<></>)}

            {/* User Tab 3 */}
            {process === 'view' && tabValue === 2? (
              <>
                <TableContainer>
                  <Table size="medium">
                    <TableHead>
                      <TableRow>
                        {columns.map((col, i) => (
                          <TableCell
                            key={i} className="ws-nowrap"
                            align={col.numeric ? 'center' : 'left'}
                            padding={col.noPadding ? 'none' : 'normal'}
                          >
                            {col.label}
                          </TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {!userActions.length? (
                        <TableRow>
                          <TableCell colSpan={columns.length} align="center">
                            ไม่พบข้อมูลในระบบ
                          </TableCell>
                        </TableRow>
                      ): userActions
                      .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                      .map((d, i) => {
                        let index = (paginate.page-1)*paginate.pp + i + 1;
                        return (
                          <TableRow key={index} hover>
                            <TableCell align="center">
                              {index}
                            </TableCell>
                            <TableCell align="center">
                              <div className="img-preview">
                                <div className="img-bg" style={{ backgroundImage: `url('${d.content.image}')` }}></div>
                              </div>
                            </TableCell>
                            <TableCell align="left">
                              {d.content.name}
                            </TableCell>
                            <TableCell align="left">
                              {d.isCompleted()? (
                                <Chip label="Completed" size="small" color="primary" />
                              ): d.isWatched()? (
                                <Chip label="Watched" size="small" color="primary" />
                              ): d.isWatching()? (
                                <Chip label="Watching" size="small" color="secondary" />
                              ): (
                                <Chip label="Visited" size="small" />
                              )}
                            </TableCell>
                            <TableCell align="center">
                              {d.isCompleted()? (
                                d.scoreType === 0? (
                                  <StarIcon className="color-p" />
                                ): (
                                  <>{d.score}/{d.maxScore}</>
                                )
                              ): (<>-</>)}
                            </TableCell>
                            <TableCell align="center">
                              {formatDate(d.updatedAt)}
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </TableContainer>
                <TablePagination
                  labelRowsPerPage="แสดง" 
                  rowsPerPageOptions={[10, 25, 50, 100]} 
                  component="div" 
                  count={userActions.length} 
                  rowsPerPage={paginate.pp} 
                  page={paginate.page - 1} 
                  onPageChange={onChangePage} 
                  onRowsPerPageChange={onChangePerPage} 
                />
              </>
            ): (<></>)}

            {/* User Tab 4 */}
            {process === 'view' && tabValue === 3? (
              <div className="grids">
                {!badges.length? (
                  <div className="grid sm-100">
                    <p className="h6 fw-500">ไม่พบเหรียญที่ได้รับ</p>
                  </div>
                ): (
                  badges.map((b, i) => (
                    <div className="grid xl-20 lg-25 md-1-3 sm-50 xs-50" key={i}>
                      <div className="badge-container">
                        <div className="wrapper">
                          <div className="inner-wrapper">
                            <img src={b.getImage()} alt="Badge Icon" />
                          </div>
                        </div>
                      </div>
                      <p className="color-p fw-500 text-center mt-2">
                        {b.getName()}
                      </p>
                      <p className="xxs fw-500 text-center">
                        {formatDate(b.updatedAt)}
                      </p>
                    </div>
                  ))
                )}
              </div>
            ): (<></>)}
              
            {/* Content Tab 5 */}
            {process === 'view' && tabValue === 4? (
              <>
                <div className="mt-2">
                  <TableContainer>
                    <Table size="medium">
                      <TableHead>
                        <TableRow>
                          <TableCell className="ws-nowrap color-black" align="center" padding="normal">
                            ID
                          </TableCell>
                          <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                            หัวข้อที่ทําการสรุป
                          </TableCell>
                          <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                            ประเภทของหัวข้อ
                          </TableCell>
                          <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                            ข้อที่ถูก
                          </TableCell>
                          <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                            ข้อทั้งหมด
                          </TableCell>
                          <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                            อัตราตอบถูก
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {!otims.length? (
                          <TableRow>
                            <TableCell colSpan={6} align="center">
                              ไม่พบข้อมูลในระบบ
                            </TableCell>
                          </TableRow>
                        ): otims
                        .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                        .map((d, i) => {
                          let index = (paginate.page-1)*paginate.pp + i + 1;
                          return (
                            <TableRow key={index} hover>
                              <TableCell className="color-black" align="center">
                                {index}
                              </TableCell>
                              <TableCell className="color-black" align="left">
                                {d.topicname}
                              </TableCell>
                              <TableCell className="color-black" align="left">
                                {d.topiccode}
                              </TableCell>
                              <TableCell className="color-black" align="left">
                                {d.correctcount}
                              </TableCell>
                              <TableCell className="color-black" align="left">
                                {d.total}
                              </TableCell>
                              <TableCell className="color-black" align="left">
                                {d.correctrate}
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  <TablePagination
                    labelRowsPerPage="แสดง" 
                    rowsPerPageOptions={[10, 25, 50, 100]} 
                    component="div" 
                    count={otims.length} 
                    rowsPerPage={paginateOtims.pp} 
                    page={paginateOtims.page - 1} 
                    onPageChange={onChangePageOtims} 
                    onRowsPerPageChange={onChangePerPageOtims} 
                  />
                </div>
              </>
            ): (<></>)}

            {/* User Buttons */}
            <div className="grids">
              <div className="grid sm-100">
                <div className="mt-2">
                  {process === 'add'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<AddIcon />} className={`mr-2`} 
                    >
                      สร้าง
                    </Button>
                  ): process === 'edit'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      บันทึก
                    </Button>
                  ): ('')}
                  <Button 
                    component={Link} to="/admin/users" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
            
          </form>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

UserPage.defaultProps = {
	activeIndex: 10
};
UserPage.propTypes = {
	activeIndex: PropTypes.number,
  uploadFile: PropTypes.func.isRequired,
  departmentList: PropTypes.func.isRequired,
  tagList: PropTypes.func.isRequired,
  levelList: PropTypes.func.isRequired,
  subjectList: PropTypes.func.isRequired,
  roleList: PropTypes.func.isRequired,
  badgeList: PropTypes.func.isRequired,
  historyList: PropTypes.func.isRequired,
  processRead: PropTypes.func.isRequired,
  processAdd: PropTypes.func.isRequired,
  processEdit: PropTypes.func.isRequired,
  processOtims: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  single: state.admin.user,
  roles: state.admin.roles,
  departments: state.general.departments,
  tags: state.general.tags,
  levels: state.general.levels,
  subjects: state.general.subjects
});

export default connect(mapStateToProps, {
  uploadFile: memberUploadFile,
  departmentList: qcoDepartmentList,
  tagList: qcoTagList,
  levelList: qcoLevelList,
  subjectList: qcoSubjectList,
  roleList: adminRoleList,
  badgeList: memberBadgeList,
  historyList: adminUserHistoryList,
  processRead: adminUserRead,
  processAdd: adminUserAdd,
  processEdit: adminUserEdit,
  processOtims: userProcessOtims
})(UserPage);