import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Avatar, Tabs, Tab, Chip
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import EditIcon from '@material-ui/icons/Edit';
import StarIcon from '@material-ui/icons/Star';
import { onMounted, formatDate } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { UserModel } from '../../models';
import { qcoDepartmentList } from '../../actions/qco.actions';
import { 
  userProfileEdit, userPasswordEdit, userInterestEdit, userProcessOtims
} from '../../actions/user.actions';
import { 
  memberUploadFile, memberHistoryList, memberTagList, 
  memberLevelList, memberSubjectList, memberBadgeList
} from '../../actions/member.actions';

function ProfilePage(props) {
	const user = new UserModel(props.user);
  const [tabValue, setTabValue] = useState(0);

  const [ip, setIp] = useState('');
  const url = window.location.href;
  
  const [values, setValues] = useState(user);
  const [deptId, setDeptId] = useState('');
  const [levelId, setLevelId] = useState('');
  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [badges, setBadges] = useState([]);
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  const [userActions, setUserActions] = useState([]);
  const [paginate, setPaginate] = useState({ page: 1, pp: 10 });
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'รูปภาพ', numeric: false, noPadding: false },
    { label: 'Content', numeric: false, noPadding: false },
    { label: 'สถานะ', numeric: false, noPadding: false },
    { label: 'คะแนน', numeric: true, noPadding: false },
    { label: 'วันที่', numeric: true, noPadding: true },
  ];
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };

  const onFileChange = (key) => async (e) => {
    let temp = await props.uploadFile(e.target.files[0]);
    if(temp) onChangeInput(key, temp);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    await props.processEdit({
      ...values, ip: ip, url: url, 
      deptId: deptId
    });
  };
  const onSubmitPassword = async (e) => {
    e.preventDefault();
    let result = await props.processPasswordEdit({
      ip: ip, url: url,
      password: password, 
      newPassword: newPassword,
      confirmPassword: confirmPassword
    });
    if(result) {
      setPassword('');
      setNewPassword('');
      setConfirmPassword('');
    }
  };
  const onSubmitInterest = async (e) => {
    e.preventDefault();
    await props.processInterestEdit({ ...values, levelId: levelId });
  };

  // OTIMS
  const [otims, setOtims] = useState([]);
  const [paginateOtims, setPaginateOtims] = useState({ page: 1, pp: 10 });
  const onChangePageOtims = (e, newPage) => {
    setPaginateOtims({ ...paginateOtims, page: newPage+1 });
  };
  const onChangePerPageOtims = (e) => {
    setPaginateOtims({ ...paginateOtims, page: 1, pp: e.target.value });
  };

  /* eslint-disable */
  useEffect(() => onMounted(setIp, true), []);
  useEffect(() => {
    props.departmentList({ keyword: '' });
    props.tagList({ keyword: '' });
    props.levelList({ keyword: '' });
    props.subjectList({ keyword: '' });
    props.historyList().then(d => setUserActions(d));
    props.badgeList({ id: user._id }).then(d => setBadges(d));
    if(user.department._id) setDeptId(user.department._id);
    if(user.level._id) setLevelId(user.level._id);
    props.processOtims({ email: user.email }).then(d => setOtims(d));
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title={`ข้อมูลส่วนตัว`} back="/admin" />

        <div className="app-card app-card-header" data-aos="fade-up" data-aos-delay="150">
          <Tabs
            value={tabValue} onChange={(e, val) => setTabValue(val)} 
            indicatorColor="primary" textColor="primary" variant="scrollable" 
            scrollButtons="auto" aria-label="scrollable auto tabs example" 
          >
            <Tab label="ข้อมูลทั่วไป" id="tab-0" aria-controls="tabpanel-0" />
            <Tab label="รหัสผ่าน" id="tab-1" aria-controls="tabpanel-1" />
            <Tab label="ความสนใจ" id="tab-2" aria-controls="tabpanel-2" />
            <Tab label="ประวัติการชม" id="tab-3" aria-controls="tabpanel-3" />
            <Tab label="เหรียญที่ได้รับ" id="tab-4" aria-controls="tabpanel-4" />
            <Tab label="การสอบออนไลน์" id="tab-5" aria-controls="tabpanel-5" />
          </Tabs>
        </div>
        <div className="app-card app-card-body" data-aos="fade-up" data-aos-delay="150">
          
          {/* Content Tab 1 */}
          {tabValue === 0? (
            <form onSubmit={onSubmit}>
              <div className="grids ai-center">
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="ชื่อจริง" variant="outlined" fullWidth={true} 
                    value={values.firstname? values.firstname: ''} 
                    onChange={e => onChangeInput('firstname', e.target.value)} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="นามสกุล" variant="outlined" fullWidth={true} 
                    value={values.lastname? values.lastname: ''} 
                    onChange={e => onChangeInput('lastname', e.target.value)} 
                  />
                </div>
                <div className="sep"></div>
                
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="ชื่อผู้ใช้" variant="outlined" fullWidth={true} 
                    value={values.username? values.username: ''} 
                    onChange={e => onChangeInput('username', e.target.value)} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="อีเมล" variant="outlined" fullWidth={true} 
                    value={values.email? values.email: ''} type="email" 
                    onChange={e => onChangeInput('email', e.target.value)} 
                  />
                </div>
                <div className="sep"></div>

                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    disabled={true} label="บทบาท" variant="outlined" 
                    value={user.displayRole()} type="text" fullWidth={true} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                    <InputLabel id="input-dept">หน่วยงาน</InputLabel>
                    <Select
                      labelId="input-dept" label="หน่วยงาน" 
                      value={deptId} onChange={e => setDeptId(e.target.value)} 
                    >
                      <MenuItem value="">ไม่มีหน่วยงาน</MenuItem>
                      {props.departments.map((d, i) => (
                        <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="sep"></div>
                
                <div className="grid xl-1-3 lg-45 md-40">
                  <Button 
                    variant="outlined" component="label" color="primary" 
                    disabled={process === 'view'} 
                  >
                    <Avatar src={values.avatar} />
                    <span className="ml-3">เปลี่ยนรูปโปรไฟล์</span>
                    <input 
                      hidden type="file" accept="image/png, image/gif, image/jpeg" 
                      onChange={onFileChange('avatar')} 
                    />
                  </Button>
                </div>
                <div className="sep"></div>

                <div className="grid sm-100">
                  <div className="mt-2">
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      บันทึก
                    </Button>
                    <Button 
                      component={Link} to="/admin" 
                      variant="outlined" color="primary" size="large"
                    >
                      ย้อนกลับ
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          ): (<></>)}
          
          {/* Content Tab 2 */}
          {tabValue === 1? (
            <form onSubmit={onSubmitPassword}>
              <div className="grids ai-center">
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="รหัสผ่าน" variant="outlined" 
                    value={password} type="password" fullWidth={true} 
                    onChange={e => setPassword(e.target.value)} 
                  />
                </div>
                <div className="sep"></div>
                
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="รหัสผ่านใหม่" variant="outlined" 
                    value={newPassword} type="password" fullWidth={true} 
                    onChange={e => setNewPassword(e.target.value)} 
                  />
                </div>
                <div className="grid xl-1-3 lg-45 md-40">
                  <TextField 
                    required={true} label="ยืนยันรหัสผ่าน" variant="outlined" 
                    value={confirmPassword} type="password" fullWidth={true} 
                    onChange={e => setConfirmPassword(e.target.value)} 
                  />
                </div>
                <div className="sep"></div>

                <div className="grid sm-100">
                  <div className="mt-2">
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      บันทึก
                    </Button>
                    <Button 
                      component={Link} to="/admin" 
                      variant="outlined" color="primary" size="large"
                    >
                      ย้อนกลับ
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          ): (<></>)}
          
          {/* Content Tab 3 */}
          {tabValue === 2? (
            <form onSubmit={onSubmitInterest}>
              <div className="grids">
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                    <InputLabel id="input-level">ระดับชั้น</InputLabel>
                    <Select
                      labelId="input-level" label="ระดับชั้น" 
                      value={props.levels.length && levelId? levelId: ''} 
                      onChange={e => setLevelId(e.target.value)} 
                    >
                      {props.levels.map((d, i) => (
                        <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <Autocomplete
                    multiple id="input-subjects" 
                    value={values.subjects.length? values.subjects: []} 
                    options={props.subjects} filterSelectedOptions 
                    getOptionLabel={(option) => option.name} 
                    getOptionSelected={(option, value) => option._id === value._id}
                    onChange={(e, val) => onChangeInput('subjects', val)} 
                    fullWidth={true} disabled={process === 'view'} 
                    renderTags={(value, getTagProps) =>
                      value.map((option, index) => (
                        <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                      ))
                    }
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" label="วิชาที่สนใจ" />
                    )}
                  />
                </div>
                <div className="grid xl-2-3 lg-90 md-80 sm-100">
                  <Autocomplete
                    multiple id="input-tags" 
                    value={values.tags.length? values.tags: []} 
                    options={props.tags} filterSelectedOptions 
                    getOptionLabel={(option) => option.name} 
                    getOptionSelected={(option, value) => option._id === value._id}
                    onChange={(e, val) => onChangeInput('tags', val)} 
                    fullWidth={true} disabled={process === 'view'} 
                    renderTags={(value, getTagProps) =>
                      value.map((option, index) => (
                        <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                      ))
                    }
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" label="แท็กที่สนใจ" />
                    )}
                  />
                </div>
                
                <div className="grid sm-100">
                  <div className="mt-2">
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      บันทึก
                    </Button>
                    <Button 
                      component={Link} to="/admin" 
                      variant="outlined" color="primary" size="large"
                    >
                      ย้อนกลับ
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          ): (<></>)}
          
          {/* Content Tab 4 */}
          {tabValue === 3? (
            <>
              <TableContainer>
                <Table size="medium">
                  <TableHead>
                    <TableRow>
                      {columns.map((col, i) => (
                        <TableCell
                          key={i} className="ws-nowrap"
                          align={col.numeric ? 'center' : 'left'}
                          padding={col.noPadding ? 'none' : 'normal'}
                        >
                          {col.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!userActions.length? (
                      <TableRow>
                        <TableCell colSpan={columns.length} align="center">
                          ไม่พบข้อมูลในระบบ
                        </TableCell>
                      </TableRow>
                    ): userActions
                    .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                    .map((d, i) => {
                      let index = (paginate.page-1)*paginate.pp + i + 1;
                      return (
                        <TableRow key={index} hover>
                          <TableCell align="center">
                            {index}
                          </TableCell>
                          <TableCell align="center">
                            <div className="img-preview">
                              <div className="img-bg" style={{ backgroundImage: `url('${d.content.image}')` }}></div>
                            </div>
                          </TableCell>
                          <TableCell align="left">
                            {d.content.name}
                          </TableCell>
                          <TableCell align="left">
                            {d.isCompleted()? (
                              <Chip label="Completed" size="small" color="primary" />
                            ): d.isWatched()? (
                              <Chip label="Watched" size="small" color="primary" />
                            ): d.isWatching()? (
                              <Chip label="Watching" size="small" color="secondary" />
                            ): (
                              <Chip label="Visited" size="small" />
                            )}
                          </TableCell>
                          <TableCell align="center">
                            {d.isCompleted()? (
                              d.scoreType === 0? (
                                <StarIcon className="color-p" />
                              ): (
                                <>{d.score}/{d.maxScore}</>
                              )
                            ): (<>-</>)}
                          </TableCell>
                          <TableCell align="center">
                            {formatDate(d.updatedAt)}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                labelRowsPerPage="แสดง" 
                rowsPerPageOptions={[10, 25, 50, 100]} 
                component="div" 
                count={userActions.length} 
                rowsPerPage={paginate.pp} 
                page={paginate.page - 1} 
                onPageChange={onChangePage} 
                onRowsPerPageChange={onChangePerPage} 
              />
              <div className="grids">
                <div className="grid sm-100">
                  <div className="mt-2">
                    <Button 
                      component={Link} to="/admin" 
                      variant="outlined" color="primary" size="large"
                    >
                      ย้อนกลับ
                    </Button>
                  </div>
                </div>
              </div>
            </>
          ): (<></>)}
          
          {/* Content Tab 5 */}
          {tabValue === 4? (
            <div className="grids">
              {!badges.length? (
                <div className="grid sm-100">
                  <p className="h6 fw-500">ไม่พบเหรียญที่ได้รับ</p>
                </div>
              ): (
                badges.map((b, i) => (
                  <div className="grid xl-20 lg-25 md-1-3 sm-50 xs-50" key={i}>
                    <div className="badge-container">
                      <div className="wrapper">
                        <div className="inner-wrapper">
                          <img src={b.getImage()} alt="Badge Icon" />
                        </div>
                      </div>
                    </div>
                    <p className="color-p fw-500 text-center mt-2">
                      {b.getName()}
                    </p>
                    <p className="xxs fw-500 text-center">
                      {formatDate(b.updatedAt)}
                    </p>
                  </div>
                ))
              )}
              <div className="grid sm-100">
                <div className="mt-2">
                  <Button 
                    component={Link} to="/admin" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          ): (<></>)}
              
          {/* Content Tab 6 */}
          {tabValue === 5? (
            <>
              <div className="mt-2">
                <TableContainer>
                  <Table size="medium">
                    <TableHead>
                      <TableRow>
                        <TableCell className="ws-nowrap color-black" align="center" padding="normal">
                          ID
                        </TableCell>
                        <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                          หัวข้อที่ทําการสรุป
                        </TableCell>
                        <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                          ประเภทของหัวข้อ
                        </TableCell>
                        <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                          ข้อที่ถูก
                        </TableCell>
                        <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                          ข้อทั้งหมด
                        </TableCell>
                        <TableCell className="ws-nowrap color-black" align="left" padding="normal">
                          อัตราตอบถูก
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {!otims.length? (
                        <TableRow>
                          <TableCell colSpan={6} align="center">
                            ไม่พบข้อมูลในระบบ
                          </TableCell>
                        </TableRow>
                      ): otims
                      .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                      .map((d, i) => {
                        let index = (paginate.page-1)*paginate.pp + i + 1;
                        return (
                          <TableRow key={index} hover>
                            <TableCell className="color-black" align="center">
                              {index}
                            </TableCell>
                            <TableCell className="color-black" align="left">
                              {d.topicname}
                            </TableCell>
                            <TableCell className="color-black" align="left">
                              {d.topiccode}
                            </TableCell>
                            <TableCell className="color-black" align="left">
                              {d.correctcount}
                            </TableCell>
                            <TableCell className="color-black" align="left">
                              {d.total}
                            </TableCell>
                            <TableCell className="color-black" align="left">
                              {d.correctrate}
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </TableContainer>
                <TablePagination
                  labelRowsPerPage="แสดง" 
                  rowsPerPageOptions={[10, 25, 50, 100]} 
                  component="div" 
                  count={otims.length} 
                  rowsPerPage={paginateOtims.pp} 
                  page={paginateOtims.page - 1} 
                  onPageChange={onChangePageOtims} 
                  onRowsPerPageChange={onChangePerPageOtims} 
                />
              </div>
            </>
          ): (<></>)}
        
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ProfilePage.defaultProps = {
	activeIndex: -1
};
ProfilePage.propTypes = {
	activeIndex: PropTypes.number,
  uploadFile: PropTypes.func.isRequired,
  departmentList: PropTypes.func.isRequired,
  tagList: PropTypes.func.isRequired,
  levelList: PropTypes.func.isRequired,
  subjectList: PropTypes.func.isRequired,
  historyList: PropTypes.func.isRequired,
  badgeList: PropTypes.func.isRequired,
  processEdit: PropTypes.func.isRequired,
  processPasswordEdit: PropTypes.func.isRequired,
  processInterestEdit: PropTypes.func.isRequired,
  processOtims: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  departments: state.general.departments,
  tags: state.general.tags,
  levels: state.general.levels,
  subjects: state.general.subjects
});

export default connect(mapStateToProps, {
  uploadFile: memberUploadFile,
  departmentList: qcoDepartmentList,
  tagList: memberTagList,
  levelList: memberLevelList,
  subjectList: memberSubjectList,
  historyList: memberHistoryList,
  badgeList: memberBadgeList,
  processEdit: userProfileEdit,
  processPasswordEdit: userPasswordEdit,
  processInterestEdit: userInterestEdit,
  processOtims: userProcessOtims
})(ProfilePage);