import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Divider, Chip, Switch,
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, IconButton
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { DepartmentModel, UserModel } from '../../models';
import { qcoDepartmentRead } from '../../actions/qco.actions';
import { adminDepartmentAdd, adminDepartmentEdit } from '../../actions/admin.actions';

function DepartmentPage(props) {
	const user = new UserModel(props.user);

  const history = useHistory();
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new DepartmentModel({status: 1}));
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if(process === 'add') {
      let result = await props.processAdd(values);
      if(result) history.push('/admin/departments');
    } else if(process === 'edit') {
      let result = await props.processEdit(values);
      if(result) history.push('/admin/departments');
    }
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(!user.isAdmin() && process !== 'view') history.push('/admin/departments');
    if(dataId) props.processRead({id: dataId});
  }, []);
  useEffect(() => {
    if(dataId && ['view', 'edit'].indexOf(process) > -1) {
      setValues(props.single);
    }
  }, [props.single]);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'}หน่วยงาน`} 
          back="/admin/departments" 
        />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmit}>
            <div className="grids ai-center">
              <div className="grid xl-1-3 lg-45 md-40">
                <TextField 
                  required={true} label="ชื่อหน่วยงาน" variant="outlined" fullWidth={true} 
                  value={values.name? values.name: ''} 
                  onChange={e => onChangeInput('name', e.target.value)} 
                  disabled={process === 'view'} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <FormControl 
                  variant="outlined" fullWidth={true} 
                  disabled={process === 'view'} 
                >
                  <InputLabel id="input-status">สถานะ</InputLabel>
                  <Select
                    labelId="input-status" label="สถานะ" 
                    value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                  >
                    <MenuItem value={1}>เปิดใช้งาน</MenuItem>
                    <MenuItem value={0}>ปิดใช้งาน</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div className="sep"></div>

              <div className="grid sm-100">
                <div className="mt-2">
                  {process === 'add'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<AddIcon />} className={`mr-2`} 
                    >
                      สร้าง
                    </Button>
                  ): process === 'edit'? (
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      size="large" startIcon={<EditIcon />} className={`mr-2`} 
                    >
                      บันทึก
                    </Button>
                  ): ('')}
                  <Button 
                    component={Link} to="/admin/departments" 
                    variant="outlined" color="primary" size="large"
                  >
                    ย้อนกลับ
                  </Button>
                </div>
              </div>
            </div>
          </form>
        
          {process === 'view'? (
            <>
              <div className="pt-2 mt-6 mb-6"><Divider /></div>
              <div className="h6 fw-600 mb-2">พนักงาน</div>
              <TableContainer>
                <Table size="medium">
                  <TableHead>
                    <TableRow>
                      <TableCell className="ws-nowrap" align="center" padding="none">
                        ID
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        ชื่อจริง
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        นามสกุล
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        บทบาท
                      </TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">
                        สถานะ
                      </TableCell>
                      {user.isAdmin()? (
                        <TableCell align="center" padding="none"></TableCell>
                      ): (<></>)}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!props.single.members.length? (
                      <TableRow>
                        <TableCell colSpan={user.isAdmin()? 6: 5} align="center">
                          ไม่พบพนักงานในหน่วยงาน
                        </TableCell>
                      </TableRow>
                    ): props.single.members.map((d, i) => (
                      <TableRow key={i} hover>
                        <TableCell align="center">
                          {i + 1}
                        </TableCell>
                        <TableCell align="left">
                          {d.firstname}
                        </TableCell>
                        <TableCell align="left">
                          {d.lastname}
                        </TableCell>
                        <TableCell align="left" className="ws-nowrap">
                          {d.isRegistered()? (
                            <Chip size="small" label={d.displayRole()} color="primary" />
                          ): (
                            <Chip size="small" label="Not Registered" color="secondary" />
                          )}
                        </TableCell>
                        <TableCell align="left">
                          {d.isRegistered()? (
                            <Switch checked={d.status? true: false} color="primary" />
                          ): ('')}
                        </TableCell>
                        {user.isAdmin()? (
                          <TableCell align="center" className="ws-nowrap" padding="none">
                            <IconButton 
                              size="small" component={Link} to={`/admin/user/view/${d._id}`}
                            >
                              <VisibilityIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                            {d.isAdmin()? (''): (
                              <IconButton 
                                size="small" component={Link} to={`/admin/user/edit/${d._id}`}
                              >
                                <EditIcon style={{ fontSize: '21px' }} />
                              </IconButton>
                            )}
                          </TableCell>
                        ): (<></>)}
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </>
          ): ('')}
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

DepartmentPage.defaultProps = {
	activeIndex: 9
};
DepartmentPage.propTypes = {
	activeIndex: PropTypes.number,
	user: PropTypes.object.isRequired,
  processRead: PropTypes.func.isRequired,
  processAdd: PropTypes.func.isRequired,
  processEdit: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  single: state.general.department
});

export default connect(mapStateToProps, {
  processRead: qcoDepartmentRead, 
  processAdd: adminDepartmentAdd, 
  processEdit: adminDepartmentEdit
})(DepartmentPage);