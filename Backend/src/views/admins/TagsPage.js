import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  IconButton, TextField, Button, Chip,
  Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { qcoTagList, qcoTagDelete } from '../../actions/qco.actions';

function TagsPage(props) {
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'แท็ก', numeric: false, noPadding: false },
    { label: 'สถานะ', numeric: false, noPadding: false },
    { label: '', numeric: false, noPadding: true },
  ];
  
  const [keyword, setKeyword] = useState('');
  const [paginate, setPaginate] = useState({ page: 1, pp: 10, keyword: keyword });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };
  const onKeywordSubmit = (e) => {
    e.preventDefault();
    setPaginate({ ...paginate, page: 1 });
    props.processList({ keyword: keyword });
  };

  const [selectedData, setSelectedData] = useState(null);
  const [dialog, setDialog] = useState(false);
  const onDialogOpen = (e, data) => {
    e.preventDefault();
    setSelectedData(data);
    setDialog(true);
  };
  const onDialogClose = (e) => {
    e.preventDefault();
    setSelectedData(null);
    setDialog(false);
  };
  const onDialogDelete = async (e) => {
    e.preventDefault();
    await props.processDelete({ id: selectedData._id });
    setSelectedData(null);
    setDialog(false);
    props.processList({ keyword: keyword });
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => props.processList({ keyword: keyword }), []);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="แท็ก" />
        
        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids table-options">
            <div className="grid lg-25 md-40 sm-40">
              <form onSubmit={onKeywordSubmit}>
                <TextField 
                  label="ค้นหา" variant="outlined" fullWidth={true} size="small" 
                  value={keyword} onChange={e => setKeyword(e.target.value)} 
                />
              </form>
            </div>
            <div className="grid lg-25 md-40 sm-50">
              <Button 
                component={Link} to="/admin/tag/add" startIcon={<AddIcon />} 
                variant="contained" color="primary" size="large" 
              >
                สร้างแท็ก
              </Button>
            </div>
          </div>
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  {columns.map((col, i) => (
                    <TableCell
                      key={i} className="ws-nowrap"
                      align={col.numeric ? 'center' : 'left'}
                      padding={col.noPadding ? 'none' : 'normal'}
                    >
                      {col.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {!props.list.length? (
                  <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): props.list
                .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                .map((d, i) => {
                  let index = (paginate.page-1)*paginate.pp + i + 1;
                  return (
                    <TableRow key={index} hover>
                      <TableCell align="center">
                        {index}
                      </TableCell>
                      <TableCell align="left">
                        {d.name}
                      </TableCell>
                      <TableCell align="left">
                        {d.status === -1? (
                          <Chip label="เสนอแนะ" size="small" />
                        ): d.status === 0? (
                          <Chip label="ปิดใช้งาน" size="small" color="secondary" />
                        ): (
                          <Chip label="เปิดใช้งาน" size="small" color="primary" />
                        )}
                      </TableCell>
                      <TableCell align="center" className="ws-nowrap" padding="none">
                        <IconButton 
                          size="small" component={Link} to={`/admin/tag/view/${d._id}`}
                        >
                          <VisibilityIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        <IconButton 
                          size="small" component={Link} to={`/admin/tag/edit/${d._id}`}
                        >
                          <EditIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        <IconButton size="small" onClick={e => onDialogOpen(e, d)}>
                          <DeleteIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            labelRowsPerPage="แสดง" 
            rowsPerPageOptions={[10, 25, 50, 100]} 
            component="div" 
            count={props.list.length} 
            rowsPerPage={paginate.pp} 
            page={paginate.page - 1} 
            onPageChange={onChangePage} 
            onRowsPerPageChange={onChangePerPage} 
          />
        </div>

        <FooterAdmin />
      </div>
      
      <Dialog open={dialog} onClose={onDialogClose}>
        <DialogTitle>ยืนยันการลบแท็ก</DialogTitle>
        <DialogContent>
          <DialogContentText>
            ยืนยันการลบข้อมูล โดยข้อมูลที่ถูกลบจะไม่สามารถกู้คืนได้
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDialogClose} color="primary">
            ยกเลิก
          </Button>
          <Button onClick={onDialogDelete} color="primary" autoFocus>
            ยืนยันการลบ
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TagsPage.defaultProps = {
	activeIndex: 6
};
TagsPage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired,
  processDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  list: state.general.tags
});

export default connect(mapStateToProps, {
  processList: qcoTagList, 
  processDelete: qcoTagDelete
})(TagsPage);