import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
  IconButton, Avatar, TextField, Chip, Button
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { adminUserList, adminUserDelete } from '../../actions/admin.actions';

function UsersPage(props) {
  const [ip, setIp] = useState('');
  const url = window.location.href;

  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'รูปโปรไฟล์', numeric: false, noPadding: false },
    { label: 'ชื่อจริง', numeric: false, noPadding: false },
    { label: 'นามสกุล', numeric: false, noPadding: false },
    { label: 'อีเมล', numeric: false, noPadding: false },
    { label: 'หน่วยงาน', numeric: false, noPadding: false },
    { label: 'บทบาท', numeric: false, noPadding: false },
    { label: 'สถานะ', numeric: false, noPadding: false },
    { label: '', numeric: false, noPadding: true },
  ];

  const [keyword, setKeyword] = useState('');
  const [paginate, setPaginate] = useState({ page: 1, pp: 10, keyword: keyword });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };
  const onKeywordSubmit = (e) => {
    e.preventDefault();
    setPaginate({ ...paginate, page: 1, keyword: keyword });
  };

  const [selectedData, setSelectedData] = useState(null);
  const [dialog, setDialog] = useState(false);
  const onDialogOpen = (e, data) => {
    e.preventDefault();
    setSelectedData(data);
    setDialog(true);
  };
  const onDialogClose = (e) => {
    e.preventDefault();
    setSelectedData(null);
    setDialog(false);
  };
  const onDialogDelete = async (e) => {
    e.preventDefault();
    await props.processDelete({ id: selectedData._id, ip: ip, url: url });
    setSelectedData(null);
    setDialog(false);
    props.processList(paginate);
  };
  
  /* eslint-disable */
  useEffect(() => onMounted(setIp, true), []);
  useEffect(() => props.processList(paginate), [paginate]);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="บัญชีผู้ใช้" />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids table-options">
            <div className="grid lg-25 md-40 sm-40">
              <form onSubmit={onKeywordSubmit}>
                <TextField 
                  label="ค้นหา" variant="outlined" fullWidth={true} size="small" 
                  value={keyword} onChange={e => setKeyword(e.target.value)} 
                />
              </form>
            </div>
            <div className="grid lg-25 md-40 sm-50">
              <Button 
                component={Link} to="/admin/user/add" startIcon={<AddIcon />} 
                variant="contained" color="primary" size="large" 
              >
                สร้างผู้ใช้
              </Button>
            </div>
          </div>
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  {columns.map((col, i) => (
                    <TableCell
                      key={i} className="ws-nowrap"
                      align={col.numeric ? 'center' : 'left'}
                      padding={col.noPadding ? 'none' : 'normal'}
                    >
                      {col.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {!props.list.result.length? (
                  <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): props.list.result.map((d, i) => (
                  <TableRow key={i} hover>
                    <TableCell align="center">
                      {props.list.paginate.anchorId() + i}
                    </TableCell>
                    <TableCell align="center">
								      <Avatar src={d.avatar} className="sm" />
                    </TableCell>
                    <TableCell align="left">
								      {d.firstname}
                    </TableCell>
                    <TableCell align="left">
								      {d.lastname}
                    </TableCell>
                    <TableCell align="left">
								      {d.email}
                    </TableCell>
                    <TableCell align="left" className="ws-nowrap">
								      {d.displayDepartment()? (
                        <>{d.displayDepartment()}</>
                      ): ('-')}
                    </TableCell>
                    <TableCell align="left" className="ws-nowrap">
								      {d.isRegistered()? (
                        <Chip size="small" label={d.displayRole()} color="primary" />
                      ): (
                        <Chip size="small" label="Not Registered" color="secondary" />
                      )}
                    </TableCell>
                    <TableCell align="left">
								      {d.isRegistered()? (
                        <>
                          {d.status === 0? (
                            <Chip label="ปิดใช้งาน" size="small" color="secondary" />
                          ): (
                            <Chip label="เปิดใช้งาน" size="small" color="primary" />
                          )}
                        </>
                      ): ('')}
                    </TableCell>
                    <TableCell align="center" className="ws-nowrap" padding="none">
								      {d.isRegistered()? (
                        <>
                          <IconButton 
                            size="small" component={Link} to={`/admin/user/view/${d._id}`}
                          >
                            <VisibilityIcon style={{ fontSize: '21px' }} />
                          </IconButton>
                          {d.isAdmin()? (''): (
                            <IconButton 
                              size="small" component={Link} to={`/admin/user/edit/${d._id}`}
                            >
                              <EditIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                          )}
                          {d.isQCO()? (''): (
                            <IconButton size="small" onClick={e => onDialogOpen(e, d)}>
                              <DeleteIcon style={{ fontSize: '21px' }} />
                            </IconButton>
                          )}
                        </>
                      ): ('')}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            labelRowsPerPage="แสดง" 
            rowsPerPageOptions={[10, 25, 50, 100]} 
            component="div" 
            count={props.list.paginate.total} 
            rowsPerPage={paginate.pp} 
            page={paginate.page - 1} 
            onPageChange={onChangePage} 
            onRowsPerPageChange={onChangePerPage} 
          />
        </div>

        <FooterAdmin />
      </div>

      <Dialog open={dialog} onClose={onDialogClose}>
        <DialogTitle>ยืนยันการลบบัญชีผู้ใช้</DialogTitle>
        <DialogContent>
          <DialogContentText>
            ยืนยันการลบข้อมูล โดยข้อมูลที่ถูกลบจะไม่สามารถกู้คืนได้
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDialogClose} color="primary">
            ยกเลิก
          </Button>
          <Button onClick={onDialogDelete} color="primary" autoFocus>
            ยืนยันการลบ
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

UsersPage.defaultProps = {
	activeIndex: 10
};
UsersPage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired,
  processDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  list: state.admin.users
});

export default connect(mapStateToProps, {
  processList: adminUserList, 
  processDelete: adminUserDelete
})(UsersPage);