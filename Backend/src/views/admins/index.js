export {default as DashboardPage} from './DashboardPage';
export {default as ProfilePage} from './ProfilePage';

export {default as ChannelsPage} from './ChannelsPage';
export {default as ChannelPage} from './ChannelPage';
export {default as PlaylistsPage} from './PlaylistsPage';
export {default as PlaylistPage} from './PlaylistPage';
export {default as ContentsPage} from './ContentsPage';
export {default as ContentPage} from './ContentPage';
export {default as ContentPreviewPage} from './ContentPreviewPage';

export {default as TagsPage} from './TagsPage';
export {default as TagPage} from './TagPage';
export {default as LevelsPage} from './LevelsPage';
export {default as LevelPage} from './LevelPage';
export {default as SubjectsPage} from './SubjectsPage';
export {default as SubjectPage} from './SubjectPage';

export {default as DepartmentsPage} from './DepartmentsPage';
export {default as DepartmentPage} from './DepartmentPage';
export {default as UsersPage} from './UsersPage';
export {default as UserPage} from './UserPage';

export {default as ImportContentsPage} from './ImportContentsPage';
