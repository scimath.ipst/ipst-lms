import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TextField, Button, Divider,
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell,
  FormControl, InputLabel, Select, MenuItem, Chip
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { UserModel } from '../../models';
import { memberUserList } from '../../actions/member.actions';
import { 
  qcoTagList, qcoPlaylistList, qcoLevelList, qcoSubjectList, qcoContentAdd 
} from '../../actions/qco.actions';
import { adminYoutubeGetPlaylistItems } from '../../actions/admin.actions';

function ImportContentsPage(props) {
  const user = new UserModel(props.user);

  const [values, setValues] = useState({
    youtubePlaylistId: '',
    playlistId: '',
    levels: [],
    subjectId: '',
    tags: [],
    userId: user._id,
    status: 1
  });
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  const [foundPlaylist, setFoundPlaylist] = useState(0);
  const [playlistItems, setPlaylistItems] = useState([]);

  const onSubmitYoutubePlaylistId = async (e) => {
    e.preventDefault();
    setFoundPlaylist(0);
    setPlaylistItems([]);
    props.youtubeGetPlaylistItems({ playlistId: values.youtubePlaylistId }).then(res => {
      if(res){
        setFoundPlaylist(2);
        var data = res.map((d, i) => {
          let image = null;
          if(d.snippet.thumbnails){
            if(d.snippet.thumbnails.maxres) image = d.snippet.thumbnails.maxres.url;
            else if(d.snippet.thumbnails.high) image = d.snippet.thumbnails.high.url;
            else if(d.snippet.thumbnails.medium) image = d.snippet.thumbnails.medium.url;
            else if(d.snippet.thumbnails.standard) image = d.snippet.thumbnails.standard.url;
            else if(d.snippet.thumbnails.default) image = d.snippet.thumbnails.default.url;
          }
          return {
            name: d.snippet.title,
            description: d.snippet.description? d.snippet.description: '',
            url: d.snippet.resourceId.videoId,
            image: image,
            type: 1,
            youtubeVideoId: d.snippet.resourceId.videoId,
            order: i
          };
        });
        setPlaylistItems(data);
      }else{
        setFoundPlaylist(1);
      }
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    playlistItems.forEach(async (playlistItem) => {
      await props.processAdd({ ...values, ...playlistItem });
    });
    setFoundPlaylist(3);
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    props.playlistList({ keyword: '' });
    props.tagList({ keyword: '' });
    props.levelList({ keyword: '' });
    props.subjectList({ keyword: '' });
    props.processUserList({ page: 1, pp: 9999, keyword: '' });
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="นำเข้าเนื้อหา" />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <form onSubmit={onSubmitYoutubePlaylistId}>
            <div className="grids ai-center">
              <div className="grid xl-1-3 lg-45 md-60">
                <TextField 
                  required={true} label="Youtube Playlist ID" variant="outlined" fullWidth={true} 
                  value={values.youtubePlaylistId? values.youtubePlaylistId: ''} 
                  onChange={e => onChangeInput('youtubePlaylistId', e.target.value)} 
                />
              </div>
              <div className="grid xl-1-3 lg-45 md-40">
                <Button 
                  type="submit" variant="contained" color="primary" 
                  size="large" startIcon={<AddIcon />} className={`mr-2`} 
                >
                  ตรวจสอบ
                </Button>
              </div>
            </div>
          </form>

          {foundPlaylist === 1? (
            <h6 className="fw-600 mt-4">ไม่พบ Playlist บน Youtube</h6>
          ): foundPlaylist === 2? (
            <div className="mt-4">
              <Divider />
              <TableContainer>
                <Table size="medium">
                  <TableHead>
                    <TableRow>
                      <TableCell className="ws-nowrap" align="center" padding="normal">รูปภาพ</TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">ชื่อวิดีโอ</TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">คำบรรยาย</TableCell>
                      <TableCell className="ws-nowrap" align="left" padding="normal">วิดีโอไอดี</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!playlistItems.length? (
                      <TableRow>
                        <TableCell colSpan={4} align="center">
                          ไม่พบวิดีโอใน Playlist นี้
                        </TableCell>
                      </TableRow>
                    ): playlistItems.map((d, i) => {
                      return (
                        <TableRow key={i} hover>
                          <TableCell align="center">
                            <div className="img-preview">
                              <div className="img-bg" style={{ backgroundImage: `url('${d.image}')` }}></div>
                            </div>
                          </TableCell>
                          <TableCell align="left">
                            {d.name}
                          </TableCell>
                          <TableCell align="left" style={{ maxWidth: 700 }}>
                            {d.description}
                          </TableCell>
                          <TableCell align="left">
                            {d.youtubeVideoId}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              <form onSubmit={onSubmit} className="mt-4">
                <div className="grids">
                  <div className="grid xl-2-3 md-70 sm-100">
                    <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                      <InputLabel id="input-playlist">Playlist *</InputLabel>
                      <Select
                        required={true} labelId="input-playlist" label="Playlist *" 
                        value={props.playlists.length? values.playlistId: ''} 
                        onChange={e => onChangeInput('playlistId', e.target.value)} 
                      >
                        {props.playlists.map((d, i) => (
                          <MenuItem key={i} value={d._id}>{d.displayFullName()}</MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                  <div className="sep"></div>

                  <div className="grid xl-2-3 md-70 sm-100">
                    <Autocomplete
                      multiple id="input-levels" 
                      value={values.levels.length? values.levels: []} 
                      options={props.levels} filterSelectedOptions 
                      getOptionLabel={(option) => option.name} 
                      getOptionSelected={(option, value) => option._id === value._id}
                      onChange={(e, val) => onChangeInput('levels', val)} 
                      fullWidth={true} disabled={process === 'view'} 
                      renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                          <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                        ))
                      }
                      renderInput={(params) => (
                        <TextField 
                          {...params} variant="outlined" label="ระดับชั้น" 
                          required={values.levels.length === 0} 
                          InputLabelProps={{ required: true }} 
                        />
                      )}
                    />
                  </div>
                  <div className="grid xl-2-3 md-70 sm-100">
                    <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                      <InputLabel id="input-subject">วิชา *</InputLabel>
                      <Select
                        required={true} labelId="input-subject" label="วิชา *" 
                        value={props.subjects.length? values.subjectId: ''} 
                        onChange={e => onChangeInput('subjectId', e.target.value)} 
                      >
                        {props.subjects.map((d, i) => (
                          <MenuItem key={i} value={d._id}>{d.name}</MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                  <div className="grid xl-2-3 md-70 sm-100">
                    <Autocomplete
                      multiple id="input-tags" 
                      value={values.tags.length? values.tags: []} 
                      options={props.tags} filterSelectedOptions 
                      getOptionLabel={(option) => option.name} 
                      getOptionSelected={(option, value) => option._id === value._id}
                      onChange={(e, val) => onChangeInput('tags', val)} 
                      fullWidth={true} disabled={process === 'view'} 
                      renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                          <Chip variant="outlined" label={option.name} {...getTagProps({ index })} />
                        ))
                      }
                      renderInput={(params) => (
                        <TextField {...params} variant="outlined" label="แท็ก" />
                      )}
                    />
                  </div>
                  <div className="sep"></div>
                  
                  <div className="grid xl-2-3 md-70 sm-100">
                    <FormControl variant="outlined" fullWidth={true} disabled={process === 'view'}>
                      <InputLabel id="input-user">เจ้าของ *</InputLabel>
                      <Select
                        required={true} labelId="input-user" label="เจ้าของ *" 
                        value={props.userList && props.userList.result && props.userList.result.length? values.userId: ''} 
                        onChange={e => onChangeInput('userId', e.target.value)} 
                      >
                        {props.userList.result.map((d, i) => {
                          if(d.isRegistered()){
                            return (
                              <MenuItem key={i} value={d._id}>{d.displayName()}</MenuItem>
                            );
                          }else{
                            return false;
                          }
                        })}
                      </Select>
                    </FormControl>
                  </div>
                  <div className="sep"></div>

                  <div className="grid xl-1-3 md-35 sm-50">
                    <FormControl 
                      variant="outlined" fullWidth={true} 
                      disabled={process === 'view'} 
                    >
                      <InputLabel id="input-status">สถานะ</InputLabel>
                      <Select
                        labelId="input-status" label="สถานะ" 
                        value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                      >
                        <MenuItem value={0}>Unlisted</MenuItem>
                        <MenuItem value={1}>Drafted</MenuItem>
                        <MenuItem value={2}>Private</MenuItem>
                        <MenuItem value={3}>Public</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                </div>
                <div className="mt-4">
                  <Button 
                    type="submit" variant="contained" color="primary" 
                    size="large" startIcon={<AddIcon />} className={`mr-2`} 
                  >
                    นำเข้าเนื้อหา
                  </Button>
                </div>
              </form>
            </div>
          ): foundPlaylist === 3? (
            <h6 className="fw-600 mt-4">การนำเข้า Playlist จาก Youtube สำเร็จ</h6>
          ): (<></>)}
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ImportContentsPage.defaultProps = {
	activeIndex: 11
};
ImportContentsPage.propTypes = {
	activeIndex: PropTypes.number,
  youtubeGetPlaylistItems: PropTypes.func.isRequired,
  playlistList: PropTypes.func.isRequired,
  tagList: PropTypes.func.isRequired,
  processUserList: PropTypes.func.isRequired,
  processAdd: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user,
  playlists: state.general.playlists,
  tags: state.general.tags,
  levels: state.general.levels,
  subjects: state.general.subjects,
  userList: state.general.users
});

export default connect(mapStateToProps, {
  youtubeGetPlaylistItems: adminYoutubeGetPlaylistItems, 
  playlistList: qcoPlaylistList,
  tagList: qcoTagList,
  levelList: qcoLevelList,
  subjectList: qcoSubjectList,
  processUserList: memberUserList,
  processAdd: qcoContentAdd
})(ImportContentsPage);