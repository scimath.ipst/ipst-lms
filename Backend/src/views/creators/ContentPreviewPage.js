import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import YoutubeVideoPreview from '../../components/YoutubeVideoPreview';
import { Chip, Avatar, Button } from '@material-ui/core';
import { onMounted, formatDate, formatNumber } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { creatorContentRead } from '../../actions/creator.actions';

function ContentPreviewPage(props) {
  const process = props.match.params.process;
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(dataId) props.processRead({id: dataId});
  }, []);
  /* eslint-enable */

  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`ดูตัวอย่าง Content - ${props.single.name}`} 
          back={`/creator/content/${process}/${dataId}`} 
        />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids jc-center">
            <div className="grid xl-70 lg-80 md-80 sm-100">
          
              {props.single.type === 1? (
                // Youtube Video
                <YoutubeVideoPreview content={props.single} />
              ): props.single.type === 2? (
                <div className="video-container">
                  <div className="wrapper">
                    <iframe src={props.single.filePath} title="Content MP4"></iframe>
                  </div>
                </div>
              ): props.single.type === 3? (
                <div className="pdf-container">
                  <div className="wrapper">
                    <iframe src={props.single.filePath} title="Content PDF"></iframe>
                  </div>
                </div>
              ): props.single.type === 4? (
                // iFrame
                <div className="iframe-container">
                  <div className="wrapper">
                    <iframe src={props.single.filePath} title="Content iFrame"></iframe>
                  </div>
                </div>
              ): (<></>)}

              <div className="pt-4 pb-5">
                <div className="d-flex fw-wrap">
                  {props.single.levels.map((level, j) => (
                    <div className="mt-1 mr-1" key={`level-${j}`}>
                      <Chip color="primary" label={level.name} />
                    </div>
                  ))}
                  {props.single.subject.name? (
                    <div className="mt-1">
                      <Chip color="primary" label={props.single.subject.name} />
                    </div>
                  ): (<></>)}
                </div>
                <h5 className="fw-600 mt-3">{props.single.name}</h5>
                <p className="xs fw-400 op-80 mt-2">
                  ผู้ชม {formatNumber(props.single.visitCount, 0)} คน 
                  <span className="p-dot ml-3 mr-3"></span> 
                  {formatDate(props.single.createdAt)}
                </p>
                <div className="d-flex ai-center mt-5">
                  <Avatar src={props.single.user.avatar} className={`sm`} />
                  <p className="fw-300 pl-3">{props.single.user.displayName()}</p>
                </div>
                {props.single.description? (
                  <p className="fw-400 lh-md mt-5">{props.single.description}</p>
                ): (<></>)}
                {props.single.tags.length? (
                  <p className="fw-500 mt-5">
                    <span className="mr-2">แท็ก :</span> 
                    {props.single.tags.map((tag, i) => (
                      <span className="color-p mr-2" key={i}>#{tag.name}</span>
                    ))}
                  </p>
                ): (<></>)}
                
              </div>
            </div>
          </div>
          {/* Content Buttons */}
          <div className="text-center mt-2">
            <Button 
              component={Link} to={`/creator/content/${process}/${dataId}`} 
              variant="outlined" color="primary" size="large"
            >ย้อนกลับ</Button>
          </div>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

ContentPreviewPage.defaultProps = {
	activeIndex: 3
};
ContentPreviewPage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  single: state.general.content
});

export default connect(mapStateToProps, {
  processRead: creatorContentRead
})(ContentPreviewPage);