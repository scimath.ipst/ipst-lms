import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TextField, FormControl, InputLabel, Select, MenuItem, Button, Chip,
  OutlinedInput, InputAdornment
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { FRONTEND_URL } from '../../actions/types';
import { ChannelModel } from '../../models';
import { creatorChannelRead } from '../../actions/creator.actions';

function CreatorChannelPage(props) {
  const process = 'view';
  const dataId = props.match.params.dataId? props.match.params.dataId: null;
  
  const [values, setValues] = useState(new ChannelModel({status: 1}));
  const onChangeInput = (key, val) => {
    setValues({ ...values, [key]: val });
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    if(dataId) props.processRead({id: dataId});
  }, []);
  useEffect(() => {
    if(dataId && ['view', 'edit'].indexOf(process) > -1) {
      setValues(props.single);
    }
  }, [props.single]);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle 
          title={`${process === 'add'? 'สร้าง': process === 'view'? 'ดู': 'แก้ไข'} Channel`} 
          back="/creator/channels" 
        />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids">

            <div className="grid xl-2-3 md-70 sm-100 mt-0">
              <div className="grids">
                <div className="grid sm-100">
                  <TextField 
                    required={true} label="ชื่อ Channel" variant="outlined" fullWidth={true} 
                    value={values.name? values.name: ''} 
                    onChange={e => onChangeInput('name', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid sm-100">
                  <FormControl fullWidth variant="outlined">
                    <InputLabel htmlFor="input-url">ลิงค์ *</InputLabel>
                    <OutlinedInput
                      id="input-url"
                      required={true} label="ลิงค์ *" variant="outlined" fullWidth={true} 
                      value={values.url? values.url: ''} 
                      onChange={e => onChangeInput('url', e.target.value)} 
                      disabled={process === 'view'} 
                      startAdornment={
                        <InputAdornment position="start" className="op-60">
                          {FRONTEND_URL}channel/
                        </InputAdornment>
                      } 
                    />
                  </FormControl>
                </div>
                
                <div className="grid sm-100">
                  <TextField 
                    label="คำบรรยาย" variant="outlined" fullWidth={true} 
                    value={values.description? values.description: ''} 
                    onChange={e => onChangeInput('description', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>

                <div className="grid sm-100">
                  <TextField 
                    label="คำบรรยาย Meta" variant="outlined" fullWidth={true} 
                    value={values.metadesc? values.metadesc: ''} 
                    onChange={e => onChangeInput('metadesc', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="sep"></div>

                <div className="grid sm-100">
                  <Autocomplete
                    multiple freeSolo id="input-keywords" 
                    value={values.keywords} options={[]} 
                    onChange={(e, val) => onChangeInput('keywords', val)} 
                    fullWidth={true} disabled={process === 'view'} 
                    renderTags={(value, getTagProps) =>
                      value.map((option, index) => (
                        <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                      ))
                    }
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" label="คำค้นหา" />
                    )}
                  />
                </div>
                <div className="sep"></div>

                <div className="grid sm-50">
                  <TextField 
                    label="ลำดับ" variant="outlined" fullWidth={true} 
                    value={values.order? values.order: 0} type="number" 
                    onChange={e => onChangeInput('order', e.target.value)} 
                    disabled={process === 'view'} 
                  />
                </div>
                <div className="grid sm-50">
                  <FormControl 
                    variant="outlined" fullWidth={true} 
                    disabled={process === 'view'} 
                  >
                    <InputLabel id="input-status">สถานะ</InputLabel>
                    <Select
                      labelId="input-status" label="สถานะ" 
                      value={values.status} onChange={e => onChangeInput('status', e.target.value)} 
                    >
                      <MenuItem value={0}>Unlisted</MenuItem>
                      <MenuItem value={1}>Drafted</MenuItem>
                      <MenuItem value={2}>Private</MenuItem>
                      <MenuItem value={3}>Public</MenuItem>
                    </Select>
                  </FormControl>
                </div>
              </div>
            </div>

            <div className="grid xl-1-3 md-30">
              <div className="input-upload">
                <div className="wrapper">
                  <div className="img-wrapper">
                    {values.image? (
                      <div className="img-bg" style={{ backgroundImage: `url('${values.image}')` }}></div>
                    ): (
                      <div className="img-bg" style={{ backgroundImage: `url('/assets/img/default/img.jpg')` }}></div>
                    )}
                  </div>
                  {process === 'view'? <template></template>: (
                    <p className="fw-400 mt-3">คลิกเพื่อ Upload รูปภาพ</p>
                  )}
                </div>
              </div>
            </div>

            <div className="grid sm-100">
              <div className="mt-2">
                <Button 
                  component={Link} to="/creator/channels" 
                  variant="outlined" color="primary" size="large"
                >
                  ย้อนกลับ
                </Button>
              </div>
            </div>

          </div>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

CreatorChannelPage.defaultProps = {
	activeIndex: 1
};
CreatorChannelPage.propTypes = {
	activeIndex: PropTypes.number,
  processRead: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  single: state.general.channel,
  departments: state.general.departments
});

export default connect(mapStateToProps, {
  processRead: creatorChannelRead
})(CreatorChannelPage);