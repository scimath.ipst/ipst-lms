import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { 
  TableContainer, Table, TableHead, TableBody, TableRow, TableCell, TablePagination,
  IconButton, TextField, Button, Chip, FormControl, InputLabel, Select, MenuItem,
  Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { onMounted, formatNumber } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { creatorContentList, creatorContentDelete } from '../../actions/creator.actions';

function CreatorPlaylistPage(props) {
  const columns = [
    { label: 'ID', numeric: true, noPadding: true },
    { label: 'รูปภาพ', numeric: false, noPadding: false },
    { label: 'Content', numeric: false, noPadding: false },
    { label: 'ลิงค์', numeric: false, noPadding: false },
    { label: 'ผู้สร้าง', numeric: false, noPadding: false },
    { label: 'ผู้ชม', numeric: false, noPadding: false },
    { label: 'สถานะ', numeric: false, noPadding: false },
    { label: 'การตรวจสอบ', numeric: false, noPadding: false },
    { label: '', numeric: false, noPadding: true },
  ];
  
  const [keyword, setKeyword] = useState('');
  const [status, setStatus] = useState('');
  const [approvedStatus, setApprovedStatus] = useState('');
  const [paginate, setPaginate] = useState({ page: 1, pp: 10, keyword: keyword });
  const onChangePage = (e, newPage) => {
    setPaginate({ ...paginate, page: newPage+1 });
  };
  const onChangePerPage = (e) => {
    setPaginate({ ...paginate, page: 1, pp: e.target.value });
  };
  const onKeywordSubmit = (e) => {
    e.preventDefault();
    setPaginate({ ...paginate, page: 1 });
    props.processList({ keyword: keyword, status: status, approvedStatus: approvedStatus });
  };
  const onChangeFilterStatus = (val) => {
    setStatus(val);
    setPaginate({ ...paginate, page: 1 });
    props.processList({ keyword: keyword, status: val, approvedStatus: approvedStatus });
  };
  const onChangeFilterApprovedStatus = (val) => {
    setApprovedStatus(val);
    setPaginate({ ...paginate, page: 1 });
    props.processList({ keyword: keyword, status: status, approvedStatus: val });
  };

  const [selectedData, setSelectedData] = useState(null);
  const [dialog, setDialog] = useState(false);
  const onDialogOpen = (e, data) => {
    e.preventDefault();
    setSelectedData(data);
    setDialog(true);
  };
  const onDialogClose = (e) => {
    e.preventDefault();
    setSelectedData(null);
    setDialog(false);
  };
  const onDialogDelete = async (e) => {
    e.preventDefault();
    await props.processDelete({ id: selectedData._id });
    setSelectedData(null);
    setDialog(false);
    props.processList({ keyword: keyword, status: status, approvedStatus: approvedStatus });
  };

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  useEffect(() => {
    props.processList({ keyword: keyword, status: status, approvedStatus: approvedStatus });
  }, []);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title="การจัดการ Contents" />
        
        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids table-options">
            <div className="grid lg-75 sm-100 mt-0">
              <div className="grids">
                <div className="grid xl-30 lg-1-3 md-40 sm-100">
                  <form onSubmit={onKeywordSubmit}>
                    <TextField 
                      label="ค้นหา" variant="outlined" fullWidth={true} size="small" 
                      value={keyword} onChange={e => setKeyword(e.target.value)} 
                    />
                  </form>
                </div>
                <div className="grid xl-25 lg-1-3 md-30 sm-50">
                  <FormControl variant="outlined" fullWidth={true} size="small">
                    <InputLabel id="input-status">สถานะ</InputLabel>
                    <Select
                      labelId="input-status" label="สถานะ" 
                      value={status} onChange={e => onChangeFilterStatus(e.target.value)} 
                    >
                      <MenuItem value={''}>Show all</MenuItem>
                      <MenuItem value={0}>Unlisted</MenuItem>
                      <MenuItem value={1}>Drafted</MenuItem>
                      <MenuItem value={2}>Private</MenuItem>
                      <MenuItem value={3}>Public</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div className="grid xl-25 lg-1-3 md-30 sm-50">
                  <FormControl variant="outlined" fullWidth={true} size="small">
                    <InputLabel id="input-approved-status">สถานะตรวจสอบ</InputLabel>
                    <Select
                      labelId="input-approved-status" label="สถานะตรวจสอบ" 
                      value={approvedStatus} 
                      onChange={e => onChangeFilterApprovedStatus(e.target.value)} 
                    >
                      <MenuItem value={''}>Show all</MenuItem>
                      <MenuItem value={-1}>Pending</MenuItem>
                      <MenuItem value={0}>Not approved</MenuItem>
                      <MenuItem value={1}>Approved</MenuItem>
                    </Select>
                  </FormControl>
                </div>
              </div>
            </div>
            <div className="grid lg-25 sm-100">
              <Button 
                component={Link} to="/creator/content/add" startIcon={<AddIcon />} 
                variant="contained" color="primary" size="large" 
              >
                สร้าง Content
              </Button>
            </div>
          </div>
          <TableContainer>
            <Table size="medium">
              <TableHead>
                <TableRow>
                  {columns.map((col, i) => (
                    <TableCell
                      key={i} className="ws-nowrap"
                      align={col.numeric ? 'center' : 'left'}
                      padding={col.noPadding ? 'none' : 'normal'}
                    >
                      {col.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {!props.list.length? (
                  <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                      ไม่พบข้อมูลในระบบ
                    </TableCell>
                  </TableRow>
                ): props.list
                .slice((paginate.page-1)*paginate.pp, paginate.page*paginate.pp)
                .map((d, i) => {
                  let index = (paginate.page-1)*paginate.pp + i + 1;
                  return (
                    <TableRow key={index} hover>
                      <TableCell align="center">
                        {index}
                      </TableCell>
                      <TableCell align="center">
                        <div className="img-preview">
                          <div className="img-bg" style={{ backgroundImage: `url('${d.image}')` }}></div>
                        </div>
                      </TableCell>
                      <TableCell align="left">
                        {d.name}
                      </TableCell>
                      <TableCell align="left">
                        {d.url}
                      </TableCell>
                      <TableCell align="left" style={{ minWidth: '200px' }}>
                        <span className="fw-600">Channel :</span> {d.displayChannel()} <br />
                        <span className="fw-600">Playlist :</span> {d.displayPlaylist()}
                      </TableCell>
                      <TableCell align="left">
                        {formatNumber(d.visitCount, 0)}
                      </TableCell>
                      <TableCell align="left">
                        {d.status === 0? (
                          <Chip label="Unlisted" size="small" color="secondary" />
                        ): d.status === 1? (
                          <Chip label="Drafted" size="small" />
                        ): d.status === 2? (
                          <Chip label="Private" size="small" color="primary" />
                        ): (
                          <Chip label="Public" size="small" color="primary" />
                        )}
                      </TableCell>
                      <TableCell align="left">
                        {d.approvedStatus === -1? (
                          <Chip label="Pending" size="small" />
                        ): d.approvedStatus === 0? (
                          <Chip label="Not approved" size="small" color="secondary" />
                        ): (
                          <Chip label="Approved" size="small" color="primary" />
                        )}
                      </TableCell>
                      <TableCell align="center" className="ws-nowrap" padding="none">
                        <IconButton 
                          size="small" component={Link} to={`/creator/content/view/${d._id}`}
                        >
                          <VisibilityIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        <IconButton 
                          size="small" component={Link} to={`/creator/content/edit/${d._id}`}
                        >
                          <EditIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                        <IconButton size="small" onClick={e => onDialogOpen(e, d)}>
                          <DeleteIcon style={{ fontSize: '21px' }} />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            labelRowsPerPage="แสดง" 
            rowsPerPageOptions={[10, 25, 50, 100]} 
            component="div" 
            count={props.list.length} 
            rowsPerPage={paginate.pp} 
            page={paginate.page - 1} 
            onPageChange={onChangePage} 
            onRowsPerPageChange={onChangePerPage} 
          />
        </div>

        <FooterAdmin />
      </div>
      
      <Dialog open={dialog} onClose={onDialogClose}>
        <DialogTitle>ยืนยันการลบ Content</DialogTitle>
        <DialogContent>
          <DialogContentText>
            ยืนยันการลบข้อมูล โดยข้อมูลที่ถูกลบจะไม่สามารถกู้คืนได้
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDialogClose} color="primary">
            ยกเลิก
          </Button>
          <Button onClick={onDialogDelete} color="primary" autoFocus>
            ยืนยันการลบ
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

CreatorPlaylistPage.defaultProps = {
	activeIndex: 3
};
CreatorPlaylistPage.propTypes = {
	activeIndex: PropTypes.number,
  processList: PropTypes.func.isRequired,
  processDelete: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  list: state.general.contents
});

export default connect(mapStateToProps, {
  processList: creatorContentList, 
  processDelete: creatorContentDelete
})(CreatorPlaylistPage);