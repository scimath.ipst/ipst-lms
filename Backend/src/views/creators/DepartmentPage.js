import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TopnavAdmin from '../../components/TopnavAdmin';
import FooterAdmin from '../../components/FooterAdmin';
import AppTitle from '../../components/AppTitle';
import { TextField, Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { DepartmentModel, UserModel } from '../../models';

function CreatorDepartmentPage(props) {
	const user = new UserModel(props.user);
  
  const [values] = useState(new DepartmentModel(user.department));

  /* eslint-disable */
  useEffect(() => onMounted(), []);
  /* eslint-enable */
  return (
    <>
      <TopnavAdmin activeIndex={props.activeIndex} />
      <div className="app-container">
        <AppTitle title={`หน่วยงาน`} back="/creator" />

        <div className="app-card" data-aos="fade-up" data-aos-delay="150">
          <div className="grids ai-center">
            <div className="grid xl-1-3 lg-45 md-40">
              <TextField 
                label="ชื่อหน่วยงาน" variant="outlined" fullWidth={true} 
                value={values.name? values.name: ''} disabled={true} 
              />
            </div>
            <div className="grid xl-1-3 lg-45 md-40">
              <TextField 
                label="สถานะ" variant="outlined" fullWidth={true} 
                value={values.status? 'เปิดใช้งาน': 'ปิดใช้งาน'} disabled={true} 
              />
            </div>
            <div className="sep"></div>

            <div className="grid sm-100">
              <div className="mt-2">
                <Button 
                  component={Link} to="/creator" 
                  variant="outlined" color="primary" size="large"
                >
                  ย้อนกลับ
                </Button>
              </div>
            </div>
          </div>
        </div>

        <FooterAdmin />
      </div>
    </>
  );
}

CreatorDepartmentPage.defaultProps = {
	activeIndex: 4
};
CreatorDepartmentPage.propTypes = {
	activeIndex: PropTypes.number,
	user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, {})(CreatorDepartmentPage);