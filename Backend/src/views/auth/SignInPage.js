import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import TopnavAdmin from '../../components/TopnavAdmin';
import Footer from '../../components/Footer';
import { TextField, Button } from '@material-ui/core';
import { onMounted } from '../../helpers/frontend';

import { connect } from 'react-redux';
import { userSignin } from '../../actions/user.actions';

function SignInPage(props) {
  const [values, setValues] = useState({
    username: '',
    password: '',
    ip: '',
    url: window.location.href
  });
  const onChangeInput = (key, fromInput=true) => (event) => {
    if(fromInput) setValues({ ...values, [key]: event.target.value });
    else setValues({ ...values, [key]: event });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    var res = await props.userSignin(values);
    if(res) {
      setTimeout(() => {
        window.location.href = '/admin';
      }, 300);
    } else {
      onChangeInput('password', false)('');
    }
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => onMounted(onChangeInput('ip', false), true), []);
  return (
    <>
      <TopnavAdmin forBackend={false} />
      <section className="auth-01">
        <div className="wrapper">

          <div className="bg-container">
            <div className="wrapper">
              <div 
                className="img-bg" style={{ backgroundImage: `url('/assets/img/bg/auth.png')` }} 
                data-aos="fade-up" data-aos-delay="450"
              ></div>
              <div className="hide-mobile">
                <h4 className="sm fw-500 color-white lh-sm" data-aos="fade-up" data-aos-delay="300">
                  นำสู่ความปกติใหม่ทางการศึกษา <br />
                  (New Normal Education)
                </h4>
              </div>
            </div>
          </div>

          <div className="auth-container" data-aos="fade-up" data-aos-delay="0">
            <div className="auth-wrapper">
              <h4 className="fw-600 text-center">
                เข้าสู่ระบบ
              </h4>
              <form onSubmit={onSubmit} autoComplete="off">
                <div className="grids">
                  <div className="grid sm-100">
                    <TextField 
                      required={true} label="อีเมล / ชื่อผู้ใช้" 
                      variant="outlined" fullWidth={true} 
                      value={values.username} onChange={onChangeInput('username')} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <TextField 
                      required={true} type="password" label="รหัสผ่าน" 
                      variant="outlined" fullWidth={true} 
                      value={values.password} onChange={onChangeInput('password')} 
                    />
                  </div>
                  <div className="grid sm-100">
                    <Button 
                      type="submit" variant="contained" color="primary" 
                      fullWidth={true} size="large" 
                    >
                      เข้าสู่ระบบ
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>

        </div>
      </section>
      <Footer />
    </>
  );
}

SignInPage.defaultProps = {
	
};
SignInPage.propTypes = {
	userSignin: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {userSignin})(SignInPage);