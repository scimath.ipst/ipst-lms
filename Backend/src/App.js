import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { UserModel } from './models';
import AlertPopup from './components/AlertPopup';

import CryptoJS from 'crypto-js';
import { TOKEN_KEY, REFRESH_KEY } from './actions/types';

import { SignInPage, Page404, NoPermissionPage } from './views/auth';
import { 
  DashboardPage, ProfilePage,
  ChannelsPage, ChannelPage,
  PlaylistsPage, PlaylistPage,
  ContentsPage, ContentPage, ContentPreviewPage,
  TagsPage, TagPage, LevelsPage, LevelPage,
  SubjectsPage, SubjectPage,
  DepartmentsPage, DepartmentPage, UsersPage, UserPage,
  ImportContentsPage
} from './views/admins';
import { 
  ManagerDashboardPage, ManagerProfilePage,
  ManagerChannelsPage, ManagerChannelPage,
  ManagerPlaylistsPage, ManagerPlaylistPage,
  ManagerContentsPage, ManagerContentPage, ManagerContentPreviewPage,
  ManagerDepartmentPage, ManagerUsersPage, ManagerUserPage
} from './views/managers';
import { 
  CreatorDashboardPage, CreatorProfilePage,
  CreatorChannelsPage, CreatorChannelPage,
  CreatorPlaylistsPage, CreatorPlaylistPage,
  CreatorContentsPage, CreatorContentPage, CreatorContentPreviewPage,
  CreatorDepartmentPage, CreatorUsersPage, CreatorUserPage
} from './views/creators';

function App() {
  VerifySignedIn();
  return (
    <BrowserRouter>
      <Switch>
        <NotSignedInRoute exact path="/" component={SignInPage} />

        
        {/* START: Creator */}
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator" component={CreatorDashboardPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/profile" component={CreatorProfilePage} 
        />
        
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/channels" component={CreatorChannelsPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/channel/:process/:dataId?" component={CreatorChannelPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/playlists" component={CreatorPlaylistsPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/playlist/:process/:dataId?" component={CreatorPlaylistPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/contents" component={CreatorContentsPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/content/:process/:dataId?" component={CreatorContentPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/content-preview/:process/:dataId" component={CreatorContentPreviewPage} 
        />
        
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/department" component={CreatorDepartmentPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/users" component={CreatorUsersPage} 
        />
        <ProtectedRoute 
          auth={GuardCreator()} redirect="/" 
          exact path="/creator/user/:process/:dataId?" component={CreatorUserPage} 
        />
        {/* END: Creator */}

        
        {/* START: Manager */}
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager" component={ManagerDashboardPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/profile" component={ManagerProfilePage} 
        />
        
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/channels" component={ManagerChannelsPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/channel/:process/:dataId?" component={ManagerChannelPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/playlists" component={ManagerPlaylistsPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/playlist/:process/:dataId?" component={ManagerPlaylistPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/contents" component={ManagerContentsPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/content/:process/:dataId?" component={ManagerContentPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/content-preview/:process/:dataId" component={ManagerContentPreviewPage} 
        />
        
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/department" component={ManagerDepartmentPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/users" component={ManagerUsersPage} 
        />
        <ProtectedRoute 
          auth={GuardManager()} redirect="/" 
          exact path="/manager/user/:process/:dataId?" component={ManagerUserPage} 
        />
        {/* END: Manager */}


        {/* START: Admin & Quality Control Officer */}
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin" component={DashboardPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/profile" component={ProfilePage} 
        />
        
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/channels" component={ChannelsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/channel/:process/:dataId?" component={ChannelPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/playlists" component={PlaylistsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/playlist/:process/:dataId?" component={PlaylistPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/contents" component={ContentsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/content/:process/:dataId?" component={ContentPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/content-preview/:process/:dataId" component={ContentPreviewPage} 
        />
        
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/tags" component={TagsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/tag/:process/:dataId?" component={TagPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/levels" component={LevelsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/level/:process/:dataId?" component={LevelPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/subjects" component={SubjectsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/subject/:process/:dataId?" component={SubjectPage} 
        />
        
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/departments" component={DepartmentsPage} 
        />
        <ProtectedRoute 
          auth={GuardQCO()} redirect="/" 
          exact path="/admin/department/:process/:dataId?" component={DepartmentPage} 
        />
        <ProtectedRoute 
          auth={GuardAdmin()} redirect="/" 
          exact path="/admin/users" component={UsersPage} 
        />
        <ProtectedRoute 
          auth={GuardAdmin()} redirect="/" 
          exact path="/admin/user/:process/:dataId?" component={UserPage} 
        />
        
        <ProtectedRoute 
          auth={GuardAdmin()} redirect="/" 
          exact path="/admin/import-contents" component={ImportContentsPage} 
        />
        {/* END: Admin & Quality Control Officer */}


        <Route exact path="/no-permission" component={NoPermissionPage} />
        <Route path="*" component={Page404} />
      </Switch>
      <AlertPopup />
    </BrowserRouter>
  );
}


// Verify
const VerifySignedIn = async () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return true;

  user = JSON.parse(user);
  if(!user.refreshToken) return true;
  
  let bytes = CryptoJS.AES.decrypt(user.refreshToken, REFRESH_KEY);
  let refreshToken = bytes.toString(CryptoJS.enc.Utf8);
  const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/refresh-token`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ refreshToken: refreshToken })
  });
  if(!fetch1.ok || fetch1.status !== 200){
    localStorage.removeItem(`${process.env.REACT_APP_PREFIX}_USER`);
    window.location.reload();
    return false;
  }else{
    let data1 = await fetch1.json();
    user.accessToken = CryptoJS.AES.encrypt(data1.accessToken, TOKEN_KEY).toString();
    user.refreshToken = CryptoJS.AES.encrypt(data1.refreshToken, REFRESH_KEY).toString();
    localStorage.setItem(`${process.env.REACT_APP_PREFIX}_USER`, JSON.stringify(user));
  }
  
  return true;
};


// Routes
const ProtectedRoute = ({ component: Component, auth, redirect="/", ...rest }) => {
  if(auth) {
    return (<Route {...rest} render={(props) => (<Component {...props} />)} />);
  } else {
    return <Redirect to={redirect} />;
  }
};
const NotSignedInRoute = ({ component: Component, ...rest }) => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return (<Route {...rest} render={(props) => (<Component {...props} />)} />);

  user = new UserModel(JSON.parse(user));
  if(!user.isSignedIn()) return (<Route {...rest} render={(props) => (<Component {...props} />)} />);

  if(user.isAdmin()) return <Redirect to="/admin" />;
  else if(user.isQCO()) return <Redirect to="/admin" />;
  else if(user.isManager()) return <Redirect to="/manager" />;
  else if(user.isCreator()) return <Redirect to="/creator" />;
  else return <Redirect to="/no-permission" />;
};


// Guards
const GuardAdmin = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isAdmin()) {
    return false;
  }
  return true;
};
const GuardQCO = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isQCO()) {
    return false;
  }
  return true;
};
const GuardManager = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isManager()) {
    return false;
  }
  return true;
};
const GuardCreator = () => {
  var user = localStorage.getItem(`${process.env.REACT_APP_PREFIX}_USER`);
  if(!user) return false;
  user = new UserModel(JSON.parse(user));
  if(!user.isCreator()) {
    return false;
  }
  return true;
};


export default App;
