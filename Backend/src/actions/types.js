
// Environtment Variables
export const FRONTEND_URL = process.env.REACT_APP_FRONTEND_URL;
export const TOKEN_KEY = process.env.REACT_APP_TOKEN_KEY;
export const REFRESH_KEY = process.env.REACT_APP_REFRESH_KEY;


// Alert
export const ALERT_CHANGE = 'alert/change';
export const ALERT_DISAPPEAR = 'alert/disappear';
export const ALERT_LOADING = 'alert/loading';


// User
export const USER_SIGNIN = 'user/signin';
export const USER_SIGNOUT = 'user/signout';
export const USER_UPDATE = 'user/update';
export const USER_UPDATE_DEPT = 'user/update/department';
export const USER_LIST = 'general/user/list';
export const USER_READ = 'general/user/read';


// Channel
export const CHANNEL_LIST = 'general/channel/list';
export const CHANNEL_READ = 'general/channel/read';
// Playlist
export const PLAYLIST_LIST = 'general/playlist/list';
export const PLAYLIST_READ = 'general/playlist/read';
// Content
export const CONTENT_LIST = 'general/content/list';
export const CONTENT_READ = 'general/content/read';
// Approved Content
export const APPROVED_CONTENT_LIST = 'general/approved-content/list';
// Tag
export const TAG_LIST = 'general/tag/list';
export const TAG_READ = 'general/tag/read';
// Level
export const LEVEL_LIST = 'general/level/list';
export const LEVEL_READ = 'general/level/read';
// Subject
export const SUBJECT_LIST = 'general/subject/list';
export const SUBJECT_READ = 'general/subject/read';
// Department
export const DEPT_LIST = 'general/department/list';
export const DEPT_READ = 'general/department/read';


// Admin User
export const ADMIN_USER_LIST = 'admin/user/list';
export const ADMIN_USER_READ = 'admin/user/read';
// Admin Role
export const ADMIN_ROLE_LIST = 'admin/role/list';


// Paginate
export const CONTENT_PAGINATE = 'general/content/paginate';
