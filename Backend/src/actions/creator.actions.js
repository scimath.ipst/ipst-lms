import {
  CHANNEL_LIST, CHANNEL_READ,
  PLAYLIST_LIST, PLAYLIST_READ,
  CONTENT_LIST, APPROVED_CONTENT_LIST,
  USER_LIST, CONTENT_READ, USER_READ
} from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader } from '../helpers/header';
import { 
  UserModel, ChannelModel, PlaylistModel, ContentModel, ApprovedContentModel 
} from '../models';


// Dashboard
export const creatorDashboardCountContents = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/dashboard/count-contents`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};
export const creatorDashboardMostVisitedContents = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/dashboard/most-visited-contents`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};
export const creatorDashboardUsers = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/dashboard/users`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};


// User
export const creatorUserList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/user/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: USER_LIST,
      payload: data1.result.map(user => new UserModel(user))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorUserRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/user?userId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: USER_READ,
      payload: new UserModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Channel
export const creatorChannelList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/channel/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: CHANNEL_LIST,
      payload: data1.result.map(data => new ChannelModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorChannelRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/channel?channelId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: CHANNEL_READ,
      payload: new ChannelModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Playlist
export const creatorPlaylistList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/playlist/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: PLAYLIST_LIST,
      payload: data1.result.map(data => new PlaylistModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorPlaylistRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/playlist?playlistId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: PLAYLIST_READ,
      payload: new PlaylistModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Content
export const creatorContentList = ({ keyword, status, approvedStatus }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));

    var url = `${process.env.REACT_APP_API_URL}creator/content/list?keyword=${keyword}`;
    if(status !== undefined && status !== '') url += `&status=${status}`;
    if(approvedStatus !== undefined && approvedStatus !== '') url += `&approvedStatus=${approvedStatus}`;

    const fetch1 = await fetch(url, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: CONTENT_LIST,
      payload: data1.result.map(data => new ContentModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorContentRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/content?contentId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: CONTENT_READ,
      payload: new ContentModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorContentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/content`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorContentEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/content`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorContentDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/content`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ contentId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};

export const creatorApprovedContentList = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/approved-content/list?contentId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: APPROVED_CONTENT_LIST,
      payload: data1.result.map(data => new ApprovedContentModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const creatorApprovedContentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}creator/approved-content`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
