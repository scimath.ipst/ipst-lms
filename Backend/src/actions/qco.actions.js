import {
  CHANNEL_LIST, CHANNEL_READ,
  PLAYLIST_LIST, PLAYLIST_READ,
  CONTENT_LIST, CONTENT_READ, APPROVED_CONTENT_LIST,
  DEPT_LIST, DEPT_READ,
  TAG_LIST, TAG_READ,
  LEVEL_LIST, LEVEL_READ,
  SUBJECT_LIST, SUBJECT_READ
} from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader } from '../helpers/header';
import { 
  ChannelModel, PlaylistModel, ContentModel, ApprovedContentModel, 
  TagModel, LevelModel, SubjectModel,
  DepartmentModel, UserModel
} from '../models';


// Dashboard
export const dashboardCountContents = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/dashboard/count-contents`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};
export const dashboardMostVisitedContents = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/dashboard/most-visited-contents`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};
export const dashboardDepartments = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/dashboard/departments`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};


// Channel
export const qcoChannelList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/channel/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: CHANNEL_LIST,
      payload: data1.result.map(data => new ChannelModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoChannelRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/channel?channelId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: CHANNEL_READ,
      payload: new ChannelModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoChannelAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/channel`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoChannelEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/channel`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoChannelDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/channel`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ channelId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Playlist
export const qcoPlaylistList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/playlist/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: PLAYLIST_LIST,
      payload: data1.result.map(data => new PlaylistModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoPlaylistRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/playlist?playlistId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: PLAYLIST_READ,
      payload: new PlaylistModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoPlaylistAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/playlist`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoPlaylistEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/playlist`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoPlaylistDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/playlist`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ playlistId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Content
export const qcoContentList = ({ keyword, status, approvedStatus }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));

    var url = `${process.env.REACT_APP_API_URL}qco/content/list?keyword=${keyword}`;
    if(status !== undefined && status !== '') url += `&status=${status}`;
    if(approvedStatus !== undefined && approvedStatus !== '') url += `&approvedStatus=${approvedStatus}`;

    const fetch1 = await fetch(url, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: CONTENT_LIST,
      payload: data1.result.map(data => new ContentModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoContentRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/content?contentId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: CONTENT_READ,
      payload: new ContentModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoContentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/content`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoContentEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/content`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoContentDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/content`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ contentId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};

export const qcoApprovedContentList = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/approved-content/list?contentId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: APPROVED_CONTENT_LIST,
      payload: data1.result.map(data => new ApprovedContentModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoApprovedContentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/approved-content`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Tag
export const qcoTagList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/tag/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: TAG_LIST,
      payload: data1.result.map(data => new TagModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoTagRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/tag?tagId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: TAG_READ,
      payload: new TagModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoTagAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/tag`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoTagEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/tag`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoTagDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/tag`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ tagId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Level
export const qcoLevelList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/level/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: LEVEL_LIST,
      payload: data1.result.map(data => new LevelModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoLevelRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/level?levelId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: LEVEL_READ,
      payload: new LevelModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoLevelAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/level`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoLevelEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/level`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoLevelDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/level`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ levelId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Subject
export const qcoSubjectList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/subject/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: SUBJECT_LIST,
      payload: data1.result.map(data => new SubjectModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoSubjectRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/subject?subjectId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: SUBJECT_READ,
      payload: new SubjectModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoSubjectAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/subject`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoSubjectEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/subject`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoSubjectDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/subject`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ subjectId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Department
export const qcoDepartmentList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/department/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: DEPT_LIST,
      payload: data1.result.map(data => new DepartmentModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const qcoDepartmentRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}qco/department?deptId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: DEPT_READ,
      payload: new DepartmentModel({
        ...data1.result,
        members: data1.members.map(data => new UserModel(data))
      })
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
