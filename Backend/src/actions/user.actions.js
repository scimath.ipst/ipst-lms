import { USER_SIGNIN, USER_SIGNOUT, USER_UPDATE } from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader } from '../helpers/header';

import CryptoJS from 'crypto-js';
import { TOKEN_KEY, REFRESH_KEY } from '../actions/types';


// Auth
export const userSignin = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}auth/signin`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    const data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    if(!data1.role || data1.role.level < 1) {
      dispatch(alertChange('Warning', 'บัญชีผู้ใช้ไม่มีสิทธิ์ในการเข้าใช้ระบบผู้สร้างสื่อ'));
      return false;
    }

    data1.accessToken = CryptoJS.AES.encrypt(data1.accessToken, TOKEN_KEY).toString();
    data1.refreshToken = CryptoJS.AES.encrypt(data1.refreshToken, REFRESH_KEY).toString();
    
    dispatch(alertChange('Info', 'เข้าสู่ระบบสำเร็จแล้ว'));
    dispatch({ type: USER_SIGNIN, payload: data1 });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const userSignout = () => (dispatch) => {
  dispatch(alertChange('Info', 'ออกจากระบบสำเร็จแล้ว'));
  dispatch({ type: USER_SIGNOUT, payload: true });
};


// Profile
export const userProfileEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/profile`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({ type: USER_UPDATE, payload: data1.result });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const userPasswordEdit = (input) => async (dispatch) => {
  try {
    if(input.newPassword !== input.confirmPassword) {
      dispatch(alertChange('Warning', 'รหัสผ่านใหม่ไม่ตรงกัน'));
      return false;
    }

    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/password`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const userInterestEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/interest`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({ type: USER_UPDATE, payload: data1.result });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// OTIMS
export const userProcessOtims = ({ email }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_OTIMS_API_URL}ml?email=${email}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': `${process.env.REACT_APP_OTIMS_KEY}`
        }
      });
      if(!fetch1.ok || fetch1.status !== 200){
        resolve([]);
      }else{
        const data1 = await fetch1.json();
        resolve(data1);
      }
    } catch (error) {
      console.log(error);
    }
  });
};
