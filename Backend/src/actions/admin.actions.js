import { ADMIN_USER_LIST, ADMIN_ROLE_LIST, ADMIN_USER_READ } from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader } from '../helpers/header';
import { PaginateModel, UserModel, UserRoleModel, UserActionModel } from '../models';


// User
export const adminUserList = ({ page, pp, keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/user/list?page=${page}&pp=${pp}&keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: ADMIN_USER_LIST,
      payload: {
        paginate: new PaginateModel(data1.paginate),
        result: data1.result.map(user => new UserModel(user))
      }
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const adminUserRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/user?userId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: ADMIN_USER_READ,
      payload: new UserModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const adminUserAdd = (input) => async (dispatch) => {
  try {
    if(input.password !== input.confirmPassword) {
      dispatch(alertChange('Warning', 'รหัสผ่านไม่ตรงกัน'));
      return false;
    }

    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/user`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const adminUserEdit = (input) => async (dispatch) => {
  try {
    if(input.password || input.confirmPassword) {
      if(input.password !== input.confirmPassword) {
        dispatch(alertChange('Warning', 'รหัสผ่านไม่ตรงกัน'));
        return false;
      }
    }

    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/user`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const adminUserDelete = ({ id, ip, url }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/user`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ userId: id, ip: ip, url: url }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};

export const adminUserHistoryList = ({ id }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/user/history/list?userId=${id}`, {
        method: 'GET',
        headers: apiHeader()
      });
      var data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) reject(data1);

      var result = data1.result.map(d => new UserActionModel(d));
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};


// Role
export const adminRoleList = () => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/role/list`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: ADMIN_ROLE_LIST,
      payload: data1.result.map(user => new UserRoleModel(user))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Department
export const adminDepartmentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/department`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const adminDepartmentEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/department`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const adminDepartmentDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}admin/department`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ deptId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Youtube API
export const adminYoutubeGetPlaylistItems = ({ playlistId }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      dispatch(alertLoading(true));
      const fetch1 = await fetch(`${process.env.REACT_APP_GOOGLE_API_URL}playlistItems?part=snippet&playlistId=${playlistId}&key=${process.env.REACT_APP_GOOGLE_API_KEY}&maxResults=50`, {
        method: 'GET'
      });
      var data1 = await fetch1.json();
      dispatch(alertLoading(false));

      if(!fetch1.ok || fetch1.status !== 200) {
        resolve(false);
      }else{
        resolve(data1.items);
      }
    } catch (error) {
      dispatch(alertLoading(true));
      console.log(error);
    }
  });
};
