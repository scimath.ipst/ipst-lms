import {
  USER_UPDATE_DEPT,
  CHANNEL_LIST, CHANNEL_READ,
  PLAYLIST_LIST, PLAYLIST_READ,
  CONTENT_LIST, CONTENT_READ
} from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader } from '../helpers/header';
import { 
  ChannelModel, PlaylistModel, ContentModel 
} from '../models';


// Dashboard
export const managerDashboardCountContents = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/dashboard/count-contents`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};
export const managerDashboardMostVisitedContents = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/dashboard/most-visited-contents`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    console.log(error);
  }
};
export const managerDashboardUsers = () => async (dispatch) => {
  try {
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/dashboard/users`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      return false;
    }
    return data1.result;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Channel
export const managerChannelList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/channel/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: CHANNEL_LIST,
      payload: data1.result.map(data => new ChannelModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerChannelRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/channel?channelId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: CHANNEL_READ,
      payload: new ChannelModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerChannelAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/channel`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerChannelEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/channel`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerChannelDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/channel`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ channelId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Playlist
export const managerPlaylistList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/playlist/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: PLAYLIST_LIST,
      payload: data1.result.map(data => new PlaylistModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerPlaylistRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/playlist?playlistId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: PLAYLIST_READ,
      payload: new PlaylistModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerPlaylistAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/playlist`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerPlaylistEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/playlist`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerPlaylistDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/playlist`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ playlistId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Content
export const managerContentList = ({ keyword, status, approvedStatus }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));

    var url = `${process.env.REACT_APP_API_URL}manager/content/list?keyword=${keyword}`;
    if(status !== undefined && status !== '') url += `&status=${status}`;
    if(approvedStatus !== undefined && approvedStatus !== '') url += `&approvedStatus=${approvedStatus}`;

    const fetch1 = await fetch(url, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: CONTENT_LIST,
      payload: data1.result.map(data => new ContentModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerContentRead = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/content?contentId=${id}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: CONTENT_READ,
      payload: new ContentModel(data1.result)
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerContentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/content`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerContentEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/content`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
export const managerContentDelete = ({ id }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/content`, {
      method: 'DELETE',
      headers: apiHeader(),
      body: JSON.stringify({ contentId: id }),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};

export const managerApprovedContentAdd = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/approved-content`, {
      method: 'POST',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Department
export const managerDepartmentEdit = (input) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}manager/department`, {
      method: 'PUT',
      headers: apiHeader(),
      body: JSON.stringify(input),
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message, data1.errors? data1.errors: []));
      return false;
    }

    dispatch(alertChange('Info', data1.message));
    dispatch({
      type: USER_UPDATE_DEPT,
      payload: data1.result
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
