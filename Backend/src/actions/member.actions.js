import {
  USER_LIST, TAG_LIST, LEVEL_LIST, SUBJECT_LIST, CONTENT_PAGINATE
} from './types';
import { alertChange, alertLoading } from './alert.actions';
import { apiHeader, apiHeaderFormData } from '../helpers/header';
import { 
  UserModel, PaginateModel, UserActionModel, TagModel, LevelModel, SubjectModel, BadgeModel
} from '../models';


// Upload File
export const memberUploadFile = (file) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    
    const formData = new FormData();
    formData.append('file', file);
    
    const fetch1 = await fetch(`${process.env.REACT_APP_CDN_URL}upload`, {
      method: 'POST',
      headers: apiHeaderFormData(),
      body: formData
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    return data1.result;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// User
export const memberUserList = ({ page, pp, keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/user/list?page=${page}&pp=${pp}&keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }

    dispatch(alertLoading(false));
    dispatch({
      type: USER_LIST,
      payload: {
        paginate: new PaginateModel(data1.paginate),
        result: data1.result.map(user => new UserModel(user))
      }
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Member
export const memberHistoryList = () => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/history/list`, {
        method: 'GET',
        headers: apiHeader()
      });
      var data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) reject(data1);

      var result = data1.result.map(d => new UserActionModel(d));
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};


// Content
export const memberSetContentPaginate = (input) => async (dispatch) => {
  dispatch({
    type: CONTENT_PAGINATE,
    payload: input
  });
  return true;
};


// Tag
export const memberTagList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/tag/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: TAG_LIST,
      payload: data1.result.map(data => new TagModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
// Level
export const memberLevelList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/level/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: LEVEL_LIST,
      payload: data1.result.map(data => new LevelModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};
// Subject
export const memberSubjectList = ({ keyword }) => async (dispatch) => {
  try {
    dispatch(alertLoading(true));
    const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/subject/list?keyword=${keyword}`, {
      method: 'GET',
      headers: apiHeader()
    });
    var data1 = await fetch1.json();
    if(!fetch1.ok || fetch1.status !== 200) {
      dispatch(alertChange('Warning', data1.message));
      return false;
    }
    
    dispatch(alertLoading(false));
    dispatch({
      type: SUBJECT_LIST,
      payload: data1.result.map(data => new SubjectModel(data))
    });
    return true;
  } catch (error) {
    dispatch(alertLoading(true));
    console.log(error);
  }
};


// Badge
export const memberBadgeList = ({ id }) => async (dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fetch1 = await fetch(`${process.env.REACT_APP_API_URL}member/badge/list?userId=${id}`, {
        method: 'GET',
        headers: apiHeader()
      });
      var data1 = await fetch1.json();
      if(!fetch1.ok || fetch1.status !== 200) reject(data1);
      
      var result = data1.result.map(d => new BadgeModel(d));
      resolve(result);
    } catch (error) {
      console.log(error);
    }
  });
};
