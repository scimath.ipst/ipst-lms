import PropTypes from 'prop-types';
import { useState, useEffect, useRef } from 'react';
import {
  Button, FormControl, RadioGroup, FormControlLabel, Radio
} from '@material-ui/core';
import YouTube from 'react-youtube';

import { connect } from 'react-redux';

function useInterval(callback, delay) {
  const intervalRef = useRef();
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  useEffect(() => {
    if(typeof delay === 'number'){
      intervalRef.current = window.setInterval(() => callbackRef.current(), delay);
      return () => window.clearInterval(intervalRef.current);
    }
  }, [delay]);
  
  return intervalRef;
}

function YoutubeVideoPreview(props) {
  
  // https://developers.google.com/youtube/player_parameters
  const options = {
    playerVars: {
      autoplay: false
    },
  };
  
  const [isValid, setIsValid] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [Player, setPlayer] = useState(null);
  const [setup, setSetup] = useState(true);

  const playerRef = useRef();
  var playerState;
  var currentTime = 0;
  var isEnded = false;
  var questioning = false;

  const onEnd = async () => {
    if(!isEnded){
      isEnded = true;
    }
  };

  const onChoiceChange = (e, i) => {
    e.preventDefault();
    questions[i].setSelectedChoice(parseInt(e.target.value));
    setQuestions(questions);
    setCounter(counter + 1);
  };
  const onQuestionSubmit = (e, i) => {
    e.preventDefault();
    questions[i].setState(2);
    setQuestions(questions);
    setCounter(counter + 1);
  };
  const onQuestionComplete = async (e, i) => {
    e.preventDefault();
    questions[i].setState(3);
    setQuestions(questions);
    let goTo = questions[i].goTo();
    if(goTo){
      let t = await playerRef.current.internalPlayer.getCurrentTime();
      if(goTo > t) playerRef.current.internalPlayer.seekTo(goTo);
    }
    playerRef.current.internalPlayer.playVideo();
    setCounter(counter + 1);
  };

  // Video Logic
  const [counter, setCounter] = useState(0);
  useInterval(async () => {
    setCounter(counter + 1);
    if(!Player){
      if(playerRef && playerRef.current && playerRef.current.internalPlayer){
        setPlayer(playerRef.current.internalPlayer);
      }
    }else{
      playerState = await Player.getPlayerState();
      currentTime = await Player.getCurrentTime();

      if(setup) setSetup(false);

      if(playerState === 1 && !questioning && questions.length){
        for(let i=0; i<questions.length; i++){
          if(
            questions[i]['state'] === 0 
            && currentTime > questions[i]['at'] 
            && currentTime < questions[i]['at']+5 
          ){
            questioning = true;
            Player.pauseVideo();

            questions[i].setState(1);
            setQuestions(questions);

            break;
          }
        }
      }
    }
  }, 500);

  /* eslint-disable */
  useEffect(() => {
    if(!isValid && props.content.isValid() && props.content.youtubeVideoId){
      setQuestions(props.content.videoQuestions);
      setIsValid(true);
    }
  }, [props.content]);
  /* eslint-enable */

  return isValid? (
    <>
      <div className="video-container">
        <div className="wrapper">
          <YouTube 
            ref={playerRef} containerClassName="youtube-video" 
            videoId={props.content.youtubeVideoId} 
            opts={options} onEnd={onEnd} 
          />
        </div>
      </div>
      {questions.map((q, i) => (
        <div 
          className={`popup-container ${q.state === 1 || q.state === 2? 'active': ''}`} 
          key={`question-${i}`}
        >
          <div className="wrapper">
            <div className="popup-box xl">
              {q.state === 1? (
                <>
                  <h6 className="fw-500">{q.question}</h6>
                  <div className="mt-2 pl-5">
                    <form onSubmit={e => onQuestionSubmit(e, i)}>
                      <FormControl component="fieldset">
                        <RadioGroup 
                          value={q.selectedChoice} onChange={e => onChoiceChange(e, i)} 
                        >
                          {q.choices.map((choice, j) => (
                            <FormControlLabel 
                              key={`choice-${i}-${j}`} value={j} 
                              control={<Radio required={true} />} label={choice.text} 
                            />
                          ))}
                        </RadioGroup>
                      </FormControl>
                      <div className="mt-5">
                        <Button type="submit" variant="contained" color="primary" size="large">
                          ตอบคำถาม
                        </Button>
                      </div>
                    </form>
                  </div>
                </>
              ): (
                <>
                  {typeof q.isCorrect === 'function'? (
                    <>
                      <h5 className="fw-600">
                        {q.isCorrect()? 'คุณตอบถูก': 'คุณตอบผิด'}
                      </h5>
                      {q.selectedExplanation()? (
                        <p className="h6 mt-3">{q.selectedExplanation()}</p>
                      ): (<></>)}
                      <div className="mt-6">
                        <Button 
                          variant="contained" color="primary" size="large" 
                          onClick={e => onQuestionComplete(e, i)} 
                        >
                          ดูวิดีโอต่อ
                        </Button>
                      </div>
                    </>
                  ): (<></>)}
                </>
              )}
            </div>
          </div>
        </div>
      ))}
    </>
  ): (<></>);
}

YoutubeVideoPreview.defaultProps = {

};
YoutubeVideoPreview.propTypes = {
  content: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps, {
  
})(YoutubeVideoPreview);