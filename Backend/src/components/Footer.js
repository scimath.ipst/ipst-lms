import { Link } from 'react-router-dom';
import { Divider, IconButton } from '@material-ui/core';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';

function Footer(props) {
  return (
    <nav className="footer">
      <Divider />
      <div className="container">
        <div className="wrapper">
          <Link to="/" className="logo">
            <h6>Project 14+</h6>
          </Link>
          <div className="copyright">
            <p className="xxs op-90">Copyright 2021 IPST. All rights reserved.</p>
          </div>
          <div className="socials">
            <a 
              rel="noopener noreferrer" target="_blank" 
              href="https://sso.ipst.ac.th/documentation/"
            >
              <IconButton size="medium">
                <FacebookIcon style={{ fontSize: 20 }} />
              </IconButton>
            </a>
            <a 
              rel="noopener noreferrer" target="_blank" 
              href="https://sso.ipst.ac.th/documentation/"
            >
              <IconButton size="medium">
                <YouTubeIcon style={{ fontSize: 24 }} />
              </IconButton>
            </a>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Footer;