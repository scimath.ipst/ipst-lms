import PropTypes from 'prop-types';
import YouTube from 'react-youtube';

function YoutubeVideo(props) {
  // https://developers.google.com/youtube/player_parameters
  const options = {
    playerVars: {
      autoplay: false
    },
  };

  return (
    <>
      {props.videoId? (
        <div className="video-container">
          <div className="wrapper">
            <YouTube containerClassName="youtube-video" videoId={props.videoId} opts={options} />
          </div>
        </div>
      ): (<></>)}
    </>
  );
}

YoutubeVideo.defaultProps = {
  videoId: ''
};
YoutubeVideo.propTypes = {
	videoId: PropTypes.string
};

export default YoutubeVideo;