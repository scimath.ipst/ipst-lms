import PropTypes from 'prop-types';
import { CircularProgress } from '@material-ui/core';
import { connect } from 'react-redux';
import { alertChange } from '../actions/alert.actions';

function AlertPopup(props) {
  const classActive = () => {
    if(props.alert.status) return 'active';
    else return '';
  };
  const classType = () => {
    if(props.alert.type) {
      if(props.alert.type === 'Info') return 'info';
      else if(props.alert.type === 'Success') return 'success';
      else if(props.alert.type === 'Warning') return 'warning';
      else if(props.alert.type === 'Danger') return 'danger';
    }
    return '';
  };

  return (
    <>
      <div className={`alert-loading ${props.alert.loading? 'active': ''}`}>
        <div className="wrapper">
          <CircularProgress size="55px" thickness={4.4} />
        </div>
      </div>
      <div className={`alert-popup ${classActive()} ${classType()}`}>
        <div className="wrapper">
          <div className="text-container">
            <h6>{props.alert.type}</h6>
            <p>{props.alert.message}</p>
            {Object.keys(props.alert.errors).length? (
              <ul>
                {Object.keys(props.alert.errors).map((k, i) => (
                  <li key={i}>{props.alert.errors[k]}</li>
                ))} 
              </ul>
            ): (<></>)}
          </div>
        </div>
      </div>
    </>
  );
}

AlertPopup.defaultProps = {
  
};
AlertPopup.propTypes = {
	alertChange: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	alert: state.alert
});

export default connect(mapStateToProps, {alertChange})(AlertPopup);