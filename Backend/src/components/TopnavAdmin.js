import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Avatar, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import EqualizerIcon from '@material-ui/icons/Equalizer';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';
import SubscriptionsIcon from '@material-ui/icons/Subscriptions';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import ApartmentIcon from '@material-ui/icons/Apartment';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import SubjectIcon from '@material-ui/icons/Subject';
import StarIcon from '@material-ui/icons/Star';
import GetAppIcon from '@material-ui/icons/GetApp';

import { connect } from 'react-redux';
import { userSignout } from '../actions/user.actions';
import { UserModel } from '../models';

function TopnavAdmin(props) {
  const history = useHistory();
	const user = new UserModel(props.user);

	const [link, setLink] = useState('');
	const [offsetY, setOffsetY] = useState(0);
	const [sidenavActive, setSidenavStatus] = useState(false);
	const [menu, setMenu] = useState([]);
	
	const onSignout = (e) => {
		e.preventDefault();
		props.userSignout();
		setTimeout(() => {
			window.location.href = '/';
		}, 300);
	};
	
	const backToTop = () => {
		window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
	};

  /* eslint-disable */
	useEffect(() => {
    const unlisten = history.listen(() => {
      window.scrollTo(0, 0);
    });
		return async () => {
      unlisten();
    };
  }, []);
	useEffect(() => {
		if(user.isAdmin()) {
			setLink('/admin');
			setMenu([
				{
					group: 'ทั่วไป',
					children: [
						{ name: 'Dashboard', to: `/admin`, activeIndex: 0, icon: <EqualizerIcon />  },
						{ name: 'Channels', to: `/admin/channels`, activeIndex: 1, icon: <OndemandVideoIcon />  },
						{ name: 'Playlists', to: `/admin/playlists`, activeIndex: 2, icon: <SubscriptionsIcon />  },
						{ name: 'Contents', to: `/admin/contents`, activeIndex: 3, icon: <LibraryBooksIcon />  }
					]
				}, {
					group: 'หมวดหมู่',
					children: [
						{ name: 'แท็ก', to: `/admin/tags`, activeIndex: 6, icon: <LocalOfferIcon />  },
						{ name: 'ระดับชั้น', to: `/admin/levels`, activeIndex: 7, icon: <SubjectIcon />  },
						{ name: 'วิชา', to: `/admin/subjects`, activeIndex: 8, icon: <StarIcon />  },
					]
				}, {
					group: 'ผู้ดูแลระบบ',
					children: [
						{ name: 'นำเข้าเนื้อหา', to: `/admin/import-contents`, activeIndex: 11, icon: <GetAppIcon />  },
						{ name: 'หน่วยงาน', to: `/admin/departments`, activeIndex: 9, icon: <ApartmentIcon />  },
						{ name: 'บัญชีผู้ใช้', to: `/admin/users`, activeIndex: 10, icon: <PeopleAltIcon />  },
					]
				}
			]);
		} else if(user.isQCO()) {
			setLink('/admin');
			setMenu([
				{
					group: 'ทั่วไป',
					children: [
						{ name: 'Dashboard', to: `/admin`, activeIndex: 0, icon: <EqualizerIcon />  },
						{ name: 'Channels', to: `/admin/channels`, activeIndex: 1, icon: <OndemandVideoIcon />  },
						{ name: 'Playlists', to: `/admin/playlists`, activeIndex: 2, icon: <SubscriptionsIcon />  },
						{ name: 'Contents', to: `/admin/contents`, activeIndex: 3, icon: <LibraryBooksIcon />  }
					]
				}, {
					group: 'หมวดหมู่',
					children: [
						{ name: 'แท็ก', to: `/admin/tags`, activeIndex: 6, icon: <LocalOfferIcon />  },
						{ name: 'ระดับชั้น', to: `/admin/levels`, activeIndex: 7, icon: <SubjectIcon />  },
						{ name: 'วิชา', to: `/admin/subjects`, activeIndex: 8, icon: <StarIcon />  },
					]
				}, {
					group: 'ผู้ดูแลระบบ',
					children: [
						{ name: 'หน่วยงาน', to: `/admin/departments`, activeIndex: 9, icon: <ApartmentIcon />  }
					]
				}
			]);
		} else if(user.isManager()) {
			setLink('/manager');
			setMenu([
				{
					group: 'ทั่วไป',
					children: [
						{ name: 'Dashboard', to: `/manager`, activeIndex: 0, icon: <EqualizerIcon />  },
						{ name: 'Channels', to: `/manager/channels`, activeIndex: 1, icon: <OndemandVideoIcon />  },
						{ name: 'Playlists', to: `/manager/playlists`, activeIndex: 2, icon: <SubscriptionsIcon />  },
						{ name: 'Contents', to: `/manager/contents`, activeIndex: 3, icon: <LibraryBooksIcon />  }
					]
				}, {
					group: 'หน่วยงาน',
					children: [
						{ name: 'หน่วยงาน', to: `/manager/department`, activeIndex: 4, icon: <ApartmentIcon />  },
						{ name: 'บัญชีผู้ใช้', to: `/manager/users`, activeIndex: 5, icon: <PeopleAltIcon />  },
					]
				}
			]);
		} else if(user.isCreator()) {
			setLink('/creator');
			setMenu([
				{
					group: 'ทั่วไป',
					children: [
						{ name: 'Dashboard', to: `/creator`, activeIndex: 0, icon: <EqualizerIcon />  },
						{ name: 'Channels', to: `/creator/channels`, activeIndex: 1, icon: <OndemandVideoIcon />  },
						{ name: 'Playlists', to: `/creator/playlists`, activeIndex: 2, icon: <SubscriptionsIcon />  },
						{ name: 'Contents', to: `/creator/contents`, activeIndex: 3, icon: <LibraryBooksIcon />  }
					]
				}, {
					group: 'หน่วยงาน',
					children: [
						{ name: 'หน่วยงาน', to: `/creator/department`, activeIndex: 4, icon: <ApartmentIcon />  },
						{ name: 'บัญชีผู้ใช้', to: `/creator/users`, activeIndex: 5, icon: <PeopleAltIcon />  },
					]
				}
			]);
		}

		setOffsetY(window.pageYOffset);
		const handleScroll = () => {
			setOffsetY(window.pageYOffset);
		};
		window.addEventListener('scroll', handleScroll);
		return function cleanup() {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
	/* eslint-enable */

  return (
		<>
			<nav className="topnav-admin">
				<div className="wrapper">
					<div className="options">

						<div className="option">
							<div className="menu-option" onClick={() => setSidenavStatus(!sidenavActive)}>
								<div className={`hamburger ${sidenavActive? 'active': ''}`}>
									<div></div><div></div><div></div>
								</div>
							</div>
							<Link to={link} className="logo">
								<h6>Project 14+</h6>
							</Link>
						</div>
						
						{user.isSignedIn()? (
							<div className="option">
								<div className="avatar-container">
									<Avatar src={user.avatar} className="sm" />
									<div className="dropdown">
										<List>
											<ListItem className="d-block">
												<p className="text-line fw-500">{user.displayName()}</p>
												<p className="text-line xs fw-400 op-60">{user.displayRole()}</p>
												<p className="text-line xs fw-400 op-60">{user.email}</p>
											</ListItem>
											{props.forBackend? (
												<ListItem button component={Link} to={`${link}/profile`}>
													<ListItemIcon>
														<AccountCircleIcon />
													</ListItemIcon>
													<ListItemText primary="ข้อมูลส่วนตัว" />
												</ListItem>
											): (<></>)}
											<ListItem button onClick={onSignout}>
												<ListItemIcon>
													<ExitToAppIcon />
												</ListItemIcon>
												<ListItemText primary="ออกจากระบบ" />
											</ListItem>
										</List>
									</div>
								</div>
							</div>
						): (<></>)}

					</div>
				</div>
			</nav>
			{props.forBackend? (
				<div className="topnav-spacer"></div>
			): (<></>)}

			{props.forBackend && user.isSignedIn()? (
				<nav className={`sidenav-admin ${sidenavActive? 'active': ''}`}>
					<div className="wrapper">
						<div className="menu-container">
							{menu.map((g, i) => (
								<div className="menu-group" key={i}>
									<div className="title">{g.group}</div>
									{g.children.map((d, j) => (
										<Link className={`menu ${props.activeIndex === d.activeIndex? 'active': ''}`} 
											to={d.to} key={i+'_'+j}
										>
											<div className="icon">{d.icon}</div>
											{d.name}
										</Link>
									))}
								</div>
							))}
						</div>
					</div>
				</nav>
			): (<></>)}

			<div className={`back-to-top ${offsetY > 50? 'active': ''}`} onClick={backToTop}>
				<KeyboardArrowUpIcon style={{ fontSize: 32 }} />
			</div>
		</>
  );
}

TopnavAdmin.defaultProps = {
	activeIndex: 0,
	forBackend: true
};
TopnavAdmin.propTypes = {
	activeIndex: PropTypes.number,
	forBackend: PropTypes.bool,
	user: PropTypes.object.isRequired,
	userSignout: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user
});

export default connect(mapStateToProps, {userSignout})(TopnavAdmin);