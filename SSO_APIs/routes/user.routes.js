module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const UserController = require('../controllers/user.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });


  // User
  router.patch(
    '/profile',
    [ authJwt.verifyToken ],
    UserController.profileUpdate
  );
  router.patch(
    '/detail',
    [ authJwt.verifyToken ],
    UserController.detailUpdate
  );
  router.patch(
    '/password',
    [ authJwt.verifyToken ],
    UserController.passwordUpdate
  );
  
  router.patch(
    '/request-to-delete',
    [ authJwt.verifyToken ],
    UserController.requestToDelete
  );
  
  router.post(
    '/user-types',
    [ authJwt.verifyToken ],
    UserController.userTypeList
  );
  router.get(
    '/user-type',
    [ authJwt.verifyToken ],
    UserController.userTypeRead
  );


  // Custom Columns
  router.post(
    '/custom-columns',
    [ authJwt.verifyToken ],
    UserController.customColumnList
  );
  router.get(
    '/custom-column',
    [ authJwt.verifyToken ],
    UserController.customColumnRead
  );

  
  app.use('/user', router);
};