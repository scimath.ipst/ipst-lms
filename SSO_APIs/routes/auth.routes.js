module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const AuthController = require('../controllers/auth.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });


  router.post(
    '/signin',
    AuthController.signin
  );
  router.post(
    '/verify-signin',
    AuthController.verifySignin
  );

  router.post(
    '/signup',
    AuthController.signup
  );
  
  router.get(
    '/verify/:token',
    AuthController.verify
  );

  router.post(
    '/forget-password',
    AuthController.forgetPassword
  );
  router.get(
    '/check-reset-password',
    AuthController.checkResetPassword
  );
  router.post(
    '/reset-password',
    AuthController.resetPassword
  );

  router.post(
    '/signin-facebook',
    AuthController.signinFacebook
  );
  router.post(
    '/signin-google',
    AuthController.signinGoogle
  );
  router.post(
    '/signin-liff',
    AuthController.signinLIFF
  );

  
  app.use('/auth', router);
};