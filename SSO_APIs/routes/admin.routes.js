module.exports = function(app) {
  var router = require('express').Router();
  const { authJwt } = require('../middlewares');
  const AdminController = require('../controllers/admin.controller');

  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });


  // User
  router.post(
    '/roles',
    [ authJwt.verifyToken ],
    AdminController.roleList
  );
  router.get(
    '/role',
    [ authJwt.verifyToken ],
    AdminController.roleRead
  );
  router.post(
    '/role',
    [ authJwt.verifyToken ],
    AdminController.roleCreate
  );
  router.patch(
    '/role',
    [ authJwt.verifyToken ],
    AdminController.roleUpdate
  );
  router.delete(
    '/role',
    [ authJwt.verifyToken ],
    AdminController.roleDelete
  );
  
  router.get(
    '/role-permissions',
    [ authJwt.verifyToken ],
    AdminController.rolePermissionsRead
  );
  router.patch(
    '/role-permissions',
    [ authJwt.verifyToken ],
    AdminController.rolePermissionsUpdate
  );
  
  router.post(
    '/users',
    [ authJwt.verifyToken ],
    AdminController.userList
  );
  router.get(
    '/user',
    [ authJwt.verifyToken ],
    AdminController.userRead
  );
  router.post(
    '/user',
    [ authJwt.verifyToken ],
    AdminController.userCreate
  );
  router.patch(
    '/user',
    [ authJwt.verifyToken ],
    AdminController.userUpdate
  );
  router.delete(
    '/user',
    [ authJwt.verifyToken ],
    AdminController.userDelete
  );
  
  router.patch(
    '/user-detail',
    [ authJwt.verifyToken ],
    AdminController.userDetailUpdate
  );
  
  router.post(
    '/user-types',
    [ authJwt.verifyToken ],
    AdminController.userTypeList
  );
  router.get(
    '/user-type',
    [ authJwt.verifyToken ],
    AdminController.userTypeRead
  );
  router.post(
    '/user-type',
    [ authJwt.verifyToken ],
    AdminController.userTypeCreate
  );
  router.patch(
    '/user-type',
    [ authJwt.verifyToken ],
    AdminController.userTypeUpdate
  );
  router.delete(
    '/user-type',
    [ authJwt.verifyToken ],
    AdminController.userTypeDelete
  );


  // Custom Columns
  router.post(
    '/custom-columns',
    [ authJwt.verifyToken ],
    AdminController.customColumnList
  );
  router.get(
    '/custom-column',
    [ authJwt.verifyToken ],
    AdminController.customColumnRead
  );
  router.post(
    '/custom-column',
    [ authJwt.verifyToken ],
    AdminController.customColumnCreate
  );
  router.patch(
    '/custom-column',
    [ authJwt.verifyToken ],
    AdminController.customColumnUpdate
  );


  // External App
  router.post(
    '/external-apps',
    [ authJwt.verifyToken ],
    AdminController.externalAppList
  );
  router.get(
    '/external-app',
    [ authJwt.verifyToken ],
    AdminController.externalAppRead
  );
  router.post(
    '/external-app',
    [ authJwt.verifyToken ],
    AdminController.externalAppCreate
  );
  router.patch(
    '/external-app',
    [ authJwt.verifyToken ],
    AdminController.externalAppUpdate
  );
  router.delete(
    '/external-app',
    [ authJwt.verifyToken ],
    AdminController.externalAppDelete
  );


  // Module
  router.post(
    '/modules',
    [ authJwt.verifyToken ],
    AdminController.moduleList
  );
  router.get(
    '/module',
    [ authJwt.verifyToken ],
    AdminController.moduleRead
  );
  router.post(
    '/module',
    [ authJwt.verifyToken ],
    AdminController.moduleCreate
  );
  router.patch(
    '/module',
    [ authJwt.verifyToken ],
    AdminController.moduleUpdate
  );
  router.delete(
    '/module',
    [ authJwt.verifyToken ],
    AdminController.moduleDelete
  );


  // Report
  router.post(
    '/report-traffics',
    [ authJwt.verifyToken ],
    AdminController.reportTraffics
  );
  router.post(
    '/report-actions',
    [ authJwt.verifyToken ],
    AdminController.reportActions
  );
  router.post(
    '/report-registrations',
    [ authJwt.verifyToken ],
    AdminController.reportRegistrations
  );

  
  app.use('/admin', router);
};