const jwt = require('jsonwebtoken');
const sanitize = require('mongo-sanitize');
const config = require('../config');


verifyToken = async (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'].split(' ');
    if(authHeader.length != 2 || authHeader[0] != 'Bearer') {
      return res.status(403).send({message: 'No bearer token provided.'});
    }
    
    const token = authHeader[1];
    req.accessToken = token;
    next();
  } catch(err) {
    return res.status(500).send({message: 'Internal server error.'});
  }
};


const authJwt = {
  verifyToken
};

module.exports = authJwt;
