const config = require('../config');
const fetch = require('node-fetch');


call = async (method, url, input={}, jwt='') => {
  let header = { 'Content-Type': 'application/json' };
  if(jwt) header['Authorization'] = `Bearer ${jwt}`;
  
  let fetch1;
  if(method == 'GET'){
    fetch1 = await fetch(`${config.externalApiUrl}${url}`, {
      method: method,
      headers: header,
    });
  }else{
    fetch1 = await fetch(`${config.externalApiUrl}${url}`, {
      method: method,
      headers: header,
      body: JSON.stringify({ ...input, app_id: config.externalApiId }),
    });
  }

  return await fetch1.json();
};


const apiHelper = {
  call
};

module.exports = apiHelper;
