const formater = {
  
  user: (data=null) => {
    if(!data){
      return {
        id: null,
        firstname: '',
        lastname: '',
        email: '',
        profile: '',
        status: 0,
        last_ip: '',
        facebook_id: '',
        google_id: '',
        liff_id: '',
        role: {
          id: null,
          name: '',
          is_admin: 0,
          is_super_admin: 0
        }
      };
    }else{
      return {
        id: data.id? data.id: null,
        firstname: data.firstname? data.firstname: '',
        lastname: data.lastname? data.lastname: '',
        email: data.email? data.email: '',
        profile: data.profile? data.profile: '',
        status: data.status? Number(data.status): 0,
        last_ip: data.last_ip? data.last_ip: '',
        facebook_id: data.facebook_id? data.facebook_id: '',
        google_id: data.google_id? data.google_id: '',
        liff_id: data.liff_id? data.liff_id: '',
        role: {
          id: data.role_id? data.role_id: null,
          name: data.role? data.role: '',
          is_admin: data.role_is_admin? Number(data.role_is_admin): 0,
          is_super_admin: data.role_is_super_admin? Number(data.role_is_super_admin): 0
        }
      };
    }
  },

};

module.exports = formater;