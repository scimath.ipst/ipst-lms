const { apiHelper, formater, resProcess } = require('../helpers');


// User
exports.roleList = async (req, res) => {
  try {
    const data1 = await apiHelper.call('GET', 'api/admin/user-role-list', {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, {
      result: data1.data
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.roleRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/user-role-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.roleCreate = async (req, res) => {
  try {
    var error = {};
    const { name, is_admin, is_default, order, status }= req.body;
    
    if(!name) error['name'] = 'name is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-role-create`, {
      name: name,
      is_admin: is_admin? is_admin: 0,
      is_default: is_default? is_default: 0,
      order: order? order: 1,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.roleUpdate = async (req, res) => {
  try {
    var error = {};
    const { id, name, is_admin, is_default, order, status }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(!name) error['name'] = 'name is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-role-update`, {
      id: id,
      name: name,
      is_admin: is_admin? is_admin: 0,
      is_default: is_default? is_default: 0,
      order: order? order: 1,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.roleDelete = async (req, res) => {
  try {
    var error = {};
    const { id }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-role-delete`, { id: id }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.rolePermissionsRead = async (req, res) => {
  try {
    const { role_id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/role-permissions-read/${role_id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.rolePermissionsUpdate = async (req, res) => {
  try {
    var error = {};
    const { permissions }= req.body;
    
    if(!permissions) error['permissions'] = 'permissions is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    await Promise.all(
      permissions.map(async (d) => {
        let data1 = await apiHelper.call('POST', `api/sadmin/role-permissions-update`, {
          module_id: Number(d.module_id),
          role_id: Number(d.role_id),
          create: Number(d.create),
          read: Number(d.read),
          update: Number(d.update),
          delete: Number(d.delete)
        }, req.accessToken);
        if(!data1 || data1.status != 200){
          return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
        }
      })
    );
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.userList = async (req, res) => {
  try {
    const { page, pp, keyword }= req.body;

    const data1 = await apiHelper.call('GET', 
      `api/admin/user-list?page=${page}&pp=${pp}&keyword=${keyword? keyword: ''}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, {
      paginate: {
        page: Number(data1.data.page),
        pp: Number(data1.data.pp),
        total: Number(data1.data.total),
        total_pages: Number(data1.data.total_pages),
        keyword: keyword? keyword: ''
      },
      result: data1.data.result.map(d => formater.user(d))
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/user-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    let custom_columns = [];
    const data0 = await apiHelper.call('GET', `api/admin/user-custom-column-list`, {}, req.accessToken);
    if(data0 && data0.status == 200){
      custom_columns = data0.data;
    }
    data1.data['custom_columns'] = custom_columns;
    
    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userCreate = async (req, res) => {
  try {
    var error = {};
    const {
      firstname, lastname, email, username, roleId, status,
      password, confirmPassword
    }= req.body;
    
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(!email) error['email'] = 'email is required.';
    if(!username) error['username'] = 'username is required.';
    if(!roleId) error['roleId'] = 'roleId is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(!password) error['password'] = 'password is required.';
    if(!confirmPassword) error['confirmPassword'] = 'confirmPassword is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/admin/user-create`, {
      firstname: firstname,
      lastname: lastname,
      email: email,
      username: username,
      role_id: roleId,
      status: status,
      password: password,
      password_confirm: confirmPassword
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userUpdate = async (req, res) => {
  try {
    var error = {};
    const {
      id, firstname, lastname, email, username, roleId, status,
      password, confirmPassword
    }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(!email) error['email'] = 'email is required.';
    if(!username) error['username'] = 'username is required.';
    if(!roleId) error['roleId'] = 'roleId is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/admin/user-update`, {
      id: id,
      firstname: firstname,
      lastname: lastname,
      email: email,
      username: username,
      role_id: roleId,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    if(password && confirmPassword){
      const data2 = await apiHelper.call('POST', `api/admin/user-update-password`, {
        id: id,
        new_password: password,
        new_password_confirm: confirmPassword
      }, req.accessToken);
      if(!data2 || data2.status != 200){
        return resProcess['400'](res, data2.messages, 'เกิดข้อผิดพลาด');
      }
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userDelete = async (req, res) => {
  try {
    var error = {};
    const { id }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/admin/user-delete`, { id: id }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.userDetailUpdate = async (req, res) => {
  try {
    var error = {};
    const {
      user_id, address, phone, title, company, company_address, company_phone,
      user_type_id, user_subtype_id
    }= req.body;

    if(!user_id) error['user_id'] = 'user_id is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    let updateInput = { user_id: user_id };
    if(address!==undefined) updateInput['address'] = address;
    if(phone!==undefined) updateInput['phone'] = phone;
    if(title!==undefined) updateInput['title'] = title;
    if(company!==undefined) updateInput['company'] = company;
    if(company_address!==undefined) updateInput['company_address'] = company_address;
    if(company_phone!==undefined) updateInput['company_phone'] = company_phone;
    if(user_type_id!==undefined) updateInput['user_type_id'] = user_type_id;
    if(user_subtype_id!==undefined) updateInput['user_subtype_id'] = user_subtype_id;

    const data0 = await apiHelper.call('GET', `api/admin/user-custom-column-list`, {}, req.accessToken);
    if(data0 && data0.status == 200){
      data0.data.forEach(d => {
        if(req.body[d.name]!==undefined) updateInput[d.name] = req.body[d.name];
      });
    }

    const data1 = await apiHelper.call('POST', `api/admin/user-update-detail`, updateInput, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.userTypeList = async (req, res) => {
  try {
    const { page, pp, keyword }= req.body;

    const data1 = await apiHelper.call('GET', 
      `api/admin/user-type-list?page=${page}&pp=${pp}&keyword=${keyword? keyword: ''}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, {
      paginate: {
        page: Number(data1.data.page),
        pp: Number(data1.data.pp),
        total: Number(data1.data.total),
        total_pages: Number(1),
        keyword: keyword? keyword: ''
      },
      result: data1.data
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userTypeRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/user-type-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userTypeCreate = async (req, res) => {
  try {
    var error = {};
    const { name, parentId, status }= req.body;
    
    if(!name) error['name'] = 'name is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    let updateInput = {
      name: name,
      status: status
    };
    if(parentId) updateInput['parent_id'] = parentId;

    const data1 = await apiHelper.call('POST', `api/sadmin/user-type-create`, updateInput, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userTypeUpdate = async (req, res) => {
  try {
    var error = {};
    const { id, name, status }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(!name) error['name'] = 'name is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-type-update`, {
      id: id,
      name: name,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userTypeDelete = async (req, res) => {
  try {
    var error = {};
    const { id }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-type-delete`, { id: id }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};


// Custom Column
exports.customColumnList = async (req, res) => {
  try {
    const { page, pp, keyword }= req.body;

    const data1 = await apiHelper.call('GET', 
      `api/admin/user-custom-column-list?page=${page}&pp=${pp}&keyword=${keyword? keyword: ''}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, {
      paginate: {
        page: Number(data1.data.page),
        pp: Number(data1.data.pp),
        total: Number(data1.data.total),
        total_pages: Number(1),
        keyword: keyword? keyword: ''
      },
      result: data1.data
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.customColumnRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/user-custom-column-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.customColumnCreate = async (req, res) => {
  try {
    var error = {};
    const { name, status }= req.body;
    
    if(!name) error['name'] = 'name is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-custom-column-create`, {
      name: name,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.customColumnUpdate = async (req, res) => {
  try {
    var error = {};
    const { id, name, status }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(!name) error['name'] = 'name is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/user-custom-column-update`, {
      id: id,
      name: name,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};


// External App
exports.externalAppList = async (req, res) => {
  try {
    const data1 = await apiHelper.call('GET', `api/admin/external-app-list`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.externalAppRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/external-app-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.externalAppCreate = async (req, res) => {
  try {
    var error = {};
    const { name, description, url, status }= req.body;
    
    if(!name) error['name'] = 'name is required.';
    if(!url) error['url'] = 'url is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/admin/external-app-create`, {
      name: name,
      description: description? description: '',
      url: url,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.externalAppUpdate = async (req, res) => {
  try {
    var error = {};
    const { id, name, description, url, status }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(!name) error['name'] = 'name is required.';
    if(!url) error['url'] = 'url is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/admin/external-app-update`, {
      id: id,
      name: name,
      description: description? description: '',
      url: url,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.externalAppDelete = async (req, res) => {
  try {
    var error = {};
    const { id }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/admin/external-app-delete`, { id: id }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};


// Module
exports.moduleList = async (req, res) => {
  try {
    const data1 = await apiHelper.call('GET', `api/admin/module-list`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.moduleRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/admin/module-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.moduleCreate = async (req, res) => {
  try {
    var error = {};
    const { name, code, status }= req.body;
    
    if(!name) error['name'] = 'name is required.';
    if(!code) error['code'] = 'code is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/module-create`, {
      name: name,
      code: code,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.moduleUpdate = async (req, res) => {
  try {
    var error = {};
    const { id, name, code, status }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(!name) error['name'] = 'name is required.';
    if(!code) error['code'] = 'code is required.';
    if(!status && status!==0) error['status'] = 'status is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/module-update`, {
      id: id,
      name: name,
      code: code,
      status: status
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.moduleDelete = async (req, res) => {
  try {
    var error = {};
    const { id }= req.body;
    
    if(!id) error['id'] = 'id is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/sadmin/module-delete`, { id: id }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};


// Report
exports.reportTraffics = async (req, res) => {
  try {
    var error = {};
    const { type, startDate, endDate } = req.body;
    
    if(!type) error['type'] = 'type is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);
    
    const data1 = await apiHelper.call('POST', `api/admin/traffic-report`, {
      type: type,
      start_date: startDate? startDate: null,
      end_date: endDate? endDate: null
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.reportActions = async (req, res) => {
  try {
    var error = {};
    const { type, startDate, endDate, userId } = req.body;
    
    if(!type) error['type'] = 'type is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);
    
    const data1 = await apiHelper.call('POST', `api/admin/action-report`, {
      type: type,
      start_date: startDate? startDate: null,
      end_date: endDate? endDate: null,
      user_id: userId? userId: null
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.reportRegistrations = async (req, res) => {
  try {
    var error = {};
    const { type, startDate, endDate } = req.body;
    
    if(!type) error['type'] = 'type is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);
    
    const data1 = await apiHelper.call('POST', `api/admin/user-registration-report`, {
      type: type,
      start_date: startDate? startDate: null,
      end_date: endDate? endDate: null
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
