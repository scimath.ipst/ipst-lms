const config = require('../config');
const sanitize = require('mongo-sanitize');
const jwt = require('jsonwebtoken');
const { apiHelper, resProcess } = require('../helpers');


exports.signin = async (req, res) => {
  try {
    const { username, password, ip, url } = req.body;

    // Check user
    const data1 = await apiHelper.call('POST', 'api/auth/signin', {
      username: username,
      password: password,
      ip: ip? ip: '',
      url: url? url: ''
    });
    if(!data1 || data1.status != 200) {
      if(data1.messages && data1.messages.username){
        return res.status(401).send({message: data1.messages.username});
      }else{
        return res.status(401).send({message: 'ไม่พบผู้ใช้ หรือรหัสผ่านผิด'});
      }
    }
    
    // Check user info
    const externalJWT = data1.jwt;
    const data2 = await apiHelper.call('GET', 'api/user/read', {}, externalJWT);
    if(!data2 || data2.status != 200) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    // Custom columns
    let custom_columns = [];
    const data0 = await apiHelper.call('GET', `api/user/user-custom-column-list`, {}, externalJWT);
    if(data0 && data0.status == 200){
      custom_columns = data0.data;
    }
    data2.data['custom_columns'] = custom_columns;

    return res.status(200).send({
      result: {
        ...data2.data,
        accessToken: externalJWT
      }
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.verifySignin = async (req, res) => {
  try {
    const { accessToken } = req.body;
    if (!accessToken) {
      return res.status(403).send({message: 'No access token provided.'});
    }
    
    const data1 = await apiHelper.call('GET', 'api/user/read', {}, accessToken);
    if(!data1 || data1.status != 200) {
      return res.status(403).send({message: 'Access token is invalid.'});
    }

    return res.status(200).send({message: 'Access token is valid.'});
  } catch (err) {
    return resProcess['500'](res, err);
  }
}

exports.signup = async (req, res) => {
  try {
    var error = {};
    const {
      firstname, lastname, username, email, password, confirmPassword, ip, url
    } = req.body;
    
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(!username) error['username'] = 'username is required.';
    if(!email) error['email'] = 'email is required.';
    if(!password) error['password'] = 'password is required.';
    if(!confirmPassword) error['confirmPassword'] = 'confirmPassword is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', 'api/auth/signup', {
      firstname: firstname,
      lastname: lastname,
      username: username,
      email: email,
      password: password,
      password_confirm: confirmPassword,
      ip: ip? ip: '',
      url: url? url: ''
    });
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.verify = async (req, res) => {
  try {
    if(!req.params.token) {
      return res.status(401).send({message: 'Verify Toekn ไม่ถูกต้อง'});
    }

    const decoded = jwt.verify(req.params.token, config.tokenVerifySecret);
    const user = await db.User.findById(sanitize(decoded._id));
    if(!user) {
      return res.status(401).send({message: 'Verify Toekn ไม่ถูกต้อง'});
    }

    user.status = 1;
    await user.save();
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.forgetPassword = async (req, res) => {
  try {
    var error = {};
    const { email, ip, url } = req.body;
    
    if(!email) error['email'] = 'email is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);
    
    const data1 = await apiHelper.call('POST', 'api/auth/forget-password', {
      email: email,
      ip: ip? ip: '',
      url: url? url: ''
    });
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.checkResetPassword = async (req, res) => {
  try {
    var error = {};
    const { token } = req.query;
    
    if(!token) error['token'] = 'token is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);
    
    const data1 = await apiHelper.call('GET', `api/auth/reset-password/${token}`);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.resetPassword = async (req, res) => {
  try {
    var error = {};
    const { token, newPassword, confirmNewPassword, ip, url } = req.body;
    
    if(!token) error['token'] = 'token is required.';
    if(!newPassword) error['newPassword'] = 'newPassword is required.';
    if(!confirmNewPassword) error['confirmNewPassword'] = 'confirmNewPassword is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);
    
    const data1 = await apiHelper.call('POST', 'api/auth/reset-password', {
      salt: token,
      password_new: newPassword,
      password_confirm: confirmNewPassword,
      ip: ip? ip: '',
      url: url? url: ''
    });
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return res.status(200).send({ message: 'การตั้งรหัสผ่านใหม่สำเร็จแล้ว' });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.signinFacebook = async (req, res) => {
  try {
    var error = {};
    const { facebookId, accessToken, firstname, lastname, email, profile, ip, url } = req.body;
    
    if(!facebookId) error['facebookId'] = 'facebookId is required.';
    if(!accessToken) error['accessToken'] = 'accessToken is required.';
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    let updateInput = {
      facebook_id: facebookId,
      access_token: accessToken,
      firstname: firstname,
      lastname: lastname,
      ip: ip? ip: '',
      url: url? url: ''
    };
    if(email) updateInput['email'] = email;
    if(profile) updateInput['profile'] = profile;

    // Check user
    const data1 = await apiHelper.call('POST', 'api/auth/signin-with-facebook', updateInput);
    if(!data1 || data1.status != 200) {
      if(data1.messages && data1.messages.username){
        return res.status(401).send({message: data1.messages.username});
      }else{
        return res.status(401).send({message: 'ไม่พบผู้ใช้ หรือรหัสผ่านผิด'});
      }
    }
    
    // Check user info
    const externalJWT = data1.jwt;
    const data2 = await apiHelper.call('GET', 'api/user/read', {}, externalJWT);
    if(!data2 || data2.status != 200) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    // Custom columns
    let custom_columns = [];
    const data0 = await apiHelper.call('GET', `api/user/user-custom-column-list`, {}, externalJWT);
    if(data0 && data0.status == 200){
      custom_columns = data0.data;
    }
    data2.data['custom_columns'] = custom_columns;

    return res.status(200).send({
      result: {
        ...data2.data,
        accessToken: externalJWT
      }
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.signinGoogle = async (req, res) => {
  try {
    var error = {};
    const { googleId, idToken, firstname, lastname, email, profile, ip, url } = req.body;
    
    if(!googleId) error['googleId'] = 'googleId is required.';
    if(!idToken) error['idToken'] = 'idToken is required.';
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(!email) error['email'] = 'email is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    let updateInput = {
      google_id: googleId,
      id_token: idToken,
      firstname: firstname,
      lastname: lastname,
      email: email,
      ip: ip? ip: '',
      url: url? url: ''
    };
    if(profile) updateInput['profile'] = profile;

    // Check user
    const data1 = await apiHelper.call('POST', 'api/auth/signin-with-google', updateInput);
    if(!data1 || data1.status != 200) {
      if(data1.messages && data1.messages.username){
        return res.status(401).send({message: data1.messages.username});
      }else{
        return res.status(401).send({message: 'ไม่พบผู้ใช้ หรือรหัสผ่านผิด'});
      }
    }
    
    // Check user info
    const externalJWT = data1.jwt;
    const data2 = await apiHelper.call('GET', 'api/user/read', {}, externalJWT);
    if(!data2 || data2.status != 200) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    // Custom columns
    let custom_columns = [];
    const data0 = await apiHelper.call('GET', `api/user/user-custom-column-list`, {}, externalJWT);
    if(data0 && data0.status == 200){
      custom_columns = data0.data;
    }
    data2.data['custom_columns'] = custom_columns;

    return res.status(200).send({
      result: {
        ...data2.data,
        accessToken: externalJWT
      }
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.signinLIFF = async (req, res) => {
  try {
    var error = {};
    const { liffId, firstname, lastname, profile, ip, url } = req.body;
    
    if(!liffId) error['liffId'] = 'liffId is required.';
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    let updateInput = {
      liff_id: liffId,
      firstname: firstname,
      lastname: lastname,
      ip: ip? ip: '',
      url: url? url: ''
    };
    if(profile) updateInput['profile'] = profile;

    // Check user
    const data1 = await apiHelper.call('POST', 'api/auth/signin-with-liff', updateInput);
    if(!data1 || data1.status != 200) {
      if(data1.messages && data1.messages.username){
        return res.status(401).send({message: data1.messages.username});
      }else{
        return res.status(401).send({message: 'ไม่พบผู้ใช้ หรือรหัสผ่านผิด'});
      }
    }
    
    // Check user info
    const externalJWT = data1.jwt;
    const data2 = await apiHelper.call('GET', 'api/user/read', {}, externalJWT);
    if(!data2 || data2.status != 200) {
      return res.status(401).send({message: 'ไม่พบผู้ใช้ในระบบ'});
    }

    // Custom columns
    let custom_columns = [];
    const data0 = await apiHelper.call('GET', `api/user/user-custom-column-list`, {}, externalJWT);
    if(data0 && data0.status == 200){
      custom_columns = data0.data;
    }
    data2.data['custom_columns'] = custom_columns;

    return res.status(200).send({
      result: {
        ...data2.data,
        accessToken: externalJWT
      }
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
