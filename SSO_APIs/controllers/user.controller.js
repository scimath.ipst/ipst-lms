const { apiHelper, resProcess } = require('../helpers');


// User
exports.profileUpdate = async (req, res) => {
  try {
    var error = {};
    const { firstname, lastname, email, username }= req.body;
    
    if(!firstname) error['firstname'] = 'firstname is required.';
    if(!lastname) error['lastname'] = 'lastname is required.';
    if(!email) error['email'] = 'email is required.';
    if(!username) error['username'] = 'username is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/user/update`, {
      firstname: firstname,
      lastname: lastname,
      email: email,
      username: username
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, { ...data1.data, accessToken: data1.jwt });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.detailUpdate = async (req, res) => {
  try {
    const {
      address, phone, title, company, company_address, company_phone,
      user_type_id, user_subtype_id
    }= req.body;

    let updateInput = {};
    if(address!==undefined) updateInput['address'] = address;
    if(phone!==undefined) updateInput['phone'] = phone;
    if(title!==undefined) updateInput['title'] = title;
    if(company!==undefined) updateInput['company'] = company;
    if(company_address!==undefined) updateInput['company_address'] = company_address;
    if(company_phone!==undefined) updateInput['company_phone'] = company_phone;
    if(user_type_id!==undefined) updateInput['user_type_id'] = user_type_id;
    if(user_subtype_id!==undefined) updateInput['user_subtype_id'] = user_subtype_id;

    const data0 = await apiHelper.call('GET', `api/user/user-custom-column-list`, {}, req.accessToken);
    if(data0 && data0.status == 200){
      data0.data.forEach(d => {
        if(req.body[d.name]!==undefined) updateInput[d.name] = req.body[d.name];
      });
    }

    const data1 = await apiHelper.call('POST', `api/user/update-detail`, updateInput, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, data1.data);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.passwordUpdate = async (req, res) => {
  try {
    var error = {};
    const { password, newPassword, confirmNewPassword }= req.body;
    
    if(!password) error['password'] = 'password is required.';
    if(!newPassword) error['newPassword'] = 'newPassword is required.';
    if(!confirmNewPassword) error['confirmNewPassword'] = 'confirmNewPassword is required.';
    if(Object.keys(error).length) return resProcess['checkError'](res, error);

    const data1 = await apiHelper.call('POST', `api/user/update-password`, {
      password: password,
      new_password: newPassword,
      new_password_confirm: confirmNewPassword
    }, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, data1.data);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.requestToDelete = async (req, res) => {
  try {
    const data1 = await apiHelper.call('POST', `api/user/request-to-delete`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }
    
    return resProcess['200'](res, data1.data);
  } catch (err) {
    return resProcess['500'](res, err);
  }
};

exports.userTypeList = async (req, res) => {
  try {
    const { page, pp, keyword }= req.body;

    const data1 = await apiHelper.call('GET', 
      `api/user/user-type-list?page=${page}&pp=${pp}&keyword=${keyword? keyword: ''}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, {
      paginate: {
        page: Number(data1.data.page),
        pp: Number(data1.data.pp),
        total: Number(data1.data.total),
        total_pages: Number(1),
        keyword: keyword? keyword: ''
      },
      result: data1.data
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.userTypeRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/user/user-type-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};


// Custom Column
exports.customColumnList = async (req, res) => {
  try {
    const { page, pp, keyword }= req.body;

    const data1 = await apiHelper.call('GET', 
      `api/user/user-custom-column-list?page=${page}&pp=${pp}&keyword=${keyword? keyword: ''}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, {
      paginate: {
        page: Number(data1.data.page),
        pp: Number(data1.data.pp),
        total: Number(data1.data.total),
        total_pages: Number(1),
        keyword: keyword? keyword: ''
      },
      result: data1.data
    });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
exports.customColumnRead = async (req, res) => {
  try {
    const { id }= req.query;

    const data1 = await apiHelper.call('GET', `api/user/user-custom-column-read/${id}`, {}, req.accessToken);
    if(!data1 || data1.status != 200){
      return resProcess['400'](res, data1.messages, 'เกิดข้อผิดพลาด');
    }

    return resProcess['200'](res, { result: data1.data });
  } catch (err) {
    return resProcess['500'](res, err);
  }
};
